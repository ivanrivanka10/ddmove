import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  LogBox,
  Platform,
  View,
  Text,
} from 'react-native';
import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from './src/utils/store';
import {Colors} from '@styles/index';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import RootNavigation from '@navigations/RootNavigation';

export default function App() {
  LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  LogBox.ignoreAllLogs();
  console.disableYellowBox = true; // Disable all yellow box warnings
  LogBox.ignoreLogs([
    'Animated.event now requires a second argument for options',
  ]);
  LogBox.ignoreLogs([
    "Accessing the 'state' property of the 'route' object is not supported",
  ]);

  LogBox.ignoreLogs([
    'VirtualizedLists should never be nested inside plain ScrollViews with the same orientation',
  ]);
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <GestureHandlerRootView style={{flex: 1}}>
          <SafeAreaView style={styles.screen}>
            <StatusBar
              barStyle={
                Platform.OS === 'android' ? 'light-content' : 'dark-content'
              }
              backgroundColor={Colors.primary}
            />
            <RootNavigation />
          </SafeAreaView>
        </GestureHandlerRootView>
      </PersistGate>
    </Provider>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Platform.OS === 'android' ? '#F9F9F9' : '#fff',
  },
});
