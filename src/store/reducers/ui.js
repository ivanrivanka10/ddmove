import { UPDATE_MODAL, SHOW_MODAL, UPDATE_LOADING } from "@store/actions/ui";

const initialState = {
    loading: false,
    modal: {
        show: false,
        title: '',
        message: ''
    }
}

export default (state = initialState, action) => {
    switch(action.type){
        case UPDATE_LOADING: {
            return {
                ...state,
                loading: action.loading
            }
        }
        case UPDATE_MODAL: {
            return {
                ...state,
                modal: {
                    ...state.modal,
                    ...action.modal
                }
            }
        }
        case SHOW_MODAL: {
            return {
                ...state,
                modal: {
                    ...state.modal,
                    show: action.show
                }
            }
        }
        default: {
            return state;
        }
    }
}