import { combineReducers } from 'redux';

import langReducer from './lang';
import authReducer from './auth';
import uiReducer from './ui';

const reducers = combineReducers({
    lang: langReducer,
    auth: authReducer,
    ui: uiReducer
})

export default reducers;