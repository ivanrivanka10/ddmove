import { API } from "@constants/url";
import { updateLoading } from "./ui";

export const getHomeBuyer = () => {
    return async (dispatch, getState) => {

        const tokenType = getState().auth.tokenType;
        const token = getState().auth.token;

        const response = await fetch(API + '/home-buyer', {
            method: 'GET',
            headers: {
                "Accept": "application/json",
                Authorization: `${tokenType} ${token}`
            }
        })


        const resData = await response.json();

        if(!response.ok){
            throw new Error(resData.data.info);
        }

        if(!resData.success){
            throw new Error(resData.data.info);
        }

        return resData.data;
    }
}

export const getProductBuyerDetail = (id) => {
    return async (dispatch, getState) => {
        await dispatch(updateLoading(true));

        const tokenType = getState().auth.tokenType;
        const token = getState().auth.token;

        const response = await fetch(`${API}/product-buyer/${id}`, {
            method: 'GET',
            headers: {
                "Accept": "application/json",
                Authorization: `${tokenType} ${token}`
            }
        })

        const resData = await response.json();

        if(!response.ok){
            await dispatch(updateLoading(false));
            throw new Error(resData.data.info);
        }

        if(!resData.success){
            await dispatch(updateLoading(false));
            throw new Error(resData.data.info);
        }
        await dispatch(updateLoading(false));

        return resData;
    }
}