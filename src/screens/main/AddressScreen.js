import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  FlatList,
  Platform,
} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import {Size, Fonts, Icons, Colors} from '@styles/index';
import {Button, HeaderTitle} from '@components/global';
import {useSelector, useDispatch} from 'react-redux';
import API from '@utils/services/settingProvider';
import {debugConsoleLog, showToastMessage} from '@helpers/functionFormat';

const AddressScreen = props => {
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const {userData} = useSelector(state => state.auth);
  const [listAddress, setListAddress] = React.useState([]);

  const getListAddress = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.GetAddress(userData.token);
    console.log('list address', res);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status == 200) {
      if (res.data.data.length > 0) {
        setListAddress(res.data.data);
      } else {
        var status = false;
        dispatch({type: 'UPDATE_STATUS_ADDRESS', status});
      }
    }
  };

  const deleteAddress = async id => {
    const res = await API.DeleteAddress(id, userData.token);
    getListAddress();
  };

  const handleEdit = async id => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.GetAddressDetail(userData.token, id);
    console.log('cek address detail', res.data.data);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status == 200) {
      const data = res.data.data;
      const item = {
        label: data.label,
        name: data.name,
        phone: data.phone,
        address_detail: data.address_detail,
        is_main: data.is_main,
        province: data.province.name,
        city: data.city.name,
        district: data.district.name,
        subdistrict: data?.subdistrict?.name ?? "-",
        id_subdistrict: data?.subdistrict?.id ?? "-",
        id: data.id,
      };
      props.navigation.navigate('AddNewAddress', {item});
    }
  };

  const setMainAddress = async item => {
    console.log('cek picked address', item);
    const data = new FormData();
    data.append('label', item.label);
    data.append('name', item.name);
    data.append('phone', item.phone);
    data.append('address_detail', item.address);
    data.append('province_id', item.province_id);
    data.append('city_id', item.city_id);
    data.append('district_id', item.district_id);
    data.append('subdistrict_id', item.subdistrict_id);
    data.append('is_main', 'yes');
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.UpdateAddress(data, userData.token, item.id);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      showToastMessage('Alamat baru berhasil diganti');
      return props.navigation.goBack();
    }
  };

  React.useEffect(() => {
    getListAddress();
  }, [isFocused]);
  console.log('props address', props);
  return (
    <View style={styles.screen}>
      <StatusBar barStyle="dark-content" backgroundColor="#FFF" />
      <HeaderTitle title="Pilih Alamat" />
      <FlatList
        data={listAddress}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => {
              setMainAddress(item);
              debugConsoleLog('nilai',item)
              // if (props.route.params) {
              //  
              // }
            }}
            style={[
              styles.addressContainer,
              item.is_main === 'yes' && styles.addressContainerSelected,
            ]}>
            <Text allowFontScaling={false} style={styles.addressTitle}>
              {item.label}
            </Text>
            <Text allowFontScaling={false} style={styles.address}>
              {item.address}
            </Text>
            <Text allowFontScaling={false} style={styles.addressPhone}>
              {item.phone}
            </Text>
            <View allowFontScaling={false} style={styles.addressFooter}>
              {props.route.params ? (
                <View />
              ) : (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Button
                    style={{paddingHorizontal: Size.scaleSize(25)}}
                    onPress={() => handleEdit(item.id)}
                    title="Edit"
                  />
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={{marginLeft: 20}}
                    onPress={() => {
                      if (item.is_main === 'yes') {
                        showToastMessage('Tidak bisa menghapus alamat utama');
                      } else {
                        deleteAddress(item.id);
                      }
                    }}>
                    <Icons.TrashSVG
                      color={Colors.primary}
                      size={Size.scaleSize(30)}
                      style={{marginRight: Size.scaleSize(20)}}
                    />
                  </TouchableOpacity>
                </View>
              )}
              {item.is_main === 'yes' ? (
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Icons.CircleCheckSVG
                    size={Size.scaleSize(24)}
                    color={
                      item.is_main === 'yes' ? Colors.secondary : 'transparent'
                    }
                    style={{marginRight: Size.scaleSize(10)}}
                  />
                  <Text
                    allowFontScaling={false}
                    style={[
                      styles.addressSelected,
                      item.is_main !== 'yes' && {color: 'transparent'},
                    ]}>
                    Alamat Utama
                  </Text>
                </View>
              ) : (
                <View />
              )}
            </View>

            {item.is_main === 'yes' && (
              <View style={{flexDirection: 'row', alignItems: 'center' , marginTop : 10}}>
                <Text
                  allowFontScaling={false}
                  style={{ fontSize : 10 , color : Colors.primary }}>
                  *alamat utama adalah alamat yang digunakan sebagai acuan untuk
                  pencarian produk
                </Text>
              </View>
            )}
          </TouchableOpacity>
        )}
      />
      <Button
        containerStyle={{
          marginHorizontal: Size.scaleSize(15),
          backgroundColor: '#fff',
          marginBottom: Size.scaleSize(10),
        }}
        style={{borderWidth: 2, borderColor: Colors.primary}}
        pressColor={'#FFF'}
        textStyle={{color: Colors.primary}}
        title="+ Alamat Baru"
        onPress={() => props.navigation.navigate('AddNewAddress')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  addressContainer: {
    backgroundColor: '#FFF',
    borderWidth: 2,
    borderRadius: 16,
    borderColor: '#F2F4F5',
    marginHorizontal: Size.scaleSize(10),
    padding: Size.scaleSize(20),
    marginTop: Size.scaleSize(20),
  },
  addressContainerSelected: {
    backgroundColor: '#FFF9F3',
    borderColor: Colors.secondary,
  },
  addressTitle: {
    fontFamily: Fonts.mulishExtraBold,
    fontSize: Size.scaleFont(16),
    color: '#333',
  },
  address: {
    fontFamily: Fonts.mulishRegular,
    fontSize: Size.scaleFont(14),
    color: '#000',
    marginVertical: Size.scaleSize(10),
  },
  addressPhone: {
    fontFamily: Fonts.mulishRegular,
    fontSize: Size.scaleFont(14),
    color: '#000',
  },
  addressFooter: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: Size.scaleSize(10),
    justifyContent: 'space-between',
  },
  addressSelected: {
    fontFamily: Fonts.mulishRegular,
    color: Colors.secondary,
    fontSize: Size.scaleFont(14),
  },
});

export const screenOptions = navData => {
  return {
    headerTitle: 'Pilih Alamat',
    headerTitleAlign: 'left',
    headerTitleStyle: {
      fontFamily: Fonts.mulishExtraBold,
      fontSize: Size.scaleFont(16),
      color: '#333333',
    },
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft: Size.scaleSize(5)}}
        onPress={() => navData.navigation.goBack()}>
        <View style={{padding: Size.scaleSize(10)}}>
          <Icons.ChevronLeft color="#333" size={Size.scaleSize(18)} />
        </View>
      </TouchableOpacity>
    ),
  };
};

export default AddressScreen;
