import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  View,
  Text,
  FlatList,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  Dimensions,
  Alert,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation, CommonActions} from '@react-navigation/native';
import {Colors, Fonts, Size, Icons, Images} from '@styles/index';
import {InputText, Button, Header, TextRegular} from '@components/global';
import Category from '@components/part/Category';
import Product from '@components/part/Product';
import Store from '@components/part/Store';
import API from '@utils/services/buyer/homeProvider';
import APISETTING from '@utils/services/settingProvider';
// import Slider from '@components/global/Slider'
import {useIsFocused} from '@react-navigation/native';
import {IconAsset} from '@utils/index';
import {debugConsoleLog} from '@helpers/functionFormat';

import ModalChooseLocation from '@components/modal/ModalChooseLocation';

const HomeScreen = props => {
  // console.log('props home', props);
  const isFocused = useIsFocused();
  const [categories, setCategories] = useState([]);
  const [products, setProducts] = useState([]);
  const [shopArround, setShopArround] = useState([]);
  const [banner, setBanner] = useState([]);
  const [activeIndex, setActiveIndex] = useState(0);
  const {userData, location, userPickedAddress} = useSelector(
    state => state.auth,
  );
  const [listAddress, setListAddress] = useState([]);
  const [pickedAddress, setPickedAddress] = useState({});
  const [showModalLocation, setShowModalLocation] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({type: 'HIDE_LOADING'});
    // if(!userData.fromSeller) {
    //     if(!userData.hasAddress && props.route.name === 'Home') {
    //         Alert.alert('Alamat utama belum ditambahkan', 'Alamat utama diperlukan untuk mengakses toko disekitar anda', [
    //             {
    //                 text: "Tambah Alamat Sekarang",
    //                 onPress: () => {
    //                     props.navigation.dispatch({
    //                         ...CommonActions.reset({
    //                             index: 0,
    //                             routes: [
    //                                 {
    //                                     name: 'UserTab',
    //                                     state: {
    //                                         routes: [
    //                                             {
    //                                                 name: 'User',
    //                                             },
    //                                             {
    //                                                 name: 'Address',
    //                                             },
    //                                         ],
    //                                     },
    //                                 },
    //                             ],
    //                         }),
    //                     });
    //                 }
    //             },
    //             {
    //                 text: "Nanti Saja",
    //                 onPress: () => loadData()
    //             }
    //         ])
    //     } else {
    //         loadData();
    //     }
    // } else {
    //     loadData();
    // }
    loadData();
    // console.log('userData home', userData);
  }, [isFocused]);

  const loadData = async () => {
    const res = await API.GetHomeBuyer(userData.token);
    if (res.status === 200) {
      var dataCat = [];
      if (res.data.data.categories.length > 3) {
        for (let index = 0; index < 3; index++) {
          dataCat.push(res.data.data.categories[index]);
        }
      } else {
        for (let index = 0; index < res.data.data.categories.length; index++) {
          dataCat.push(res.data.data.categories[index]);
        }
      }
      dataCat.push({
        id: 0,
        icon: (
          <Icons.CategorySVG size={Size.scaleSize(41)} color={Colors.primary} />
        ),
        name: 'Kategori Lainnya',
      });
      console.log('ini data hasil', res);
      setCategories(dataCat);
      setProducts(res.data.data.products);
      setShopArround(res.data.data.shop_arround);
      setBanner(res.data.data.banners);
      getListAddress();
    }
  };

  const getListAddress = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await APISETTING.GetAddress(userData.token);
    // console.log('list address', res);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status == 200) {
      if (res.data.data.length > 0) {
        setListAddress(res.data.data);
        console.log('ini addres', res.data.data);
        var address = res.data.data.filter(value => {
          return value.is_main === 'yes';
        });
        debugConsoleLog('cek address', address);
        var addressPicked = address[0];
        dispatch({type: 'SET_USER_PICKED_ADDRESS', addressPicked});
        setPickedAddress(address[0]);
      } else {
        var status = false;
        dispatch({type: 'UPDATE_STATUS_ADDRESS', status});
      }
    }
  };

  const setAddressToMain = async data => {
    dispatch({type: 'SHOW_LOADING'});
    const fd = new FormData();
    fd.append('label', data.label);
    fd.append('name', data.name);
    fd.append('phone', data.phone);
    fd.append('address_detail', data.address);
    fd.append('province_id', data.province_id);
    fd.append('city_id', data.city_id);
    fd.append('district_id', data.district_id);
    fd.append('subdistrict_id', data.subdistrict_id);
    fd.append('is_main', 'yes');
    const res = await APISETTING.UpdateAddress(fd, userData.token, data.id);
    debugConsoleLog('set main', res);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      console.log('berhasil', res);
      setPickedAddress(data);
      var addressPicked = data;
      dispatch({type: 'SET_USER_PICKED_ADDRESS', addressPicked});
      getListAddress();
    }
  };

  // console.log('user data home', userData);
  return (
    <ScrollView>
      <ModalChooseLocation
        show={showModalLocation}
        onClose={() => setShowModalLocation(false)}
        data={listAddress}
        onPick={item => {
          debugConsoleLog('picked address', item);
          setShowModalLocation(false);
          setAddressToMain(item);
        }}
      />
      <View style={styles.screen}>
        <ImageBackground
          source={Images.HeaderHome}
          style={{backgroundColor: Colors.primary, height: 170}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginHorizontal: Size.scaleSize(15),
              marginBottom: Size.scaleSize(20),
            }}>
            <Icons.LocationSVG size={24} color="white" />
            <View style={{marginLeft: Size.scaleSize(10)}}>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
                onPress={() => setShowModalLocation(true)}>
                <Text
                  allowFontScaling={false}
                  style={{
                    fontFamily: Fonts.mulishRegular,
                    color: 'white',
                    fontSize: Size.scaleFont(14),
                  }}>
                  Dikirim ke
                </Text>
                <Image
                  source={IconAsset.ChevronDown}
                  style={{
                    width: 20,
                    height: 20,
                    marginLeft: 5,
                    marginTop: 2,
                    tintColor: '#fff',
                  }}
                />
              </TouchableOpacity>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishMedium,
                  color: 'white',
                  fontSize: Size.scaleFont(14),
                  fontWeight: 'bold',
                }}>
                {pickedAddress?.label + ' (' + pickedAddress.name + ')'}
              </Text>
            </View>
          </View>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.btnSearch}
            onPress={() => props.navigation.navigate('SearchScreen')}>
            <TextRegular text="Cari nama produk" color={Colors.grey_1} />
            <Icons.SearchSVG size={24} color={Colors.grey_1} />
          </TouchableOpacity>
        </ImageBackground>
        <View
          style={{
            top: Size.scaleSize(-35),
            backgroundColor: 'white',
            borderTopLeftRadius: 0,
            borderTopRightRadius: 0,
          }}>
          {/* <Image 
                        style={{width: Dimensions.get('screen').width * 0.9, height: Dimensions.get('screen').width * 0.6, alignSelf: 'center', }}
                        resizeMode="contain"
                        source={Images.Banner}
                    /> */}
          {/* <Slider
                        data={banner}
                        activeIndex={activeIndex}
                        setActiveIndex={(index) => setActiveIndex(index)}
                    /> */}
          <Text allowFontScaling={false} style={[styles.titleText]}>
            Kategori
          </Text>
          <View
            style={{
              marginHorizontal: 20,
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 10,
            }}>
            {categories.map(category => (
              <Category
                key={category.id.toString()}
                style={{width: '24%'}}
                label={category.name}
                icon={category.icon}
                onPress={() => {
                  if (category.name === 'Kategori Lainnya') {
                    props.navigation.navigate('AllCategory', {item: category});
                  } else {
                    props.navigation.navigate('Category', {item: category});
                  }
                }}
              />
            ))}
          </View>
          <View style={styles.titleProductContainer}>
            <Text allowFontScaling={false} style={styles.titleProductText}>
              Produk Terlaris
            </Text>
            <TouchableOpacity
              onPress={() => props.navigation.navigate('AllProducts')}>
              <View style={styles.seeAll}>
                <Text allowFontScaling={false} style={styles.seeAllText}>
                  Lihat Semua
                </Text>
                <Icons.ArrowRightSVG size={24} color={Colors.primary} />
              </View>
            </TouchableOpacity>
          </View>
          <FlatList
            style={{marginVertical: Size.scaleSize(20)}}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={products}
            keyExtractor={item => item.id}
            renderItem={itemData => (
              <Product
                containerStyle={
                  itemData.index === products.length - 1 && {
                    marginRight: Size.scaleSize(15),
                  }
                }
                onPress={() =>
                  props.navigation.navigate('Product', {product: itemData.item})
                }
                data={itemData.item}
              />
              // <Product
              //     containerStyle={itemData.index === products.length - 1 && {marginRight: Size.scaleSize(15)}}
              //     onPress={() => props.navigation.navigate('Product')}
              // />
            )}
          />
          {shopArround.length > 0 && (
            <View>
              <Text allowFontScaling={false} style={styles.titleText}>
                Toko Disekitarmu
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  marginHorizontal: 20,
                  justifyContent: 'space-between',
                  flexWrap: 'wrap',
                  marginBottom: Size.scaleSize(30),
                }}>
                {shopArround.map((value, index) => {
                  return (
                    <Store
                      key={index.toString()}
                      image={value.photo}
                      label={value.name}
                      onPress={() =>
                        props.navigation.navigate('DetailStore', {value})
                      }
                    />
                  );
                })}
              </View>
              <Button
                containerStyle={{
                  top: 20,
                  marginHorizontal: 20,
                  backgroundColor: '#FFF',
                }}
                style={{borderWidth: 2, borderColor: Colors.primary}}
                textStyle={{color: Colors.primary}}
                title="Lihat Semua"
                pressColor={'#FFF'}
                onPress={() => props.navigation.navigate('AllShop')}
              />
            </View>
          )}
        </View>
      </View>
    </ScrollView>
  );
};

export const screenOptions = navData => {
  const name = useSelector(state => state.auth.userData.name);
  return {
    header: () => (
      <Header
        navData={navData}
        headerLeft={
          <View>
            <Text
              allowFontScaling={false}
              style={{
                color: '#FFF',
                fontFamily: Fonts.mulishRegular,
                fontSize: Size.scaleFont(14),
              }}>
              Selamat Datang,
            </Text>
            <Text
              allowFontScaling={false}
              style={{
                color: '#FFF',
                fontFamily: Fonts.mulishExtraBold,
                fontSize: Size.scaleFont(16),
              }}>
              {name}
            </Text>
          </View>
        }
      />
    ),
  };
};

const styles = StyleSheet.create({
  btnSearch: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#fff',
    borderRadius: 8,
    paddingHorizontal: 10,
    paddingVertical: 13,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  screen: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  titleText: {
    fontFamily: Fonts.mulishExtraBold,
    color: '#333',
    marginHorizontal: Size.scaleSize(20),
    fontSize: Size.scaleFont(20),
  },
  titleProductContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: Size.scaleSize(20),
    marginTop: Size.scaleSize(40),
  },
  titleProductText: {
    fontFamily: Fonts.mulishExtraBold,
    color: '#333',
    fontSize: Size.scaleFont(20),
  },
  seeAll: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  seeAllText: {
    fontFamily: Fonts.mulishRegular,
    color: Colors.primary,
    fontSize: Size.scaleFont(14),
    marginRight: Size.scaleSize(5),
  },
});

export default HomeScreen;
