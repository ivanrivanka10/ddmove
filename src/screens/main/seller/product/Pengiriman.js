import React, {useState, useCallback, useEffect} from 'react';
import {
  View,
  Modal,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Text,
} from 'react-native';
import {Colors, Fonts, Size, Icons, Images} from '@styles/index';
import {
  InputText,
  Button,
  SelectDropDown,
  TextSemiBold,
  TextBold,
  HeaderTitle,
  TextRegular,
} from '@components/global';
import {IconAsset} from '@utils/index';
import {showToastMessage} from '@helpers/functionFormat';

const ModalPengiriman = ({
  show,
  onClose,
  onSubmit,
  listCourier,
  showForm = true,
  showCourier = true,
  widths = 0,
  lengths = 0,
  heights = 0,
  weights = 0,
  weight_volumes = 0,
}) => {
  const [data, setData] = useState([]);
  const [width, setWidth] = useState(widths);
  const [length, setLength] = useState(lengths);
  const [height, setHeight] = useState(heights);
  const [weight, setWeight] = useState(weights);
  const [volume, setVolume] = useState(weight_volumes);

  const validation = () => {
    var isDisable = false;
    if (showForm) {
      const obj = {
        width,
        height,
        length,
        weight,
      };
      const dataEmpty = Object.keys(obj).filter(data => obj[data] == '');
      if (dataEmpty.length > 0) {
        isDisable = true;
      }
    } else {
      isDisable = false;
    }
    return isDisable;
  };

  const checkData = () => {
    var checkCourier = data.filter(value => {
      return value.checked === true;
    });
    console.log('checkCourier', checkCourier);
    if (showForm) {
      if (showCourier) {
        if (checkCourier.length === 0) {
          showToastMessage('Silahkan pilih jasa pengiriman');
          return false;
        } else {
          const obj = {
            width,
            height,
            length,
            weight,
            selectedCourier: checkCourier,
            weight_volume: volume,
          };
          console.log('obj parsing', obj);
          onSubmit(obj);
        }
      } else {
        const obj = {
          width,
          height,
          length,
          weight,
          weight_volume: volume,
          selectedCourier: [],
        };
        console.log('obj parsing', obj);
        onSubmit(obj);
      }
    } else {
      const obj = {
        selectedCourier: checkCourier,
      };
      console.log('obj parsing', obj);
      onSubmit(obj);
    }
  };

  const countVolume = () => {
    var volumes = 0;
    volumes = (length * width * height) / 6000;
    setVolume(volumes.toFixed(2));
  };

  useEffect(() => {
    setWidth(`${widths}`);
    setLength(`${lengths}`);
    setHeight(`${heights}`);
    setWeight(`${weights}`);
    setVolume(`${weight_volumes}`);
  }, []);

  useEffect(() => {
    if (showForm) {
      var arr = [...listCourier];
      for (let i = 0; i < arr.length; i++) {
        arr[i].checked = false;
      }
      setData(arr);
    } else {
      setData(listCourier);
    }
    countVolume();
  }, []);
  console.log('cek volume', volume);
  console.log('cek width', width);
  console.log('cek length', length);
  console.log('cek height', height);
  console.log('cek weight', weight);
  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View
        style={{
          flex: 1,
          backgroundColor: '#fff',
        }}>
        <HeaderTitle title="Pengiriman" back={onClose} />
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 100}}>
          {showForm && (
            <>
              <View>
                <View
                  style={{
                    marginTop: 20,
                    marginBottom: 10,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginHorizontal: 15,
                  }}>
                  <Text
                    style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
                    Berat Produk (gram)
                  </Text>
                </View>
                <InputText
                  containerStyle={{marginHorizontal: 15}}
                  placeholder="Masukan Berat Produk"
                  value={weight}
                  onChangeText={text => setWeight(text)}
                  keyboardType="number-pad"
                />
                {weight >= 5000 && (
                  <>
                    <View
                      style={{
                        marginTop: 20,
                        marginBottom: 10,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginHorizontal: 15,
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.mulishExtraBold,
                          color: '#333',
                        }}>
                        Lebar Produk (Cm / Diameter)
                      </Text>
                    </View>
                    <InputText
                      containerStyle={{marginHorizontal: 15}}
                      placeholder="Masukan Lebar Produk"
                      value={width}
                      onChangeText={text => setWidth(Number(text))}
                      keyboardType="number-pad"
                    />
                    <View
                      style={{
                        marginTop: 20,
                        marginBottom: 10,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginHorizontal: 15,
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.mulishExtraBold,
                          color: '#333',
                        }}>
                        Panjang Produk (Cm)
                      </Text>
                    </View>
                    <InputText
                      containerStyle={{marginHorizontal: 15}}
                      placeholder="Masukan Panjang Produk"
                      value={length}
                      onChangeText={text => setLength(Number(text))}
                      keyboardType="number-pad"
                    />
                    <View
                      style={{
                        marginTop: 20,
                        marginBottom: 10,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginHorizontal: 15,
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.mulishExtraBold,
                          color: '#333',
                        }}>
                        Tinggi Produk (Cm)
                      </Text>
                    </View>
                    <InputText
                      containerStyle={{marginHorizontal: 15}}
                      placeholder="Masukan Tinggi Produk"
                      value={height}
                      onChangeText={text => setHeight(text)}
                      keyboardType="number-pad"
                      onBlur={() => countVolume()}
                    />
                    <View
                      style={{
                        width: '100%',
                        paddingHorizontal: 15,
                        marginTop: 15,
                      }}>
                      <View
                        style={{
                          marginBottom: 10,
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}>
                        <Text
                          style={{
                            fontFamily: Fonts.mulishExtraBold,
                            color: '#333',
                          }}>
                          Volume Produk (Dihitung Otomatis)
                        </Text>
                      </View>
                      <View
                        style={{
                          width: '100%',
                          borderColor: '#E0E0E0',
                          borderRadius: 10,
                          borderWidth: 1,
                          paddingVertical: 15,
                          paddingHorizontal: 5,
                          backgroundColor: Colors.bgGrey,
                        }}>
                        <TextRegular text={volume} />
                      </View>
                    </View>
                  </>
                )}
              </View>
              <View
                style={{
                  width: '100%',
                  height: 10,
                  backgroundColor: '#F7F9F8',
                  marginTop: 24,
                }}
              />
            </>
          )}
          <View style={{paddingHorizontal: 18, marginTop: 40}}>
            {showCourier === true &&
              data.map((value, index) => (
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 40,
                  }}>
                  <TextRegular text={value.name} />
                  <TouchableOpacity
                    style={[styles.checkbox, value.checked && {borderWidth: 0}]}
                    onPress={() => {
                      var arr = [...data];
                      arr[index].checked = arr[index].checked ? false : true;
                      setData(arr);
                    }}>
                    {value.checked && (
                      <Image
                        source={IconAsset.IcCheckSquere}
                        style={{width: 20, height: 20}}
                      />
                    )}
                  </TouchableOpacity>
                </View>
              ))}
            <TouchableOpacity
              style={[
                styles.btnSubmit,
                {
                  backgroundColor: Colors.primary
                },
              ]}
              onPress={() => checkData()}>
              <TextBold text="Atur" color="#fff" />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  btnSubmit: {
    width: '100%',
    marginTop: 32,
    borderRadius: 16,
    paddingVertical: 17,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
  },
  checkbox: {
    width: 20,
    height: 20,
    borderColor: Colors.primary,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ModalPengiriman;
