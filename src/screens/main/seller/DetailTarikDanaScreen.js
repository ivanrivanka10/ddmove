import React, {useState, useEffect} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, StatusBar } from 'react-native';

import { Icons, Colors, Fonts, Images, Size } from '@styles/index';
import { Button } from '@components/global';
import {useSelector, useDispatch} from 'react-redux'
import API from '@utils/services/seller/homeProvider'
import moment from 'moment';
import 'moment/locale/id'
import {showToastMessage, rupiahFormat} from '@helpers/functionFormat'
import ModalPassword from '@components/modal/ModalPassword';

const DetailTarikDanaScreen = ({navigation, route}) => {
    const { userData } = useSelector(state => state.auth);
    const dispatch = useDispatch();

    const [dataDetail, setDataDetail] = useState({})
    const [password, setPassword] = React.useState('')
    const [showPassword, setshowPassword] = React.useState(false)

    const getDetail = async () => {
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetDetailHistoryWD(route.params.item.id, userData.token)
        console.log('res detail wd', res);
        if(res.status === 200) {
            setDataDetail(res.data.data)
        }
        dispatch({type: "HIDE_LOADING"});
    }

    const cancelWD = async () => {
        setshowPassword(false)
        if(password == '') {
            return showToastMessage('Silahkan masukkan password anda')
        }
        dispatch({type: "SHOW_LOADING"});
        const formData = new FormData();
        formData.append('shop_id', dataDetail.shop_id)
        formData.append('bank_account_id', dataDetail.destination_account.account_number)
        formData.append('nominal', dataDetail.nominal)
        formData.append('password', password)
        const res = await API.CancelWd(dataDetail.id, formData, userData.token)
        console.log('res cancel', res);
        if(res.status === 200) {
            showToastMessage('Penarikan dana telah dibatalkan')
            navigation.goBack()
        } else {
            return showToastMessage('Server Error')
        }
        dispatch({type: "HIDE_LOADING"});
    }

    useEffect(() => {
        getDetail()
    },[])

    console.log('props detail tarik', route);
    return (
        <View style={styles.screen}>
            <StatusBar backgroundColor={"#FFF"} barStyle="dark-content"/>
            <ModalPassword
                show={showPassword}
                onClose={() => setshowPassword(false)}
                password={password}
                setPassword={(val) => setPassword(val)}
                onSubmit={() => {                    
                    cancelWD()
                }}
            />
            <Icons.DownloadSVG 
                style={{marginTop: 30, marginBottom: 10}}
                size={80}
                color={Colors.secondary}
            />
            {
                Object.keys(dataDetail).length > 0 && (
                    <>
                        <Text style={{fontFamily: Fonts.mulishExtraBold, fontSize: 16}}>{dataDetail?.status}</Text>
                        <Text style={{fontFamily: Fonts.mulishRegular}}>{moment(dataDetail?.date).format('DD MMMM YYYY • HH:mm')}</Text>
                        <Text style={{alignSelf: 'flex-start', fontFamily: Fonts.mulishExtraBold, marginHorizontal: 15, marginTop: 20, marginBottom: 10}}>Transfer ke</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center', backgroundColor: '#FFF9F3', alignSelf: 'flex-start', marginHorizontal: 15, width: '90%', padding: 20}}>
                            <Image
                                source={Images.BankBCA}
                                style={{width: 40, height: 40, marginRight: 20}}
                            />
                            <View>
                                <Text style={{fontFamily: Fonts.mulishExtraBold, marginBottom: 5}}>{dataDetail?.destination_account.bank_name}</Text>
                                <Text style={{fontFamily: Fonts.mulishRegular}}>{dataDetail?.destination_account.name}</Text>
                                <Text style={{fontFamily: Fonts.mulishRegular}}>{dataDetail?.destination_account.account_number}</Text>
                            </View>
                        </View>
                        <Text style={{alignSelf: 'flex-start', fontFamily: Fonts.mulishExtraBold, marginHorizontal: 15, marginTop: 20, marginBottom: 10}}>Rincian</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', paddingHorizontal: 15, marginBottom: 10}}>
                            <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>Jumlah Penarikan</Text>
                            <Text style={{fontFamily: Fonts.mulishMedium, color: '#333'}}>{rupiahFormat(dataDetail?.nominal,'Rp ')}</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', paddingHorizontal: 15, marginBottom: 30}}>
                            <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>Biaya Transaksi</Text>
                            <Text style={{fontFamily: Fonts.mulishMedium, color: '#333'}}>-{rupiahFormat(dataDetail?.handling_fee,'Rp ')}</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', paddingHorizontal: 15, marginBottom: 40}}>
                            <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>Total Penarikan</Text>
                            <Text style={{fontFamily: Fonts.mulishMedium, color: '#333'}}>{rupiahFormat(dataDetail?.total,'Rp ')}</Text>
                        </View>
                        {
                            dataDetail && dataDetail.status === 'pending' && (
                                <Button 
                                    containerStyle={{marginHorizontal: Size.scaleSize(15), backgroundColor: '#fff', marginBottom: Size.scaleSize(10), width: '90%'}}
                                    style={{borderWidth: 2, borderColor: Colors.primary}}
                                    pressColor={"#FFF"}
                                    textStyle={{color: Colors.primary}}
                                    title="Batalkan"
                                    onPress={() => setshowPassword(true)}
                                />
                            )
                        }                        
                        <Button 
                            onPress={() => navigation.goBack()}
                            containerStyle={{marginHorizontal: Size.scaleSize(15), backgroundColor: '#fff', marginBottom: Size.scaleSize(10), width: '90%'}}
                            // style={{borderWidth: 2, borderColor: Colors.primary}}
                            pressColor={"#FFF"}
                            textStyle={{color: Colors.primary}}
                            title="Kembali"
                        />
                    </>
                )
            }            
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF',
        alignItems: 'center'
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: "Detail",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default DetailTarikDanaScreen;