import { View, Text } from 'react-native'
import React from 'react'
import Fonts from '@styles/Fonts';
import { Size } from '@styles/index';
import {useSelector, useDispatch} from 'react-redux';
import ModalVariants from '@components/modal/ModalVariant';

const InputProductsScreendua = ({navigation, route}) => {
  return (
    <View>
      <Text>InputProductsScreendua</Text>
      <ModalVariants/>
    </View>
  )
}
export const screenOptions = navData => {
    return {
      headerTitle: 'Tambah Produk',
      headerTitleAlign: 'left',
      headerTitleStyle: {
        marginTop: 10,
        fontFamily: Fonts.mulishExtraBold,
        fontSize: Size.scaleFont(16),
        color: '#333333',
      },
      header: () => (
        <View style={{height: Size.scaleSize(80), backgroundColor: '#FFF'}}>
          <HeaderTitle title="Tambah Produk" />
        </View>
      ),
      headerLeft: () => (
        <TouchableOpacity
          style={{marginLeft: Size.scaleSize(5), bottom: 0, position: 'absolute'}}
          onPress={() => navData.navigation.goBack()}>
          <View style={{padding: Size.scaleSize(10)}}>
            <Icons.ChevronLeft color="#333" size={Size.scaleSize(18)} />
          </View>
        </TouchableOpacity>
      ),
    };
  };
export default InputProductsScreendua