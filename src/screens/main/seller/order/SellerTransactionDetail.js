import React from 'react'
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    Alert,
    Platform
} from 'react-native'
import { Size, Fonts, Icons, Colors } from '@styles/index';
import TransactionDetailComponent from './component/SellerTransactionDetailComponent';
import { useSelector, useDispatch } from 'react-redux';
import API from '@utils/services/seller/orderProvider'
import APICHAT from '@utils/services/buyer/chatProvider'
import APICHATSELLER from '@utils/services/seller/chatProvider'
import {showToastMessage} from '@helpers/functionFormat'
import {HeaderTitle} from '@components/global'
import ModalInputResi from '@components/modal/ModalInputResi';
import ModalChatAdmin from '@components/modal/ModalChatAdmin';

const TransactionDetail = ({
    navigation,
    route
}) => {
    const { userData } = useSelector(state => state.auth);
    const dispatch = useDispatch(); 
    const [dataDetail, setDataDetail] = React.useState({})
    const [resiNumber, setResiNumber] = React.useState('')
    const [status, setStatus] = React.useState('')
    const [modalResi, setModalResi] = React.useState(false)
    const [showChatAdmin, setShowChatAdmin] = React.useState(false)
    const [message, setMessage] = React.useState('')
    const [sentTo, setSentTo] = React.useState('admin')

    const getDetail = async () => {
        const params = {
            transaction_id: route.params.item.transaction_id
        }
        console.log(params,'datatog');
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetDetailOrder(params, userData.token);
        console.log('=== res detail', res.data);
        dispatch({type: "HIDE_LOADING"});
        if(res.status === 200) {
            setDataDetail(res.data.data)
        } else {
            if(res.status === 500) {
                Alert.alert('Server error', 'Silahkan coba lagi')
            }
        }
    }

    const changeStatus = async (statusOrder = '', resi = '') => {
        const data = new FormData();
        data.append('transaction_id', route.params.item.transaction_id)
        data.append('status', statusOrder ? statusOrder : status)
        data.append('no_resi', resi ? resi : resiNumber) 
        // console.log('cek payload', data);
        dispatch({type: "SHOW_LOADING"});
        const res = await API.ChangeStatus(data, userData.token)
        dispatch({type: "HIDE_LOADING"});
        // console.log('res change status', res);
        if(res.status === 200) {
            showToastMessage('Pesanan anda berhasil di selesaikan')
            navigation.goBack()
        } else {
            if(res.status === 400) {
                showToastMessage(`${res.data.data.info}`)
            } else {
                showToastMessage('Status Transaksi Gagal Diubah, Silahkan Coba Lagi')
            }            
        }
    }

    const sendMessageToAdmin = async () => {
        setShowChatAdmin(false)
        const formdata = new FormData();
        formdata.append('message', message)
        formdata.append('role', userData.is_seller === "no" ? "buyer" : "seller")
        dispatch({type: "SHOW_LOADING"});
        const res = await APICHAT.SendChatToAdmin(formdata, userData.token)
        dispatch({type: "HIDE_LOADING"});
        if(res.status === 200) {
            return showToastMessage('Pesan anda telah dikirim, admin akan memproses pesan anda')
        } else {
            return showToastMessage('Pesan gagal dikirim, silahkan coba lagi')
        }
    }

    const sendMessageToEkspedition = async () => {
        setShowChatAdmin(false)
        const formdata = new FormData();
        formdata.append('message', message)
        formdata.append('expedition_id', dataDetail.shipping.expedition_id)
        dispatch({type: "SHOW_LOADING"});
        const res = await APICHATSELLER.SendChatToEkspedition(formdata, userData.token)
        dispatch({type: "HIDE_LOADING"});
        if(res.status === 200) {
            return showToastMessage('Pesan anda telah dikirim ke ekspedisi')
        } else {
            return showToastMessage('Pesan gagal dikirim, silahkan coba lagi')
        }
    }

    React.useEffect(() => {
        getDetail();
    },[])

    return(
        <View style={{flex: 1, backgroundColor: Colors.bgGrey}}>
            <ModalInputResi
                show={modalResi}
                onClose={() => setModalResi(false)}
                resi={resiNumber}
                setResi={(text) => setResiNumber(text)}
                onConfirm={() => {
                    setModalResi(false)
                    changeStatus()
                }}
            />
            <ModalChatAdmin
                show={showChatAdmin}
                onClose={() => setShowChatAdmin(false)}
                sendTo={sentTo === 'admin' ? "Admin" : "Ekspedition"}
                message={message}
                setMessage={(text) => setMessage(text)}
                onSubmit={() => {
                    if(sentTo === 'admin') {
                        sendMessageToAdmin()
                    } else {
                        sendMessageToEkspedition()
                    }
                }}
            />
            <HeaderTitle
                title="Detail"
            />
            <View style={{padding: 18}}>
                {
                    Object.keys(dataDetail).length === 0 ?
                        null
                    :
                        <TransactionDetailComponent
                            dataDetail={dataDetail}
                            onPressChangeStatus={(status) => {
                                if(dataDetail.type_product === 'material') {                                    
                                    changeStatus(status, dataDetail.shipping.resi)
                                } else {
                                    setStatus(status)
                                    setModalResi(true)
                                }                                
                            }}
                            onPressChatAdmin={() => {
                                setSentTo('admin')
                                setTimeout(() => {
                                    setShowChatAdmin(true)
                                }, 500);
                            }}
                            onPressChatEkspedition={() => {
                                setSentTo('ekspedition')
                                setTimeout(() => {
                                    setShowChatAdmin(true)
                                }, 500);
                            }}
                        />
                }
            </View>            
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: "Detail",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default TransactionDetail;