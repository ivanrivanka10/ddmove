import React, {useEffect, useCallback, useState} from 'react';
import { View, Text, StyleSheet, Image, useWindowDimensions, FlatList, TouchableOpacity, StatusBar } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import { Button, InputText, Header } from '@components/global';
import { Size, Fonts, Colors, Icons } from '@styles/index';
import moment from 'moment'
import 'moment/locale/id'
import {rupiahFormat} from '@helpers/functionFormat'
import {useIsFocused, useNavigation} from '@react-navigation/native'

import { useSelector, useDispatch } from 'react-redux';
import {IconAsset} from '@utils/index'
import API from '@utils/services/seller/orderProvider'
import DatePicker from 'react-native-date-picker'

const FirstRoute = props => {
    // console.log('cek props ', props);
    return (
        <View style={styles.screen}>
            
            <FlatList 
                style={{paddingHorizontal: Size.scaleSize(18), paddingVertical: Size.scaleSize(20)}}
                contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
                data={props.data}
                keyExtractor={item => item.id}
                ListHeaderComponent={(
                    <View style={styles.titleContainer}>
                        {/* <InputText 
                            placeholder="Cari Kode Pesan"
                            rightIcon={(
                                <Icons.SearchSVG 
                                    size={20}
                                    color="#333333"
                                />
                            )}
                        /> */}
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                            <Text style={{fontFamily: Fonts.mulishBold}}>{props.data.length} Pesanan Baru</Text>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <TouchableOpacity 
                                    onPress={() => props.setShowFilter(!props.showFilter)}
                                    style={{flexDirection: 'row', alignItems: 'center', marginVertical: 20}}>
                                    <Text style={{fontFamily: Fonts.mulishRegular, color: Colors.primary, marginRight: 10}}>{props.filterDate === 'all' ? 'Semua' : props.filterDate}</Text>
                                    <Icons.FilterSVG 
                                        color={Colors.primary}
                                        size={24}
                                    />
                                </TouchableOpacity>
                                {
                                    props.filterDate !== 'all' && (
                                        <TouchableOpacity
                                            onPress={() => props.setFilterDate('all')}
                                        >
                                            <Image
                                                source={IconAsset.IcClose}
                                                style={{width: 22, height: 22}}
                                            />
                                        </TouchableOpacity>
                                    )
                                }                                
                            </View>                            
                        </View>
                    </View>
                )}
                renderItem={(itemData) => (
                    <View style={{marginBottom: 20, borderRadius: 20, borderColor: '#F2F4F5', borderWidth: 1, padding: 20}}>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 10}}>
                            <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>{itemData.item.transaction_date}</Text>
                            <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>Total</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>{itemData.item.transaction_id}</Text>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>{rupiahFormat(itemData.item.grand_total)}</Text>
                        </View>
                        <View style={{width: '100%', height: 1, backgroundColor: '#F2F4F5', marginVertical: 15}}/>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                            {/* <Button 
                                containerStyle={{backgroundColor: props.btnColor, borderRadius: 10}}
                                title={props.status}
                                textStyle={{fontFamily: Fonts.mulishRegular, paddingHorizontal: 20, fontSize: 14}}
                            /> */}
                            <View style={{
                                flexDirection: 'row',
                                padding: 10,
                                backgroundColor: props.btnColor,
                                borderRadius: 10
                            }}>
                                <Text style={{fontFamily: Fonts.mulishRegular, paddingHorizontal: 20, color: '#fff', fontSize: 14}}>{props.status}</Text>
                            </View>
                            <TouchableOpacity 
                                onPress={() => {
                                    props.navigation.navigate('SellerTransactionDetail', {
                                        item: itemData.item
                                    })
                                }}
                                style={{backgroundColor: Colors.primary, padding: 5, borderRadius: 60}}>
                                <Icons.ChevronRight 
                                    size={22}
                                    color="#FFF"
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
            />            
        </View>
    )
};

const SellingScreen = props => {
    const { userData } = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const isFocused = useIsFocused()
    const navigation = useNavigation()
    const [listData, setListData] = useState([])
    const [filterDate, setFilterDate] = useState('all')
    const [pickedDate, setPickedDate] = useState(new Date())
    const [showFilter, setShowFilter] = useState(false)

    const renderScene = SceneMap({
        first: () => <FirstRoute {...props} filterDate={filterDate} setFilterDate={(value) => setFilterDate(value)} showFilter={showFilter} setShowFilter={(value) => setShowFilter(value)} status="Belum Dibayar" btnColor="#FF5247" data={listData}/>,
        second: () => <FirstRoute {...props} filterDate={filterDate} setFilterDate={(value) => setFilterDate(value)} showFilter={showFilter} setShowFilter={(value) => setShowFilter(value)} status="Diproses" btnColor="#FF5247" data={listData}/>,
        third: () => <FirstRoute {...props} filterDate={filterDate} setFilterDate={(value) => setFilterDate(value)} showFilter={showFilter} setShowFilter={(value) => setShowFilter(value)} status="Dikemas" btnColor="#FFB323" data={listData}/>,
        fourth: () => <FirstRoute {...props} filterDate={filterDate} setFilterDate={(value) => setFilterDate(value)} showFilter={showFilter} setShowFilter={(value) => setShowFilter(value)} status="Dikirim" btnColor="#1DA1F2" data={listData}/>,
        fifth: () => <FirstRoute {...props} filterDate={filterDate} setFilterDate={(value) => setFilterDate(value)} showFilter={showFilter} setShowFilter={(value) => setShowFilter(value)} status="Selesai" btnColor="#23C16B" data={listData}/>,
        sixth: () => <FirstRoute {...props} filterDate={filterDate} setFilterDate={(value) => setFilterDate(value)} showFilter={showFilter} setShowFilter={(value) => setShowFilter(value)} status="Dibatalkan" btnColor="#B4B6B8" data={listData}/>
    });

    const layout = useWindowDimensions();
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'Belum Dibayar' },
        { key: 'second', title: 'Diproses' },
        { key: 'third', title: 'Dikemas' },
        { key: 'fourth', title: 'Dikirim' },
        { key: 'fifth', title: 'Selesai' },
        { key: 'sixth', title: 'Pembatalan' }
    ]);

    const getListOrder = async () => {
        var params = {}
        if(props.route.params !== undefined && props.route.params.fromNotif && index === 0) {
            const selectedStatus = props.route.params.tab
            params.status = selectedStatus === 1 ? 'paid' : 'success'
            params.start_date = filterDate
        } else {
            params.status = index === 0 ? 'no-paid' : index === 1 ? 'paid' : index === 2 ? 'packed' : index === 3 ? 'sent' : index === 4 ? 'success' : 'canceled'
            params.start_date = filterDate
        }
        console.log('res order', params);

        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetListOrder(params, userData.token);
        dispatch({type: "HIDE_LOADING"});
        setListData(res.data.data)
        navigation.setParams({fromNotif: false})
    }

    useEffect(() => {
        if(props.route.params !== undefined && props.route.params.fromNotif) {
            console.log('masuk sini', props);
            setIndex(props.route.params.tab)
        }
        getListOrder()     
    },[isFocused, index, filterDate])

    console.log('state index', index);
    console.log('cek props', props);
    return (
        <>
            {/* <StatusBar backgroundColor={Colors.primary} barStyle="light-content"/> */}
            <Header 
                navData={props}
                isSeller
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishExtraBold, fontSize: Size.scaleFont(16)}}>Penjualan Saya</Text>
                    </View>
                )}
            />
            <DatePicker
                modal
                open={showFilter}
                date={pickedDate}
                onConfirm={(date) => {
                    console.log('date picked', moment(date).format('YYYY-MM-DD'));
                    setShowFilter(false)
                    setPickedDate(date)
                    setFilterDate(moment(date).format('YYYY-MM-DD'))
                }}
                onCancel={() => {
                    setShowFilter(false)
                }}
                mode="date"
                theme='light'
            />
            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={(index) => {                    
                    setIndex(index)
                }}
                initialLayout={{ width: layout.width }}
                // swipeEnabled={false}
                renderTabBar={props => (
                    <TabBar 
                        {...props} 
                        scrollEnabled={true}
                        tabStyle={{width: 'auto'}}
                        indicatorStyle={styles.indidcatorStyle}
                        style={styles.tabStyle}
                        activeColor={Colors.secondary}
                        inactiveColor={'#CDCFD0'}
                        pressColor={"transparent"}
                        renderLabel={({ route, color }) => (
                            <View style={[
                                styles.labelContainer, 
                                { borderColor: color, backgroundColor: Colors.opacityColor(color, 0.08) }
                            ]}>
                                <Text 
                                    allowFontScaling={false} 
                                    style={[styles.tabLabel, { color: color }]}
                                >
                                    {route.title}
                                </Text>
                            </View>
                        )}
                    />
                )}
            />
        </>
    )
}

const styles = StyleSheet.create({
    screen: { 
        flex: 1, 
        backgroundColor: '#FFF'
    },
    headerTitle: {
        color: '#FFF', 
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16)
    },
    indidcatorStyle: {
        height: 0
    },
    tabStyle: {
        elevation: 0.5, 
        backgroundColor: '#FFF'
    },
    labelContainer: {
        borderWidth: 1, 
        padding: Size.scaleSize(10), 
        borderRadius: 30, 
        paddingHorizontal: Size.scaleSize(20)
    },
    tabLabel: {
        fontFamily: Fonts.mulishBold,
        textTransform: 'capitalize',
        fontSize: Size.scaleFont(14)
    },
    titleContainer: { 
        marginBottom: Size.scaleSize(8)
    },
    title: {
        fontFamily: Fonts.mulishExtraBold,
        fontSize: Size.scaleFont(16),
        color: '#333',
        marginBottom: 20
    }
})

export const screenOptions = navData =>{
    return {
        header: () => (
            <Header 
                navData={navData}
                isSeller
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishExtraBold, fontSize: Size.scaleFont(16)}}>Penjualan Saya</Text>
                    </View>
                )}
            />
        )
    }
}

export default SellingScreen;