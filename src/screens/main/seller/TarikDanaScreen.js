import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, useWindowDimensions, FlatList, TouchableHighlight, Image, TouchableOpacity, StatusBar, TextInput } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import API from '@utils/services/seller/homeProvider'
import APISETTING from '@utils/services/seller/settingProvider'
import {useSelector, useDispatch} from 'react-redux'
import {useIsFocused} from '@react-navigation/native'
import { Button, InputText, HeaderTitle } from '@components/global';
import { Size, Fonts, Colors, Icons, Images } from '@styles/index';
import {ImageAsset} from '@utils/index'
import {rupiahFormat,showToastMessage,inputRupiah} from '@helpers/functionFormat'
import moment, { isDate } from 'moment';
import ModalPassword from '@components/modal/ModalPassword';
  
const TarikDanaScreen = props => {
    const isFocused = useIsFocused()
    const renderScene = SceneMap({
        first: FirstRoute,
        second: () => <SecondRoute {...props}/>
    });

    const { userData } = useSelector(state => state.auth);
    const dispatch = useDispatch();

    const layout = useWindowDimensions();
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'Penarikan' },
        { key: 'second', title: 'Riwayat Penarikan' }
    ]);
    const [listBank, setListBank] = useState([])
    const [history, setHistory] = useState([])
    const [Saldo,setSaldo]      = useState()
    const [nominal, setNominal] = useState(null)
    const [selected, setSelected] = useState(0);
    const [dataShop, setDataShop] = useState({})

    const getData = async () => {
        dispatch({type: "SHOW_LOADING"});
        const getListBank = APISETTING.GetListPaymentAccount(userData.token)
        const getHistory = API.GetHistoryWD(userData.token)
        const getShop = APISETTING.GetDataShop(userData.token)
        const [bank, history, shop] = await Promise.all([
            getListBank,
            getHistory,
            getShop
        ])
        if (bank.status=='200'){
            console.log('cek history', history);
            console.log('cek shop', shop);
            console.log('cek bank', bank.data.data.saldo);
            setSaldo    (bank.data.data.saldo)
            setListBank(bank.data.data.bank)
            setHistory(history.data.data)
            setDataShop(shop.data.data)
        }else{
            console.log('cek history', history);
            console.log('cek shop', shop);
            setHistory(history.data.data)
            setDataShop(shop.data.data)
        }
        

        dispatch({type: "HIDE_LOADING"});
    }

    const requestWD = async (nominal, password) => {
        // setNominal(nominal)
        console.log('wdnominal',nominal);
        const nominalku = parseFloat(nominal.toString().split('.').join(""));
        console.log('isiconvert',nominalku);
        if(nominalku < 50000) {
            showToastMessage('Minimal penarikan dana adalah Rp. 50.000 nilai')
            return false
        }

        if(password.length < 8) {
            showToastMessage('Password minimal 8 karakter')
            return false
        }
        const formData = new FormData()
        formData.append('shop_id', dataShop.id)
        formData.append('bank_account_id', listBank[selected].id)
        formData.append('nominal', nominalku)
        formData.append('password', password)
        dispatch({type: "SHOW_LOADING"});
        const res = await API.RequestWd(formData, userData.token)
        console.log('res wd', res.data.data);
        if(res.status === 200) {
            getData()
            showToastMessage('Permintaan tarik dana berhasil, permintaan akan diproses admin')
        } else {
            showToastMessage('Saldo anda tidak mencukupi untuk melakukan')
        }
        dispatch({type: "HIDE_LOADING"});
    }

    useEffect(() => {
        getData()
    },[isFocused])
    // console.log('list bank', listBank);
    // console.log('history', dataShop);
    return (
        <>
            <HeaderTitle
                title="Tarik Dana"
            />
            <TabView
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                renderTabBar={props => (
                    <TabBar 
                        {...props} 
                        indicatorStyle={styles.indidcatorStyle}
                        style={styles.tabStyle}
                        activeColor={Colors.secondary}
                        inactiveColor={'#CDCFD0'}
                        pressColor={"transparent"}
                        renderLabel={({ route, color }) => (
                            <Text 
                                allowFontScaling={false} 
                                style={[styles.tabLabel, { color: color }]}
                            >
                                {route.title}
                            </Text>
                        )}
                    />
                )}
            />
        </>        
    )

    function FirstRoute(){
        const [value, setValue] = useState(null)
        const [password, setPassword] = useState('')
        const [showModal, setShowModal] = useState(false)
        return (
            <View style={styles.screen}>
                <ModalPassword
                    show={showModal}
                    onClose={() => setShowModal(false)}
                    password={password}
                    setPassword={(val) => setPassword(val)}
                    onSubmit={() => {
                        setShowModal(false)
                        requestWD(value, password)
                    }}
                />
                <FlatList 
                    style={{paddingHorizontal: Size.scaleSize(18), paddingVertical: Size.scaleSize(20)}}
                    contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
                    data={listBank}
                    keyExtractor={item => item.id}
                    ListHeaderComponent={(
                        <View style={styles.titleContainer}>
                         <Text allowFontScaling={false} style={styles.title}>Saldo Toko</Text>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishExtraBold,
                  color: Colors.blackBase,
                  fontSize: Size.scaleFont(18),
                  marginBottom: Size.scaleSize(15),
                }}>
                Rp. {inputRupiah(""+Saldo)}</Text>
                            <Text allowFontScaling={false} style={styles.title}>Nominal Penarikan</Text>
                            <InputText 
                                placeholder={"Contoh: 1.000.000"}
                                value={value}
                                keyboardType="number-pad"
                                onChangeText={(text) => {
                                    // var inpt = inputRupiah(text, '')
                                    setValue(inputRupiah(text, ''))
                                    // setNominal(inputRupiah(text, ''))
                                }}
                            />
                            {/* <TextInput
                                placeholder={"Contoh: 1.000.000"}
                                value={nominal}
                                keyboardType="number-pad"
                                onChangeText={(text) => {
                                    setValue(text)
                                    var inpt = inputRupiah(text, '')
                                    setNominal(inpt)
                                }}
                                onFocus={() => ''}
                            /> */}
                            <Text allowFontScaling={false} style={[styles.title, { marginTop: 20, marginBottom: 10 }]}>Rekening Tujuan</Text>
                        </View>
                    )}
                    renderItem={(itemData) => (
                        <View style={{marginBottom: 20, borderRadius: 20}}>
                            <TouchableHighlight 
                                style={{borderRadius: 20}}
                                onPress={() => setSelected(itemData.index)}
                                underlayColor={Colors.secondary}
                            >
                                <View style={{backgroundColor: selected === itemData.index? '#FFF9F3' : '#FFF', borderColor: selected === itemData.index? Colors.secondary : '#F2F4F5', borderWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderRadius: 20, padding: 20}}>
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Image 
                                            source={ImageAsset.LogoBca}
                                            style={{width: 40, height: 40, marginRight: 10}}
                                        />
                                        <View>
                                            <Text style={{fontFamily: Fonts.mulishExtraBold, marginBottom: 5}}>{itemData.item.bank_name}</Text>
                                            <Text style={{fontFamily: Fonts.mulishRegular}}>{itemData.item.account_number}</Text>
                                        </View>
                                    </View>
                                    <View style={{width: 25, height: 25, borderColor: Colors.secondary, borderWidth: 2, borderRadius: 25, alignItems: 'center', justifyContent: 'center'}}>
                                        {selected === itemData.index && <View style={{width: 15, height: 15, backgroundColor: Colors.secondary, borderRadius: 15}}/>}
                                    </View>
                                </View>
                            </TouchableHighlight>
                        </View>
                    )}
                />
                <Button 
                    containerStyle={{marginHorizontal: 20, marginBottom: 5, backgroundColor: value === null || value === 0 ? Colors.grey_1 : Colors.primary}}
                    title="Tarik Sekarang"
                    onPress={() => setShowModal(true)}
                    disable={value === null || value === 0}
                />
            </View>
        )
    };

    function SecondRoute() {
        const colorStatus = (value) => {
            var colors = ''
            switch (value) {
                case 'cancel':
                    return colors = 'red'
                case 'pending':
                    return colors = Colors.secondary
                case 'success':
                    return colors = 'green'
                default:
                    break;
            }
            return colors;
        }
        return (
            <View style={styles.screen}>
                <StatusBar backgroundColor={"#FFF"} barStyle="dark-content"/>
                {/* <View style={{backgroundColor: '#F7F9F8', paddingHorizontal: 20, paddingVertical: 10}}>
                    <Text style={{fontFamily: Fonts.mulishRegular}}>25 Juni 2022</Text>
                </View> */}
                <FlatList
                    data={history}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item, index}) => (
                        <TouchableHighlight onPress={() => props.navigation.navigate('DetailTarikDana',{item, dataShop})}>
                            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: '#FFF', padding: 20}}>
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <Icons.DownloadSVG 
                                        size={35}
                                        color={Colors.secondary}
                                    />
                                    <View style={{marginLeft: 10}}>
                                        <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Penarikan</Text>
                                        <View style={{
                                            flexDirection: 'row',
                                            alignItems: 'flex-end'
                                        }}>
                                            <Text style={{color: colorStatus(item.status), fontFamily: Fonts.mulishRegular, fontSize: 16}}>
                                                {item.status}
                                            </Text>
                                            <Text style={{color: '#B4B6B8', marginLeft: 10, fontFamily: Fonts.mulishRegular, fontSize: 12}}>
                                                {`${moment(item.date).format('HH:mm')}`}
                                            </Text>
                                        </View>                                        
                                    </View>
                                </View>
                                <Text style={{fontFamily: Fonts.mulishRegular, color: '#333'}}>{rupiahFormat(item.nominal)}</Text>
                            </View>
                        </TouchableHighlight>
                    )}
                />                
                {/* <View style={{backgroundColor: '#F7F9F8', paddingHorizontal: 20, paddingVertical: 10}}>
                    <Text style={{fontFamily: Fonts.mulishRegular}}>25 Mei 2022</Text>
                </View>
                <TouchableHighlight onPress={() => props.navigation.navigate('DetailTarikDana')}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: '#FFF', padding: 20}}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icons.DownloadSVG 
                                size={35}
                                color={Colors.secondary}
                            />
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Penarikan</Text>
                                <Text style={{color: '#B4B6B8', fontFamily: Fonts.mulishRegular, fontSize: 12}}>Dalam Proses • 16:40</Text>
                            </View>
                        </View>
                        <Text style={{fontFamily: Fonts.mulishRegular, color: '#333'}}>-Rp 500.000</Text>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => props.navigation.navigate('DetailTarikDana')}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: '#FFF', padding: 20}}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icons.DownloadSVG 
                                size={35}
                                color={Colors.secondary}
                            />
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>Penarikan</Text>
                                <Text style={{color: '#B4B6B8', fontFamily: Fonts.mulishRegular, fontSize: 12}}>Dalam Proses • 16:40</Text>
                            </View>
                        </View>
                        <Text style={{fontFamily: Fonts.mulishRegular, color: '#333'}}>-Rp 500.000</Text>
                    </View>
                </TouchableHighlight> */}
            </View>
        )
        
    };

}

const styles = StyleSheet.create({
    screen: { 
        flex: 1, 
        backgroundColor: '#FFF'
    },
    headerTitle: {
        color: '#FFF', 
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16)
    },
    indidcatorStyle: {
        backgroundColor: Colors.secondary, 
        height: Size.scaleSize(3), 
        borderRadius: 3
    },
    tabStyle: {
        elevation: 0.5, 
        backgroundColor: '#FFF'
    },
    tabLabel: {
        fontFamily: Fonts.mulishMedium,
        textTransform: 'capitalize',
        fontSize: Size.scaleFont(14)
    },
    titleContainer: { 
        marginBottom: Size.scaleSize(8)
    },
    title: {
        fontFamily: Fonts.mulishExtraBold,
        fontSize: Size.scaleFont(16),
        color: '#333',
        marginBottom: 20
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: "Tarik Dana",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default TarikDanaScreen;