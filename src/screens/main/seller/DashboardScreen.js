import React, {useCallback, useEffect, useState} from 'react';
import { 
    ScrollView, 
    View,  
    StyleSheet,
    Text,    
    Dimensions, 
    StatusBar,
    TouchableOpacity,
    RefreshControl
} from 'react-native';
import {Text as SText} from 'react-native-svg'
import {rupiahFormat, inputRupiah} from '@helpers/functionFormat'
import { BarChart, LineChart, PieChart } from "react-native-gifted-charts";

import { Colors, Fonts, Size, Icons} from '@styles/index';
import { Header, Button } from '@components/global';
import { useSelector, useDispatch } from 'react-redux';
import API from '@utils/services/seller/homeProvider'
import moment from 'moment'
import APISELLER from '@utils/services/seller/settingProvider'
import ModalPickMonth from '@components/modal/ModalPickMonth';
import { useFocusEffect } from '@react-navigation/native';

const DashboardScreen = props => {
    const { userData, location } = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const [chartData, setChartData] = useState([])
    const [selectedFilter, setSelectedFilter] = useState(7)
    const [dataShop, setDataShop] = useState({})
    const [dataDashboard, setDataDashboard] = useState({})
    const [refreshing, setRefreshing] = useState(false);
    const [modalMonth, setModalMonth] = useState(false)
    const [currentMonth, setCurrentMonth] = useState(moment(new Date()).format('MMMM'))

    const getDataHome = async () => {
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetDataHome(selectedFilter, userData.token)
        console.log('res data home', res.data);
        if(res.status === 200) {            
            var chart = []
            var data = res.data.data.chart
            for (let i = 0; i < data.length; i++) {
                chart.push({
                    value: Number(data[i].total),
                    frontColor: '#177AD5',
                    label: moment(data[i].date, 'DD-MM-YYYY').format('DD')
                })
            }
            console.log('cek chart', chart);
            setDataDashboard(res.data.data)
            setChartData(chart)
        }
        getDataShop()
        dispatch({type: "HIDE_LOADING"});
        setRefreshing(false)
    }

    const getDataShop = async () => {
        const res = await APISELLER.GetDataShop(userData.token)
        console.log('res shop', res);
        console.log('navigation dashboard', props);
        if(res.status === 200) {
            setDataShop(res.data.data)
            const title = res.data.data.name
            // props.navigation.setParam({title})
        }
    }
    const onRefresh = () => {
        setRefreshing(true);
        getDataHome();
      };
    useFocusEffect(
        useCallback(() => {
            console.log('user data seller', userData);        
          getDataHome();
        }, [selectedFilter])
      )


    console.log('current month', currentMonth);
    return (
        <ScrollView style={{backgroundColor: '#FFF'}}
        refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
            <ModalPickMonth
                show={modalMonth}
                onClose={() => setModalMonth(false)}
                onPick={(item) => {
                    var year = moment(new Date()).format('YYYY')
                    setSelectedFilter(`${year}-${item.value}`)
                    setCurrentMonth(item.label)
                    setModalMonth(false)
                }}
            />
            <Header 
                navData={props}
                isSeller
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14)}}>Selamat Datang,</Text>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishExtraBold, fontSize: Size.scaleFont(16)}}>{dataShop.name}</Text>
                    </View>
                )}
            />
            <View style={styles.screen}>
                <View style={{flexDirection: 'row', marginTop: 20}}>
                    <TouchableOpacity 
                        activeOpacity={0.8}
                        onPress={() => setSelectedFilter(7)}
                        style={[styles.borderFilter, selectedFilter === 7 && {backgroundColor: '#FFF9F3', borderColor: Colors.secondary}]}>
                        <Text style={{fontFamily: Fonts.mulishBold, fontSize: Size.scaleFont(14), color: selectedFilter === 7 ? Colors.secondary : '#333333'}}>7 Hari</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        activeOpacity={0.8}
                        onPress={() => setSelectedFilter(30)}
                        style={[styles.borderFilter, selectedFilter === 30 && {backgroundColor: '#FFF9F3', borderColor: Colors.secondary}]}>
                        <Text style={{fontFamily: Fonts.mulishBold, fontSize: Size.scaleFont(14), color: selectedFilter === 30 ? Colors.secondary : '#333333'}}>30 Hari</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        activeOpacity={0.8}
                        onPress={() => {
                            // setSelectedFilter('all')
                            setModalMonth(true)
                        }}
                        style={[styles.borderFilter, typeof selectedFilter === 'string' && {backgroundColor: '#FFF9F3', borderColor: Colors.secondary}]}>
                        <Text style={{fontFamily: Fonts.mulishBold, fontSize: Size.scaleFont(14), color: typeof selectedFilter === 'string' ? Colors.secondary : '#333333'}}>Semua</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between', marginHorizontal: 10, marginTop: 20}}>
                    <View>
                        <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8', fontSize: Size.scaleFont(16), marginBottom: Size.scaleSize(5)}}>Total Pendapatan</Text>
                        <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                            <Text style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8', marginRight: 5, marginTop: Size.scaleSize(3)}}>Rp</Text>
                            <Text style={{fontSize: Size.scaleFont(24), color: '#333333', fontFamily: Fonts.mulishExtraBold}}>{Object.keys(dataDashboard).length > 0 ? inputRupiah(`${dataDashboard.income_balance}`, '') : 0}</Text>
                        </View>
                    </View>
                    <Button 
                        onPress={() => props.navigation.navigate('TarikDana')}
                        containerStyle={{marginBottom: Size.scaleSize(8)}}
                        textComponent={(
                            <View style={{flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 10}}>
                                <Icons.LockedSVG
                                    size={25} 
                                    color="#FFF"
                                />
                                <Text style={{fontFamily: Fonts.mulishBold, color: '#FFF', marginLeft: 10, marginTop: 2}}>Tarik Dana</Text>
                            </View>
                        )}
                    />
                </View>
                <View>
                    <BarChart
                        barWidth={22}
                        noOfSections={3}
                        barBorderRadius={4}
                        frontColor="lightgray"
                        data={chartData}
                        yAxisThickness={0}
                        xAxisThickness={0}
                    />
                </View>
                <View style={{borderColor: '#F2F4F5', borderWidth: 1.5, marginHorizontal: 20, padding: 18, borderRadius: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 25}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{backgroundColor: '#FFF9F3', padding: 16, alignSelf: 'flex-start', borderRadius: 40, marginRight: 10}}>
                            <Icons.FileCheckSVG 
                                color={Colors.secondary}
                                size={24}
                            />
                        </View>
                        <View>
                            <Text style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: '#B4B6B8'}}>Total Penjualan</Text>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14), marginTop: 5}}>{Object.keys(dataDashboard).length > 0 ? dataDashboard.sold : 0} terjual</Text>
                        </View>
                    </View>
                    <Text style={{fontFamily: Fonts.mulishMedium, color: '#23C16B', fontSize: Size.scaleFont(14)}}>
                        {Object.keys(dataDashboard).length > 0 ? dataDashboard.sold > 0 && dataDashboard.product > 0 ? ((dataDashboard.sold/dataDashboard.product)*100).toFixed(1) : 0 : 0}%
                    </Text>
                </View>
                <View style={{borderColor: '#F2F4F5', borderWidth: 1.5, marginHorizontal: 20, padding: 18, borderRadius: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 12}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{backgroundColor: '#FFF9F3', padding: 16, alignSelf: 'flex-start', borderRadius: 40, marginRight: 10}}>
                            <Icons.FileCheckSVG 
                                color={Colors.secondary}
                                size={24}
                            />
                        </View>
                        <View>
                            <Text style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: '#B4B6B8'}}>Pesanan Terbaru</Text>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14), marginTop: 5}}>{Object.keys(dataDashboard).length > 0 ? dataDashboard.new_order : 0} pesanan</Text>
                        </View>
                    </View>
                    <Text style={{fontFamily: Fonts.mulishMedium, color: '#23C16B', fontSize: Size.scaleFont(14)}}></Text>
                </View>
                <View style={{borderColor: '#F2F4F5', borderWidth: 1.5, marginHorizontal: 20, padding: 18, borderRadius: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 12}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{backgroundColor: '#FFF9F3', padding: 16, alignSelf: 'flex-start', borderRadius: 40, marginRight: 10}}>
                            <Icons.BoxSVG 
                                color={Colors.secondary}
                                size={24}
                            />
                        </View>
                        <View>
                            <Text style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: '#B4B6B8'}}>Total Produk</Text>
                            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14), marginTop: 5}}>{Object.keys(dataDashboard).length > 0 ? dataDashboard.product : 0} produk</Text>
                        </View>
                    </View>
                    <Text style={{fontFamily: Fonts.mulishMedium, color: '#23C16B', fontSize: Size.scaleFont(14)}}></Text>
                </View>
            </View>
        </ScrollView>
    )
}

export const screenOptions = navData =>{
    console.log('navdata dashboard', navData);
    return {
        header: () => (
            <Header 
                navData={navData}
                isSeller
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14)}}>Selamat Datang,</Text>
                        <Text allowFontScaling={false} style={{color: '#FFF', fontFamily: Fonts.mulishExtraBold, fontSize: Size.scaleFont(16)}}>Jaya Sejahtera</Text>
                    </View>
                )}
            />
        )
    }
}

const styles = StyleSheet.create({
    borderFilter: {
        flex: 1, 
        height: 50, 
        backgroundColor: '#F7F9F8', 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginHorizontal: 10,
        borderRadius: 40, 
        borderWidth: 1.5, 
        borderColor: '#F7F9F8'
    },
    screen: {
        flex: 1,
        backgroundColor: '#FFF'
    }
})

export default DashboardScreen;