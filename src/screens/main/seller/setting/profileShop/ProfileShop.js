import React, {useState, useEffect} from 'react'
import {
    View,
    Platform,
    TouchableOpacity,
    Dimensions
} from 'react-native'
import { Size, Fonts, Icons, Colors } from '@styles/index';
import ProfileShopComponent from './component/ProfileShopComponent';
import {HeaderTitle} from '@components/global'
import APISELLER from '@utils/services/seller/settingProvider'
import API from '@utils/services/settingProvider'
import {useSelector, useDispatch} from 'react-redux'
import {debugConsoleLog, showToastMessage} from '@helpers/functionFormat'
import APIORDER from '@utils/services/seller/orderProvider'

const ProfileShop = ({
    navigation,
    route
}) => {

    const { userData, location } = useSelector(state => state.auth);
    const dispatch = useDispatch();

    const [detailShop, setDetailShop] = useState({})
    const [listProvince, setListProvince] = useState([])
    const [listCity, setListCity] = useState([])
    const [listDistrict, setListDistrict] = useState([])
    const [listProvinceWh, setListProvinceWh] = useState([])
    const [listCityWh, setListCityWh] = useState([])
    const [listDistrictWh, setListDistrictWh] = useState([])
    const [typeFetching, setTypeFetching] = useState('shop')
    const [listCourier, setListCourier] = useState([])    

    const getDetailShop = async () => {
        dispatch({type: "SHOW_LOADING"});
        const res = await APISELLER.GetDataShop(userData.token)
        console.log('res detail shop', res.data);
        const getListCourier = await APIORDER.GetListCourier(userData.token)
        var kurir = [...getListCourier.data.data]     
        for (let i = 0; i < kurir.length; i++) {
            kurir[i].checked = false
        }
        console.log('listCourier', kurir);
        setListCourier(kurir)
        if(res.status == 200) {
            setDetailShop(res.data.data)
            getProvince(res.data.data.province.id, res.data.data.city.id)
        } else {
            showToastMessage('Server Error')
            dispatch({type: "HIDE_LOADING"});
        }
    }

    const getProvince = async (id, cityId) => {        
        const res = await API.GetProvince();
        if(res && res.status === 200) {
            let data = []
            for (let index = 0; index < res.data.data.length; index++) {
                data.push({
                    id: res.data.data[index].province_id,
                    name: res.data.data[index].province
                })
            }
            setListProvince(data)
            setListProvinceWh(data)
            getListCity(id,cityId)
        }
    }

    const getListCity = async (id,cityId) => {
        dispatch({type: "SHOW_LOADING"});        
        const res = await API.GetCity(id)
        console.log('res list city', res);
        if(res && res.status == 200) {
            let data = []
            for (let index = 0; index < res.data.data.length; index++) {
                data.push({
                    id: res.data.data[index].city_id,
                    name: res.data.data[index].city_name
                })
            }
            setListCity(data)
            if(typeFetching !== 'shop') {
                setListCityWh(data)
            }            
            if(cityId) {
                getListDistrict(cityId)
            } {
                dispatch({type: "HIDE_LOADING"});
            }
        }
    }

    const getListDistrict = async (id) => {
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetDistrict(id)
        console.log('res distrik ===', res);
        dispatch({type: "HIDE_LOADING"});
        if(res && res.status == 200) {
            console.log('=== distrik sini');
            let data = []
            for (let index = 0; index < res.data.data.length; index++) {
                data.push({
                    id: res.data.data[index].district_id,
                    name: res.data.data[index].district_name
                })
            }
            setListDistrict(data)
            if(typeFetching !== 'shop') {
                setListDistrictWh(data)
            }
        }
    }

    const updateInfo = async (data) => {
        dispatch({type: "SHOW_LOADING"});
        debugConsoleLog('datana',data)
        const res = await API.UpdateShop(data, userData.token)
        console.log('res update shop', res);
        if(res.status === 200) {
            showToastMessage('Update info toko berhasil')
            getDetailShop()
        } else {
            showToastMessage('Update info toko gagal, silahkan coba lain kali')
            dispatch({type: "HIDE_LOADING"});   
        }        
    }

    useEffect(() => {
        getDetailShop()
    },[])
    
    return(
        <View style={{flex: 1, backgroundColor: '#F7F9F8'}}>            
            <HeaderTitle
                title="Profil Toko"
            />
            {
                Object.keys(detailShop).length > 0 && (
                    <ProfileShopComponent
                        data={detailShop}
                        listProvince={listProvince}
                        listCity={listCity}
                        listDistrict={listDistrict}
                        getListCity={(id, type) => {
                            setTypeFetching(type)
                            setListCity([])
                            getListCity(id)
                        }}
                        getListDistrct={(id, type) => {
                            setTypeFetching(type)
                            setListDistrict([])
                            getListDistrict(id)
                        }}
                        updateInfo={(data) => updateInfo(data)}
                        dataLocation={location}
                        listCourier={listCourier}
                    />
                )

            }            
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: "Profil Toko",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default ProfileShop;