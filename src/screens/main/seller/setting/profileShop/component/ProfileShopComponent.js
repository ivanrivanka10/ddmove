import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  Image,
  StyleSheet,
  TextInput,
  PermissionsAndroid,
  KeyboardAvoidingView,
  Text,
} from 'react-native';
import {TextBold, TextRegular, SelectDropDown} from '@components/global';
import ModalMaps from '@components/modal/ModalMaps';
import {Colors, Fonts} from '@styles/index';
import {IconAsset, ImageAsset} from '@utils/index';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ModalPickImage from '@components/modal/ModalPickImage';
import ModalPengiriman from '@screens/main/seller/product/Pengiriman';
import Geolocation from 'react-native-geolocation-service';
import {
  login,
  updateModal,
  setUserLocation,
  updateLoading,
} from '@store/actions';
import {checkPermissions, PERMISSIONS_TYPE} from '@helpers/permissions';
import { debugConsoleLog } from '@helpers/functionFormat';

const ProfileShopComponent = ({
  navigation,
  data,
  listProvince,
  listCity,
  listDistrict,
  getListCity,
  getListDistrct,
  updateInfo,
  dataLocation,
  listCourier,
}) => {
  console.log('data detail shop', data);
  const [pickedProvince, setPickedProvince] = useState(null);
  const [name, setName] = useState(data.name ?? '');
  const [phone, setPhone] = useState(data.phone ?? '');
  const [descriptions, setDescription] = useState(data.descriptions ?? '');
  const [provinceShop, setProvinceShop] = useState(data.province.name ?? null);
  const [cityShop, setCityShop] = useState(data.city.name ?? null);
  const [districtShop, setdistrictShop] = useState(data.district.name ?? null);
  const [postal_code, setPostalCode] = useState(data.postal_code ?? '');
  const [address_details, setAddressDetail] = useState(
    data.address_details ?? '',
  );
  const [latlngShop, setLatlngShop] = useState(
    `${data.lat ?? '-'}:${data.lng ?? '-'}`,
  );
  const [latlngWarehouse, setLatlngWarehouse] = useState(
    `${data.warehouse.wh_lat}:${data.warehouse.wh_lng}`,
  );
  const [provinceWarehouse, setProvinceWarehouse] = useState(
    data.warehouse.province.name ?? null,
  );
  const [cityWarehouse, setCityWarehouse] = useState(
    data.warehouse.city.name ?? null,
  );
  const [districtWarehouse, setdistrictWarehouse] = useState(
    data.warehouse.district.name ?? null,
  );
  const [postal_codeWh, setPostalCodeWh] = useState(
    data.warehouse.wh_postal_code ?? '',
  );
  const [addressDetailWh, setAddressDetailWh] = useState(
    data.warehouse.wh_address_details ?? '',
  );
  const [pickedCourier, setPickedCourier] = useState([]);
  const [pickedIdCourier, setPickedIdCourier] = useState([]);
  const [showModalPengiriman, setShowModalPengiriman] = useState(false);
  const [pickedType, setPickedType] = useState(null);
  const [existspickedType, existssetPickedType] = useState('');
  const [existspickedShip, existssetPickedShip] = useState(null);
  const [pickedOngkir, setPickedOngkir] = useState(null);

  const [localImgPath, setLocalImgPath] = useState('');
  const [showModalOption, setShowModalOption] = useState(false);
  const [modalMaps, setModalMaps] = useState(false);
  const [lat, setLat] = useState(37.78825);
  const [long, setLong] = useState(-122.4324);

  const captureImage = async type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      saveToPhotos: true,
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      setTimeout(() => {
        launchCamera(options, response => {
          if (response.didCancel) {
            // alert('User cancelled camera picker');
            return;
          } else if (response.errorCode == 'camera_unavailable') {
            // alert('Camera not available on device');
            return;
          } else if (response.errorCode == 'permission') {
            // alert('Permission not satisfied');
            return;
          } else if (response.errorCode == 'others') {
            // alert(response.errorMessage);
            return;
          }
          setLocalImgPath(response);
        });
      }, 500);
    }
  };

  const chooseFile = type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    setTimeout(() => {
      launchImageLibrary(options, response => {
        if (response.didCancel) {
          alert('User cancelled camera picker');
          return;
        } else if (response.errorCode == 'camera_unavailable') {
          alert('Camera not available on device');
          return;
        } else if (response.errorCode == 'permission') {
          alert('Permission not satisfied');
          return;
        } else if (response.errorCode == 'others') {
          alert(response.errorMessage);
          return;
        }
        setLocalImgPath(response);
      });
    }, 500);
  };

  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Persetujuan akses kamera',
            message: 'Berikan Baba Market untuk mengakses kamera ?',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Persetujuan akses file',
            message: 'Berikan Baba Market untuk mengakses file ?',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  const setLocationShop = () => {
    // setLatlngShop(`37.78825:-122.4324`);
    loadLocation();
    // setModalMaps(true);
  };

  const loadLocation = async res => {
    // props.navigation.replace('MainNavigation');
    try {
      const permissions = await checkPermissions(PERMISSIONS_TYPE.location);
      Geolocation.getCurrentPosition(
        userLocation => {
          console.log('userLocation', userLocation);
          setLat(userLocation.coords.latitude);
          setLong(userLocation.coords.longitude);
          setLatlngShop(
            `${userLocation.coords.latitude}:${userLocation.coords.longitude}`,
          );
          setModalMaps(true);
        },
        error => {
          dispatch(updateModal(true, 'LOGIN ERROR', error.message));
        },
        {
          enableHighAccuracy: false,
          timeout: 5000,
          maximumAge: 10000,
        },
      );
      if (permissions) {
        console.log('ok');
      } else {
        console.log('error');
      }
    } catch (error) {
      console.log('error');
    }
  };

  const setLocationWh = () => {
    setLatlngWarehouse(`${dataLocation.lat}:${dataLocation.lng}`);
  };

  const updateData = () => {
    const _provinceShop = listProvince.filter(value => {
      return value.name === provinceShop;
    });
    const _cityShop = listCity.filter(value => {
      return value.name === cityShop;
    });
    const _districtShop = listDistrict.filter(value => {
      return value.name === districtShop;
    });
    const _provinceWh = listProvince.filter(value => {
      return value.name === provinceWarehouse;
    });
    const _cityWh = listCity.filter(value => {
      return value.name === cityWarehouse;
    });
    const _districtWh = listDistrict.filter(value => {
      return value.name === districtWarehouse;
    });
    const shopLocation = latlngShop.split(':');
    const whLocation = latlngWarehouse.split(':');
    const dataForm = new FormData();

    if (localImgPath !== '') {
      const foto = {
        type: !localImgPath ? '' : localImgPath.assets[0].type,
        uri: !localImgPath ? '' : localImgPath.assets[0].uri,
        name: !localImgPath ? '' : localImgPath.assets[0].fileName,
      };
      dataForm.append('photo', foto);
    }

    dataForm.append('name', name);
    dataForm.append('phone', phone);
    dataForm.append(
      'province_id',
      _provinceShop.length > 0 ? _provinceShop[0].id : data.province.id,
    );
    dataForm.append(
      'city_id',
      _cityShop.length > 0 ? _cityShop[0].id : data.city.id,
    );
    dataForm.append(
      'district_id',
      _districtShop.length > 0 ? _districtShop[0].id : data.district.id,
    );
    dataForm.append('postal_code', postal_code);
    dataForm.append('address_details', address_details);
    dataForm.append('lat', shopLocation ? shopLocation[0] : '-');
    dataForm.append('lng', shopLocation ? shopLocation[1] : '-');
    dataForm.append(
      'wh_province_id',
      _provinceWh.length > 0 ? _provinceWh[0].id : data.warehouse.province.id,
    );
    dataForm.append(
      'wh_city_id',
      _cityWh.length > 0 ? _cityWh[0].id : data.warehouse.city.id,
    );
    dataForm.append(
      'wh_distric_id',
      _districtWh.length > 0 ? _districtWh[0].id : data.warehouse.district.id,
    );
    dataForm.append('wh_postal_code', postal_codeWh);
    dataForm.append('wh_address_details', addressDetailWh);
    dataForm.append('wh_lat', '-');
    dataForm.append('wh_lng', '-');
    dataForm.append('descriptions', descriptions);

    if (pickedCourier.length > 0) {
      listCourier.map(e => {
        if (e.checked === true) {
          dataForm.append('courier[]', e.id);
        }
      });
    }

    dataForm.append(
      'type',
      pickedType == 'Non Material'
        ? 'non-material'
        : pickedType == 'Material'
        ? 'material'
        : 'both',
    );

    if (pickedOngkir == null) {
      dataForm.append('is_free_shipping', existspickedShip);
    } else {
      dataForm.append(
        'is_free_shipping',
        pickedOngkir == 'Semua Produk / Beberapa Produk' ? 1 : 0,
      );
    }
    console.log('body form data', dataForm);
    updateInfo(dataForm);
  };

  useEffect(() => {
    var cour = [];
    for (let i = 0; i < data.courier.length; i++) {
      cour.push(data.courier[i].courier_code);
    }
    setPickedCourier(cour);
    existssetPickedType(data.type);
    if (data?.type == 'both') {
      setPickedType('Material dan Non Material');
    } else if (data?.type == 'non-material') {
      setPickedType('Non Material');
    } else if (data?.type == 'material') {
      setPickedType('Material');
    }

    if (data?.is_free_shipping == 1) {
      setPickedOngkir('Semua Produk / Beberapa Produk');
    } else {
      setPickedOngkir('Tidak Ada');
    }
  }, []);

  // console.log('data location', dataLocation);
  // console.log('data location', listProvince);
  // console.log('data location', listCity);
  return (
    <>
      <ModalMaps
        data={{lat: lat, long: long}}
        setData={e => {
          setLatlngShop(`${e.lat ?? '-'}:${e.lng ?? '-'}`);
        }}
        show={modalMaps}
        onClose={() => setModalMaps(false)}
        close={() => setModalMaps(false)}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <ModalPickImage
          show={showModalOption}
          onClose={() => setShowModalOption(false)}
          onPressCamera={() => {
            setShowModalOption(false);
            captureImage('photo');
          }}
          onPressGalery={() => {
            setShowModalOption(false);
            chooseFile('photo');
          }}
        />
        {showModalPengiriman && (
          <ModalPengiriman
            show={showModalPengiriman}
            onClose={() => setShowModalPengiriman(false)}
            listCourier={listCourier}
            showForm={false}
            onSubmit={(data)=>{
              debugConsoleLog('datapicked',pickedCourier)
              var arr = [];
              for (let i = 0; i < data.selectedCourier.length; i++) {
                arr.push(data.selectedCourier[i].name);
              }
              setPickedCourier(arr);
              setShowModalPengiriman(false);
            }}
            // onSubmit={(data) => {
         
            // }}
          />
        )}
        <ImageBackground
          style={{
            width: '100%',
            height: 200,
          }}
          // source={ImageAsset.Placeholder}
          source={{uri: localImgPath ? localImgPath.assets[0].uri : data.photo}}
          resizeMode="cover">
          <View style={styles.viewPlaceholder}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.btnChangeImg}
              onPress={() => setShowModalOption(true)}>
              <TextRegular text="Ganti Foto" color={Colors.primary} />
              <Image
                source={IconAsset.IcCamera}
                style={{width: 20, height: 20, resizeMode: 'contain'}}
              />
            </TouchableOpacity>
          </View>
        </ImageBackground>
        <View style={{padding: 18, backgroundColor: '#fff'}}>
          <TextBold text="Nama Toko" color={Colors.blackBase} />
          <TextInput
            placeholder="Nama Toko"
            style={styles.txtInput}
            value={name}
            onChangeText={text => setName(text)}
          />
          <TextBold
            text="No. Telepon"
            color={Colors.blackBase}
            style={{marginTop: 24}}
          />
          <TextInput
            placeholder="Nomor Telepon"
            keyboardType="number-pad"
            style={styles.txtInput}
            value={phone}
            onChangeText={text => setPhone(text)}
          />
          <TextBold
            text="Informasi Toko"
            color={Colors.blackBase}
            style={{marginTop: 24}}
          />
          <TextInput
            placeholder="Informasi Toko"
            multiline
            style={[
              styles.txtInput,
              {
                height: 150,
                textAlignVertical: 'top',
                marginBottom: 24,
              },
            ]}
            value={descriptions}
            onChangeText={text => setDescription(text)}
          />
        </View>
        <View style={{padding: 18, marginTop: 10, backgroundColor: '#fff'}}>
          <TextBold
            text="Alamat Toko / Alamat Penjemputan"
            color={Colors.blackBase}
          />
          <SelectDropDown
            searchable
            value={provinceShop}
            onSelectItem={val => {
              setProvinceShop(val.name);
              getListCity(val.id, 'shop');
            }}
            // disabled={listCity.length < 1}
            valueColor="#000"
            colorArrowIcon="#000"
            style={{marginTop: 14, borderRadius: 6}}
            placeholderText="Pilih Provinsi"
            items={listProvince}
            maxHeight={200}
            typeValueText="Medium"
          />
          <SelectDropDown
            searchable
            value={cityShop}
            onSelectItem={val => {
              setCityShop(val.name);
              getListDistrct(val.id, 'shop');
            }}
            disabled={listCity.length < 1}
            valueColor="#000"
            colorArrowIcon="#000"
            style={{marginTop: 24, borderRadius: 6}}
            placeholderText="Pilih Kabupaten/Kota"
            items={listCity}
            maxHeight={200}
            typeValueText="Medium"
          />
          <SelectDropDown
            searchable
            value={districtShop}
            onSelectItem={val => {
              setdistrictShop(val.name);
            }}
            disabled={listDistrict.length < 1}
            valueColor="#000"
            colorArrowIcon="#000"
            style={{marginTop: 24, borderRadius: 6}}
            placeholderText="Pilih Kecamatan"
            items={listDistrict}
            maxHeight={200}
            typeValueText="Medium"
          />
          <TextInput
            placeholder="Kode Pos"
            style={styles.txtInput}
            value={postal_code}
            keyboardType="number-pad"
            onChangeText={text => setPostalCode(text)}
          />
          <TextInput
            placeholder="Alamat lengkap: Jalan, gedung, patokan, dll"
            multiline
            style={[
              styles.txtInput,
              {
                height: 150,
                textAlignVertical: 'top',
                marginBottom: 24,
              },
            ]}
            value={address_details}
            onChangeText={text => setAddressDetail(text)}
          />
          <TextBold text="Ambil Titik Lokasi" color={Colors.blackBase} />
          <View
            style={{
              marginTop: 8,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 24,
            }}>
            <View style={styles.viewKoor}>
              <TextRegular text={latlngShop} color={Colors.blackBase} />
            </View>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.btnPickKoor}
              onPress={setLocationShop}>
              <TextBold text="Pilih" color={Colors.primary} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{padding: 18, marginTop: 10, backgroundColor: '#fff'}}>
          {/* <TextBold
                    text="Alamat Pickup atau Gudang"
                    color={Colors.blackBase}
                />
                <SelectDropDown
                    searchable
                    value={provinceWarehouse}
                    onSelectItem={(val) => {
                       setProvinceWarehouse(val.name)
                       getListCity(val.id, 'wh')
                    }}
                    // disabled={listCity.length < 1}
                    valueColor="#000"
                    colorArrowIcon="#000"
                    style={{marginTop: 14, borderRadius: 6}}
                    placeholderText="Pilih Provinsi"
                    items={listProvince}
                    maxHeight={200}
                    typeValueText="Medium"
                />
                <SelectDropDown
                    searchable
                    value={cityWarehouse}
                    onSelectItem={(val) => {
                       setCityWarehouse(val.name)
                       getListDistrct(val.id, 'wh')
                    }}
                    disabled={listCity.length < 1}
                    valueColor="#000"
                    colorArrowIcon="#000"
                    style={{marginTop: 24, borderRadius: 6}}
                    placeholderText="Pilih Kabupaten/Kota"
                    items={listCity}
                    maxHeight={200}
                    typeValueText="Medium"
                />
                <SelectDropDown
                    searchable
                    value={districtWarehouse}
                    onSelectItem={(val) => {
                       setdistrictWarehouse(val.name)
                    }}
                    // disabled={listCity.length < 1}
                    valueColor="#000"
                    colorArrowIcon="#000"
                    style={{marginTop: 24, borderRadius: 6}}
                    placeholderText="Pilih Kecamatan"
                    items={listDistrict}
                    maxHeight={200}
                    typeValueText="Medium"
                />
                <TextInput
                    placeholder='Kode Pos'
                    style={styles.txtInput}
                    value={postal_codeWh}
                    onChangeText={(text) => setPostalCodeWh(text)}
                />
                <TextInput
                    placeholder='Alamat lengkap: Jalan, gedung, patokan, dll'
                    multiline
                    style={[
                        styles.txtInput,
                        {
                            height: 150,
                            textAlignVertical: 'top',
                            marginBottom: 24
                        }
                    ]}
                    value={addressDetailWh}
                    onChangeText={(text) => setAddressDetailWh(text)}
                /> */}

          <TextRegular
            text="Tipe Barang Yang Di Jual"
            color={Colors.blackBase}
            style={{marginTop: 24}}
          />
          <SelectDropDown
            value={pickedType}
            onSelectItem={val => {
              setPickedType(val.name);
              existssetPickedType(null);
              if(val.name === 'Material dan Non Material'){
                setPickedOngkir('Semua Produk / Beberapa Produk')
              }
            }}
            valueColor="#000"
            colorArrowIcon="#000"
            style={{marginTop: 10, borderRadius: 6}}
            placeholderText={pickedType}
            items={[
              // {id: 1, name: 'Material'},
              {id: 2, name: 'Non Material'},
              {id: 3, name: 'Material dan Non Material'},
            ]}
            maxHeight={200}
            typeValueText="Medium"
          />

          <>
            <TextRegular
              text="Gratis Antar"
              color={Colors.blackBase}
              style={{marginTop: 24}}
            />
            <SelectDropDown
              value={pickedOngkir}
              containerStyle={{
                backgroundColor:
                pickedType === 'Material dan Non Material' ? 'lightgrey' : 'white',
              }}
              disabled={pickedType === 'Material dan Non Material' ? true :false}
              onSelectItem={val => {
                if (val.name == 'Tidak Ada') {
                  if (
                    pickedType == 'Material' ||
                    pickedType == 'Material dan Non Material'
                  ) {
                    alert('Tipe barang ini tidak bisa memilih opsi ini');
                  } else {
                    setPickedOngkir(val.name);
                  }
                } else {
                  setPickedOngkir(val.name);
                }
              }}
              valueColor="#000"
              colorArrowIcon="#000"
              style={{marginTop: 10, borderRadius: 6}}
              placeholderText={
                existspickedShip == 1
                  ? 'Semua Produk / Beberapa Produk'
                  : 'Tidak Ada'
              }
              items={[
                {id: 1, name: 'Semua Produk / Beberapa Produk'},
                {id: 2, name: 'Tidak Ada'},
              ]}
              maxHeight={200}
              typeValueText="Medium"
            />

            {pickedOngkir !== 'Tidak Ada' && (
              <Text
                style={{
                  fontFamily: Fonts.mulishExtraBold,
                  color: Colors.primary,
                  fontSize: 10,
                  marginTop: 8,
                }}>
                * wajib mengantarkan barang ke konsumen. Baik antar langsung
                maupun melalui ekspedisi selama jangkauan masih tercover
              </Text>
            )}
          </>

          <TouchableOpacity
            style={styles.btnPengiriman}
            onPress={() => setShowModalPengiriman(true)}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={IconAsset.IcTruck}
                style={{width: 25, height: 25, resizeMode: 'contain'}}
              />
              <TextBold
                text="Pengiriman"
                color={Colors.blackBase}
                style={{marginLeft: 10}}
              />
            </View>
            <Image
              source={IconAsset.ChevronRight}
              style={{width: 20, height: 20, tintColor: Colors.blackBase}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: '#fff',
            paddingLeft: 8,
            alignItems: 'center',
            flexWrap: 'wrap',
            marginBottom: 15,
          }}>
          {pickedCourier.map((value, index) => {
            return (
              <TextBold
                text={value}
                style={{
                  padding: 10,
                  borderRadius: 4,
                  backgroundColor: Colors.secondary,
                  marginLeft: 10,
                  marginVertical: 10,
                }}
                color="#fff"
              />
            );
          })}
        </View>
        <View style={{padding: 18, backgroundColor: '#fff'}}>
          {/* <TextBold
                    text="Ambil Titik Lokasi"
                    color={Colors.blackBase}
                />
                <View style={{
                    marginTop: 8,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginBottom: 24
                }}>
                    <View style={styles.viewKoor}>
                        <TextRegular
                            text={latlngWarehouse}
                            color={Colors.blackBase}
                        />
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.btnPickKoor}
                        onPress={setLocationWh}
                    >
                        <TextBold
                            text="Pilih"
                            color={Colors.primary}
                        />
                    </TouchableOpacity>
                </View> */}
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.btnSave}
            onPress={() => updateData()}>
            <TextBold text="Simpan" color="#fff" />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  btnPengiriman: {
    width: '100%',
    // paddingHorizontal: 15,
    marginTop: 25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnSave: {
    width: '100%',
    marginTop: 14,
    borderRadius: 10,
    paddingVertical: 17,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnPickKoor: {
    width: '26%',
    borderRadius: 10,
    borderColor: Colors.primary,
    borderWidth: 2,
    paddingVertical: 11,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewKoor: {
    width: '70%',
    borderRadius: 10,
    borderColor: Colors.primary,
    borderWidth: 1,
    paddingVertical: 12,
    paddingHorizontal: 18,
    backgroundColor: Colors.secondaryLighttest,
  },
  txtInput: {
    width: '100%',
    borderRadius: 10,
    marginTop: 14,
    borderColor: Colors.borderGrey,
    borderWidth: 1,
    padding: 10,
    color: Colors.blackBase,
  },
  btnChangeImg: {
    borderRadius: 8,
    borderColor: Colors.primary,
    borderWidth: 2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8,
    paddingHorizontal: 24,
    backgroundColor: '#fff',
  },
  viewPlaceholder: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 200,
  },
});

export default ProfileShopComponent;
