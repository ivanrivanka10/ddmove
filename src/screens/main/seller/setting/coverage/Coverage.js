import React, {useState, useEffect} from 'react'
import {
    View,
    Text,
    ScrollView
} from 'react-native'
import {HeaderTitle} from '@components/global'
import {useSelector, useDispatch} from 'react-redux'
import APISELLER from '@utils/services/seller/settingProvider'
import {showToastMessage} from '@helpers/functionFormat'

import CoverageComponent from './components/CoverageComponent'

const Coverage = ({
    navigation,
    route
}) => {
    const { userData } = useSelector(state => state.auth);
    const dispatch = useDispatch();

    const [material, setMaterial] = useState([])
    const [nonMaterial, setNonMaterial] = useState([])
    const [dataShop, setDataShop] = useState({})
    console.log("userdata",userData)
    const getData = async () => {
        dispatch({type: "SHOW_LOADING"}); 
        const [res, shop] = await Promise.all([
            APISELLER.GetDataExpedition(userData.token),
            APISELLER.GetDataShop(userData.token)
        ])
        console.log('=== res coverage', res);
        if(res.status == 200) {
            const data = res.data.data
            // const datane = Object.keys(data)

            for (const [key, value] of Object.entries(data)) {
                console.log(`${key}`);
                if(key === 'non-material') {
                    console.log(`${data[key]}`);
                    setNonMaterial(data[key])
                }                
            }            
            setMaterial(res.data.data.material)
            // setNonMaterial(`${res.data.data.non-material}`)
        }
        console.log('cek shop', shop);
        if(shop.status === 200) {
            setDataShop(shop.data.data)
        }
        dispatch({type: "HIDE_LOADING"});
    }

    const updateStatus = async (data, section) => {
        console.log('data update status', data);
        const formData = new FormData();
        formData.append('shop_id', dataShop.id)
        formData.append('expedition_id', data)
        dispatch({type: "SHOW_LOADING"});
        const res = await APISELLER.updateStatusExpedition(formData, userData.token)
        dispatch({type: "HIDE_LOADING"});
        console.log('res update expedition');
        if(res.status === 200) {            
            getData()
        } else {
            return showToastMessage('Update status gagal, silahkan coba lagi')
        }
    }

    useEffect(() => {
        getData()
    },[])
    console.log('cek shop data', dataShop);
    return(
        <View style={{flex: 1, backgroundColor: '#F7F9F8'}}>
            <HeaderTitle
                 title="Jangkauan Pengiriman Saya"
            />
            <CoverageComponent
                navigation={navigation}
                material={material}
                dataShop={dataShop}
                nonMaterial={nonMaterial}
                updateStatus={(data, section) => { 
                    if(section === 'material') {
                        var arr = [...material]
                        var index = arr.findIndex((value, index) => { return value.id === data })
                        arr[index].status = !arr[index].status
                        setMaterial(arr)
                    } else {
                        var arr = [...nonMaterial]
                        var index = arr.findIndex((value, index) => { return value.id === data })
                        arr[index].status = !arr[index].status
                        setNonMaterial(arr)
                    }
                    updateStatus(data, section)
                }}
            />
        </View>
    )
}

export default Coverage