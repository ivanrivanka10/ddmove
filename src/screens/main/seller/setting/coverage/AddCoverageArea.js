import React, {useState, useEffect} from 'react';
import {
  View,
  Modal,
  TouchableOpacity,
  Image,
  StyleSheet,
  FlatList,
} from 'react-native';
import {HeaderTitle, SelectDropDown, TextBold} from '@components/global';
import AddCoverageAreaComponent from './components/AddCoverageAreaComponent';
import {useSelector, useDispatch} from 'react-redux';
import API from '@utils/services/settingProvider';
import APISELLER from '@utils/services/seller/settingProvider';
import Colors from '@styles/Colors';
import {IconAsset} from '@utils/index';
import APISELLERX from '@utils/services/seller/settingProvider';
import {useFocusEffect} from '@react-navigation/native';
import {debugConsoleLog} from '@helpers/functionFormat';

const AddCoverageArea = ({navigation, route}) => {
  const {userData} = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const [showModalAdd, setShowModalAdd] = useState(false);
  const [typeAddArea, setTypeAddArea] = useState('in_city');
  const [listProvince, setListProvince] = useState([]);
  const [listCity, setListCity] = useState([]);
  const [listDistrict, setListDistrict] = useState([]);
  const [listSubDistrict, setListSubDistrict] = useState([]);
  const [pickedProvince, setPickedProvince] = useState(null);
  const [pickedNameProvince, setPickedNameProvince] = useState(null);
  const [pickedNameCity, setPickedNameCity] = useState(null);
  const [pickedNameDistrict, setPickedNameDistrict] = useState(null);
  const [pickedCity, setPickedCity] = useState([]);
  const [pickedDistrict, setPickedDistrict] = useState([]);
  const [dataInCity, setInCity] = useState([]);
  const [dataArroundCity, setArroundCity] = useState([]);
  const [dataOtherCity, setOtherCity] = useState([]);
  const [detailShop, setDetailShop] = useState(null);
  const [dataSekitar, setSekitar] = useState([]);

  const getDetailShop = async () => {
    const res = await APISELLERX.GetDataShop(userData.token);
    if (res.status == 200) {
      setDetailShop(res.data.data);
      setPickedNameProvince(res.data.data.province.name);
      setPickedProvince(res.data.data.province.id);
      setPickedNameCity(res.data.data.city.name);
      setListCity([
        {
          id: res.data.data.city.id,
          name: res.data.data.city.name,
          checked: true,
        },
      ]);
      setPickedNameDistrict({
        id: res.data.data.district.id,
        name: res.data.data.district.name,
      });
      getListDistrict(res.data.data.city.id, 'change');
      setTimeout(() => {
        getListSubDistrict(res.data.data.district.id, typeAddArea);
      }, 150);
    }
  };

  const getProvince = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.GetProvince();
    if (res && res.status === 200) {
      let data = [];
      for (let index = 0; index < res.data.data.length; index++) {
        data.push({
          id: res.data.data[index].province_id,
          name: res.data.data[index].province,
        });
      }
      setListProvince(data);
    }
    getData();
    // dispatch({type: "HIDE_LOADING"});
  };

  const getListCity = async (id, type) => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.GetCity(id);
    if (res && res.status == 200) {
      let data = [];
      for (let index = 0; index < res.data.data.length; index++) {
        data.push({
          id: res.data.data[index].city_id,
          name: res.data.data[index].city_name,
          checked:
            type != undefined && type == res.data.data[index].city_id
              ? true
              : false,
        });
      }
      setListCity(data);
    }
    dispatch({type: 'HIDE_LOADING'});
  };

  const getListDistrict = async (id, type) => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.GetDistrict(id);
    if (res && res.status == 200) {
      let data = [];
      for (let index = 0; index < res.data.data.length; index++) {
        data.push({
          id: res.data.data[index].district_id,
          name: res.data.data[index].district_name,
        });
      }
      setListDistrict(data);
    }
    dispatch({type: 'HIDE_LOADING'});
  };

  const getListSubDistrict = async (id, type) => {
    if (typeAddArea == 'in-city') {
      dispatch({type: 'SHOW_LOADING'});
      const res = await APISELLER.GetSubDistrict(id);
      if (res && res.status == 200) {
        if (dataInCity.length > 0) {
          const firstArray = res?.data?.data;
const secondArray = dataInCity.map(e => ({
  subdistrict_id: parseInt(e.sub_district.id),
  subdistrict_name: e.sub_district.name,
}));
const idsToDelete = new Set(secondArray.map(item => item.subdistrict_id));

const filteredArray = firstArray.filter(
  item => !idsToDelete.has(item.subdistrict_id),
);

setListSubDistrict(filteredArray ?? []);

debugConsoleLog('mmk', filteredArray);
debugConsoleLog('mmk2', secondArray);

        } else {
          setListSubDistrict(res.data.data ?? []);
        }
      } else {
        setListSubDistrict([]);
      }
      dispatch({type: 'HIDE_LOADING'});
    } else {
      dispatch({type: 'SHOW_LOADING'});
      const res = await APISELLER.GetSubDistrict(id);
      if (res && res.status == 200) {
        debugConsoleLog('mikoli', dataSekitar);
        debugConsoleLog('mikolixx', res.data.data);
        if (dataSekitar.length > 0) {
          const firstArray = res?.data?.data;
          const secondArray = [];
          dataSekitar.map(e => {
            secondArray.push(e.sub_district);
          });
          const idsToDelete = new Set(secondArray.map(item => item.id));
          const filteredArray = firstArray.filter(
            item => !idsToDelete.has(item.subdistrict_id),
          );
          debugConsoleLog('mikolisss', filteredArray);

          setListSubDistrict(filteredArray ?? []);
        } else {
          setListSubDistrict(res.data.data ?? []);
        }
      } else {
        setListSubDistrict([]);
      }
      dispatch({type: 'HIDE_LOADING'});
    }
  };

  const addCoverageArea = async () => {
    if (listSubDistrict.length == 0) {
      alert('Data Tidak Lengkap!');
    } else {
      if (typeAddArea == 'in-city') {
        setShowModalAdd(false);
        dispatch({type: 'SHOW_LOADING'});
        let city = null;
        listCity.map(e => {
          if (e.checked === true) {
            city = e.id;
          }
        });
        let request = [];
        listSubDistrict.map(async e => {
          if (e.checked === true) {
            const data = new FormData();
            data.append('province_id', pickedProvince);
            data.append('city_id[]', city);
            data.append('district_id[]', pickedNameDistrict?.id ?? 0);
            data.append('subdistrict_id[]', e.subdistrict_id);
            debugConsoleLog('datakuki',data)
            request.push(APISELLER.AddCoverageArea(data, userData.token));
          }
        });
        
        await Promise.all(request).then(results => {
          dispatch({type: 'HIDE_LOADING'});
          getData();
        });
      } else {
        setShowModalAdd(false);
        dispatch({type: 'SHOW_LOADING'});
        let city = null;
        listCity.map(e => {
          if (e.checked === true) {
            city = e.id;
          }
        });
        let request = [];
        listSubDistrict.map(async e => {
          if (e.checked === true) {
            const data = new FormData();
            data.append('province_id', pickedProvince);
            data.append('city_id[]', city);
            data.append('district_id[]', pickedNameDistrict?.id ?? 0);
            data.append('subdistrict_id[]', e.subdistrict_id);
            debugConsoleLog('yang di push', data);
            request.push(
              APISELLER.AddCoverageAreaSekitar(data, userData.token),
            );
          }
        });

        await Promise.all(request).then(results => {
          dispatch({type: 'HIDE_LOADING'});
          getData();
        });
      }
    }
  };

  const getData = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await APISELLER.GetDataCoverage(userData.token);
    const res1 = await APISELLER.GetDataCoverageSekitar(userData.token);
    if (res.status == 200 && res.message !== 'Not Found') {
      setInCity(res.data.data);
    } else {
      setInCity([]);
    }

    if (res1.status == 200 && res1.message !== 'Not Found') {
      setSekitar(res1.data.data);
    } else {
      setSekitar([]);
    }
    getDetailShop();
    dispatch({type: 'HIDE_LOADING'});
  };

  const deleteRoute = async id => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await APISELLER.deleteRoute(id, userData.token);
    if (res.status === 200) {
      getData();
    } else {
      dispatch({type: 'HIDE_LOADING'});
    }
    console.log(res.data);
  };

  const deleteRouteSekitar = async id => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await APISELLER.deleteRouteSekitar(id, userData.token);
    if (res.status === 200) {
      getData();
    } else {
      dispatch({type: 'HIDE_LOADING'});
    }
    console.log(res.data, 'dattta');
  };

  useEffect(() => {
    // dispatch({type: "HIDE_LOADING"});
    getProvince();
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#F7F9F8'}}>
      {ModalAddCoverage()}
      <HeaderTitle title="Jangkauan Pengiriman" />
      <AddCoverageAreaComponent
        navigation={navigation}
        inCity={() => {
          setTypeAddArea('in-city');
          getDetailShop();
          setPickedNameDistrict(null);
          setListSubDistrict([]);
          setTimeout(() => {
            setShowModalAdd(true);
          }, 500);
        }}
        dataProvince={listProvince}
        aroundCity={() => {
          setTypeAddArea('around-city');
          setPickedNameDistrict(null);
          setPickedNameCity(null);
          setPickedProvince(null);
          setPickedNameProvince(null);
          setPickedCity(null);
          setPickedDistrict(null);
          setPickedProvince(null);
          setListSubDistrict([]);
          setTimeout(() => {
            setShowModalAdd(true);
          }, 500);
        }}
        otherCity={() => {
          setTypeAddArea('other-city');
          setPickedProvince(null);
          setPickedNameProvince(null);
          setPickedCity(null);
          setPickedDistrict(null);
          setPickedProvince(null);
          setTimeout(() => {
            setShowModalAdd(true);
          }, 500);
        }}
        dataInCity={dataInCity}
        dataAroundCity={dataArroundCity}
        dataOtherCity={dataOtherCity}
        dataShop={route.params.dataShop}
        dataSekitar={dataSekitar}
        onDeleteRoute={id => deleteRoute(id)}
        onDeleteRouteSekitar={id => deleteRouteSekitar(id)}
      />
    </View>
  );

  function renderItemCity(item, index) {
    return (
      <TouchableOpacity
        onPress={() => {
          let data = [...listCity];
          data.map((e, i) => {
            let x = data[i];
            x.checked = false;
          });
          let items = data[index];
          items.checked = true;

          setListCity(data);
          setListDistrict([]);
          getListDistrict(item.id, 'change');
        }}
        style={{
          flex: 1,
          padding: 10,
          borderWidth: 2,
          borderColor: item.checked === true ? 'white' : Colors.primary,
          borderRadius: 30,
          backgroundColor: item.checked === true ? Colors.primary : 'white',
          margin: 10,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TextBold size={12} text={item?.name ?? '-'} numberOfLines={100} />
      </TouchableOpacity>
    );
  }

  function renderItemKecamatan(item, index) {
    return (
      <TouchableOpacity
        onPress={() => {
          let data = [...listSubDistrict];
          let items = data[index];
          if (items.checked === undefined) {
            items.checked = true;
          } else {
            if (items.checked) {
              items.checked = false;
            } else {
              items.checked = true;
            }
          }
          setListSubDistrict(data);
        }}
        style={{
          flex: 1,
          padding: 10,
          borderWidth: 2,
          borderColor: item.checked === true ? 'white' : Colors.primary,
          borderRadius: 30,
          backgroundColor: item.checked === true ? Colors.primary : 'white',
          margin: 10,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TextBold
          size={12}
          text={item?.subdistrict_name ?? '-'}
          numberOfLines={100}
        />
      </TouchableOpacity>
    );
  }

  function ModalAddCoverage() {
    return (
      <Modal
        transparent
        visible={showModalAdd}
        onRequestClose={() => {
          setShowModalAdd(false);
          setPickedCity(null);
          setPickedDistrict(null);
          setListDistrict([]);
        }}>
        <View style={styles.containerModal}>
          <View
            style={{
              height: '80%',
              backgroundColor: 'white',
              borderTopStartRadius: 12,
              borderTopEndRadius: 12,
            }}>
            <FlatList
              style={{flex: 1, padding: 10}}
              ListHeaderComponent={<View style={{height: 30}} />}
              ListFooterComponent={<View style={{height: 100}} />}
              ListEmptyComponent={
                <>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <TextBold
                      text="Masukkan daerah jangkauan"
                      color={Colors.blackBase}
                      size={16}
                    />
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => {
                        setShowModalAdd(false);
                        setPickedCity(null);
                        setPickedDistrict(null);
                        setPickedProvince(null);
                      }}>
                      <Image
                        source={IconAsset.IcClose}
                        style={{width: 25, height: 25}}
                      />
                    </TouchableOpacity>
                  </View>

                  <SelectDropDown
                    searchable
                    value={pickedNameProvince}
                    onSelectItem={val => {
                      setListCity([]);
                      setListDistrict([]);
                      setPickedCity(null);
                      setPickedDistrict(null);
                      setPickedNameProvince(val.name);
                      setPickedProvince(val.id);
                      getListCity(val.id);
                    }}
                    disabled={typeAddArea == 'in-city' ? true : false}
                    valueColor="#000"
                    colorArrowIcon="#000"
                    style={{marginTop: 32, borderRadius: 6}}
                    placeholderText="Pilih provinsi"
                    items={listProvince}
                    maxHeight={300}
                    typeValueText="Medium"
                  />

                  <TextBold
                    text="Pilih Kabupaten"
                    color={Colors.blackBase}
                    size={14}
                    style={{marginVertical: 16}}
                  />

                  <SelectDropDown
                    searchable
                    value={pickedNameCity}
                    onSelectItem={val => {
                      let data = [...listCity];
                      let index = 0;
                      data.map((e, i) => {
                        if (e.id == val.id) {
                          let x = data[i];
                          x.checked = false;
                          index = i;
                        }
                      });
                      let items = data[index];
                      items.checked = true;

                      setPickedNameCity(val?.name ?? '-');
                      setListCity(data);
                      getListDistrict(val.id, 'change');
                    }}
                    valueColor="#000"
                    colorArrowIcon="#000"
                    style={{borderRadius: 6}}
                    disabled={typeAddArea == 'in-city' ? true : false}
                    placeholderText="Pilih kabupaten"
                    items={listCity}
                    maxHeight={300}
                    typeValueText="Medium"
                  />

                  <TextBold
                    text="Pilih Kecamatan"
                    color={Colors.blackBase}
                    size={14}
                    style={{marginVertical: 16}}
                  />

                  <SelectDropDown
                    searchable
                    value={pickedNameDistrict?.name ?? null}
                    onSelectItem={val => {
                      setPickedNameDistrict(val);
                      getListSubDistrict(val.id, 'change');
                    }}
                    valueColor="#000"
                    colorArrowIcon="#000"
                    style={{borderRadius: 6}}
                    placeholderText="Pilih Kecamatan"
                    items={listDistrict}
                    maxHeight={300}
                    typeValueText="Medium"
                  />

                  {/* <FlatList
                    style={{width: '100%'}}
                    data={listCity}
                    renderItem={({item, index}) => renderItemCity(item, index)}
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                  /> */}

                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <TextBold
                      text="Pilih Kelurahan (Minimal 1)"
                      color={Colors.blackBase}
                      size={14}
                      style={{marginVertical: 16}}
                    />
                    <TouchableOpacity
                      onPress={() => {
                        let data = [...listSubDistrict];
                        data.map(e => {
                          e.checked = true;
                        });
                        setListSubDistrict(data);
                      }}>
                      <TextBold
                        text="Tambah Semua"
                        color={Colors.primary}
                        size={12}
                        style={{marginVertical: 16}}
                      />
                    </TouchableOpacity>
                  </View>

                  <FlatList
                    style={{width: '100%'}}
                    data={listSubDistrict}
                    renderItem={({item, index}) =>
                      renderItemKecamatan(item, index)
                    }
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                  />

                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.submit}
                    onPress={addCoverageArea}>
                    <TextBold text="Tambahkan" color="#fff" />
                  </TouchableOpacity>
                </>
              }
            />
          </View>
        </View>
      </Modal>
    );
  }
};

const styles = StyleSheet.create({
  submit: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 17,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
  },
  containerModal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
  },
});

export default AddCoverageArea;
