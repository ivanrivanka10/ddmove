import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {TextBold, TextRegular} from '@components/global';
import {Colors} from '@styles/index';
import {IconAsset} from '@utils/index';

const AddCoverageAreaComponent = ({
  navigation,
  inCity,
  aroundCity,
  otherCity,
  dataInCity,
  dataAroundCity,
  dataOtherCity,
  dataShop,
  onDeleteRoute,
  onDeleteRouteSekitar,
  dataProvince,
  dataSekitar,
}) => {
  return (
    <View style={{flex: 1}}>
      <View
        style={{
          flex: 1,
          borderRadius: 10,
          margin: 10,
          padding: 18,
          backgroundColor: '#fff',
        }}>
        <TextBold
          text="Lokasi 1 Kota dengan Toko "
          color={Colors.blackBase}
          size={16}
          style={{marginTop: 24}}
        />
        <ScrollView>
          {dataInCity.map(value => (
            <View key={value.id} style={styles.item}>
              <TextBold
                text={
                  value?.district?.name + ' | ' + value?.sub_district?.name ??
                  '-'
                }
                size={10}
                color={Colors.blackBase}
              />
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => onDeleteRoute(value.id)}>
                <Image
                  source={IconAsset.IcTrash}
                  style={{width: 20, height: 20}}
                />
              </TouchableOpacity>
            </View>
          ))}
        </ScrollView>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.btnAdd}
          onPress={() => inCity()}>
          <Image
            source={IconAsset.IcPlus}
            style={{
              width: 20,
              height: 20,
              tintColor: Colors.primary,
              marginRight: 15,
            }}
          />
          <TextBold text="Tambah" size={16} color={Colors.primary} />
        </TouchableOpacity>
      </View>
      <View
        style={{
          flex: 1,
          borderRadius: 10,
          margin: 10,
          padding: 8,
          backgroundColor: '#fff',
        }}>
        {dataSekitar.length !== 0 && (
          <TextBold
            text="Kota / Kabupaten Sekitar"
            color={Colors.blackBase}
            size={16}
            style={{marginTop: 24}}
          />
        )}
        <ScrollView>
          {dataSekitar.map(value => (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View key={value.id} style={styles.item}>
                <TextBold
                  text={
                      value?.city?.name +
                      ' | ' +
                      value?.district?.name +
                      ' | ' +
                      value?.sub_district?.name ?? '-'
                  }
                  size={10}
                  color={Colors.blackBase}
                />
              </View>
              <TouchableOpacity
                style={{ marginLeft : -40 }}
                activeOpacity={0.8}
                onPress={() => onDeleteRouteSekitar(value.id)}>
                <Image
                  source={IconAsset.IcTrash}
                  style={{width: 20, height: 20}}
                />
              </TouchableOpacity>
            </View>
          ))}
        </ScrollView>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.btnAdd}
          onPress={() => aroundCity()}>
          <Image
            source={IconAsset.IcPlus}
            style={{
              width: 20,
              height: 20,
              tintColor: Colors.primary,
              marginRight: 15,
            }}
          />
          <TextBold
            text={
              dataSekitar.length !== 0
                ? 'Tambah'
                : 'Tambah Kota / Kabupaten Sekitar'
            }
            size={16}
            color={Colors.primary}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 26,
    width: '100%',
    borderRadius: 16,
    paddingVertical: 17,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: Colors.primary,
    borderWidth: 1,
  },
  item: {
    marginTop: 8,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 8,
    paddingHorizontal: 18,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
  },
  card: {
    width: '100%',
    marginTop: 10,
    borderColor: Colors.border,
    borderWidth: 1,
    borderRadius: 16,
    padding: 18,
  },
});

export default AddCoverageAreaComponent;
