import React from 'react'
import {
    View,
    Image,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    Switch,    
} from 'react-native'
import {
    TextBold,
    TextRegular
} from '@components/global'
import {Colors} from '@styles/index'
import {IconAsset} from '@utils/index'


const CoverageComponent = ({
    navigation,
    material,
    nonMaterial,
    updateStatus,
    dataShop
}) => {
    return(
        <ScrollView
            showsVerticalScrollIndicator={false}
        >
            <View style={{
                width: '100%',
                padding: 24,
                backgroundColor: '#fff'
            }}>
                <TouchableOpacity 
                    activeOpacity={0.8}
                    onPress={() => navigation.navigate('AddCoverageArea', {
                        dataShop
                        // area: {
                        //     inCity,
                        //     aroundCity,
                        //     otherCity
                        // }
                    })}
                    style={styles.card}>
                    <View style={{width: '15%', justifyContent: 'center', alignItems: 'center'}}>
                        <Image
                            source={IconAsset.IcSitemap}
                            style={{width: 20, height: 20, resizeMode: 'contain'}}
                        />
                    </View>
                    <View style={{
                        width: '70%',
                        paddingHorizontal: 10
                    }}>
                        <TextBold
                            text="Jangkauan Pengiriman"
                            color={Colors.blackBase}
                        />
                        <TextRegular
                            text="Pengaturan lokasi bebas ongkir"
                            color={Colors.grey_1}
                            style={{marginTop: 4}}
                        />
                    </View>
                    <View style={{width: '15%', justifyContent: 'center', alignItems: 'center'}}>
                        <Image
                            source={IconAsset.ChevronRight}
                            style={{width: 20, height: 20, resizeMode: 'contain'}}
                        />
                    </View>
                </TouchableOpacity>
            </View>
            <View 
                key="material"
                style={{
                width: '100%',
                marginTop: 15,
                padding: 18,
                backgroundColor: '#fff'
            }}>
                <TextBold
                    text="Ekspedisi Bahan Bangunan"
                    color={Colors.blackBase}
                />
                {
                    material.map((value, index) => {
                        return (
                            <View 
                                key={value.id}
                                style={{
                                    marginTop: 18,
                                    width: '100%',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between'
                                }}>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center'
                                }}>
                                    {/* <View style={{width: 50, height: 50, borderRadius: 12, backgroundColor: Colors.greyDummy}} /> */}
                                    <TextBold
                                        text={value.name}
                                        color={Colors.blackBase}
                                        style={{marginLeft: 14}}
                                    />
                                </View>
                                <Switch
                                    value={value.status}                        
                                    trackColor={Colors.primary}
                                    thumbColor={value.status ? Colors.primary : Colors.grey_1}
                                    onChange={() => updateStatus(value.id, 'material')}
                                />
                            </View>
                        )
                    })
                }
            </View>
            <View 
                key="non-material"
                style={{
                    width: '100%',
                    marginTop: 15,
                    padding: 18,
                    backgroundColor: '#fff'
                }}>
                <TextBold
                    text="Komersial Ekspedisi (Non Bahan Bangunan)"
                    color={Colors.blackBase}
                />
                {
                    nonMaterial.map((value) => {
                        return (
                            <View 
                                key={value.id}
                                style={{
                                    marginTop: 18,
                                    width: '100%',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between'
                                }}>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center'
                                }}>
                                    {/* <View style={{width: 50, height: 50, borderRadius: 12, backgroundColor: Colors.greyDummy}} /> */}
                                    <TextBold
                                        text={value.name}
                                        color={Colors.blackBase}
                                        style={{marginLeft: 14}}
                                    />
                                </View>
                                <Switch
                                    value={value.status}                        
                                    trackColor={Colors.primary}
                                    thumbColor={Colors.primary}
                                    onChange={() => updateStatus(value.id, 'non-material')}
                                />
                            </View>
                        )
                    })
                }                
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    card: {
        width: '100%',
        borderRadius: 16,
        borderColor: Colors.border,
        borderWidth: 1,
        padding: 18,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
})

export default CoverageComponent