import React, {useState, useEffect} from 'react'
import {
    View,
    Image,
    TouchableOpacity,
    StyleSheet,
    Platform
} from 'react-native'
import { Size, Fonts, Icons, Colors } from '@styles/index';
import {IconAsset} from '@utils/index'
import {HeaderTitle} from '@components/global'
import {useIsFocused} from '@react-navigation/native'
import PaymentAccountComponent from './component/PaymentAccountComponent';
import { TextBold } from '@components/global';
import {useSelector, useDispatch} from 'react-redux'
import API from '@utils/services/seller/settingProvider'

const PaymentAccount = ({
    navigation,
    route
}) => {
    const isFocused = useIsFocused()
    const { userData } = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const [dataList, setDataList] = useState([])

    const getData = async () => {
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetListPaymentAccount(userData.token);
        console.log('list payment acc', res.data);
        if(res.status === 200) {
            setDataList(res.data.data.bank)
        }
        dispatch({type: "HIDE_LOADING"});
    }

    useEffect(() => {
        getData()
    },[isFocused])

    return(
        <View style={{flex: 1, backgroundColor: '#fff'}}>
            {
                Platform.OS === 'ios' ?
                    <HeaderTitle
                        title="Rekening Saya"
                    />
                : null
            }
            <View style={{flex: 1, padding: 18}}>
                <PaymentAccountComponent
                    data={dataList}
                    ongoing={() => getData()}
                    setDataList={setDataList}
                    navigation={navigation}
                />
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.btnAdd}
                    onPress={() => navigation.navigate('AddPaymentAccount')}
                >
                    <Image
                        source={IconAsset.IcPlus}
                        style={{width: 20, height: 20, resizeMode: 'contain'}}
                    />
                    <TextBold
                        text="Rekening Baru"
                        color="#fff"
                        style={{marginLeft: 10}}
                    />
                </TouchableOpacity>
            </View>            
        </View>
    )
}

const styles = StyleSheet.create({
    btnAdd: {
        position: 'absolute',
        bottom: 18,
        width: '100%',
        alignSelf: 'center',
        borderRadius: 16,
        paddingVertical: 17,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.primary
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: "Rekening Saya",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default PaymentAccount;