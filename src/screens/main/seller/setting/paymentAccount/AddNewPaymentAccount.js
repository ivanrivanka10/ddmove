import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Platform,
  ScrollView,
  KeyboardAvoidingView,
  TextInput,
} from 'react-native';
import {Size, Fonts, Icons, Colors} from '@styles/index';
import {IconAsset} from '@utils/index';
import API from '@utils/services/seller/settingProvider';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

// import PaymentAccountComponent from './component/PaymentAccountComponent';
import {TextBold, SelectDropDown, HeaderTitle} from '@components/global';

import ModalSuccess from '@components/modal/ModalSuccess';
import {SelectDropDownBank} from '@components/global/DropDownBank';
import normalize from 'react-native-normalize';
import {debugConsoleLog} from '@helpers/functionFormat';

const AddNewPaymentAccount = ({
  // navigation,
  route,
}) => {
  const {userData} = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  debugConsoleLog('dataa', route.params);
  const dat = {
    channel_code: route?.params?.channel_code ?? '',
    channel_name: route?.params?.bank_name ?? 'Pilih Bank',
  };
  const [pickedBank, setPickedBank] = React.useState(dat);
  const [listBank, setListBank] = React.useState([]);
  const [name, setName] = React.useState(route?.params?.name);
  const [noRek, setNoRek] = React.useState(route?.params?.account_number);
  const [modalSuccess, setModalSuccess] = React.useState(false);
  const [successMessage, setSuccessMessage] = React.useState(
    'Rekening Baru Berhasil Ditambahkan',
  );

  const getData = async () => {
    dispatch({type: 'SHOW_LOADING'});
    console.log('ini jalan bank');
    const res = await API.GetListBank(userData.token);
    console.log('res bank', res);
    if (res.status === 200) {
      setListBank(res.data.data);
    }
    dispatch({type: 'HIDE_LOADING'});
  };

  const postData = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const data = new FormData();
    data.append('name', name);
    data.append('account_number', Number(noRek));
    data.append('bank_name', pickedBank?.channel_name ?? '-');
    data.append('channel_code', pickedBank?.channel_code ?? '-');
    const res = await API.AddBankAccount(data, userData.token);
    console.log('res add bank', res);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      console.log('sukses sini');
      setModalSuccess(true);
      setTimeout(() => {
        setName('');
        setNoRek('');
        setPickedBank(null);
        setModalSuccess(false);
        navigation.goBack();
      }, 1500);
    }
  };
  const postUpdte = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const data = new FormData();
    data.append('name', name);
    data.append('account_number', Number(noRek));
    data.append('bank_name', pickedBank?.channel_name ?? '-');
    data.append('channel_code', pickedBank?.channel_code ?? '-');
    const res = await API.AddUpdatebank(data, route.params.id, userData.token);
    console.log('res add bank', res);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      console.log('sukses sini');
      setModalSuccess(true);
      setTimeout(() => {
        setName('');
        setNoRek('');
        setPickedBank(null);
        setModalSuccess(false);
        navigation.goBack();
      }, 1500);
    }
  };

  const validate = () => {
    var isDisable = false;
    const obj = {
      name,
      noRek,
      pickedBank,
    };
    const dataEmpty = Object.keys(obj).filter(
      data => obj[data] == '' || obj[data] == null,
    );
    if (dataEmpty.length > 0) {
      isDisable = true;
    } else {
      isDisable = false;
    }
    return isDisable;
  };

  React.useEffect(() => {
    getData();
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ModalSuccess
        show={modalSuccess}
        message={successMessage}
        onClose={() => setModalSuccess(false)}
      />
      <KeyboardAvoidingView
        behavior="padding"
        enabled
        style={{
          flex: 1,
          padding: 18,
        }}
        keyboardVerticalOffset={-500}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <TextInput
            placeholder="Nama Pemilik Sesuai Rekening"
            style={styles.txtInput}
            value={name}
            onChangeText={text => setName(text)}
            // placeholderTextColor="black"
          />
          <TextInput
            placeholder="Nomor Rekening"
            keyboardType="number-pad"
            style={styles.txtInput}
            value={noRek}
            onChangeText={text => setNoRek(text)}
          />
          <SelectDropDownBank
            searchable
            value={pickedBank?.channel_name ?? 'Pilih Bank'}
            onSelectItem={val => {
              setPickedBank(val);
            }}
            // disabled={listCity.length < 1}
            valueColor="#000"
            colorArrowIcon="#000"
            style={{marginTop: 24, borderRadius: 6}}
            placeholderText="Pilih Bank"
            items={listBank}
            maxHeight={200}
            typeValueText="Medium"
          />
        </ScrollView>
      </KeyboardAvoidingView>
      <TouchableOpacity
        activeOpacity={0.8}
        style={[
          styles.btnAdd,
          validate() && {backgroundColor: Colors.borderGrey},
        ]}
        disabled={validate()}
        onPress={() => {
          if (route?.params === undefined) {
            postData();
          } else {
            postUpdte();
          }
        }}>
        <TextBold
          text={route?.params === undefined ? 'Simpan' : 'Update'}
          color="#fff"
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
    btnAdd: {
        position: 'absolute',
        bottom: 18,
        width: '90%',
        alignSelf: 'center',
        borderRadius: 16,
        paddingVertical: 17,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.primary
    },
    txtInput: {
        width: '100%',
        borderRadius: 10,
        marginTop: 24,
        color:Colors.blackBase,
        borderColor: Colors.borderGrey,
        borderWidth: 1,
        padding: 10
    },
})

export const screenOptions = navData => {
  return {
    headerTitle: 'Tambah Rekening',
    headerTitleAlign: 'left',
    headerTitleStyle: {
      fontFamily: Fonts.mulishExtraBold,
      fontSize: Size.scaleFont(16),
      color: '#333333',
    },
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft: normalize(5, 'width')}}
        onPress={() => navData.navigation.goBack()}>
        <View style={{padding: Size.scaleSize(10)}}>
          <Icons.ChevronLeft color="#333" size={Size.scaleSize(18)} />
        </View>
      </TouchableOpacity>
    ),
  };
};

export default AddNewPaymentAccount;
