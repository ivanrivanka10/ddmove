import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  FlatList,
  Text,
  StyleSheet,
} from 'react-native';
import {Button, TextBold, TextRegular} from '@components/global';
import {IconAsset, ImageAsset} from '@utils/index';
import Colors from '@styles/Colors';
import {Fonts, Icons, Size} from '@styles/index';
import normalize from 'react-native-normalize';
import {debugConsoleLog} from '@helpers/functionFormat';
import API from '@utils/services/seller/settingProvider';
import {useDispatch, useSelector} from 'react-redux';

const PaymentAccountComponent = ({navigation, data, setDataList, ongoing}) => {
  const {userData} = useSelector(state => state.auth);
  const dispatch = useDispatch();

  return (
    <View>
      <TextBold text="Daftar Rekening" color={Colors.blackBase} size={16} />
      <FlatList
        data={data}
        keyExtractor={(item, index) => index.toString()}
        contentContainerStyle={{paddingBottom: 100}}
        showsVerticalScrollIndicator={false}
        renderItem={({item, index}) => {
          return (
            <TouchableOpacity
              key={index.toString()}
              activeOpacity={0.8}
              style={{
                marginTop: 18,
                width: '100%',
                borderRadius: 16,
                borderColor: Colors.borderGrey,
                borderWidth: 1,
                flexDirection: 'row',
                alignItems: 'center',
                paddingHorizontal: 18,
                paddingVertical: 24,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',

                  justifyContent: 'space-between',
                  alignItems: 'flex-start',
                }}>
                {/* <Image
                                    source={ImageAsset.LogoBca}
                                    style={{width: 50, height: 50, resizeMode: 'contain'}}
                                /> */}
                <View
                  style={{marginLeft: 0, marginRight: normalize(5, 'width')}}>
                  <TextBold style={{width:normalize(180,'width')}} text={item.bank_name} color={Colors.blackBase} />
                  <TextRegular
                    text={item.account_number}
                    color={Colors.blackBase}
                    style={{marginTop: 4}}
                  />
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View>
                    <Button
                      onPress={() =>
                        navigation.navigate('AddPaymentAccount', item)
                      }
                      containerStyle={{
                        marginBottom: Size.scaleSize(2),
                        backgroundColor: Colors.secondaryBase,
                        marginHorizontal: normalize(5, 'width'),
                      }}
                      textComponent={
                        <View
                          style={{
                            flexDirection: 'row',
                            paddingHorizontal: 15,
                            paddingVertical: 10,
                          }}>
                          <Icons.Editproduk size={15} color="#FFF" />
                          <Text
                            style={{
                              fontFamily: Fonts.mulishBold,
                              color: '#FFF',
                              fontSize: normalize(10, 'height'),
                              margin: 2,
                            }}>
                            Edit
                          </Text>
                        </View>
                      }
                    />
                  </View>
                  <View>
                    <Button
                      onPress={async () => {
                        dispatch({type: 'SHOW_LOADING'});
                        const data = await API.DeleteBank(
                          item.id,
                          userData.token,
                        );
                        if (data.data.message === 'Sukses') {
                          dispatch({type: 'HIDE_LOADING'});
                          ongoing();
                        }
                      }}
                      containerStyle={{marginBottom: Size.scaleSize(2)}}
                      textComponent={
                        <View
                          style={{
                            flexDirection: 'row',
                            paddingHorizontal: 10,
                            paddingVertical: 10,
                          }}>
                          <Icons.TrashSVG size={15} color="#FFF" />
                          <Text
                            style={{
                              fontFamily: Fonts.mulishBold,
                              color: '#FFF',
                              fontSize: normalize(10, 'height'),
                              marginTop: 2,
                            }}>
                            Hapus
                          </Text>
                        </View>
                      }
                    />
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default PaymentAccountComponent;
