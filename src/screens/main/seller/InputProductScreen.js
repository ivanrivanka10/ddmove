import React, {useState, useEffect} from 'react';
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  StyleSheet,
  PermissionsAndroid,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';

import {Colors, Fonts, Size, Icons, Images} from '@styles/index';
import {
  InputText,
  Button,
  SelectDropDown,
  TextSemiBold,
  TextBold,
  TextRegular,
} from '@components/global';
import {IconAsset} from '@utils/index';
import {useSelector, useDispatch} from 'react-redux';
import API from '@utils/services/buyer/homeProvider';
import APISELLER from '@utils/services/seller/orderProvider';
import APISELLERX from '@utils/services/seller/settingProvider';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ModalPickImage from '@components/modal/ModalPickImage';
import {
  debugConsoleLog,
  rupiahFormat,
  inputRupiah,
  showToastMessage,
} from '@helpers/functionFormat';

import ModalSuccess from '@components/modal/ModalSuccess';
import ModalPengiriman from './product/Pengiriman';
import {HeaderTitle} from '@components/global';
import {KeyboardAvoidingView} from 'react-native';
import {useCallback} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import ModalAllKategori from '@components/modal/ModalAllKategori';
import ModalVariants from '@components/modal/ModalVariant';
import Modalvariaantnew from '@components/modal/ModalVariantnew';
import {transformSync} from '@babel/core';
import {debug} from 'react-native-reanimated';

const InputProductScreen = ({navigation, route}) => {
  const {userData} = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const [detailShop, setDetailShop] = useState(null);
  const [type, setType] = useState('material');
  const [jenis, setJenis] = useState('Baru');
  const [listImage, setListImage] = useState([]);
  const [oriListImage, setOriListImage] = useState([]);
  const [name, setName] = useState('');
  const [category, setCategory] = useState([]);
  const [datasubvarian, setdatavarians] = useState([]);
  const [description, setDescription] = useState('');
  const [linkProduct, setLinkProduct] = useState('');
  const [price, setPrice] = useState('');
  const [stock, setStock] = useState('');
  const [merk, setMerk] = useState('');
  const [width, setWidth] = useState('');
  const [length, setLength] = useState('');
  const [height, setHeight] = useState('');
  const [weight, setWeight] = useState('');
  const [weight_volume, setWeightVolume] = useState('');
  const [pickedCategory, setPickedCategory] = useState([]);
  const [localImgPath, setLocalImgPath] = useState('');
  const [showModalOption, setShowModalOption] = useState(false);
  const [modalSuccess, setModalSuccess] = useState(false);
  const [successMessage, setSuccessMessage] = useState(
    'Produk Baru Berhasil Ditambahkan',
  );
  const [listCourier, setListCourier] = useState([]);
  const [pickedCourier, setPickedCourier] = useState([]);
  const [showmodalkaategori, setModlkkategori] = useState(false);
  const [showmodalvarians, setmodalvarians] = useState(false);
  const [showPengiriman, setShowPengiriman] = useState(false);
  const [pickedSubCategory, setPickedSubCategory] = useState([]);
  const [pickedSubSubCategory, setPickedSubSubCategory] = useState([]);
  const [listSubCategory, setListSubCategory] = useState([]);
  const [listSubSubCategory, setListSubSubCategory] = useState([]);
  const [wholePrice, setWholePrice] = useState([]);
  const [titleWholePrice, setTitleWholePrice] = useState('');
  const [startPrice, setStartPrice] = useState('');
  const [isvariant, setisvariant] = useState(false);
  const [idvariant, setidvariant] = useState('');
  const [selectkategori, seSelectKategori] = useState(',,,');
  const [minimumOrder, setMinimumOrder] = useState('');
  const [bundleImage, setBundleImage] = useState([]);
  const [rekomendasi, setRekomendasi] = useState([]);
  const [pickGratisOngkir, setPickGratisOngkir] = useState('Ya');
  const [isFreeShip, setIsFreeShip] = useState(0);
  const [disable, setDisable] = useState(false);
  const [disable2, setDisable2] = useState(false);
  const [volume, setVolume] = useState(0);

  const res = useCallback(() => {
    getrekomend();
  }, [name]);
  useFocusEffect(res);

  const getrekomend = async () => {
    const res = await APISELLERX.GetRekomendasi(userData.token, name, type);
    if (res.data.message === 'Sukses') {
      setRekomendasi(prev => res.data.data);
    } else {
      if (
        pickedCategory.length === 0 &&
        pickedSubCategory.length === 0 &&
        pickedSubSubCategory.length === 0
      ) {
        setRekomendasi([]);
      } else {
        const dataa = `${pickedCategory[0].id}, ${pickedSubCategory[0].id}, ${pickedSubSubCategory[0].id}`;
        const dataadi = `${pickedCategory[0].name}/${pickedSubCategory[0].name}/${pickedSubSubCategory[0].name}`;
        const paarams = {
          id: dataa,
          group: route.params.item.type,
          name: dataadi,
        };
        setRekomendasi([paarams]);
        seSelectKategori(dataa);
      }
    }
  };

  const dataType = [
    {
      id: 1,
      name: 'Material',
      value: 'material',
    },
    {
      id: 2,
      name: 'Non Material',
      value: 'non-material',
    },
  ];
  const dataType2 = [
    {
      id: 1,
      name: 'Baru',
      value: 'Baru',
    },
    {
      id: 2,
      name: 'Bekas',
      value: 'Bekas',
    },
  ];

  const countVolume = () => {
    var volumes = 0;
    volumes = (length * width * height) / 6000;
    setVolume(volumes.toFixed(2));
  };

  useEffect(() => {
    getDetailShop();
    validate();
  }, []);

  const getDetailShop = async () => {
    const res = await APISELLERX.GetDataShop(userData.token);
    if (res.status == 200) {
      setDetailShop(res.data.data);
      if (res?.data?.data?.type == 'both') {
        setDisable(false);
      } else {
        setDisable(true);
        setType(res?.data?.data?.type ?? 'material');
      }
      setIsFreeShip(res?.data?.data?.is_free_shipping ?? 0);
    }
  };

  const getAllDataCategory = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const paramsMain = {
      group: type,
      type: 'main',
    };
    const paramsSub = {
      group: type,
      type: 'sub',
    };
    const paramsSubSub = {
      group: type,
      type: 'sub-sub',
    };
    const getMain = API.GetAllCategory(userData.token, paramsMain);
    const getListCourier = APISELLER.GetListCourier(userData.token);

    const [main, listCourier] = await Promise.all([getMain, getListCourier]);
    var arrCat = [];
    if (main.status === 200) {
      for (let i = 0; i < main.data.data.length; i++) {
        arrCat.push({
          id: main.data.data[i].id,
          name: main.data.data[i].name,
        });
      }
    }
    debugConsoleLog('dataconsole', arrCat);
    debugConsoleLog('dataconsole', listCourier);
    setCategory(arrCat);
    setListCourier(listCourier.data.data);
    dispatch({type: 'HIDE_LOADING'});
  };

  const getSubCategory = async id => {
    const paramsSub = {
      group: type,
      type: 'sub',
      id: id,
    };
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.GetAllCategory(userData.token, paramsSub);
    var arrCat = [];
    if (res.status === 200) {
      for (let i = 0; i < res.data.data.length; i++) {
        arrCat.push({
          id: res.data.data[i].id,
          name: res.data.data[i].name,
        });
      }
      setListSubCategory(arrCat);
    }
    dispatch({type: 'HIDE_LOADING'});
  };

  const getSubSubCategory = async id => {
    const params = {
      group: type,
      type: 'sub-sub',
      id: id,
    };
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.GetAllCategory(userData.token, params);
    var arrCat = [];
    if (res.status === 200) {
      for (let i = 0; i < res.data.data.length; i++) {
        arrCat.push({
          id: res.data.data[i].id,
          name: res.data.data[i].name,
        });
      }
      setListSubSubCategory(arrCat);
    }
    dispatch({type: 'HIDE_LOADING'});
  };

  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Persetujuan akses kamera',
            message: 'Berikan Baba Market untuk mengakses kamera ?',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else {
      return true;
    }
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Persetujuan akses file',
            message: 'Berikan Baba Market untuk mengakses file ?',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else {
      return true;
    }
  };

  const captureImage = async type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      saveToPhotos: true,
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      setTimeout(() => {
        launchCamera(options, response => {
          if (response.didCancel) {
            // alert('User cancelled camera picker');
            return;
          } else if (response.errorCode == 'camera_unavailable') {
            // alert('Camera not available on device');
            return;
          } else if (response.errorCode == 'permission') {
            // alert('Permission not satisfied');
            return;
          } else if (response.errorCode == 'others') {
            // alert(response.errorMessage);
            return;
          }
          setLocalImgPath(response);
          var img = [...listImage];
          var image = [...bundleImage];
          img.push(response.assets[0].uri);
          image.push(response.assets[0]);
          debugConsoleLog('dataimage', image);
          setBundleImage(image);
          setListImage(img);
        });
      }, 500);
    }
  };

  const chooseFile = type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    setTimeout(() => {
      launchImageLibrary(options, response => {
        if (response.didCancel) {
          alert('User cancelled camera picker');
          return;
        } else if (response.errorCode == 'camera_unavailable') {
          alert('Camera not available on device');
          return;
        } else if (response.errorCode == 'permission') {
          alert('Permission not satisfied');
          return;
        } else if (response.errorCode == 'others') {
          alert(response.errorMessage);
          return;
        }
        setLocalImgPath(response);
        var img = [...listImage];
        var image = [...bundleImage];
        img.push(response.assets[0].uri);
        image.push(response.assets[0]);
        setBundleImage(image);
        setListImage(img);
      });
    }, 500);
  };

  const validate = () => {
    var isDisable = false;
    let dataArray;
    if (isvariant === true) {
      isDisable = true;
      return false;
    } else {
      if (selectkategori !== '') {
        dataArray = selectkategori.split(', ').map(Number);
      }
      let obj = {
        name: name,
        merk: merk,
        type: type,
        pickedCategory:
          pickedCategory.length === 0 ? dataArray[0] : pickedCategory,
        description: description,
        price: price,
        stock: stock,
        minimumOrder: minimumOrder,
      };
      const dataEmpty = Object.keys(obj).filter(
        data => obj[data] == '' || obj[data] == null || obj[data] == [],
      );
      if (listImage.length < 1) {
        isDisable = true;
      }
      if (type === 'material' && selectkategori) {
        obj.pickedSubCategory =
          pickedSubCategory.length === 0 ? dataArray[1] : pickedSubCategory;
        obj.pickedSubSubCategory =
          pickedSubSubCategory.length === 0 ? dataArray[2] : pickedSubCategory;
      }
      if (type === 'non-material') {
        if (weight > 5000) {
          const obj = {
            width: width,
            length: length,
            height: height,
            weight: weight,
          };
          const dataEmpty = Object.keys(obj).filter(
            data => obj[data] == '' || obj[data] == null,
          );
          if (dataEmpty.length > 0) {
            return true;
          } else {
            isDisable = false;
          }
        } else {
          if (weight == '' || weight == undefined || weight == null) {
            return true;
          }
        }
      }
      if (dataEmpty.length > 0) {
        isDisable = true;
      } else {
        isDisable = false;
      }
      return isDisable;
    }
  };
  const addProduct = async () => {
    var image = [];
    var categories = [];
    var subcategories = [];
    var subsubcategories = [];
    const data = new FormData();
    if(bundleImage.length  === 0){
      for (let i = 0; i < listImage.length; i++) {
        image.push({
          type: 'image/jpg',
          uri: listImage[i],
          name: `Product ${i + 1}`,
        });
      }
      image.forEach((img, index) => {
        data.append(`photo[]`, {
          uri: img.uri,
          type: img.type,
          name: img.name,
        });
      });
    }else{
      for (let i = 0; i < bundleImage.length; i++) {
        if (listImage[i] !== undefined) {
          image.push({
            type: 'image/jpg',
            uri: listImage[i],
            name: `Product ${i + 1}`,
          });
          const foto = {
            type: bundleImage[i].type,
            uri: bundleImage[i].uri,
            name: bundleImage[i].fileName,
          };
          data.append('photo[]', foto);
        }
      }
    }
    if (selectkategori !== ',,,') {
      debugConsoleLog('kkat', selectkategori);
      const dataArray = selectkategori.split(', ').map(Number);
      debugConsoleLog('dat', dataArray[0]);
      debugConsoleLog('dat', dataArray[1]);
      debugConsoleLog('dat', dataArray[2]);
      categories.push(dataArray[0]);
      categories.push(dataArray[1]);
      categories.push(dataArray[2]);
    } else {
      for (let i = 0; i < pickedCategory.length; i++) {
        categories.push(pickedCategory[i].id);
      }
      for (let i = 0; i < pickedSubCategory.length; i++) {
        categories.push(pickedSubCategory[i].id);
      }
      for (let i = 0; i < pickedSubSubCategory.length; i++) {
        categories.push(pickedSubSubCategory[i].id);
      }
    }
    categories.map(e => {
      debugConsoleLog('categories[]', e);
    });
    data.append('type', type);
    categories.map(e => {
      data.append('categories[]', e);
    });
    data.append('name', name);
    data.append('description', description);
    data.append('video_url', linkProduct);
    data.append('price', price.split('.').join(''));
    data.append('stock', stock);
    data.append('merk', merk);
    if (type === 'non-material') {
      data.append('width', width);
      data.append('length', length);
      data.append('height', height);
      data.append('weight', weight);
      data.append('weight_volume', weight_volume);
    }

    if (wholePrice.length > 0) {
      data.append('range_price', JSON.stringify(wholePrice));
      data.append('multi_price', 'yes');
    } else {
      data.append('multi_price', 'no');
    }
    data.append('condition', jenis === 'Baru' ? 'new' : 'old');
    data.append('minimum', Number(minimumOrder));

    if (isFreeShip === 0) {
      data.append('is_free_shipping', '0');
      if (pickGratisOngkir == 'Ya') {
        data.append('shipping_cost', '0');
      } else {
        data.append('shipping_cost', '0');
      }
    } else {
      data.append('is_free_shipping', '1');
      if (pickGratisOngkir == 'Ya') {
        data.append('shipping_cost', '0');
      } else {
        data.append('shipping_cost', '0');
      }
    }
    if (datasubvarian.length !== 0) {
      // const categoryArray = datasubvarian.map(item => ({name: item.name}));
      // const subvarianArray = datasubvarian.flatMap(item =>
      //   item.subvarian.map(subItem => ({
      //     name: subItem.namasubvarian,
      //     price: parseFloat(subItem.price.replace(/\./g, '')),
      //     stock: subItem.stok,
      //     weight: subItem.weight,
      //   })),
      // );
      data.append('have_variant', 'yes');
      // data.append('variant', JSON.stringify(categoryArray));
      // data.append('variant_detail', JSON.stringify(subvarianArray));
    } else {
      data.append('have_variant', 'no');
    }

    dispatch({type: 'SHOW_LOADING'});
    var res = {};
    if (route.params) {
      res = await APISELLER.UpdateProduct(
        data,
        route.params.item.id,
        userData.token,
      );
    } else {
      debugConsoleLog('fulldtaa', data);
      res = await APISELLER.AddProduct(data, userData.token);
    }
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      if (route.params) {
        // showToastMessage('Produk berhasil diperbaharui')
        setSuccessMessage('Produk berhasil diperbaharui');
        setTimeout(() => {
          setModalSuccess(true);
        }, 500);
      } else {
        setSuccessMessage('Produk berhasil ditambahkan');
        setTimeout(() => {
          setModalSuccess(true);
        }, 500);
      }
      setType(null);
      setListImage([]);
      setName('');
      setDescription('');
      setLinkProduct('');
      setPrice('');
      setStock('');
      setMerk('');
      setPickedCategory([]);
      setPickedSubCategory([]);
      setPickedSubSubCategory([]);
      setLocalImgPath('');
      setTitleWholePrice('');
      setWholePrice([]);
      setStartPrice('');
      setMinimumOrder('');
      setTimeout(() => {
        setModalSuccess(false);
        navigation.goBack();
      }, 1500);
    }
  };

  const addProduct2 = async () => {
    var image = [];
    var categories = [];
    var subcategories = [];
    var subsubcategories = [];
    const data = new FormData();
    if(bundleImage.length  === 0){
      for (let i = 0; i < listImage.length; i++) {
        image.push({
          type: 'image/jpg',
          uri: listImage[i],
          name: `Product ${i + 1}`,
        });
      }
      image.forEach((img, index) => {
        data.append(`photo[]`, {
          uri: img.uri,
          type: img.type,
          name: img.name,
        });
      });
    }else{
      for (let i = 0; i < bundleImage.length; i++) {
        if (listImage[i] !== undefined) {
          image.push({
            type: 'image/jpg',
            uri: listImage[i],
            name: `Product ${i + 1}`,
          });
          const foto = {
            type: bundleImage[i].type,
            uri: bundleImage[i].uri,
            name: bundleImage[i].fileName,
          };
          data.append('photo[]', foto);
        }
      }
    }
    if (selectkategori !== ',,,') {
      debugConsoleLog('kkat', selectkategori);
      const dataArray = selectkategori.split(', ').map(Number);
      debugConsoleLog('dat', dataArray[0]);
      debugConsoleLog('dat', dataArray[1]);
      debugConsoleLog('dat', dataArray[2]);
      categories.push(dataArray[0]);
      categories.push(dataArray[1]);
      categories.push(dataArray[2]);
    } else {
      for (let i = 0; i < pickedCategory.length; i++) {
        categories.push(pickedCategory[i].id);
      }
      for (let i = 0; i < pickedSubCategory.length; i++) {
        categories.push(pickedSubCategory[i].id);
      }
      for (let i = 0; i < pickedSubSubCategory.length; i++) {
        categories.push(pickedSubSubCategory[i].id);
      }
    }
    categories.map(e => {
      debugConsoleLog('categories[]', e);
    });
    data.append('type', type);
    categories.map(e => {
      data.append('categories[]', e);
    });
    data.append('name', name);
    data.append('description', description);
    data.append('video_url', linkProduct);
    data.append('merk', merk);
    if (type === 'non-material') {
      data.append('width', width);
      data.append('length', length);
      data.append('height', height);
      data.append('weight', weight);
      data.append('weight_volume', weight_volume);
    }
    data.append('condition', jenis === 'Baru' ? 'new' : 'old');
    data.append('minimum', Number(minimumOrder));

    if (isFreeShip === 0) {
      data.append('is_free_shipping', '0');
      if (pickGratisOngkir == 'Ya') {
        data.append('shipping_cost', '0');
      } else {
        data.append('shipping_cost', '0');
      }
    } else {
      data.append('is_free_shipping', '1');
      if (pickGratisOngkir == 'Ya') {
        data.append('shipping_cost', '0');
      } else {
        data.append('shipping_cost', '0');
      }
    } 
    dispatch({type: 'SHOW_LOADING'});
    var res = {};
      debugConsoleLog('fulldtaa', data);
      debugConsoleLog('fulldtaa', idvariant);
      res = await APISELLER.AddProductoke(data,idvariant,userData.token);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      if (route.params) {
        // showToastMessage('Produk berhasil diperbaharui')
        setSuccessMessage('Produk berhasil diperbaharui');
        setTimeout(() => {
          setModalSuccess(true);
        }, 500);
      } else {
        setSuccessMessage('Produk berhasil ditambahkan');
        setTimeout(() => {
          setModalSuccess(true);
        }, 500);
      }
      setType(null);
      setListImage([]);
      setName('');
      setDescription('');
      setLinkProduct('');
      setPrice('');
      setStock('');
      setMerk('');
      setPickedCategory([]);
      setPickedSubCategory([]);
      setPickedSubSubCategory([]);
      setLocalImgPath('');
      setTitleWholePrice('');
      setWholePrice([]);
      setStartPrice('');
      setMinimumOrder('');
      setTimeout(() => {
        setModalSuccess(false);
        navigation.goBack();
      }, 1500);
    }
  };





  // const addProduct2 = async () => {
  //   var image = [];
  //   var categories = [];
  //   var subcategories = [];
  //   var subsubcategories = [];
  //   const data = new FormData();
  //   if (bundleImage.length === 0) {
  //     for (let i = 0; i < listImage.length; i++) {
  //       if (listImage[i] !== undefined) {
  //         const parts = listImage[i].split('/');
  //         const fileNameWithExtension = parts[parts.length - 1];

  //         // Memisahkan nama file dari ekstensinya
  //         const [fileNameWithoutExtension, fileExtension] =
  //           fileNameWithExtension.split('.');

  //         // Menentukan tipe berdasarkan ekstensi file
  //         const fileType = `image/${fileExtension}`;

  //         // Membuat objek untuk setiap iterasi
  //         const foto = {
  //           type: fileType,
  //           uri: listImage[i],
  //           name: fileNameWithoutExtension,
  //         };
  //         data.append('photo[]', foto);
  //       }
  //     }
  //   } else {
  //     for (let i = 0; i < bundleImage.length; i++) {
  //       if (listImage[i] !== undefined) {
  //         const foto = {
  //           type: bundleImage[i].type,
  //           uri: bundleImage[i].uri,
  //           name: bundleImage[i].fileName,
  //         };
  //         data.append('photo[]', foto);
  //         debugConsoleLog('dataku', foto);
  //       }
  //     }
  //   }
  //   if (selectkategori !== ',,,') {
  //     const dataArray = selectkategori.split(', ').map(Number);
  //     categories.push(dataArray[0]);
  //     categories.push(dataArray[1]);
  //     categories.push(dataArray[2]);
  //   } else {
  //     for (let i = 0; i < pickedCategory.length; i++) {
  //       categories.push(pickedCategory[i].id);
  //     }
  //     for (let i = 0; i < pickedSubCategory.length; i++) {
  //       categories.push(pickedSubCategory[i].id);
  //     }
  //     for (let i = 0; i < pickedSubSubCategory.length; i++) {
  //       categories.push(pickedSubSubCategory[i].id);
  //     }
  //   }
  //   categories.map(e => {
  //     debugConsoleLog('categories[]', e);
  //   });
  //   data.append('type', type);
  //   categories.map(e => {
  //     data.append('categories[]', e);
  //   });
  //   data.append('name', name);
  //   data.append('description', description);
  //   data.append('video_url', linkProduct);
  //   data.append('merk', merk);
  //   if (type === 'non-material') {
  //     data.append('width', width);
  //     data.append('length', length);
  //     data.append('height', height);
  //     data.append('weight', weight);
  //     data.append('weight_volume', weight_volume);
  //   }

  //   // if (wholePrice.length > 0) {
  //   //   data.append('range_price', JSON.stringify(wholePrice));
  //   //   data.append('multi_price', 'yes');
  //   // } else {
  //   //   data.append('multi_price', 'no');
  //   // }
  //   data.append('condition', jenis === 'Baru' ? 'new' : 'old');
  //   data.append('minimum', Number(minimumOrder));

  //   if (isFreeShip === 0) {
  //     data.append('is_free_shipping', '0');
  //     if (pickGratisOngkir == 'Ya') {
  //       data.append('shipping_cost', '0');
  //     } else {
  //       data.append('shipping_cost', '0');
  //     }
  //   } else {
  //     data.append('is_free_shipping', '1');
  //     if (pickGratisOngkir == 'Ya') {
  //       data.append('shipping_cost', '0');
  //     } else {
  //       data.append('shipping_cost', '0');
  //     }
  //   }
  //   // if (datasubvarian.length !== 0) {
  //   //   const categoryArray = datasubvarian.map(item => ({name: item.name}));
  //   //   const subvarianArray = datasubvarian.flatMap(item =>
  //   //     item.subvarian.map(subItem => ({
  //   //       name: subItem.namasubvarian,
  //   //       price: parseFloat(subItem.price.replace(/\./g, '')),
  //   //       stock: subItem.stok,
  //   //       weight: subItem.weight,
  //   //     })),
  //   //   );
  //   //   data.append('have_variant', 'yes');
  //   //   data.append('variant', JSON.stringify(categoryArray));
  //   //   data.append('variant_detail', JSON.stringify(subvarianArray));
  //   // } else {
  //   //   data.append('have_variant', 'no');
  //   // }

  //   dispatch({type: 'SHOW_LOADING'});
  //   var res = {};
  //     res = await APISELLER.AddProductoke(data,idvariant,userData.token);
  //   }
  //   debugConsoleLog('produk2oke',res.data)
  //   dispatch({type: 'HIDE_LOADING'});
  //   if (res.status === 200) {
  //     if (route.params) {
  //       // showToastMessage('Produk berhasil diperbaharui')
  //       setSuccessMessage('Produk berhasil diperbaharui');
  //       setTimeout(() => {
  //         setModalSuccess(true);
  //       }, 500);
  //     } else {
  //       setSuccessMessage('Produk berhasil ditambahkan');
  //       setTimeout(() => {
  //         setModalSuccess(true);
  //       }, 500);
  //     }
  //     setType(null);
  //     setListImage([]);
  //     setName('');
  //     setDescription('');
  //     setLinkProduct('');
  //     setPrice('');
  //     setStock('');
  //     setMerk('');
  //     setPickedCategory([]);
  //     setPickedSubCategory([]);
  //     setPickedSubSubCategory([]);
  //     setLocalImgPath('');
  //     setTitleWholePrice('');
  //     setWholePrice([]);
  //     setStartPrice('');
  //     setMinimumOrder('');
  //     setTimeout(() => {
  //       setModalSuccess(false);
  //       navigation.goBack();
  //     }, 1500);
  // };

  const getvarint = async () => {};

  const addProductpre = async () => {
    var image = [];
    var categories = [];
    const data = new FormData();
    if(bundleImage.length  === 0){
      for (let i = 0; i < listImage.length; i++) {
        image.push({
          type: 'image/jpg',
          uri: listImage[i],
          name: `Product ${i + 1}`,
        });
      }
      image.forEach((img, index) => {
        data.append(`photo[]`, {
          uri: img.uri,
          type: img.type,
          name: img.name,
        });
      });
    }else{
      for (let i = 0; i < bundleImage.length; i++) {
        if (listImage[i] !== undefined) {
          image.push({
            type: 'image/jpg',
            uri: listImage[i],
            name: `Product ${i + 1}`,
          });
          const foto = {
            type: bundleImage[i].type,
            uri: bundleImage[i].uri,
            name: bundleImage[i].fileName,
          };
          data.append('photo[]', foto);
        }
      }
    }
    if (selectkategori !== ',,,') {
      const dataArray = selectkategori.split(', ').map(Number);
      categories.push(dataArray[0]);
      categories.push(dataArray[1]);
      categories.push(dataArray[2]);
    } else {
      for (let i = 0; i < pickedCategory.length; i++) {
        categories.push(pickedCategory[i].id);
      }
      for (let i = 0; i < pickedSubCategory.length; i++) {
        categories.push(pickedSubCategory[i].id);
      }
      for (let i = 0; i < pickedSubSubCategory.length; i++) {
        categories.push(pickedSubSubCategory[i].id);
      }
    }
    categories.map(e => {
      debugConsoleLog('categories[]', e);
    });
    data.append('type', type);
    categories.map(e => {
      data.append('categories[]', e);
    });
    data.append('name', name);
    data.append('description', description);
    data.append('video_url', linkProduct);
    data.append('merk', merk);
    if (type === 'non-material') {
      data.append('width', width);
      data.append('length', length);
      data.append('height', height);
      data.append('weight', weight);
      data.append('weight_volume', weight_volume);
    }

    data.append('condition', jenis === 'Baru' ? 'new' : 'old');
    data.append('minimum', Number(minimumOrder));

    if (isFreeShip === 0) {
      data.append('is_free_shipping', '0');
      if (pickGratisOngkir == 'Ya') {
        data.append('shipping_cost', '0');
      } else {
        data.append('shipping_cost', '0');
      }
    } else {
      data.append('is_free_shipping', '1');
      if (pickGratisOngkir == 'Ya') {
        data.append('shipping_cost', '0');
      } else {
        data.append('shipping_cost', '0');
      }
    }

    dispatch({type: 'SHOW_LOADING'});
    var res = {};
    debugConsoleLog('fulldtaa', data);
    res = await APISELLER.AddProductPre(data, userData.token);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {  
      debugConsoleLog('asma',res.data) 
      setidvariant(res.data.data.id);
      setmodalvarians(true);
    }
  };
  const deleteImage = async (value, index) => {
    var img = {};
    img = oriListImage.filter(val => {
      return val.url === value;
    });
    if (route.params) {
      const res = await APISELLER.DeleteFile(
        route.params.item.id,
        img[0].id,
        userData.token,
      );
      var arr = [...listImage];
      arr.splice(index, 1);
      setListImage(arr);
    } else {
      var arr = [...listImage];
      arr.splice(index, 1);
      setListImage(arr);
    }
  };
  useEffect(async () => {
    if (route.params) {
      if (route.params.item) {
        dispatch({type: 'SHOW_LOADING'});
        const data = route.params.item;
        setTimeout(() => {
          console.log(data);
        }, 10 * 1000);
        var poto = [];
        data.photo.map(value => {
          poto.push(value.url);
        });
        setIsFreeShip(data.is_free_shipping);
        if (data.is_free_shipping == 1) {
          setPickGratisOngkir('Ya');
        } else {
          setPickGratisOngkir('Tidak');
        }
        setOriListImage(data.photo);
        setType(data.type);
        setListImage(poto);
        setName(data.name);

        setDescription(data.description);
        setLinkProduct(data.video_url === null ? '' : data.video_url);
        debugConsoleLog('nilaiii', data);
        if (data.have_variant === 'yes') {
          setisvariant(true);
          setidvariant(prev => {
            return data.id;
          });
          setPrice('');
          setStock('');
        } else {
          setPrice(`${data.price}`);
          setStock(`${data.stock}`);
        }
        setMerk(data.merk);
        setLocalImgPath(data);
        setWholePrice(route.params.item.range_price);
        if (route.params.item.type === 'material') {
          const findMain = data.category.filter(value => {
            return value.type === 'main';
          });
          const findSub = data.category.filter(value => {
            return value.type === 'sub';
          });
          const findSubSub = data.category.filter(value => {
            return value.type === 'sub-sub';
          });
          setPickedCategory(findMain);
          setPickedSubCategory(findSub);
          setPickedSubSubCategory(findSubSub);
          const dataa = `${findMain[0].id}, ${findSub[0].id}, ${findSubSub[0].id}`;
          const dataadi = `${findMain[0].name}/${findSub[0].name}/${findSubSub[0].name}`;
          const paarams = {
            id: dataa,
            group: route.params.item.type,
            name: dataadi,
          };
          debugConsoleLog('datassss', dataadi);
          setRekomendasi([paarams]);
          seSelectKategori(prv => dataa);
          setMinimumOrder(data?.min?.toString() ?? '');
        } else {
          const findMain = data.category.filter(value => {
            return value.type === 'main';
          });
          const findSub = data.category.filter(value => {
            return value.type === 'sub';
          });
          const findSubSub = data.category.filter(value => {
            return value.type === 'sub-sub';
          });
          setPickedCategory(findMain);
          setPickedSubCategory(findSub);
          setPickedSubSubCategory(findSubSub);
          const dataa = `${findMain[0].id}, ${findSub[0].id}, ${findSubSub[0].id}`;
          const dataadi = `${findMain[0].name}/${findSub[0].name}/${findSubSub[0].name}`;
          const paarams = {
            id: dataa,
            group: route.params.item.type,
            name: dataadi,
          };
          setRekomendasi([paarams]); 
          seSelectKategori(prv => dataa);
          setMinimumOrder(data?.min?.toString() ?? '');
          setWidth(data?.width?.toString() ?? '');
          setLength(data?.length?.toString() ?? '');
          setHeight(data?.height?.toString() ?? '');
          setWeight(data?.weight?.toString() ?? '');
          setWeightVolume(data?.weight_volume?.toString() ?? '');
        }
        dispatch({type: 'HIDE_LOADING'});
        debugConsoleLog('xxxitem', data);
      }
    }
  }, []);

  useEffect(() => {
    if (route.params) {
      const data = route.params.item;
      if (data.type === 'material') {
        if (data.multi_price === 'yes') {
          const findSub = data.category.filter(value => {
            return value.type === 'sub';
          });
          const findSubSub = data.category.filter(value => {
            return value.type === 'sub-sub';
          });
          debugConsoleLog('cek sub', findSub);
          debugConsoleLog('cek sub', findSubSub);
          if (findSub.length > 0) {
            getSubCategory(findSub[0].parent_id);
          }
          if (findSubSub.length > 0) {
            getSubSubCategory(findSubSub[0].parent_id);
          }
        }
      }
    }
  }, []);
  const onvariant = async () => {};
  const dataxxx = useCallback(async () => {
    const res = await APISELLER.GetVarints(idvariant, userData.token);
    if (res.data.message === 'Gagal') {
    } else {
      setisvariant(true)
      const dataToTransform = res.data.data.variant;
      setdatavarians(dataToTransform);
    }
  }, [idvariant]);
  useFocusEffect(dataxxx);
  useEffect(() => {
    getAllDataCategory();
  }, [type]);

  return (
    <KeyboardAvoidingView
      style={{backgroundColor: '#FFF', flex: 1}}
      keyboardVerticalOffset={Platform.OS === 'android' ? -200 : 50}
      behavior={'padding'}>
      <View style={{flex: 1, backgroundColor: '#FFF'}}>
        <ModalSuccess
          show={modalSuccess}
          message={successMessage}
          onClose={() => setModalSuccess(false)}
        />
        <ModalPickImage
          show={showModalOption}
          onClose={() => setShowModalOption(false)}
          onPressCamera={() => {
            setShowModalOption(false);
            captureImage('photo');
          }}
          onPressGalery={() => {
            setShowModalOption(false);
            chooseFile('photo');
          }}
        />
        {showPengiriman && (
          <ModalPengiriman
            show={showPengiriman}
            onClose={() => setShowPengiriman(false)}
            listCourier={listCourier}
            onSubmit={data => {
              setHeight(data.height);
              setWeight(data.weight);
              setWidth(data.width);
              setLength(data.length);
              setWeightVolume(data.weight_volume);
              if (data.selectedCourier.length > 0) {
                var cour = [];
                for (let o = 0; o < data.selectedCourier.length; o++) {
                  cour.push(data.selectedCourier[o].rajaongkir);
                }
                setPickedCourier(cour);
              }
              setShowPengiriman(false);
            }}
            widths={width}
            lengths={length}
            heights={height}
            weights={weight}
            weight_volumes={weight_volume}
            showCourier={false}
          />
        )}

        <ScrollView keyboardShouldPersistTaps="always">
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View>
              <View
                style={{
                  flex: 1,
                  backgroundColor: '#FFF',
                  marginHorizontal: 15,
                  marginTop: 20,
                }}>
                <Text
                  style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
                  Foto Produk <Text style={{color: Colors.primary}}>*</Text>
                </Text>
              </View>
              <View style={styles.wrap}>
                {listImage.map((value, index) => {
                  return (
                    <View
                      key={index.toString()}
                      style={{marginLeft: 15, marginTop: 15}}>
                      <View
                        style={{
                          borderColor: '#F2F4F5',
                          width: 78,
                          height: 78,
                          borderWidth: 2,
                          borderRadius: 10,
                          alignItems: 'center',
                          justifyContent: 'center',
                          overflow: 'hidden',
                        }}>
                        <Image
                          source={{uri: value}}
                          style={{width: 78, height: 78}}
                        />
                        <View
                          style={{
                            backgroundColor: '#B4B6B8',
                            width: 15,
                            height: 15,
                            position: 'absolute',
                            top: 2,
                            right: 2,
                            borderRadius: 15,
                          }}>
                          <TouchableOpacity
                            onPress={() => deleteImage(value, index)}>
                            <Icons.MinusSVG color={'#FFF'} size={15} />
                          </TouchableOpacity>
                        </View>
                        {/* {
                    index === 0 && (
                        <View style={{backgroundColor: Colors.opacityColor("#333333", 0.5), width: '100%', position: 'absolute', bottom: 0, paddingVertical: 5, alignItems: 'center'}}>
                            <Text style={{fontFamily: Fonts.mulishRegular, fontSize: 10, color: '#FFF'}}>Foto Sampul</Text>
                        </View>
                    )
                } */}
                      </View>
                    </View>
                  );
                })}
                <TouchableOpacity
                  onPress={() => setShowModalOption(true)}
                  style={{marginLeft: 15, marginTop: 15}}>
                  <View
                    style={{
                      borderColor: Colors.primary,
                      width: 78,
                      height: 78,
                      borderStyle: 'dashed',
                      backgroundColor: '#FFF2F3',
                      borderWidth: 2,
                      borderRadius: 10,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Icons.PlusSVG size={28} color={Colors.primary} />
                    <Text
                      style={{
                        fontFamily: Fonts.mulishRegular,
                        color: Colors.primary,
                      }}>
                      Foto
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{width: '100%', height: 10, backgroundColor: '#F7F9F8'}}
              />
              <View
                style={{
                  marginTop: 20,
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginHorizontal: 15,
                }}>
                <Text
                  style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
                  Tipe Produk <Text style={{color: Colors.primary}}>*</Text>
                </Text>
              </View>
              <SelectDropDown
                disabled={disable}
                onSelectItem={val => {
                  if (val.value !== type) {
                    setPickedCategory([]);
                    setPickedSubCategory([]);
                    setPickedSubSubCategory([]);
                    setType(val.value);
                  }
                }}
                value={type}
                valueColor="#000"
                colorArrowIcon="#000"
                style={{
                  marginTop: 10,
                  width: '93%',
                  alignSelf: 'center',
                  borderRadius: 6,
                }}
                containerStyle={{
                  backgroundColor: disable ? 'lightgrey' : 'white',
                }}
                placeholderText="Pilih Tipe Produk"
                items={dataType}
                maxHeight={300}
                typeValueText="Medium"
              />
              <View
                style={{
                  marginVertical: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginHorizontal: 15,
                }}>
                <Text
                  style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
                  Nama Produk <Text style={{color: Colors.primary}}>*</Text>
                </Text>
                <Text
                  style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>
                  {name.length}/255
                </Text>
              </View>
              <InputText
                multiline={true}
                textAlignVertical={'top'}
                containerStyle={{marginHorizontal: 15}}
                placeholder={
                  'Masukan Nama Produk \n\nTips : Jenis Produk + Merek Produk + Keterangan Tambahan'
                }
                value={name}
                onChangeText={text => setName(text)}
                maxLength={255}
              />
              <View
                style={{
                  marginVertical: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginHorizontal: 15,
                }}>
                <Text
                  style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
                  Merk Produk <Text style={{color: Colors.primary}}>*</Text>
                </Text>
                <Text
                  style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>
                  {merk.length}/255
                </Text>
              </View>
              <InputText
                containerStyle={{marginHorizontal: 15}}
                placeholder="Masukan Merk Produk"
                value={merk}
                onChangeText={text => setMerk(text)}
                maxLength={255}
              />
              <FlatList
                data={rekomendasi}
                ListHeaderComponent={
                  <View style={styles.item}>
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: '#333',
                      }}>
                      Kategori <Text style={{color: Colors.primary}}>*</Text>
                    </Text>
                    <Text
                      onPress={() => {
                        setModlkkategori(true);
                      }}
                      style={{
                        fontFamily: Fonts.mulishMedium,
                        color: Colors.primary,
                      }}>
                      Kategori Lainnya
                    </Text>
                  </View>
                }
                renderItem={({item, index}) => {
                  return (
                    <TouchableOpacity
                      style={[
                        styles.item,
                        {
                          backgroundColor:
                            selectkategori === item.id ? '#FFF9F3' : '#fff',
                        },
                      ]}
                      onPress={() => {
                        seSelectKategori(prv => item.id);
                      }}>
                      <TextRegular
                        text={item.name}
                        style={{width: '80%'}}
                        color={
                          selectkategori === item.id
                            ? Colors.blackBase
                            : Colors.normal
                        }
                      />
                      <Image
                        source={
                          selectkategori === item.id
                            ? IconAsset.IC_CIRCLE_SELECTED
                            : IconAsset.IcUnselect
                        }
                        style={{width: 25, height: 25}}
                      />
                    </TouchableOpacity>
                  );
                }}
              />
              <View
                style={{
                  marginTop: 20,
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginHorizontal: 15,
                }}>
                <Text
                  style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
                  Kondisi Produk <Text style={{color: Colors.primary}}>*</Text>
                </Text>
              </View>
              <SelectDropDown
                disabled={disable2}
                onSelectItem={val => {
                  if (val.value !== type) {
                    setJenis(val.value);
                  }
                }}
                value={jenis}
                valueColor="#000"
                colorArrowIcon="#000"
                style={{
                  marginTop: 10,
                  width: '93%',
                  alignSelf: 'center',
                  borderRadius: 6,
                }}
                containerStyle={{
                  backgroundColor: disable2 ? 'lightgrey' : 'white',
                }}
                placeholderText="Pilih Kondisi Produk"
                items={dataType2}
                maxHeight={300}
                typeValueText="Medium"
              />
              {isFreeShip === 1 && (
                <>
                  <View
                    style={{
                      marginTop: 20,
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: '#333',
                      }}>
                      Gratis Antar{' '}
                      <Text style={{color: Colors.primary}}>*</Text>
                    </Text>
                  </View>
                  <SelectDropDown
                    searchable
                    value={pickGratisOngkir}
                    onSelectItem={val => {
                      setPickGratisOngkir(val.name);
                    }}
                    disabled={type == 'material' ? true : false}
                    containerStyle={{
                      backgroundColor:
                        type == 'material' ? 'lightgrey' : 'white',
                    }}
                    valueColor="#000"
                    colorArrowIcon="#000"
                    style={{
                      marginTop: 10,
                      width: '93%',
                      alignSelf: 'center',
                      borderRadius: 6,
                    }}
                    placeholderText="-"
                    items={[
                      {id: 1, name: 'Ya'},
                      {id: 2, name: 'Tidak'},
                    ]}
                    maxHeight={300}
                    typeValueText="Medium"
                  />

                  {pickGratisOngkir == 'Ya' && (
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: Colors.primary,
                        fontSize: 10,
                        marginHorizontal: 15,
                        marginTop: 8,
                      }}>
                      * wajib mengantarkan barang ke konsumen. Baik antar
                      langsung maupun melalui ekspedisi selama jangkauan masih
                      tercover
                    </Text>
                  )}
                </>
              )}

              <View
                style={{
                  marginTop: 20,
                  marginBottom: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginHorizontal: 15,
                }}>
                <Text
                  style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
                  Deskripsi Produk{' '}
                  <Text style={{color: Colors.primary}}>*</Text>
                </Text>
                <Text
                  style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>
                  {description.length}/3000
                </Text>
              </View>
              <InputText
                style={{textAlignVertical: 'top'}}
                containerStyle={{marginHorizontal: 15}}
                placeholder="Masukan Deskripsi Produk"
                multiline={true}
                numberOfLines={6}
                value={description}
                onChangeText={text => setDescription(text)}
                maxLength={3000}
              />
              <View
                style={{
                  marginTop: 20,
                  marginBottom: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginHorizontal: 15,
                }}>
                <Text
                  style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
                  Link Video Produk (Opsional)
                </Text>
                <Text
                  style={{fontFamily: Fonts.mulishRegular, color: '#B4B6B8'}}>
                  {linkProduct.length}/255
                </Text>
              </View>
              <InputText
                containerStyle={{marginHorizontal: 15}}
                placeholder="Ketikkan link dari youtube"
                value={linkProduct}
                onChangeText={text => setLinkProduct(text)}
                maxLength={255}
              />

              <View
                style={{
                  width: '100%',
                  height: 10,
                  backgroundColor: '#F7F9F8',
                  marginVertical: 15,
                }}
              />
              <View
                style={{
                  marginBottom: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginHorizontal: 15,
                }}>
                <Text
                  style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
                  Minimum Pembelian{' '}
                  <Text style={{color: Colors.primary}}>*</Text>
                </Text>
              </View>
              <InputText
                containerStyle={{marginHorizontal: 15}}
                placeholder="Masukan Minimum Pembelian"
                value={minimumOrder}
                onChangeText={text => setMinimumOrder(text)}
              />
              {type !== 'material'&&(<TouchableOpacity
                style={[styles.btnPengiriman, {marginTop: 10}]}
                onPress={() => {
                  if(listImage.length === 0){
                    showToastMessage('Harap pilih foto minimal 1')
                  }else if(selectkategori ===  ',,,'){
                    showToastMessage('Harap pilih kategori')
                  }else if(name  === ''){
                    showToastMessage('Nama produk tidak boleh kosong')
                  }else if (merk === ''){
                    showToastMessage('Merek produk tidak boleh kosong')
                  }else if (minimumOrder  === ''){
                    showToastMessage('Minimum pembelian tidak boleh kosong')
                  }else{
                    if(isvariant === true){
                      setmodalvarians(true);
                    }else{
                      addProductpre();
                    }
                  }
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={IconAsset.IcPriceTag}
                    style={{
                      width: 25,
                      height: 25,
                      marginRight: 10,
                      resizeMode: 'contain',
                    }}
                  />
                  <Text
                    style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
                    Varian
                  </Text>
                  
                </View>
                {isvariant === true &&(
                   <Text
                   style={{fontFamily: Fonts.mulishExtraBold, color: Colors.primary}}>
                   Ubah 
                 </Text>
                )}
               {isvariant === false &&( <Image
                  source={IconAsset.ChevronRight}
                  style={{width: 20, height: 20, tintColor: Colors.blackBase}}
                />)}
              </TouchableOpacity>)}
              {isvariant === false && (
                <>
                  <View
                    style={{
                      marginTop: 20,
                      marginBottom: 10,
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <Image
                      source={IconAsset.IcPriceTag}
                      style={{
                        width: 24,
                        height: 24,
                        resizeMode: 'contain',
                        marginRight: 10,
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: '#333',
                      }}>
                      Harga (Rupiah){' '}
                      <Text style={{color: Colors.primary}}>*</Text>
                    </Text>
                  </View>
                  <InputText
                    containerStyle={{marginHorizontal: 15}}
                    placeholder="Masukan Harga"
                    value={price}
                    keyboardType="number-pad"
                    onChangeText={text => setPrice(inputRupiah(text))}
                  />
                  <>
                    <View
                      style={{
                        marginTop: 20,
                        marginBottom: 10,
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginHorizontal: 15,
                      }}>
                      <Image
                        source={IconAsset.IcPriceTag}
                        style={{
                          width: 24,
                          height: 24,
                          resizeMode: 'contain',
                          marginRight: 10,
                        }}
                      />
                      <Text
                        style={{
                          fontFamily: Fonts.mulishExtraBold,
                          color: '#333',
                          fontSize: 12,
                        }}>
                        Harga Borongan (Hanya Jumlah Pembelian)
                      </Text>
                    </View>
                    <InputText
                      containerStyle={{marginHorizontal: 15}}
                      placeholder="cnth: 5"
                      value={titleWholePrice}
                      onChangeText={text => setTitleWholePrice(text)}
                    />
                    <View
                      style={{
                        width: '100%',
                        paddingHorizontal: 15,
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                        marginTop: 15,
                      }}>
                      <InputText
                        containerStyle={{width: '70%'}}
                        placeholder="Masukkan Harga Borongan"
                        value={startPrice}
                        keyboardType="number-pad"
                        onChangeText={text => setStartPrice(inputRupiah(text))}
                      />
                      <TouchableOpacity
                        style={styles.btnAddPrice}
                        onPress={() => {
                          var arr = [...wholePrice];
                          arr.push({
                            qty: titleWholePrice,
                            price: startPrice.split('.').join(''),
                          });
                          setWholePrice(arr);
                          setTitleWholePrice('');
                          setStartPrice('');
                        }}>
                        <Image
                          source={IconAsset.IcPlus}
                          style={{width: 20, height: 20, tintColor: '#fff'}}
                        />
                      </TouchableOpacity>
                    </View>
                    <FlatList
                      data={wholePrice}
                      contentContainerStyle={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                      }}
                      keyExtractor={(item, index) => index.toString()}
                      renderItem={({item, index}) => {
                        return (
                          <View
                            style={[
                              styles.catPicked,
                              {
                                flexDirection: 'row',
                                alignItems: 'flex-start',
                                justifyContent: 'space-between',
                              },
                            ]}>
                            <TextSemiBold
                              text={`${item.qty} - ${inputRupiah(
                                item.price,
                                'Rp',
                              )}`}
                              color="#fff"
                              style={{marginRight: 15}}
                            />
                            <TouchableOpacity
                              style={styles.btnDeleteItem}
                              onPress={() => {
                                var arr = [...wholePrice];
                                arr.splice(index, 1);
                                setWholePrice(arr);
                              }}>
                              <Image
                                source={IconAsset.IcClose}
                                style={{
                                  width: 10,
                                  height: 10,
                                  tintColor: '#fff',
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        );
                      }}
                    />
                  </>
                  <View
                    style={{
                      marginTop: 20,
                      marginBottom: 10,
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <Image
                      source={IconAsset.IcLayer}
                      style={{
                        width: 24,
                        height: 24,
                        resizeMode: 'contain',
                        marginRight: 10,
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: '#333',
                      }}>
                      Stok <Text style={{color: Colors.primary}}>*</Text>
                    </Text>
                  </View>
                  <InputText
                    containerStyle={{marginHorizontal: 15}}
                    placeholder="Masukan Stok"
                    value={stock}
                    onChangeText={text => setStock(text)}
                  />
                </>
              )}

              {type === 'non-material' && (
                <>
                  <View>
                    <View
                      style={{
                        marginTop: 20,
                        marginBottom: 10,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginHorizontal: 15,
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.mulishExtraBold,
                          color: '#333',
                        }}>
                        Berat Produk (gram){' '}
                        <Text style={{color: Colors.primary}}>*</Text>
                      </Text>
                    </View>
                    <InputText
                      containerStyle={{marginHorizontal: 15}}
                      placeholder="Masukan Berat Produk"
                      value={weight}
                      onChangeText={text => setWeight(text)}
                      keyboardType="number-pad"
                    />
                    {weight > 5000 && (
                      <>
                        <View
                          style={{
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginHorizontal: 15,
                          }}>
                          <Text
                            style={{
                              fontFamily: Fonts.mulishExtraBold,
                              color: '#333',
                            }}>
                            Lebar Produk (Cm / Diameter)
                          </Text>
                        </View>
                        <InputText
                          containerStyle={{
                            marginHorizontal: 15,
                            backgroundColor:
                              weight > 5000 ? '#FFF' : Colors.bgGrey,
                            borderRadius: 10,
                          }}
                          placeholder="Masukan Lebar Produk"
                          value={width}
                          editable={weight > 5000 ? true : false}
                          onChangeText={text => setWidth(Number(text))}
                          keyboardType="number-pad"
                        />
                        <View
                          style={{
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginHorizontal: 15,
                          }}>
                          <Text
                            style={{
                              fontFamily: Fonts.mulishExtraBold,
                              color: '#333',
                            }}>
                            Panjang Produk (Cm)
                          </Text>
                        </View>
                        <InputText
                          containerStyle={{
                            marginHorizontal: 15,
                            backgroundColor:
                              weight > 5000 ? '#FFF' : Colors.bgGrey,
                            borderRadius: 10,
                          }}
                          placeholder="Masukan Panjang Produk"
                          value={length}
                          onChangeText={text => setLength(Number(text))}
                          keyboardType="number-pad"
                        />
                        <View
                          style={{
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginHorizontal: 15,
                          }}>
                          <Text
                            style={{
                              fontFamily: Fonts.mulishExtraBold,
                              color: '#333',
                            }}>
                            Tinggi Produk (Cm)
                          </Text>
                        </View>
                        <InputText
                          containerStyle={{
                            marginHorizontal: 15,
                            backgroundColor:
                              weight > 5000 ? '#FFF' : Colors.bgGrey,
                            borderRadius: 10,
                          }}
                          placeholder="Masukan Tinggi Produk"
                          value={height}
                          onChangeText={text => setHeight(text)}
                          keyboardType="number-pad"
                          onBlur={() => countVolume()}
                        />
                        <View
                          style={{
                            width: '100%',
                            paddingHorizontal: 15,
                            marginTop: 15,
                          }}>
                          <View
                            style={{
                              marginBottom: 10,
                              flexDirection: 'row',
                              alignItems: 'center',
                              justifyContent: 'space-between',
                            }}>
                            <Text
                              style={{
                                fontFamily: Fonts.mulishExtraBold,
                                color: '#333',
                              }}>
                              Volume Produk (Dihitung Otomatis)
                            </Text>
                          </View>
                          <View
                            style={{
                              width: '100%',
                              borderColor: '#E0E0E0',
                              borderRadius: 10,
                              borderWidth: 1,
                              paddingVertical: 15,
                              paddingHorizontal: 5,
                              backgroundColor: Colors.bgGrey,
                            }}>
                            <TextRegular text={volume} />
                          </View>
                        </View>
                      </>
                    )}
                  </View>
                </>
              )}
              <View
                style={{
                  marginTop: 60,
                  marginBottom: 40,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginHorizontal: 15,
                }}></View>
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>

        <View
          style={{
            marginVertical: 20,
            flexDirection: 'row',
            justifyContent: 'space-around',
          }}>
          <Button
            containerStyle={{width: '90%'}}
            style={{
              borderColor: validate()
                ? isvariant === true
                  ? Colors.primary
                  : Colors.borderGrey
                : Colors.primary,
              borderWidth: 2,
              backgroundColor: validate()
                ? isvariant === true
                  ? Colors.primary
                  : Colors.borderGrey
                : Colors.primary,
            }}
            title={route.params ? 'Perbarui' : 'Tambahkan'}
            onPress={() => {
              if (isvariant === true && !route.params) {
                addProduct2();
              } else {
                addProduct();
              }
            }}
          />
        </View>
      </View>
      <ModalVariants
        isupdate ={!route.params?false:true}
        onupdate={isvariant}
        isvarian={setisvariant}
        show={showmodalvarians}
        onClose={() => {
          setmodalvarians(false);
        }}
        datasubs={datasubvarian}
        datasubsubs={setdatavarians}
        idvarint={idvariant}
        // show={}
        // onClose={() => {
        //   setmodalvarians(false);
        // }}
      />
      <ModalAllKategori
        show={showmodalkaategori}
        onClose={() => {
          setModlkkategori(false);
        }}
        getSubCategory={getSubCategory}
        setselected={seSelectKategori}
        category={category}
        setRekomendasi={setRekomendasi}
        pickedSubCategory={pickedSubCategory}
        pickedSubSubCategory={pickedSubSubCategory}
        pickedCategory={pickedCategory}
        getSubSubCategory={getSubSubCategory}
        listSubCategory={listSubCategory}
        listSubSubCategory={listSubSubCategory}
        setPickedCategory={setPickedCategory}
        setPickedSubSubCategory={setPickedSubSubCategory}
        setPickedSubCategory={setPickedSubCategory}
        tipematerial={type}
      />
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  btnDeleteItem: {
    width: 15,
    height: 15,
    borderRadius: 15 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  item: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 18,
  },
  btnAddPrice: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 15,
    backgroundColor: Colors.primary,
    borderRadius: 6,
  },
  btnPengiriman: {
    width: '100%',
    paddingHorizontal: 15,
    marginTop: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  wrap: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    paddingBottom: 15,
  },
  catPicked: {
    marginLeft: 16,
    marginTop: 10,
    padding: 7,
    borderRadius: 4,
    backgroundColor: Colors.secondary,
  },
});

export const screenOptions = navData => {
  return {
    headerTitle: 'Tambah Produk',
    headerTitleAlign: 'left',
    headerTitleStyle: {
      marginTop: 10,
      fontFamily: Fonts.mulishExtraBold,
      fontSize: Size.scaleFont(16),
      color: '#333333',
    },
    header: () => (
      <View style={{height: Size.scaleSize(80), backgroundColor: '#FFF'}}>
        <HeaderTitle title="Tambah Produk" />
      </View>
    ),
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft: Size.scaleSize(5), bottom: 0, position: 'absolute'}}
        onPress={() => navData.navigation.goBack()}>
        <View style={{padding: Size.scaleSize(10)}}>
          <Icons.ChevronLeft color="#333" size={Size.scaleSize(18)} />
        </View>
      </TouchableOpacity>
    ),
  };
};

export default InputProductScreen;
