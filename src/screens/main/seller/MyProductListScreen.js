import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  useWindowDimensions,
  FlatList,
  TouchableHighlight,
  StatusBar,
  Image,
  Dimensions,
} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

import {Button, InputText, Header} from '@components/global';
import {Size, Fonts, Colors, Icons, Images} from '@styles/index';
import {useSelector, useDispatch} from 'react-redux';
import API from '@utils/services/seller/productProvider';
import {
  useNavigation,
  useIsFocused,
  useFocusEffect,
} from '@react-navigation/native';
import ModalHapusPesanan from '@components/modal/ModalHapusPesanan';
import {debugConsoleLog} from '@helpers/functionFormat';
import {useCallback} from 'react';
import normalize from 'react-native-normalize';

const MyProductListScreen = props => {
  const navigation = useNavigation();
  const flatreff = useRef(null);
  const focused = useIsFocused();
  const {userData} = useSelector(state => state.auth);
  const [current, setcurrent] = useState(1);
  const [curr, setcurindex] = useState(0);
  const [maax, setmax] = useState(1);
  const ITEM_HEIGHT = normalize(50); // Ganti dengan tinggi sebenarnya dari setiap item

  const [showModalHapusPesanan, setModalHapusPesanan] = useState(false);
  const [getImageModal, setImageModal] = useState();
  const [getItem, setItem] = useState();
  const dispatch = useDispatch();

  const renderScene = SceneMap({
    first: () => <FirstRoute {...props} status="Tersedia" btnColor="#FF5247" />,
    second: () => <FirstRoute {...props} status="Habis" btnColor="#FFB323" />,
    third: () => (
      <FirstRoute {...props} status="Diarsipkan" btnColor="#1DA1F2" />
    ),
    fourth: () => (
      <FirstRoute {...props} status="Bahan Bangunan" btnColor="#23C16B" />
    ),
    fifth: () => (
      <FirstRoute {...props} status="Bahan Non Bangunan" btnColor="#B4B6B8" />
    ),
  });

  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [listProduct, setListProduct] = useState([]);
  const [routes] = React.useState([
    {key: 'first', title: 'Tersedia'},
    {key: 'second', title: 'Habis'},
    {key: 'third', title: 'Diarsipkan'},
    {key: 'fourth', title: 'Bahan Bangunan'},
    {key: 'fifth', title: 'Bahan Non Bangunan'},
  ]);

  const getDataProduct = async data => {
    dispatch({type: 'SHOW_LOADING'});
    debugConsoleLog('daata', current);
    const params = {
      filter:
        index == 0
          ? 'tersedia'
          : index === 1
          ? 'habis'
          : index === 2
          ? 'diarsip'
          : index === 3
          ? 'material'
          : 'non-material',
      page: data !== undefined ? data : curr === index ? current : 1,
    };
    try {
      const res = await API.GetListProduct(params, userData.token);
      dispatch({type: 'HIDE_LOADING'});
      if (res.data.message === 'Sukses') {
        setcurindex(index);
        setmax(res.data.meta.count_page);
        if (listProduct.length === 0) {
          setcurrent(res.data.meta.page);
          setListProduct(res.data.data);
        } else {
          if (curr === index) {
            setcurrent(res.data.meta.page);
            setListProduct(prevList => [...prevList, ...res.data.data]);
          } else {
            setcurrent(1);
            setListProduct(res.data.data);
          }
        }
      } else {
        setListProduct([]);
      }
    } catch (error) {
      setListProduct([]);
      debugConsoleLog('tmt', error);
    }
  };
  useEffect(() => {
    if (current !== 1 && flatreff.current) {
      const list = listProduct.length - 1;
      flatreff.current.scrollToIndex({animated: true, index: 3});
    }
  }, [current, listProduct.length]);

  const updateItem = async item => {
    const res = await API.GetDetailProduct(item.id, userData.token);
    console.log('res detail produk', res);
    if (res.status === 200) {
      navigation.navigate('InputProduct', {
        item: res.data.data,
      });
    }
  };

  const deleteItem = async item => {
    const res = await API.DeleteProduct(item.id, userData.token);
    console.log('res delete', res);
    getDataProduct();
  };

  const archiveItem = async item => {
    const res = await API.ArchiveProduct(item.id, userData.token);
    console.log('res arcive', res);
    getDataProduct();
  };

  const handleScroll = event => {
    const offsetY = event.nativeEvent.contentOffset.y;

    // Jika pengguna scroll ke atas dan sudah di bagian atas,
    // tampilkan Toast bahwa mereka sudah berada di item pertama
    if (offsetY <= 0) {
    }
  };
  const handleEndReached = () => {
    if (current < maax) {
      let num = current + 1;
      getDataProduct(num);
    }
  };

  useEffect(() => {
    debugConsoleLog('datfocc', focused);
    if (focused) {
      getDataProduct();
    } else {
      setListProduct([]);
      setcurrent(1);
    }
  }, [index, focused]);
  const getItemLayout = useCallback(
    (data, index) => ({
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    }),
    [],
  );
  return (
    <>
      {/* <StatusBar backgroundColor={Colors.primary} barStyle="light-content"/> */}
      <Header
        navData={props}
        isSeller
        headerLeft={
          <View>
            <Text
              allowFontScaling={false}
              style={{
                color: '#FFF',
                fontFamily: Fonts.mulishExtraBold,
                fontSize: Size.scaleFont(16),
              }}>
              Produk Saya
            </Text>
          </View>
        }
      />
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        // swipeEnabled={false}
        renderTabBar={props => (
          <TabBar
            {...props}
            scrollEnabled={true}
            tabStyle={{width: 'auto'}}
            indicatorStyle={styles.indidcatorStyle}
            style={styles.tabStyle}
            activeColor={Colors.secondary}
            inactiveColor={'#CDCFD0'}
            pressColor={'transparent'}
            renderLabel={({route, color}) => (
              <View
                style={[
                  styles.labelContainer,
                  {
                    borderColor: color,
                    backgroundColor: Colors.opacityColor(color, 0.08),
                  },
                ]}>
                <Text
                  allowFontScaling={false}
                  style={[styles.tabLabel, {color: color}]}>
                  {route.title}
                </Text>
              </View>
            )}
          />
        )}
      />
    </>
  );
  function FirstRoute(props) {
    const [key, setKey] = React.useState('');
    const filter = React.useCallback(() => {
      var data = [];
      if (key) {
        data = listProduct.filter(value => {
          return value.name.toLowerCase().includes(key.toLowerCase());
        });
      } else {
        data = [...listProduct];
      }
      return data;
    }, [key]);
    return (
      <View style={styles.screen}>
        <FlatList
          ref={flatreff}
          showsVerticalScrollIndicator={false}
          style={{
            paddingHorizontal: Size.scaleSize(18),
            paddingVertical: Size.scaleSize(20),
          }}
          contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
          data={filter()}
          onEndReachedThreshold={0.1}
          onScroll={handleScroll}
          getItemLayout={getItemLayout}
          onEndReached={handleEndReached}
          keyExtractor={item => item.id}
          ListHeaderComponent={
            <View style={styles.titleContainer}>
              <InputText
                placeholder="Cari Produk"
                rightIcon={<Icons.SearchSVG size={20} color="#333333" />}
                onChangeText={text => setKey(text)}
                containerInputStyle={{paddingHorizontal: 10}}
              />
              <Text
                style={{
                  fontFamily: Fonts.mulishExtraBold,
                  color: '#333',
                  marginVertical: 20,
                }}>
                {listProduct.length} produk tersedia
              </Text>
            </View>
          }
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  marginBottom: 20,
                  borderRadius: 20,
                  borderColor: '#F2F4F5',
                  borderWidth: 1,
                  padding: 20,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 10,
                  }}>
                  <Image
                    style={{width: 74, height: 74, marginRight: 20}}
                    source={{uri: item.photo}}
                  />
                  <View style={{flex: 1}}>
                    <Text
                      style={{
                        fontFamily: Fonts.mulishBold,
                        color: '#333',
                        marginBottom: 5,
                      }}>
                      {item.name}
                    </Text>
                    <Text
                      style={{fontFamily: Fonts.mulishRegular, color: '#333'}}>
                      {item.price}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 1,
                    backgroundColor: '#F2F4F5',
                    marginVertical: 15,
                  }}
                />
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginBottom: 20,
                  }}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icons.ShoppingBagSVG color={'#B4B6B8'} size={20} />
                    <Text
                      style={{
                        fontFamily: Fonts.mulishRegular,
                        fontSize: 12,
                        color: '#B4B6B8',
                        marginLeft: 5,
                      }}>
                      Stok {item.stock}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icons.ShoppingBagSVG color={'#B4B6B8'} size={20} />
                    <Text
                      style={{
                        fontFamily: Fonts.mulishRegular,
                        fontSize: 12,
                        color: '#B4B6B8',
                        marginLeft: 5,
                      }}>
                      Terjual {item.sold}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent:
                      props.status !== 'Habis' ? 'space-between' : 'flex-start',
                    width: '100%',
                  }}>
                  <Button
                    onPress={() => updateItem(item)}
                    containerStyle={{
                      backgroundColor: '#FFF',
                      borderRadius: 10,
                      borderColor: Colors.primary,
                      borderWidth: 2,
                    }}
                    textComponent={
                      <View
                        style={{
                          width: Dimensions.get('screen').width * 0.24,
                          height: 50,
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Icons.Editproduk size={24} color={Colors.primary} />
                        <Text
                          style={{
                            fontFamily: Fonts.mulishExtraBold,
                            marginLeft: 2,
                            fontSize: 13,
                            color: Colors.primary,
                          }}>
                          Ubah
                        </Text>
                      </View>
                    }
                  />
                  {props.status !== 'Habis' && (
                    <Button
                      containerStyle={{
                        backgroundColor: '#FFF',
                        borderRadius: 10,
                        borderColor: Colors.primary,
                        borderWidth: 2,
                      }}
                      onPress={() => archiveItem(item)}
                      textComponent={
                        <View
                          style={{
                            width: Dimensions.get('screen').width * 0.28,
                            height: 50,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Icons.Arsip size={25} color={Colors.primary} />
                          <Text
                            style={{
                              fontFamily: Fonts.mulishExtraBold,
                              marginLeft: 2,
                              fontSize: 13,
                              color: Colors.primary,
                            }}>
                            {props.status === 'Diarsipkan'
                              ? 'Tampilkan'
                              : 'Arsipkan'}
                          </Text>
                        </View>
                      }
                    />
                  )}
                  <Button
                    onPress={() => {
                      setModalHapusPesanan(true),
                        setImageModal(item.photo),
                        setItem(item);
                    }}
                    containerStyle={{
                      backgroundColor: '#FFF',
                      borderRadius: 10,
                      borderColor: Colors.primary,
                      borderWidth: 2,
                    }}
                    textComponent={
                      <View
                        style={{
                          width: Dimensions.get('screen').width * 0.24,
                          height: 50,
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Icons.TrashSVG size={24} color={Colors.primary} />
                        <Text
                          style={{
                            fontFamily: Fonts.mulishExtraBold,
                            marginLeft: 2,
                            fontSize: 13,
                            color: Colors.primary,
                          }}>
                          Hapus
                        </Text>
                      </View>
                    }
                  />
                </View>
              </View>
            );
          }}
        />
        <ModalHapusPesanan
          show={showModalHapusPesanan}
          Onpressno={() => setModalHapusPesanan(false)}
          Onpressyes={() => {
            setModalHapusPesanan(false), deleteItem(getItem);
          }}
          onClose={() => setModalHapusPesanan(false)}
          img={{uri: getImageModal}}
          onSwipeComplete={() => setModalHapusPesanan(false)}
        />
        <Button
          containerStyle={{width: '90%', alignSelf: 'center', marginBottom: 10}}
          title="+ Produk Baru"
          onPress={() => props.navigation.navigate('InputProduct')}
        />
      </View>
    );
  }
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  headerTitle: {
    color: '#FFF',
    fontFamily: Fonts.mulishExtraBold,
    fontSize: Size.scaleFont(16),
  },
  indidcatorStyle: {
    backgroundColor: Colors.secondary,
    height: 0,
    borderRadius: 3,
  },
  tabStyle: {
    elevation: 0,
    backgroundColor: '#FFF',
  },
  labelContainer: {
    borderWidth: 1,
    padding: Size.scaleSize(10),
    borderRadius: 30,
    paddingHorizontal: Size.scaleSize(20),
  },
  tabLabel: {
    fontFamily: Fonts.mulishBold,
    textTransform: 'capitalize',
    fontSize: Size.scaleFont(14),
  },
  titleContainer: {
    marginBottom: Size.scaleSize(8),
    // paddingHorizontal: 10
  },
  title: {
    fontFamily: Fonts.mulishExtraBold,
    fontSize: Size.scaleFont(16),
    color: '#333',
    marginBottom: 20,
  },
});

export const screenOptions = navData => {
  return {
    header: () => (
      <Header
        navData={navData}
        isSeller
        headerLeft={
          <View>
            <Text
              allowFontScaling={false}
              style={{
                color: '#FFF',
                fontFamily: Fonts.mulishExtraBold,
                fontSize: Size.scaleFont(16),
              }}>
              Produk Saya
            </Text>
          </View>
        }
      />
    ),
  };
};

export default MyProductListScreen;
