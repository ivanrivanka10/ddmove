import React, {useEffect, useState} from 'react';
import {View, Text, ScrollView, StyleSheet, Image} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import {Header, Card, ButtonContainer} from '@components/global';
import {Size, Fonts, Images, Icons, Colors} from '@styles/index';
import {logout} from '@store/actions';
import APISELLER from '@utils/services/seller/settingProvider';

import {IconAsset} from '@utils/index';

const SellerScreen = props => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.userData);
  const [material, setMaterial] = useState([]);
  const [nonMaterial, setNonMaterial] = useState([]);
  const [dataShop, setDataShop] = useState({});
  const [dataRekening, setDataRekening] = useState([]);
  const [isProfileToko, setIsProfileToko] = useState(false);

  const onLogout = async () => {
    console.log('logout');
    try {
      await dispatch(logout());
      props.navigation.replace('AuthNavigation');
    } catch (error) {}
  };

  const getData = async () => {
    console.log('user seller', user.token);

    dispatch({type: 'SHOW_LOADING'});
    const resData = await APISELLER.GetDataExpedition(user.token);
    const resShop = await APISELLER.GetDataShop(user.token);
    const resRekening = await APISELLER.GetListPaymentAccount(user.token);
    console.log('=== res coverage', res);
    const [res, shop, rekening] = await Promise.all([
      resData,
      resShop,
      resRekening,
    ]);
    if (res.status == 200) {
      const data = res.data.data;
      // const datane = Object.keys(data)

      for (const [key, value] of Object.entries(data)) {
        console.log(`${key}`);
        if (key === 'non-material') {
          console.log(`${data[key]}`);
          setNonMaterial(data[key]);
        }
      }
      setMaterial(res.data.data.material);
      // setNonMaterial(`${res.data.data.non-material}`)
    }
    if (rekening.status === 200) {
      setDataRekening(rekening?.data?.data?.bank ?? []);
    }

    if (shop.status === 200) {
      setIsProfileToko(false);
      setDataShop(shop.data.data);
      if (typeof shop?.data?.data?.descriptions === 'undefined') {
        setIsProfileToko(true);
      }
      if (typeof shop?.data?.data?.name === 'undefined') {
        setIsProfileToko(true);
      }
      if (typeof shop?.data?.data?.phone === 'undefined') {
        setIsProfileToko(true);
      }
      if (typeof shop?.data?.data?.address_details === 'undefined') {
        setIsProfileToko(true);
      }
    }
    dispatch({type: 'HIDE_LOADING'});
  };

  useEffect(() => {
    getData();
  }, []);

  React.useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      getData();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [props.navigation]);

  return (
    <View style={styles.screen}>
      <Header
        navData={props}
        isSeller
        headerLeft={
          <View>
            <Text allowFontScaling={false} style={styles.headerTitle}>
              Pengaturan
            </Text>
          </View>
        }
      />
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            margin: Size.scaleSize(20),
          }}>
          <Image
            source={
              user.image === null
                ? IconAsset.IC_USER
                : {
                    uri: user?.image,
                  }
            }
            style={styles.image}
          />
          <View style={{flex: 1, marginLeft: 20}}>
            <Text
              allowFontScaling={false}
              style={{
                fontFamily: Fonts.mulishExtraBold,
                color: '#333',
                fontSize: Size.scaleFont(18),
                marginBottom: Size.scaleSize(4),
              }}>
              {user?.name ?? '-'}
            </Text>
            <Text
              allowFontScaling={false}
              style={{
                fontFamily: Fonts.mulishRegular,
                color: '#B4B6B8',
                fontSize: Size.scaleFont(12),
              }}>
              {user?.email ?? '-'}
            </Text>
          </View>
        </View>
        <ButtonContainer
          style={{
            borderRadius: 10,
            paddingHorizontal: 20,
            backgroundColor: Colors.primary,
            marginHorizontal: 18,
          }}
          textStyle={{color: '#FFF'}}
          underlayColor={Colors.opacityColor(Colors.primary, 0.8)}
          image={
            <Icons.UserCircleSVG color={'#FFF'} size={Size.scaleSize(24)} />
          }
          title={'Ubah ke Role Konsumen'}
          desc={'Kembali ke halaman konsumen'}
          onPress={() => {
            const data = 'no';
            dispatch({type: 'CHANGE_TYPE', data});
            props.navigation.replace('MainNavigation');
          }}
        />
        <View style={{marginHorizontal: 18, paddingBottom: 10, marginTop: 10}}>
          <Card
            style={{
              width: '100%',
              backgroundColor: 'white',
              borderRadius: 10,
              marginTop: 10,
            }}>
            <ButtonContainer
              style={{
                borderTopRightRadius: 10,
                borderTopLeftRadius: 10,
                paddingHorizontal: 20,
              }}
              image={
                <Icons.KidSVG
                  color={Colors.secondary}
                  size={Size.scaleSize(24)}
                />
              }
              title={'Profil Akun'}
              desc={'Lihat informasi profil kamu'}
              onPress={() => props.navigation.navigate('Profile')}
            />
            <ButtonContainer
              style={{paddingHorizontal: 20}}
              styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
              image={
                <>
                  <Icons.StoreSVG
                    color={Colors.secondary}
                    size={Size.scaleSize(24)}
                  />
                  {isProfileToko && (
                    <View
                      style={{
                        width: 12,
                        height: 12,
                        backgroundColor: Colors.primary,
                        borderRadius: 12 / 2,
                        position: 'absolute',
                        top: -5,
                        left: -5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={{fontSize: 10, color: 'white', marginTop: -1}}>
                        !
                      </Text>
                    </View>
                  )}
                </>
              }
              title={'Profil Toko'}
              desc={'Pengaturan toko Anda'}
              onPress={() => props.navigation.navigate('ProfileShop')}
            />
            <ButtonContainer
              style={{paddingHorizontal: 20}}
              styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
              image={
                <>
                  <Icons.StoreSVG
                    color={Colors.secondary}
                    size={Size.scaleSize(24)}
                  />
                  {dataRekening?.length == 0 && (
                    <View
                      style={{
                        width: 12,
                        height: 12,
                        backgroundColor: Colors.primary,
                        borderRadius: 12 / 2,
                        position: 'absolute',
                        top: -5,
                        left: -5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={{fontSize: 10, color: 'white', marginTop: -1}}>
                        !
                      </Text>
                    </View>
                  )}
                </>
              }
              title={'Rekening Saya'}
              desc={'Pengaturan rekening pembayaran'}
              onPress={() => props.navigation.navigate('PaymentAccount')}
            />
            {/* <ButtonContainer 
                            style={{paddingHorizontal: 20}}
                            styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
                            image={(
                                <Icons.StoreSVG 
                                    color={Colors.secondary}
                                    size={Size.scaleSize(24)}
                                />
                            )}
                            title={"Riwayat Penarikan"}
                            desc={"Daftar penarikan yang Anda lakukan"}
                            // onPress={() => props.navigation.navigate("Address")}
                        /> */}
            <ButtonContainer
              style={{paddingHorizontal: 20}}
              styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
              image={
                <Icons.StoreSVG
                  color={Colors.secondary}
                  size={Size.scaleSize(24)}
                />
              }
              title={'Jangkauan Pengiriman Saya'}
              desc={'Pengaturan pengiriman'}
              onPress={() => {
                if (
                  dataShop?.type == 'non-material' &&
                  dataShop?.is_free_shipping == 0
                ) {
                  alert(
                    'Menu ini tidak bisa di akses jika tipe barang = non material dan gratis antar tidak ada',
                  );
                } else {
                  props.navigation.navigate('AddCoverageArea', {
                    dataShop,
                    // area: {
                    //     inCity,
                    //     aroundCity,
                    //     otherCity
                    // }
                  });
                }
              }}
            />
            <ButtonContainer
              style={{paddingHorizontal: 20}}
              styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
              image={
                <Icons.CommentInfoSVG
                  color={Colors.secondary}
                  size={Size.scaleSize(24)}
                />
              }
              title={'Tentang Kami'}
              desc={'Informasi mengenai Baba Market'}
              onPress={() => props.navigation.navigate('AboutUs')}
            />
            <ButtonContainer
              style={{paddingHorizontal: 20}}
              styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
              image={
                <Icons.OutgoingCallSVG
                  color={Colors.secondary}
                  size={Size.scaleSize(24)}
                />
              }
              title={'Hubungi Kami'}
              desc={'Hubungi admin kami untuk bantuan'}
              onPress={() => props.navigation.navigate('ContactUs')}
            />
            <ButtonContainer
              style={{paddingHorizontal: 20}}
              styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
              image={
                <Icons.ClipboardNotesSVG
                  color={Colors.secondary}
                  size={Size.scaleSize(24)}
                />
              }
              title={'Syarat & Ketentuan'}
              desc={'Pelajari syarat & ketentuan kami'}
              onPress={() => props.navigation.navigate('TermCond')}
            />
            <ButtonContainer
              style={{
                borderBottomRightRadius: 10,
                borderBottomLeftRadius: 10,
                paddingHorizontal: 20,
              }}
              styleItem={{borderTopColor: '#F2F4F5', borderTopWidth: 1}}
              image={
                <Icons.ExitSVG color={'#FF5247'} size={Size.scaleSize(24)} />
              }
              title={'Keluar'}
              desc={'Keluar dari akun Anda'}
              onPress={onLogout}
            />
          </Card>
        </View>
      </ScrollView>
    </View>
  );
};

export const screenOptions = navData => {
  return {
    header: () => (
      <Header
        navData={navData}
        isSeller
        headerLeft={
          <View>
            <Text allowFontScaling={false} style={styles.headerTitle}>
              Pengaturan
            </Text>
          </View>
        }
      />
    ),
  };
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  headerTitle: {
    color: '#FFF',
    fontFamily: Fonts.mulishExtraBold,
    fontSize: Size.scaleFont(16),
  },
  image: {
    width: Size.scaleSize(60),
    height: Size.scaleSize(60),
    borderRadius: Size.scaleSize(60),
  },
});

export default SellerScreen;
