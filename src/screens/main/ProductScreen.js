import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  StatusBar,
  FlatList,
  Platform,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Size, Fonts, Icons, Colors, Images} from '@styles/index';
import {
  HeaderTitle,
  SpinnerInputNumber,
  Button,
  TextRegular,
  TextBold,
} from '@components/global';
import Product from '@components/part/Product';
import {
  debugConsoleLog,
  inputRupiah,
  showToastMessage,
} from '@helpers/functionFormat';
import API from '@utils/services/buyer/homeProvider';
import APICHAT from '@utils/services/buyer/chatProvider';
// import Slider from '@components/global/Slider'
import ModalChatAdmin from '@components/modal/ModalChatAdmin';
import {useFocusEffect} from '@react-navigation/native';
import PushNotification from 'react-native-push-notification';
import Snackbar from 'react-native-snackbar';
import Swiper from 'react-native-swiper';
import ModalImageZoom from '@components/modal/ModalImageZoom';

const ProductScreen = props => {
  const {userData} = useSelector(state => state.auth);
  const [readMore, setReadMore] = useState(false);
  const [stock, setStock] = useState('1');
  const [loading, setLoading] = useState(false);
  const [detailProduct, setDetailProduct] = useState(null);
  const [activeIndex, setActiveIndex] = useState(0);
  const [qty, setQty] = useState(
    props.route?.params?.product?.minimum_purchase
      ? props.route?.params?.product?.minimum_purchase.toString()
      : '1',
  );
  const [shippings, setshipping] = useState([]);
  const [showChat, setShowChat] = useState(false);
  const [message, setMessage] = useState('');
  const [ekspedisi, setEkspedisi] = useState([]);
  const [jalur, setjalur] = useState();

  const [isImageZoom, setIsImageZoom] = useState(false);
  const swiperRef = useRef(null);
  // const handleButtonPress = () => {
  //   // Scroll to slide with index 2 (zero-based index)
  //   swiperRef.current.scrollTo(2, true);
  // };
  const [selectedButtonIndex, setSelectedButtonIndex] = useState(null);

  const handleButtonPress = (index, value) => {
    setSelectedButtonIndex(index);
    const indeks = dummyImage.findIndex(image => image.type === value);

    if (indeks !== -1) {
      swiperRef.current.scrollTo(indeks, true);
    }
  };

  // const getalldata = useCallback (() => {
  //     console.log('userdataku',userData.token, props.route.params.product.id);
  //     loadData();
  //   }, [loadData,props]);

  const dispatch = useDispatch();
  useFocusEffect(
    useCallback(() => {
      loadData();
    }, [loadData, props]),
  );

  const dummyImage = [
    {
      type: 'a',
      url: 'https://buffer.com/cdn-cgi/image/w=1000,fit=contain,q=90,f=auto/library/content/images/size/w1200/2023/09/instagram-image-size.jpg',
    },
    {
      type: 'b',
      url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
    },
    {
      type: 'c',
      url: 'https://www.simplilearn.com/ice9/free_resources_article_thumb/what_is_image_Processing.jpg',
    },
    {
      type: 'd',
      url: 'https://dfstudio-d420.kxcdn.com/wordpress/wp-content/uploads/2019/06/digital_camera_photo-1080x675.jpg',
    },
  ];

  const dummyVariant = [
    {
      type: 'a',
      variantName: 'Kayu',
    },
    {
      type: 'b',
      variantName: 'Besi',
    },
    {
      type: 'c',
      variantName: 'Stainless',
    },
  ];
  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.GetProductDetail(
      userData.token,
      props.route.params.product.id,
    );

    console.log('ini res ', res.data.message);
    if (res.data.message == 'Sukses') {
      setDetailProduct(res.data.data);
      console.log('ini sukses', res.data.data);
      setQty(res.data.data.min.toString());
      const nestedship = res.data.data.shipping;
      const nestedekspedisi = res.data.data.expedition;
      const land = [];
      setshipping(nestedship);
      for (let i = 0; i < nestedekspedisi.length; i++) {
        if (nestedekspedisi[i].track == 'land') {
          nestedekspedisi[i] = {...nestedekspedisi[i], track: 'Darat'};
        } else if (nestedekspedisi[i].track == 'sea') {
          nestedekspedisi[i] = {...nestedekspedisi[i], track: 'Laut'};
        } else {
          nestedekspedisi[i] = {...nestedekspedisi[i], track: 'Udara'};
        }
      }
      setEkspedisi(nestedekspedisi);
    }

    dispatch({type: 'HIDE_LOADING'});
  };

  const addToCart = async () => {
    const params = new FormData();
    params.append('product_id', detailProduct?.id);
    params.append('qty', qty);
    dispatch({type: 'SHOW_LOADING'});
    dispatch({type: 'HIDE_LOADING'});
    console.log('res cart', params);
    const res = await API.AddToCart(params, userData.token);
    console.log('res cart', res.status);
    if (res.status === 200) {
      Snackbar.show({
        text: 'Barang berhasil ditambahkan!',
        duration: Snackbar.LENGTH_LONG,
        action: {
          text: 'Lihat Keranjang',
          textColor: Colors.primary,
          onPress: () => {
            props.navigation.navigate('Cart');
          },
        },
        options: {
          marginBottom: 30,
        },
      });
      let configLocalNotif = {};
      if (Platform.OS == 'ios') {
        configLocalNotif = {
          channelId: 'baba-notif-channel',
          channelName: 'Baba Notification',
          id: parseInt(Math.random() * 1000000000, 2),
          title: 'Selamat',
          message: 'Barang berhasil ditambahkan ke keranjang belanja!',
          data: 'Test',
          userInfo: 'test',
          // largeIcon: "ic_notification_large",
          // smallIcon: "ic_notification_small_1",
          // color: Colors.PRIMARY,
        };
      } else {
        configLocalNotif = {
          channelId: 'baba-notif-channel',
          title: 'Selamat',
          message: 'Barang berhasil ditambahkan ke keranjang belanja!',
          data: 'Test',
          allowWhileIdle: true,
          // largeIcon: "ic_launcher",
          // smallIcon: "ic_launcher",
          color: Colors.primary,
        };
      }
      PushNotification.localNotification(configLocalNotif);
    } else {
      showToastMessage(res.data.data.info);
    }
    dispatch({type: 'HIDE_LOADING'});
  };
  const buyNow = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const data = new FormData();
    data.append('shop_id', detailProduct?.shop[0].id);
    data.append('product_id', detailProduct?.id.toString() + ',');
    console.log('datanya', data);
    const res = await API.Checkout(data, userData.token);
    console.log('res checkout', res.status);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      props.navigation.navigate('Payment', {
        data: res.data.data,
        product: [
          {
            checked: true,
            minimum_purchase: detailProduct?.min,
            name: detailProduct?.name,
            photo: detailProduct?.photo[0].url,
            price: detailProduct?.price,
            product_id: detailProduct?.id,
            qty: qty,
            stock: detailProduct?.stock,
            type: detailProduct?.type,
          },
        ],
        dataCheckout: data,
      });
    } else {
      if (res.status == 404) {
        return showToastMessage('hanya bisa chekout dengan toko yang sama');
      } else if (res.status === 500) {
        return showToastMessage(`Server Error code 500`);
      } else if (res.data.data.info) {
        showToastMessage(res.data.data.info);
      } else {
        showToastMessage('hanya bisa chekout dengan toko yang sama');
      }
      // if(res.status === 500) {
      //     return showToastMessage('Silahkan isi alamat pengiriman terlebih dahulu')
      // }
    }
  };

  const sendChat = async () => {
    const data = props.route.params.value;
    setShowChat(false);
    dispatch({type: 'SHOW_LOADING'});
    const formData = new FormData();
    formData.append('shop_id', detailProduct?.shop[0].id);
    formData.append('product_id', detailProduct?.id);
    formData.append('message', message);
    const res = await APICHAT.CreateChat(formData, userData.token);
    console.log('res create chat', res);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      setMessage('');
      props.navigation.navigate('Chat');
    }
  };

  // console.log('jalur',ekspedisi[0].expedition_name);

  return (
    <View style={styles.screen}>
      <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
      <ModalChatAdmin
        show={showChat}
        onClose={() => setShowChat(false)}
        daata={detailProduct}
        sendTo={detailProduct?.shop[0]?.name}
        message={message}
        setMessage={text => setMessage(text)}
        onSubmit={() => sendChat()}
      />
      <HeaderTitle title="Detail Produk" />
      <ScrollView>
        {/* <Slider
                    data={detailProduct?.photo ?? []}
                    activeIndex={activeIndex}
                    setActiveIndex={(index) => setActiveIndex(index)}
                /> */}

        <View
          style={{
            backgroundColor: '#FFF',
            paddingHorizontal: 18,
            paddingVertical: 20,
            marginBottom: 20,
          }}>
          <View
            style={{
              width: '100%',
              backgroundColor: 'white',
              paddingVertical: 20,
            }}>
            {/* {
                detailProduct ? ():null
              } */}
            {detailProduct != null ? (
              <Swiper
                ref={swiperRef}
                style={{
                  minHeight: 0,
                  padding: 0,
                  flex: 0,
                  maxHeight: 300,
                  height: 300,
                }}
                showsPagination={true}
                loop={false}
                containerStyle={{
                  minHeight: 0,
                  paddingVertical: 0,
                }}
                activeDot={
                  <View
                    style={{
                      backgroundColor: Colors.primary,
                      width: 20,
                      height: 8,
                      borderRadius: 4,
                      marginLeft: 3,
                      marginRight: 3,
                      marginBottom: 3,
                      marginTop: 3,
                      minHeight: 0,
                      minWidth: 0,
                    }}
                  />
                }>
                {detailProduct.photo.map((item, index) => (
                  <View key={index}>
                    <TouchableOpacity
                      onPress={() => setIsImageZoom(true)}
                      style={{
                        width: '100%',
                      }}>
                      <Image
                        source={{uri: item.url}}
                        style={{height: 250}}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                ))}
              </Swiper>
            ) : null}
            {detailProduct?.have_variant != 'no' &&
            detailProduct?.variant.length > 1 ? (
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  marginBottom: 10,
                  marginLeft: 5,
                }}>
                <TextBold text={'Varian '} />
                {detailProduct.variant.map((item, index) => (
                  <TextBold text={`${item.detail.length} ${item.name} `} />
                ))}
              </View>
            ) : null}
            {detailProduct?.have_variant != 'no' ? (
              <View style={{width: '100%'}}>
                {detailProduct?.variant.map((item, index) => (
                  <View>
                    <TextRegular text={item.name} style={{marginLeft: 5}} />

                    <View style={{flexDirection: 'row', marginVertical: 5}}>
                      {item.detail.map((item, index) => (
                        <TouchableOpacity
                          key={index}
                          onPress={() => handleButtonPress(index, item.type)}
                          style={{
                            marginHorizontal: 5,
                            paddingHorizontal: 10,
                            borderRadius: 10,
                            borderWidth: 1,
                            borderColor:
                              selectedButtonIndex === index
                                ? Colors.primary
                                : Colors.primary,
                            backgroundColor:
                              selectedButtonIndex === index
                                ? Colors.primary
                                : 'white',
                            paddingVertical: 6,
                          }}>
                          <View>
                            <TextRegular
                              text={item.name}
                              color={
                                selectedButtonIndex === index
                                  ? 'white'
                                  : Colors.primary
                              }
                            />
                          </View>
                        </TouchableOpacity>
                      ))}
                    </View>
                  </View>
                ))}
              </View>
            ) : null}
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icons.LocationSVG
              color={'#B4B6B8'}
              size={Size.scaleSize(18)}
              style={{marginRight: 5}}
            />
            <Text
              allowFontScaling={false}
              style={{
                fontFamily: Fonts.mulishRegular,
                color: '#B4B6B8',
                fontSize: Size.scaleFont(14),
              }}>{`${detailProduct?.shop[0]?.city}, ${detailProduct?.shop[0]?.province}`}</Text>
          </View>
          <Text
            allowFontScaling={false}
            style={{
              marginVertical: Size.scaleSize(10),
              fontFamily: Fonts.mulishExtraBold,
              fontSize: Size.scaleFont(14),
              color: '#333',
            }}>
            {detailProduct?.name}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'flex-end',
              justifyContent: 'space-between',
              marginBottom: Size.scaleSize(10),
            }}>
            <View>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishRegular,
                  fontSize: Size.scaleFont(12),
                  color: '#B4B6B8',
                }}>
                Stok
              </Text>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishBold,
                  fontSize: Size.scaleFont(14),
                  color: '#333',
                  marginTop: Size.scaleSize(5),
                }}>
                {detailProduct?.stock || null} Tersisa
              </Text>
            </View>
            <Text
              allowFontScaling={false}
              style={{
                fontFamily: Fonts.mulishBold,
                fontSize: Size.scaleFont(14),
                color: '#333',
              }}>
              Jumlah
            </Text>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            {/* <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: Colors.primary, fontSize: Size.scaleFont(16)}}>{rupiahFormat(detailProduct?.price? detailProduct?.price : '0' )}</Text> */}
            <Text
              allowFontScaling={false}
              style={{
                fontFamily: Fonts.mulishExtraBold,
                color: Colors.primary,
                fontSize: Size.scaleFont(16),
              }}>
              {detailProduct?.price ? detailProduct?.price : '0'}
            </Text>
            <SpinnerInputNumber
              count={qty}
              onChange={value => setQty(value)}
              minCount={
                props.route?.params?.product?.minimum_purchase
                  ? props.route?.params?.product?.minimum_purchase.toString()
                  : '1'
              }
              maxCount={detailProduct?.stock}
            />
          </View>
        </View>
        {/* <View style={{backgroundColor: '#FFF', paddingHorizontal: 18, paddingVertical: 20, marginBottom: 20}}>
                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: '#333', fontSize: Size.scaleFont(14), marginBottom: Size.scaleSize(10)}}>Pilih Ukuran</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap'}}>
                        <View style={{paddingVertical: 10, paddingHorizontal: 20, borderColor: '#F2F4F5', borderWidth: 1, borderRadius: 100, marginRight: 5, marginTop: 10}}>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14)}}>40 Kg</Text>
                        </View>
                        <View style={{paddingVertical: 10, paddingHorizontal: 20, borderColor: '#F2F4F5', borderWidth: 1, borderRadius: 100, marginRight: 5, marginTop: 10}}>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333', fontSize: Size.scaleFont(14)}}>50 Kg</Text>
                        </View>
                    </View>
                </View> */}
        <View
          style={{
            backgroundColor: '#FFF',
            paddingHorizontal: 18,
            paddingVertical: 20,
            marginBottom: 20,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              source={Images.Store}
              style={{width: Size.scaleSize(40), height: Size.scaleSize(40)}}
            />
            <View style={{marginLeft: Size.scaleSize(10)}}>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishExtraBold,
                  color: '#333',
                  fontSize: Size.scaleFont(14),
                }}>
                {detailProduct?.shop[0].name}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: Size.scaleSize(5),
                }}>
                <Icons.LocationSVG
                  color={'#B4B6B8'}
                  size={Size.scaleSize(18)}
                  style={{marginRight: Size.scaleSize(5)}}
                />
                <Text
                  allowFontScaling={false}
                  style={{
                    fontFamily: Fonts.mulishRegular,
                    color: '#B4B6B8',
                    fontSize: Size.scaleFont(12),
                  }}>
                  Kota {detailProduct?.shop?.province}
                </Text>
              </View>
            </View>
          </View>
          <Button
            onPress={() =>
              props.navigation.navigate('DetailStore', {
                value: detailProduct?.shop,
              })
            }
            containerStyle={{
              backgroundColor: 'white',
              borderColor: '#F2F4F5',
              borderWidth: 1,
            }}
            style={{paddingHorizontal: 20}}
            textStyle={{fontSize: Size.scaleFont(12), color: Colors.primary}}
            title="Lihat Toko"
          />
        </View>
        {detailProduct?.type == 'material' ? (
          <>
            <View
              style={{
                backgroundColor: '#FFF',
                paddingHorizontal: 18,
                paddingVertical: 20,
                marginBottom: 20,
              }}>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishExtraBold,
                  color: '#333',
                  fontSize: Size.scaleFont(16),
                  marginBottom: Size.scaleSize(20),
                }}>
                Spesifikasi
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: 10,
                }}>
                <Text
                  allowFontScaling={false}
                  style={{
                    fontFamily: Fonts.mulishRegular,
                    color: '#BDBDBD',
                    fontSize: Size.scaleFont(14),
                  }}>
                  Kategori
                </Text>
                <Text
                  allowFontScaling={false}
                  style={{
                    fontFamily: Fonts.mulishRegular,
                    color: '#333',
                    fontSize: Size.scaleFont(14),
                  }}>
                  {detailProduct?.type}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: 10,
                }}>
                <Text
                  allowFontScaling={false}
                  style={{
                    fontFamily: Fonts.mulishRegular,
                    color: '#BDBDBD',
                    fontSize: Size.scaleFont(14),
                  }}>
                  Merk
                </Text>
                <Text
                  allowFontScaling={false}
                  style={{
                    fontFamily: Fonts.mulishRegular,
                    color: '#333',
                    fontSize: Size.scaleFont(14),
                  }}>
                  {detailProduct?.merk}
                </Text>
              </View>
            </View>
            <View
              style={{
                backgroundColor: '#FFF',
                paddingHorizontal: 18,
                paddingVertical: 20,
                marginBottom: 20,
              }}>
              <FlatList
                showsHorizontalScrollIndicator={false}
                data={ekspedisi}
                keyExtractor={item => item}
                renderItem={({item}) => (
                  <>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginBottom: 10,
                      }}>
                      <Text
                        allowFontScaling={false}
                        style={{
                          fontFamily: Fonts.mulishExtraBold,
                          color: '#333',
                          fontSize: Size.scaleFont(16),
                          marginBottom: Size.scaleSize(20),
                        }}>
                        Ongkir
                      </Text>
                      <Text
                        allowFontScaling={false}
                        style={{
                          fontFamily: Fonts.mulishExtraBold,
                          color: '#333',
                          fontSize: Size.scaleFont(16),
                          marginBottom: Size.scaleSize(20),
                        }}>
                        Rp. {inputRupiah('' + item.price)}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginBottom: 10,
                      }}>
                      <Text
                        allowFontScaling={false}
                        style={{
                          fontFamily: Fonts.mulishRegular,
                          color: '#BDBDBD',
                          fontSize: Size.scaleFont(14),
                        }}>
                        Nama Ekspedisi
                      </Text>
                      <Text
                        allowFontScaling={false}
                        style={{
                          fontFamily: Fonts.mulishRegular,
                          color: '#333',
                          fontSize: Size.scaleFont(14),
                        }}>
                        {item.expedition_name}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginBottom: 10,
                      }}>
                      <Text
                        allowFontScaling={false}
                        style={{
                          fontFamily: Fonts.mulishRegular,
                          color: '#BDBDBD',
                          fontSize: Size.scaleFont(14),
                        }}>
                        Estimasi Tiba
                      </Text>
                      <Text
                        allowFontScaling={false}
                        style={{
                          fontFamily: Fonts.mulishRegular,
                          color: '#333',
                          fontSize: Size.scaleFont(14),
                        }}>
                        {item.estimation}{' '}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginBottom: 10,
                      }}>
                      <Text
                        allowFontScaling={false}
                        style={{
                          fontFamily: Fonts.mulishRegular,
                          color: '#BDBDBD',
                          fontSize: Size.scaleFont(14),
                        }}>
                        Jalur Ekspedisi
                      </Text>
                      <Text
                        allowFontScaling={false}
                        style={{
                          fontFamily: Fonts.mulishRegular,
                          color: '#333',
                          fontSize: Size.scaleFont(14),
                        }}>
                        {item.track}
                      </Text>
                    </View>
                  </>
                )}
              />
            </View>
          </>
        ) : (
          <View
            style={{
              backgroundColor: '#FFF',
              paddingHorizontal: 18,
              paddingVertical: 20,
              marginBottom: 20,
            }}>
            <Text
              allowFontScaling={false}
              style={{
                fontFamily: Fonts.mulishExtraBold,
                color: '#333',
                fontSize: Size.scaleFont(16),
                marginBottom: Size.scaleSize(20),
              }}>
              Spesifikasi
            </Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishRegular,
                  color: '#BDBDBD',
                  fontSize: Size.scaleFont(14),
                }}>
                Kategori
              </Text>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishRegular,
                  color: '#333',
                  fontSize: Size.scaleFont(14),
                }}>
                {detailProduct?.type}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishRegular,
                  color: '#BDBDBD',
                  fontSize: Size.scaleFont(14),
                }}>
                Merk
              </Text>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishRegular,
                  color: '#333',
                  fontSize: Size.scaleFont(14),
                }}>
                {detailProduct?.merk}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishRegular,
                  color: '#BDBDBD',
                  fontSize: Size.scaleFont(14),
                }}>
                Minimal Order
              </Text>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishRegular,
                  color: '#333',
                  fontSize: Size.scaleFont(14),
                }}>
                {detailProduct?.min}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishRegular,
                  color: '#BDBDBD',
                  fontSize: Size.scaleFont(14),
                }}>
                Berat
              </Text>
              <Text
                allowFontScaling={false}
                style={{
                  fontFamily: Fonts.mulishRegular,
                  color: '#333',
                  fontSize: Size.scaleFont(14),
                }}>
                {detailProduct?.weight ?? detailProduct?.weight_volume ?? '-'}
              </Text>
            </View>
          </View>
        )}

        <View
          style={{
            backgroundColor: '#FFF',
            paddingHorizontal: 18,
            paddingVertical: 20,
            marginBottom: 20,
          }}>
          <Text
            allowFontScaling={false}
            style={{
              fontFamily: Fonts.mulishExtraBold,
              color: '#333',
              fontSize: Size.scaleFont(16),
              marginBottom: Size.scaleSize(20),
            }}>
            Informasi Produk
          </Text>
          <Text
            allowFontScaling={false}
            style={{
              fontFamily: Fonts.mulishRegular,
              color: '#333',
              fontSize: Size.scaleFont(14),
            }}>
            {readMore
              ? detailProduct?.description
              : detailProduct?.description?.substr(0, 250)}
          </Text>
          {detailProduct?.description?.length > 250 && (
            <TouchableOpacity
              style={{marginTop: Size.scaleSize(10), alignSelf: 'flex-start'}}
              onPress={() => setReadMore(!readMore)}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  allowFontScaling={false}
                  style={{
                    fontFamily: Fonts.mulishBold,
                    color: Colors.primary,
                    fontSize: Size.scaleFont(14),
                  }}>
                  {readMore ? 'Sembunyikan' : 'Baca Selengkapnya'}
                </Text>
                <Icons.ChevronDown
                  size={Size.scaleSize(24)}
                  color={Colors.primary}
                  style={[
                    {marginLeft: Size.scaleSize(5)},
                    readMore && {transform: [{rotate: '180deg'}]},
                  ]}
                />
              </View>
            </TouchableOpacity>
          )}
        </View>
        <View
          style={{
            backgroundColor: '#FFF',
            paddingVertical: 20,
            marginBottom: 20,
          }}>
          <Text
            allowFontScaling={false}
            style={{
              fontFamily: Fonts.mulishExtraBold,
              color: '#333',
              fontSize: Size.scaleFont(16),
              marginBottom: Size.scaleSize(20),
              marginHorizontal: 18,
            }}>
            Produk Serupa
          </Text>
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={detailProduct?.related_products}
            keyExtractor={item => item.id}
            renderItem={itemData => (
              <Product
                containerStyle={
                  itemData.index ===
                    detailProduct?.related_products.length - 1 && {
                    marginRight: Size.scaleSize(15),
                  }
                }
                data={itemData.item}
                onPress={() =>
                  props.navigation.push('Product', {
                    product: itemData.item,
                  })
                }
              />
            )}
          />
        </View>
      </ScrollView>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-around',
          paddingVertical: 10,
          backgroundColor: '#FFF',
        }}>
        <Button
          containerStyle={{width: '15%', backgroundColor: '#FFF'}}
          style={{
            paddingVertical: Size.scaleSize(8),
            borderWidth: 2,
            borderColor: Colors.primary,
          }}
          textComponent={
            <Icons.ChatSVG color={Colors.primary} size={Size.scaleSize(24)} />
          }
          onPress={() => setShowChat(true)}
        />
        <Button
          containerStyle={{width: '39%'}}
          style={{borderWidth: 2, borderColor: Colors.primary}}
          onPress={addToCart}
          textComponent={
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingVertical: Size.scaleSize(8),
              }}>
              <Icons.CartSVG
                color={'#FFF'}
                size={Size.scaleSize(16)}
                style={{marginRight: Size.scaleSize(10)}}
              />
              <Text
                allowFontScaling={false}
                style={{
                  color: '#FFF',
                  fontFamily: Fonts.mulishExtraBold,
                  fontSize: Size.scaleFont(14),
                }}>
                Keranjang
              </Text>
            </View>
          }
        />
        <Button
          containerStyle={{width: '39%'}}
          style={{borderWidth: 2, borderColor: Colors.primary}}
          onPress={buyNow}
          textComponent={
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingVertical: Size.scaleSize(8),
              }}>
              <Icons.CartSVG
                color={'#FFF'}
                size={Size.scaleSize(16)}
                style={{marginRight: Size.scaleSize(10)}}
              />
              <Text
                allowFontScaling={false}
                style={{
                  color: '#FFF',
                  fontFamily: Fonts.mulishExtraBold,
                  fontSize: Size.scaleFont(14),
                }}>
                Beli Langsung
              </Text>
            </View>
          }
        />
      </View>
      <ModalImageZoom
        show={isImageZoom}
        onClose={() => setIsImageZoom(false)}
        imageUrl={detailProduct?.photo || []}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F7F9F8',
  },
});

export const screenOptions = navData => {
  return {
    headerTitle: 'Detail Produk',
    headerTitleAlign: 'left',
    headerTitleStyle: {
      fontFamily: Fonts.mulishExtraBold,
      fontSize: Size.scaleFont(16),
      color: '#333333',
    },
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft: Size.scaleSize(5)}}
        onPress={() => navData.navigation.goBack()}>
        <View style={{padding: Size.scaleSize(10)}}>
          <Icons.ChevronLeft color="#333" size={Size.scaleSize(18)} />
        </View>
      </TouchableOpacity>
    ),
  };
};

export default ProductScreen;
