import React, {useCallback, useEffect, useState} from 'react';
import { View, Text, Image, FlatList, StyleSheet, TouchableOpacity, TouchableHighlight, StatusBar, Dimensions } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { Header, InputText } from '@components/global';
import { Size, Fonts, Icons, Images, Colors } from '@styles/index';
import API from '@utils/services/buyer/chatProvider'
import APISELLER from '@utils/services/seller/chatProvider'
import moment from 'moment'
import 'moment/locale/id'
import {IconAsset} from '@utils/index'
import {showToastMessage} from '@helpers/functionFormat'
import {useFocusEffect, useIsFocused} from '@react-navigation/native'

import ModalChatAdmin from '@components/modal/ModalChatAdmin';

const ChatScreen = props => {
    const isFocused = useIsFocused()
    const { userData } = useSelector(state => state.auth);
    const dispatch = useDispatch();

    const [listChat, setListChat] = useState([])
    const [message, setMessage] = useState('')
    const [modalChat, setModalChat] = useState(false)
    const [search, setSearch] = useState('')

    const data = [{
        logo: Images.Store,
        name: 'UD Jaya Abadi',
        message: 'Apakah stok masih ada?',
        time: '10:32',
        isRead: true,
        count: 0
    }, {
        logo: Images.Store,
        name: 'Sejahtera Bangun',
        message: 'Tersisa 5, silahkan diorder kak.',
        time: '08:32',
        isRead: false,
        count: 1
    }, {
        logo: Images.Store,
        name: 'UD Jaya Abadi',
        message: 'Apakah stok masih ada?',
        time: '10:32',
        isRead: true,
        count: 0
    }, {
        logo: Images.Store,
        name: 'UD Jaya Abadi',
        message: 'Apakah stok masih ada?',
        time: '10:32',
        isRead: true,
        count: 0
    }]

    const getData = async () => {
        dispatch({type: "SHOW_LOADING"});
        var res = {}
        if(userData.is_seller === 'no') {
            res = await API.GetDataChat(userData.token)
        } else {
            res = await APISELLER.GetDataChat(userData.token)
        }
        console.log('res chat', res);
        dispatch({type: "HIDE_LOADING"});
        if(res.status === 200) {
            return setListChat(res.data.data)
        }
        // if(res.status === 500) {
        //     return showToastMessage('Server Error silahkan coba lagi')
        // }
    }

    const sentMessageToAdmin = async () => {
        setModalChat(false)
        const formdata = new FormData();
        formdata.append('message', message)
        formdata.append('role', userData.is_seller === "no" ? "buyer" : "seller")
        const res = await API.SendChatToAdmin(formdata, userData.token)
        console.log('res chat admin', res);
        if(res.status === 200) {
            return showToastMessage('Pesan anda telah dikirim, admin akan memproses pesan anda')
        } else {
            return showToastMessage('Pesan gagal dikirim, silahkan coba lagi')
        }
    }

    const filterChat = React.useCallback(() => {
        var data = []
        if(search !== '') {
            data = listChat.filter((value) => { return value.name.toLowerCase().includes(search.toLowerCase()) })
        } else {
            data = [...listChat]
        }
        return data;

    },[listChat, search])

    useFocusEffect(
        useCallback(() => {
            getData()
            console.log('user data chat', userData);
        }, [isFocused])
      )

    console.log('tropps',props.route.name);
    return (
        <View style={styles.screen}>
            <StatusBar 
                backgroundColor={Colors.primary}
                barStyle="light-content"
            />
            <ModalChatAdmin
                show={modalChat}
                onClose={() => setModalChat(false)}
                message={message}
                setMessage={(text) => setMessage(text)}
                onSubmit={() => {
                    sentMessageToAdmin()
                }}
            />
            <InputText 
                containerStyle={{marginHorizontal: Size.scaleSize(18), marginVertical: Size.scaleSize(10)}}
                containerInputStyle={{backgroundColor: 'white', borderColor: '#F2F4F5'}}
                placeholder="Cari Pesan"
                value={search}
                onChangeText={(text) => setSearch(text)}
                rightIcon={<Icons.SearchSVG 
                    size={24}
                    color="black"
                />}
            />
            <FlatList 
                data={filterChat()}
                keyExtractor={(item, index) => index.toString()}
                renderItem={(itemData) => (
                    <TouchableHighlight onPress={() => props.navigation.navigate('ChatDetail', {chat: itemData.item})} underlayColor="#efefef">
                        <View style={{flexDirection: 'row', alignItems: 'center', paddingVertical: Size.scaleSize(15), borderBottomColor: '#F2F4F5', borderBottomWidth: 1, marginHorizontal: Size.scaleSize(15) }}>
                            <View style={{flex: 1}}>
                                <Image 
                                    source={{uri: itemData.item.photo}}
                                    style={{width: Size.scaleSize(50), height: Size.scaleSize(50)}}
                                />
                            </View>
                            <View style={{flex: 4 }}>
                                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#333333', fontSize: Size.scaleFont(14), marginBottom: Size.scaleSize(10)}}>{itemData.item.name}</Text>
                                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, color: itemData.item.isRead? '#B4B6B8' : '#333333', fontSize: Size.scaleFont(12)}}>{itemData.item.last_message}</Text>
                            </View>
                            <View style={{alignItems: 'center', alignSelf: 'flex-start'}}>
                                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(12), marginTop: Size.scaleSize(5), marginBottom: 5}}>{moment(itemData.item.datetime).format('HH:mm')}</Text>
                                {/* {!itemData.item.isRead && <View style={{backgroundColor: Colors.secondary, width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center'}}>
                                    <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: '#FFF', fontSize: Size.scaleFont(12)}}>{itemData.item.count}</Text>
                                </View>} */}
                            </View>
                        </View>
                    </TouchableHighlight>
                )}
            />
            <TouchableOpacity
                style={styles.btnChat}
                onPress={() => setModalChat(true)}
            >
                <Image
                    source={IconAsset.IcChat}
                    style={{width: 20, height: 20, tintColor: '#fff'}}
                />
            </TouchableOpacity>
        </View>
    )
}

export const screenOptions = navData =>{

    return {
        header: () => (
            <Header 
                navData={navData}
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={styles.headerTitle}>Obrolan</Text>
                    </View>
                )}
            />
        )
    }
}
export const screenOptionstwo = navData => {
    return {
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        ),
        headerTitle: () => (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, marginLeft: Size.scaleSize(10), width: Size.scaleSize(Dimensions.get('screen').width * 0.5), fontSize: Size.scaleFont(14)}} numberOfLines={1}>Obrolan</Text>
            </View>
        ),
        headerTitleAlign: 'left'
    }
}

const styles = StyleSheet.create({
    btnChat: {
        position: 'absolute',
        bottom: 20,
        right: 10,
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        alignItems: 'center'
    },
    screen: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    headerTitle: {
        color: '#FFF', 
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16)
    }
})

export default ChatScreen;