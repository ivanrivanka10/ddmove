import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  Image,
  Platform,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  FlatList,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Pressable,
} from 'react-native';

import {Images, Size, Fonts, Icons, Colors} from '@styles/index';
import {
  Button,
  Icon,
  InputText,
  ReversedList,
  TextBold,
} from '@components/global';
import API from '@utils/services/buyer/chatProvider';
import API_PRODUK from '@utils/services/buyer/homeProvider';
import APISELLER from '@utils/services/seller/chatProvider';
import moment from 'moment';
import 'moment/locale/id';
import {useSelector, useDispatch} from 'react-redux';
import {IconAsset} from '@utils/index';
import {Keyboard} from 'react-native';
import {useNavigation} from '@react-navigation/native';
// import {Pusher} from '@pusher/pusher-websocket-react-native'
// import {PUSHER_API_KEY, PUSHER_CLUSTER} from '@utils/env'

// const pusher = Pusher.getInstance();

const ChatDetailScreen = props => {
  const {userData} = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const [sendText, setSendText] = useState('');
  const [detailChat, setDetailChat] = useState({});
  const [listMessage, setListMessage] = useState([]);
  const [firstLoad, setFirstLoad] = useState(true);
  const [count, setCount] = useState(0);
  const scrollViewRef = useRef();
  const user = useSelector(state => state.auth.userData);
  const chatUser = ['Apakah barang ini ada ?', 'Apakah bisa kirim hari ini ?'];
  const chatSeller = [
    'Barang ini ready stok, silakan order',
    'Mohon maaf barang ini sementara kosong',
    'Kami cek ketersediaan stok terlebih dahulu, segera kami infokan kembali',
  ];

  const getDetailChat = async () => {
    // dispatch({type: "SHOW_LOADING"});
    const id = props.route.params.chat.id;
    var res = {};
    if (userData.is_seller === 'no') {
      res = await API.GetDetailChat(id, userData.token);
    } else {
      res = await APISELLER.GetDetailChat(id, userData.token);
    }
    // dispatch({type: "HIDE_LOADING"});
    setFirstLoad(false);
    if (res.status === 200) {
      setDetailChat(res.data.data);
      setListMessage(res.data.data.message.reverse());
      // subscribeChat(res.data.data)
    }
  };

  const sendChat = async () => {
    var chat = [...detailChat.message];
    var newChat = {
      datetime: new Date(),
      message: sendText,
      sender: 'you',
    };
    chat.push(newChat);
    console.log('cek chat', chat);
    setDetailChat({...detailChat, message: chat.reverse()});
    const id = props.route.params.chat.id;
    const data = new FormData();
    data.append('message', sendText);
    var res = {};
    if (userData.is_seller === 'no') {
      res = await API.SendChat(data, id, userData.token);
    } else {
      res = await APISELLER.SendChat(data, id, userData.token);
    }
    console.log('res send chat', res.data);
    setSendText('');
    scrollViewRef.current.scrollToEnd({animated: true});
    Keyboard.dismiss();
  };

  const sendChatWithParams = async (x) => {
    var chat = [...detailChat.message];
    var newChat = {
      datetime: new Date(),
      message: x,
      sender: 'you',
    };
    chat.push(newChat);
    console.log('cek chat', chat);
    setDetailChat({...detailChat, message: chat.reverse()});
    const id = props.route.params.chat.id;
    const data = new FormData();
    data.append('message', x);
    var res = {};
    if (userData.is_seller === 'no') {
      res = await API.SendChat(data, id, userData.token);
    } else {
      res = await APISELLER.SendChat(data, id, userData.token);
    }
    console.log('res send chat', res.data);
    setSendText('');
    scrollViewRef.current.scrollToEnd({animated: true});
    Keyboard.dismiss();
  };

  // const subscribeChat = async (data) => {
  //     dispatch({type: "SHOW_LOADING"});
  //     await pusher.init({
  //         apiKey: PUSHER_API_KEY,
  //         cluster: PUSHER_CLUSTER
  //     });
  //     const channel = await pusher.subscribe({
  //         channelName: `chat-detail.${data.conversation_id}`,
  //         onEvent: (e) => {
  //             console.log(`Got channel event: ${e}`);
  //         },
  //         onSubscriptionError: (e) => {
  //             console.log(`Got error channel event: ${e}`);
  //         },
  //         onSubscriptionSucceeded: (e) => {
  //             console.log(`Got channel sukses: `, e);
  //         }
  //     })
  //     await pusher.connect();
  //     dispatch({type: "HIDE_LOADING"});
  // }

  useEffect(() => {
  }, []);

  useEffect(() => {
    // getDetailChat()
  }, [count]);

  useEffect(() => {
    getDetailChat();
    // if (props.route.name === 'ChatDetail') {
    //   setInterval(() => {
    //     // setCount(prev => prev+1)
    //     getDetailChat();
    //   }, 10000);
    // }
  }, []);

  return (
    <View style={{width: '100%', height: '100%'}}>
      <StatusBar barStyle="dark-content" backgroundColor="#FFF" />
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          height: '10%',
          position: 'relative',

          paddingVertical: 14,
          backgroundColor: '#FFF',
        }}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={{
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={IconAsset.ChevronLeft}
            style={{width: 25, height: 25, tintColor: Colors.blackBase}}
          />
        </TouchableOpacity>
        <View
          style={{
            marginLeft: 15,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Image
            source={{uri: props.route.params.chat.photo}}
            style={{
              width: 35,
              height: 35,
              borderRadius: 35 / 2,
              resizeMode: 'cover',
            }}
          />
          <TextBold
            text={props.route.params.chat.name}
            color={Colors.blackBase}
            style={{marginLeft: 10}}
          />
        </View>
      </View>
      <KeyboardAvoidingView
        enabled
        behavior="padding"
        keyboardVerticalOffset={Platform.OS === 'android' ? -250 : 40}
        keyboardShouldPersistTaps={'always'}
        style={styles.screen}>
        <View style={styles.chatContainer}>
          <ScrollView
            ref={scrollViewRef}
            keyboardShouldPersistTaps={'always'}
            showsVerticalScrollIndicator={false}
            onContentSizeChange={() =>
              scrollViewRef.current.scrollToEnd({animated: true})
            }
            style={{position: 'relative'}}>
            <View>
              <ReversedList
                data={
                  Object.keys(detailChat).length > 0
                    ? detailChat.message.reverse()
                    : []
                }
                keyExtractor={(item, index) => index.toString()}
                renderItem={itemData => {
                  if (itemData.item.sender === 'you') {
                    return (
                      <>
                        <RenderItemProduk id={itemData?.item?.product?.id} />
                        <View
                          style={{
                            marginHorizontal: 15,
                            marginVertical: 10,
                            marginLeft: 30,
                            alignSelf: 'flex-end',
                          }}>
                          <View
                            style={{
                              backgroundColor: Colors.secondary,
                              padding: 10,
                              borderTopRightRadius: 8,
                              borderTopLeftRadius: 8,
                              borderBottomLeftRadius: 8,
                            }}>
                            <Text
                              selectable
                              allowFontScaling={false}
                              style={{
                                fontFamily: Fonts.mulishRegular,
                                fontSize: Size.scaleFont(14),
                                color: '#FFF',
                              }}>
                              {itemData.item.message}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              marginTop: 4,
                              alignSelf: 'flex-end',
                            }}>
                            <Icons.CheckSVG
                              size={Size.scaleSize(20)}
                              color={
                                itemData.item.isRead
                                  ? Colors.secondary
                                  : '#B4B6B8'
                              }
                              style={{marginRight: 5}}
                            />
                            <Text
                              allowFontScaling={false}
                              style={{
                                textAlign: 'right',
                                color: '#B4B6B8',
                                fontFamily: Fonts.mulishRegular,
                                fontSize: Size.scaleFont(12),
                              }}>
                              {moment(itemData.item.datetime).format('HH:mm')}
                            </Text>
                          </View>
                        </View>
                      </>
                    );
                  } else {
                    return (
                      <>
                        <RenderItemProduk id={itemData?.item?.product?.id} />
                        <View
                          style={{
                            marginHorizontal: 15,
                            marginVertical: 10,
                            marginRight: 30,
                            alignSelf: 'flex-start',
                          }}>
                          <View
                            style={{
                              backgroundColor: '#757575',
                              padding: 10,
                              borderTopRightRadius: 8,
                              borderTopLeftRadius: 8,
                              borderBottomRightRadius: 8,
                            }}>
                            <Text
                              selectable
                              allowFontScaling={false}
                              style={{
                                fontFamily: Fonts.mulishRegular,
                                fontSize: Size.scaleFont(14),
                                color: '#fff',
                              }}>
                              {itemData.item.message}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              marginTop: 4,
                              alignSelf: 'flex-start',
                            }}>
                            <Text
                              allowFontScaling={false}
                              style={{
                                textAlign: 'left',
                                color: '#B4B6B8',
                                fontFamily: Fonts.mulishRegular,
                                fontSize: Size.scaleFont(12),
                              }}>
                              {moment(itemData.item.datetime).format('HH:mm')}
                            </Text>
                            <Icons.CheckSVG
                              size={Size.scaleSize(20)}
                              color={
                                itemData.item.isRead
                                  ? Colors.secondary
                                  : '#B4B6B8'
                              }
                              style={{marginLeft: 5}}
                            />
                          </View>
                        </View>
                      </>
                    );
                  }
                }}
              />
            </View>
          </ScrollView>
        </View>

        <View style={{position: 'absolute', bottom: '10%', width: '100%'}}>
          <FlatList
            showsHorizontalScrollIndicator={false}
            style={{height: 50, marginTop: 16, paddingHorizontal: 12}}
            horizontal={true}
            data={
              user?.is_seller == 'no' || user?.is_seller == undefined
                ? chatUser
                : chatSeller
            }
            renderItem={({item}) => {
              return (
                <TouchableOpacity
                  onPress={async () => {
                    sendChatWithParams(item)
                  }}>
                  <View
                    style={{
                      height: 35,
                      borderRadius: 30,
                      marginEnd: 8,
                      backgroundColor: Colors.primary,
                      paddingHorizontal: 12,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <TextBold text={item} size={12} color="#fff" />
                  </View>
                </TouchableOpacity>
              );
            }}
            keyExtractor={item => item.toString()}
          />
        </View>

        <View style={styles.chatInputContainer}>
          <InputText
            containerStyle={{width: '80%'}}
            placeholder="Tulis Pesan.."
            multiline={true}
            value={sendText}
            onChangeText={text => setSendText(text)}
          />

          <Button
            onPress={() => {
              sendChat();
            }}
            disabled={!sendText}
            containerStyle={{
              backgroundColor: !sendText ? '#ccc' : Colors.primary,
              borderRadius: 8,
            }}
            style={{
              backgroundColor: !sendText ? '#ccc' : Colors.primary,
              paddingVertical: Size.scaleSize(10),
              paddingHorizontal: Size.scaleSize(10),
              borderRadius: 8,
            }}
            textComponent={
              <Icons.SendSVG size={Size.scaleSize(22)} color={'#FFF'} />
            }
          />
        </View>
      </KeyboardAvoidingView>
    </View>

    // <SafeAreaView style={styles.screen}>
    //     <StatusBar barStyle="dark-content" backgroundColor="#FFF" />
    //     <View style={styles.productContainer}>
    //         <View style={styles.productImageContainer}>
    //             <Image
    //                 source={Images.Product}
    //                 style={styles.productImage}
    //             />

    //         </View>
    //         <View style={styles.productDetailContainer}>
    //             <Text allowFontScaling={false} style={styles.productName}>Semen Gresik 40-50 kg</Text>
    //             <Text allowFontScaling={false} style={styles.productPrice}>Rp 57.000</Text>
    //             <View style={styles.productDetailBtn}>
    //                 <Button
    //                     containerStyle={{
    //                         backgroundColor: '#FFF',
    //                         borderRadius: 8
    //                     }}
    //                     style={{
    //                         backgroundColor: '#FFF',
    //                         borderWidth: 2,
    //                         borderColor: Colors.primary,
    //                         paddingVertical: Size.scaleSize(8),
    //                         paddingHorizontal: Size.scaleSize(8),
    //                         borderRadius: 8
    //                     }}
    //                     textComponent={(
    //                         <Icons.CartSVG
    //                             size={Size.scaleSize(22)}
    //                             color={Colors.primary}
    //                         />
    //                     )}
    //                 />
    //                 <Button
    //                     containerStyle={{marginLeft: Size.scaleSize(10), borderRadius: 8}}
    //                     style={{
    //                         paddingHorizontal: Size.scaleSize(25),
    //                         borderRadius: 8,
    //                         borderWidth: 2,
    //                         borderColor: Colors.primary
    //                     }}
    //                     textStyle={{fontSize: Size.scaleSize(12), paddingVertical: 12}}
    //                     title="Beli Sekarang"
    //                 />
    //             </View>
    //         </View>
    //     </View>
    //     <View style={styles.chatContainer}>
    //         <FlatList

    //         />

    //         <ScrollView style={{paddingBottom: 80}}>
    //         <Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text>
    //             <Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text><Text>asdssasdsasdas</Text>
    //             <Text style={{color: 'red'}}>asdssasdsasdas</Text>
    //         </ScrollView>
    //     </View>
    //     <View ref={vuewRef}>
    //     <KeyboardAwareScrollView

    //         // extraScrollHeight={40}
    //         // extraHeight={0}
    //         enableOnAndroid={true}
    //     >

    //     <Animated.View style={styles.chatInputContainer} ref={innerRef}>

    //         <InputText
    //             // innerRef={innerRef}
    //             containerStyle={{width: '80%'}}
    //             placeholder="Tulis Pesan.."
    //             multiline={true}
    //             // style={{marginBottom: 16}}
    //             // containerInputStyle={{borderWidth: 0}}
    //             // style={{paddingVertical: 0}}
    //             // style={{paddingBottom: 20}}
    //         />

    //         <Button
    //             disabled={false}
    //             containerStyle={{
    //                 backgroundColor: true? '#ccc' : Colors.primary,
    //                 borderRadius: 8
    //             }}
    //             style={{
    //                 backgroundColor: true? '#ccc' : Colors.primary,
    //                 paddingVertical: Size.scaleSize(10),
    //                 paddingHorizontal: Size.scaleSize(10),
    //                 borderRadius: 8
    //             }}
    //             textComponent={(
    //                 <Icons.SendSVG
    //                     size={Size.scaleSize(22)}
    //                     color={'#FFF'}
    //                 />
    //             )}
    //         />
    //     </Animated.View>

    //     </KeyboardAwareScrollView>
    //     </View>

    // </SafeAreaView>
  );
};

function RenderItemProduk({id}) {
  const {userData} = useSelector(state => state.auth);
  const [produk, setProduk] = useState({});
  const [fotoProduk, setFotoProduk] = useState('');
  const [loading, setLoading] = useState(true);
  const navigation = useNavigation();

  React.useEffect(() => {
    getDetailProduk();
  }, []);

  const getDetailProduk = async () => {
    const response = await API_PRODUK.GetProductDetail(userData.token, id);
    if (response.status == 200) {
      setLoading(false);
      setFotoProduk(response.data.data.photo[0].url);
      setProduk(response?.data?.data);
    }
  };

  if (loading) {
    return <></>;
  }

  return (
    <Pressable
      onPress={() => navigation.navigate('Product', {product: produk})}
      style={styles.productContainer}>
      <View style={styles.productImageContainer}>
        <Image source={{uri: fotoProduk}} style={styles.productImage} />
      </View>
      <View style={styles.productDetailContainer}>
        <Text allowFontScaling={false} style={styles.productName}>
          {produk?.name ?? '-'}
        </Text>
        <Text allowFontScaling={false} style={styles.productPrice}>
          {produk?.price_text ?? '-'}
        </Text>
      </View>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    position: 'relative',
  },
  productContainer: {
    backgroundColor: '#FFF',
    marginHorizontal: Size.scaleSize(20),
    margin: Size.scaleSize(20),
    paddingHorizontal: Size.scaleSize(12),
    paddingVertical: Size.scaleSize(18),
    borderRadius: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  productImageContainer: {
    borderColor: '#F2F4F5',
    width: Size.scaleSize(64),
    height: Size.scaleSize(64),
    borderRadius: 8,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  productImage: {
    width: Size.scaleSize(60),
    height: Size.scaleSize(60),
  },
  productDetailContainer: {
    marginLeft: Size.scaleSize(10),
    width: '75%',
  },
  productDetailBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: Size.scaleSize(10),
  },
  productName: {
    color: '#333',
    fontFamily: Fonts.mulishExtraBold,
    fontSize: Size.scaleSize(14),
  },
  productPrice: {
    fontFamily: Fonts.mulishRegular,
    fontSize: Size.scaleFont(13),
    marginTop: Size.scaleSize(5),
  },
  chatContainer: {
    height: '89%',
    paddingBottom: Size.scaleSize(20),
  },
  chatInputContainer: {
    bottom: Size.scaleSize(0),
    flexDirection: 'row',
    paddingHorizontal: Size.scaleSize(16),
    justifyContent: 'space-between',
    width: '100%',
  },
});

export const screenOptions = navData => {
  return {
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft: Size.scaleSize(5)}}
        onPress={() => navData.navigation.goBack()}>
        <View style={{padding: Size.scaleSize(10)}}>
          <Icons.ChevronLeft color="#333" size={Size.scaleSize(18)} />
        </View>
      </TouchableOpacity>
    ),
    headerTitle: () => (
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Image
          source={Images.Store}
          style={{width: Size.scaleSize(32), height: Size.scaleSize(32)}}
        />
        <Text
          allowFontScaling={false}
          style={{
            fontFamily: Fonts.mulishExtraBold,
            marginLeft: Size.scaleSize(10),
            width: Size.scaleSize(Dimensions.get('screen').width * 0.5),
            fontSize: Size.scaleFont(14),
          }}
          numberOfLines={1}>
          Sejahtera Bangun
        </Text>
      </View>
    ),
    headerTitleAlign: 'left',
  };
};

export default ChatDetailScreen;
