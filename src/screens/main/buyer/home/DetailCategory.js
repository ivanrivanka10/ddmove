import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Text,
} from 'react-native';
import {Size, Icons, Fonts, Images, Colors} from '@styles/index';
import Store from '@components/part/Store';
import {Loader, TextBold, TextRegular, HeaderTitle} from '@components/global';
import {useSelector} from 'react-redux';
import API from '@utils/services/buyer/homeProvider';
import ModalSortBy from './components/ModalSortBy';
import ModalFilter from './components/ModalFilter';

const dataSortBy = [
  {
    title: 'Rekomendasi',
    value: 'rekomendasi',
  },
  {
    title: 'Produk Terlaris',
    value: 'terlaris',
  },
  {
    title: 'Produk Terbaru',
    value: 'terbaru',
  },
  {
    title: 'Harga: Rendah ke Tinggi',
    value: 'harga_rendah',
  },
  {
    title: 'Harga: Tinggi ke Rendah',
    value: 'harga_tinggi',
  },
  {
    title: 'Nama: A-Z',
    value: 'a-z',
  },
  {
    title: 'Nama: Z-A',
    value: 'z-a',
  },
];

const DetailCategory = ({navigation, route}) => {
  const {userData} = useSelector(state => state.auth);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [pickedSortBy, setPickedSortBy] = useState(0);
  const [pickedCategory, setPickedCategory] = useState(undefined);
  const [showModalSortBy, setShowModalSortBy] = useState(false);
  const [showModalFilter, setShowModalFilter] = useState(false);
  const [dataAllCategory, setDataAllCategory] = useState([]);
  const [lowestPrice, setLowestPrice] = React.useState('');
  const [highestPrice, setHighestPrice] = React.useState('');
  const [subCategory, setSubCategory] = useState([]);
  const [subsubCategory, setSubSubCategory] = useState([]);
  const [pickedSubCategory, setPickedSubCategory] = useState([]);
  const [pickedSubSubCategory, setPickedSubSubCategory] = useState([]);

  const getProduct = async () => {
    setLoading(true);
    const params = {};
    const objCategories = {
      main: route.params.item.id,
    };
    var categories = [route.params.item.id];
    if (pickedSortBy !== undefined) {
      params.sort_by = dataSortBy[pickedSortBy].value;
    }
    if (lowestPrice !== '') {
      if (lowestPrice.length > 3) {
        params.harga_rendah = lowestPrice.split('.').join('');
      } else {
        params.harga_rendah = lowestPrice;
      }
    }
    if (highestPrice !== '') {
      if (highestPrice.length > 3) {
        params.harga_tinggi = highestPrice.split('.').join('');
      } else {
        params.harga_tinggi = highestPrice;
      }
    }
    if (pickedSubCategory.length > 0 && pickedSubSubCategory.length > 0) {
      objCategories.sub = pickedSubCategory[0].id;
      objCategories.subsub = pickedSubSubCategory[0].id;
    }
    params.category = JSON.stringify(objCategories);
    // console.log('cek categories', categories);
    // console.log('cek categories join', categories.join(","));
    // console.log('cek categories convert' , `${JSON.parse(`${categories.join(",")}`)}`);
    console.log('cek params', params);
    const res = await API.GetProduct(userData.token, params);
    console.log('ini respon category', res);
    setLoading(false);
    console.log('res product', res);
    if (res.status === 200) {
      // setLoading(false);
      setData(res.data.data);
      // getAllCategory();
    } else {
      console.log('alert', res.status);
      setLoading(false);
    }
  };

  const getAllCategory = async () => {
    setLoading(true);
    const params = {
      group: route.params.item.type,
      type: 'main',
    };
    const res = await API.GetAllCategory(userData.token, params);
    console.log('res all cat', res);
    if (res.status == 200) {
      setLoading(false);
      setDataAllCategory(res.data.data);
    } else {
      setLoading(false);
    }
  };

  const getSubCategory = async id => {
    const paramsSub = {
      group: route.params.item.type,
      type: 'sub',
      id: id,
    };
    const res = await API.GetAllCategory(userData.token, paramsSub);
    console.log('=== res sub', res);
    setLoading(false);
    var arrCat = [];
    if (res.status === 200) {
      for (let i = 0; i < res.data.data.length; i++) {
        arrCat.push({
          id: res.data.data[i].id,
          name: res.data.data[i].name,
        });
      }
      setSubCategory(arrCat);
    }
  };

  const getSubSubCategory = async id => {
    const paramsSub = {
      group: route.params.item.type,
      type: 'sub-sub',
      id: id,
    };
    const res = await API.GetAllCategory(userData.token, paramsSub);
    console.log('res subsub', res);
    var arrCat = [];
    if (res.status === 200) {
      for (let i = 0; i < res.data.data.length; i++) {
        arrCat.push({
          id: res.data.data[i].id,
          name: res.data.data[i].name,
        });
      }
      setSubSubCategory(arrCat);
    }
  };

  useEffect(() => {
    // console.log('props category', props);
    getProduct();
    getSubCategory(route.params.item.id);
  }, [pickedSortBy]);
  console.log('props detail category', route);
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Loader show={loading} />
      <HeaderTitle goBack={() => navigation.goBack()} title="Detail Kategori" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Store
            image={route?.params?.item?.icon}
            label={route.params.item.name}
          />
          <View style={styles.btnContainer}>
            <TouchableOpacity
              style={{
                marginHorizontal: Size.scaleSize(5),
                borderRadius: 10,
                padding: Size.scaleSize(14),
                backgroundColor: 'white',
              }}
              onPress={() => setShowModalSortBy(true)}
              // underlayColor={Colors.opacityColor(Colors.primary, 0.2)}
            >
              <Icons.SortSVG size={Size.scaleSize(24)} color={Colors.primary} />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginHorizontal: Size.scaleSize(5),
                borderRadius: 10,
                padding: Size.scaleSize(14),
                backgroundColor: 'white',
              }}
              onPress={() => setShowModalFilter(true)}
              // underlayColor={Colors.opacityColor(Colors.primary, 0.2)}
            >
              <Icons.FilterSVG
                size={Size.scaleSize(24)}
                color={Colors.primary}
              />
            </TouchableOpacity>
          </View>
        </View>
        <FlatList
          contentContainerStyle={{
            paddingBottom: 100,
            marginTop: 20,
            paddingHorizontal: 15,
          }}
          ListHeaderComponent={
            <Text allowFontScaling={false} style={styles.title}>
              {dataSortBy[pickedSortBy].title}
            </Text>
          }
          data={data}
          columnWrapperStyle={{
            marginTop: 10,
            justifyContent: 'space-between',
            marginHorizontal: 0,
          }}
          keyExtractor={(item, index) => index.toString()}
          numColumns={2}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.cardItem}
                onPress={() => navigation.navigate('Product', {product: item})}>
                <Image
                  source={{uri: item.photo}}
                  style={{width: '100%', height: 132, resizeMode: 'contain'}}
                />
                <View
                  style={{
                    paddingHorizontal: 15,
                    height: 158,
                    justifyContent: 'space-between',
                    paddingVertical: 12,
                  }}>
                  <View
                    style={{
                      width: '100%',
                      marginTop: 10,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Icons.LocationSVG size={16} color={'#B4B6B8'} />
                    <TextRegular
                      text={item.location}
                      size={12}
                      style={{marginLeft: 10}}
                    />
                  </View>
                  <TextBold
                    text={item.name}
                    size={16}
                    color={Colors.blackBase}
                    style={{marginTop: 8}}
                  />
                  <TextRegular
                    text={
                      item.type === 'material'
                        ? 'Material Bangunan'
                        : 'Non Material Bangunan'
                    }
                    size={12}
                    color={Colors.grey_1}
                    style={{marginTop: 8}}
                  />
                  <TextBold
                    // text={inputRupiah(`${item.price}`, 'Rp.')}
                    text={item.price_text}
                    size={16}
                    color={Colors.primary}
                    style={{marginTop: 8}}
                  />
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </ScrollView>
      <ModalSortBy
        show={showModalSortBy}
        onClose={() => setShowModalSortBy(false)}
        data={dataSortBy}
        pickedData={pickedSortBy}
        onPressItem={index => {
          console.log('pickeditem', index);
          setPickedSortBy(index);
          setShowModalSortBy(false);
        }}
      />
      <ModalFilter
        show={showModalFilter}
        onClose={() => setShowModalFilter(false)}
        dataCategory={dataAllCategory}
        pickedCategory={pickedCategory}
        onPressCategory={value => setPickedCategory(value)}
        lowestPrice={lowestPrice}
        setLowestPrice={text => {
          setLowestPrice(text);
          // setLowestPrice(inputRupiah(`${text}`, ''))
        }}
        highestPrice={highestPrice}
        setHighestPrice={text => {
          setHighestPrice(text);
          // setHighestPrice(inputRupiah(`${text}`, ''))
        }}
        onSubmit={data => {
          console.log('cek data filter', data);
          setShowModalFilter(false);
          setPickedSubCategory([]);
          getProduct();
        }}
        dataSubCategory={subCategory}
        dataSubSubCategory={subsubCategory}
        pickedSubCategory={pickedSubCategory}
        pickedSubSubCategory={pickedSubSubCategory}
        setPickedSub={item => {
          setPickedSubCategory(item);
          getSubSubCategory(item[0].id);
        }}
        setPickedSubSub={item => setPickedSubSubCategory(item)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontFamily: Fonts.mulishExtraBold,
    color: '#333333',
    fontSize: Size.scaleFont(16),
    // marginHorizontal: Size.scaleSize(15)
  },
  cardItem: {
    width: '48%',
    height: 290,
    borderRadius: 8,
    borderColor: '#F2F4F5',
    borderWidth: 1,
    // paddingVertical: 12,
  },
  btnContainer: {
    // position: 'absolute',
    // alignSelf: 'center',
    backgroundColor: '#F7F9F8',
    // top: Dimensions.get('screen').height * 0.27,
    zIndex: 2,
    padding: Size.scaleSize(10),
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
});

export default DetailCategory;
