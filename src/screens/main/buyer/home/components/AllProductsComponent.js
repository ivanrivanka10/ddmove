import React from 'react';
import {
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
} from 'react-native';
import {
  TextSemiBold,
  TextBold,
  HeaderTitle,
  TextRegular,
} from '@components/global';
import {IconAsset} from '@utils/index';
import {Colors, Icons} from '@styles/index';
import {inputRupiah} from '@helpers/functionFormat';

const AllProductsComponent = ({
  navigation,
  data,
  onPressItem,
  openFilter,
  openSortBy,
}) => {
  return (
    <View style={{flex: 1}}>
      <HeaderTitle goBack={() => navigation.goBack()} title="Semua Produk" />
      <View style={{paddingHorizontal: 18}}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: 25,
          }}>
          <TextBold text="Kategori" size={16} color={Colors.blackBase} />
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity activeOpacity={0.8} onPress={openFilter}>
              <Icons.SortSVG size={24} color={Colors.primary} />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={openSortBy}
              style={{marginLeft: 15}}>
              <Icons.FilterSVG size={24} color={Colors.primary} />
            </TouchableOpacity>
          </View>
        </View>
        <FlatList
          data={data}
          keyExtractor={(item, index) => index.toString()}
          numColumns={2}
          contentContainerStyle={{
            paddingBottom: Dimensions.get('window').height * 0.2,
          }}
          showsVerticalScrollIndicator={false}
          columnWrapperStyle={{justifyContent: 'space-between'}}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.cardItem}
                onPress={() => navigation.navigate('Product', {product: item})}>
                <Image
                  source={{uri: item.photo}}
                  style={{width: '100%', height: 132, resizeMode: 'contain'}}
                />
                <View
                  style={{
                    paddingHorizontal: 15,
                    height: 158,
                    justifyContent: 'space-between',
                    paddingVertical: 12,
                  }}>
                  <View
                    style={{
                      width: '100%',
                      marginTop: 10,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Icons.LocationSVG size={16} color={'#B4B6B8'} />
                    <TextRegular
                      text={item.location}
                      size={12}
                      style={{marginLeft: 10}}
                    />
                  </View>
                  <TextBold
                    text={item.name}
                    size={16}
                    color={Colors.blackBase}
                    style={{marginTop: 8}}
                  />
                  <TextRegular
                    text={
                      item.type === 'material'
                        ? 'Material Bangunan'
                        : 'Non Material Bangunan'
                    }
                    size={12}
                    color={Colors.grey_1}
                    style={{marginTop: 8}}
                  />
                  <TextBold
                    text={inputRupiah(`${item.price}`, 'Rp.')}
                    size={16}
                    color={Colors.primary}
                    style={{marginTop: 8}}
                  />
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardItem: {
    width: '48%',
    height: 290,
    marginTop: 20,
    borderRadius: 8,
    borderColor: '#F2F4F5',
    borderWidth: 1,
    // paddingVertical: 12,
  },
  viewCircle: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: Colors.bgColorCircle,
    justifyContent: 'center',
    alignItems: 'center',
  },
  item: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 20,
  },
});

export default AllProductsComponent;
