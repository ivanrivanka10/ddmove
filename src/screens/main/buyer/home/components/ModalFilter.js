import React from 'react'
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    FlatList,
    TextInput,
    ScrollView,
    Modal
} from 'react-native'
import {TextBold, TextRegular, TextSemiBold, SelectDropDown, HeaderTitle} from '@components/global'
import {Colors, Icons} from '@styles/index'
import {inputRupiah} from '@helpers/functionFormat'
import {IconAsset} from '@utils/index'

const ModalFilter = ({
    show,
    onClose,
    dataCategory,
    pickedCategory,
    onPressCategory,
    onSubmit,
    lowestPrice,
    setLowestPrice,
    highestPrice,
    setHighestPrice,
    dataMainCategory,
    dataSubCategory,
    dataSubSubCategory,
    pickedMainCategory,
    pickedSubCategory,
    pickedSubSubCategory,
    setPickedMainCategory,
    setPickedSub,
    setPickedSubSub,
    type = 'Category',
    group,
    setGroup,
    dataGroup
}) => {
    
    const validate = () => {
        var disable = false
        const obj = {
            highestPrice,
            lowestPrice,            
        }
        const dataEmpty = Object.keys(obj).filter(data => obj[data] == '')
        if(dataEmpty.length > 0) {
            disable = true
        }
        if(pickedSubCategory.length > 0) {
            if(pickedSubSubCategory.length === 0) {
                disable = true
            }
        }
        return disable;
    }

    return(
        <Modal            
            onRequestClose={onClose}
            visible={show}
        >
            <View style={{flex: 1, backgroundColor: '#F7F9F8'}}>
                <View style={{width: '100%', backgroundColor: '#fff'}}>
                    <HeaderTitle
                        title="Filter"
                        back={onClose}
                    />
                </View>
                <ScrollView contentContainerStyle={{paddingBottom: 100}}>
                    <View style={{width: '100%', paddingBottom: 24, backgroundColor: '#fff', paddingHorizontal: 18}}>
                        <TextBold
                            text="Harga"
                            size={16}
                            color={Colors.blackBase}
                            style={{marginTop: 24}}
                        />
                        <TextRegular
                            text="Harga terendah"
                            size={12}
                            color={Colors.blackBase}
                            style={{marginTop: 18}}
                        />
                        <TextInput
                            placeholder='Contoh: Rp 100.000'
                            style={{
                                width: '100%',
                                borderRadius: 10,
                                paddingVertical: 12,
                                paddingHorizontal: 18,
                                borderColor: Colors.grey_1,
                                borderWidth: 1,
                                marginTop: 5
                            }}
                            keyboardType="number-pad"
                            value={lowestPrice}
                            onChangeText={(text) => setLowestPrice(inputRupiah(text))}
                        />
                        <TextRegular
                            text="Harga tertinggi"
                            size={12}
                            color={Colors.blackBase}
                            style={{marginTop: 24}}
                        />
                        <TextInput
                            placeholder='Contoh: Rp 100.000'
                            style={{
                                width: '100%',
                                borderRadius: 10,
                                paddingVertical: 12,
                                paddingHorizontal: 18,
                                borderColor: Colors.grey_1,
                                borderWidth: 1,
                                marginTop: 5
                            }}
                            keyboardType="number-pad"
                            value={highestPrice}
                            onChangeText={(text) => setHighestPrice(inputRupiah(text))}
                        />
                        {
                            type === 'AllProduct' && (
                                <SelectDropDown
                                    searchable
                                    onSelectItem={(val) => {
                                        // console.log('cek val', val);
                                        setGroup(val.value)
                                    }}
                                    value={group}
                                    valueColor="#000"
                                    colorArrowIcon="#000"
                                    style={{marginTop: 20, width: '100%', alignSelf: 'center', borderRadius: 6}}
                                    placeholderText="Pilih Tipe Kategori"
                                    items={dataGroup}
                                    maxHeight={300}
                                    typeValueText="Medium"
                                />
                            )
                        }
                        <SelectDropDown
                            searchable
                            onSelectItem={(val) => {
                                // console.log('cek val', val);
                                var arr = []
                                arr.push(val)
                                setPickedMainCategory(arr)
                            }}
                            valueColor="#000"
                            colorArrowIcon="#000"
                            style={{marginTop: 20, width: '100%', alignSelf: 'center', borderRadius: 6}}
                            placeholderText="Pilih Kategori Produk"
                            items={dataMainCategory}
                            maxHeight={300}
                            typeValueText="Medium"
                        />
                        <FlatList
                            data={pickedMainCategory}
                            contentContainerStyle={{flexDirection : "row", flexWrap : "wrap"}} 
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({item, index}) => {
                                return(
                                    <TextSemiBold
                                        text={item.name}
                                        style={styles.catPicked}
                                        color="#fff"
                                    />
                                )
                            }}
                        />
                        {
                            type === 'AllProduct' ?
                                <View>
                                    {
                                        group === 'material' && (
                                            <>
                                                <SelectDropDown
                                                    searchable
                                                    onSelectItem={(val) => {
                                                        // console.log('cek val', val);
                                                        var arr = []
                                                        arr.push(val)
                                                        setPickedSub(arr)
                                                    }}
                                                    valueColor="#000"
                                                    colorArrowIcon="#000"
                                                    style={{marginTop: 20, width: '100%', alignSelf: 'center', borderRadius: 6}}
                                                    placeholderText="Pilih Sub Kategori Produk"
                                                    items={dataSubCategory}
                                                    maxHeight={300}
                                                    typeValueText="Medium"
                                                />
                                                <FlatList
                                                    data={pickedSubCategory}
                                                    contentContainerStyle={{flexDirection : "row", flexWrap : "wrap"}} 
                                                    keyExtractor={(item, index) => index.toString()}
                                                    renderItem={({item, index}) => {
                                                        return(
                                                            <TextSemiBold
                                                                text={item.name}
                                                                style={styles.catPicked}
                                                                color="#fff"
                                                            />
                                                        )
                                                    }}
                                                />
                                                <SelectDropDown
                                                    searchable
                                                    onSelectItem={(val) => {
                                                        // console.log('cek val', val);
                                                        var arr = []
                                                        arr.push(val)
                                                        setPickedSubSub(arr)
                                                    }}
                                                    valueColor="#000"
                                                    colorArrowIcon="#000"
                                                    style={{marginTop: 20, width: '100%', alignSelf: 'center', borderRadius: 6}}
                                                    placeholderText="Pilih Sub Sub Kategori Produk"
                                                    items={dataSubSubCategory}
                                                    maxHeight={300}
                                                    typeValueText="Medium"
                                                />
                                                <FlatList
                                                    data={pickedSubSubCategory}
                                                    contentContainerStyle={{flexDirection : "row", flexWrap : "wrap"}} 
                                                    keyExtractor={(item, index) => index.toString()}
                                                    renderItem={({item, index}) => {
                                                        return(
                                                            <TextSemiBold
                                                                text={item.name}
                                                                style={styles.catPicked}
                                                                color="#fff"
                                                            />
                                                        )
                                                    }}
                                                />
                                            </>
                                        )
                                    }
                                </View>
                            :
                            <>
                                <SelectDropDown
                                    searchable
                                    onSelectItem={(val) => {
                                        // console.log('cek val', val);
                                        var arr = []
                                        arr.push(val)
                                        setPickedSub(arr)
                                    }}
                                    valueColor="#000"
                                    colorArrowIcon="#000"
                                    style={{marginTop: 20, width: '100%', alignSelf: 'center', borderRadius: 6}}
                                    placeholderText="Pilih Sub Kategori Produk"
                                    items={dataSubCategory}
                                    maxHeight={300}
                                    typeValueText="Medium"
                                />
                                <FlatList
                                    data={pickedSubCategory}
                                    contentContainerStyle={{flexDirection : "row", flexWrap : "wrap"}} 
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={({item, index}) => {
                                        return(
                                            <TextSemiBold
                                                text={item.name}
                                                style={styles.catPicked}
                                                color="#fff"
                                            />
                                        )
                                    }}
                                />
                                <SelectDropDown
                                    searchable
                                    onSelectItem={(val) => {
                                        // console.log('cek val', val);
                                        var arr = []
                                        arr.push(val)
                                        setPickedSubSub(arr)
                                    }}
                                    valueColor="#000"
                                    colorArrowIcon="#000"
                                    style={{marginTop: pickedSubCategory.length > 0 ? 20 : 10, width: '100%', alignSelf: 'center', borderRadius: 6}}
                                    placeholderText="Pilih Sub Sub Kategori Produk"
                                    items={dataSubSubCategory}
                                    maxHeight={300}
                                    typeValueText="Medium"
                                />
                                <FlatList
                                    data={pickedSubSubCategory}
                                    contentContainerStyle={{flexDirection : "row", flexWrap : "wrap"}} 
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={({item, index}) => {
                                        return(
                                            <TextSemiBold
                                                text={item.name}
                                                style={styles.catPicked}
                                                color="#fff"
                                            />
                                        )
                                    }}
                                />
                            </>
                        }                        
                    </View>
                    {/* <View style={{width: '100%', marginTop: 10, paddingBottom: 24, backgroundColor: '#fff'}}>
                        <TextBold
                            text="Kategori"
                            size={16}
                            color={Colors.blackBase}
                            style={{marginTop: 24, marginLeft: 18}}
                        />
                        <TouchableOpacity 
                            onPress={() => onPressCategory(0)}
                            style={[styles.item, {backgroundColor: pickedCategory === 0 ? Colors.secondaryLighttest : '#fff'}]}
                        >
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <View style={{width: 65, height: 65, borderRadius: 65/2, backgroundColor: Colors.bgColorCircle, justifyContent: 'center', alignItems: 'center'}}>
                                    <Icons.CategorySVG 
                                        size={30}
                                        color={Colors.primary}
                                    />                                
                                </View>
                                <TextRegular
                                    text="Semua Kategori"
                                    size={16}
                                    color={Colors.blackBase}
                                    style={{marginLeft: 16}}
                                />
                            </View>
                            {
                                pickedCategory === 0 ?
                                    <Image
                                        source={IconAsset.IC_CIRCLE_SELECTED}
                                        style={{width: 20, height: 20}}
                                    />
                                :
                                    <View style={{width: 25, height: 25, borderRadius: 25/2, borderColor: Colors.grey_1, borderWidth: 1}} />
                            }
                        </TouchableOpacity>
                        <FlatList
                            data={dataCategory}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({item, index}) => {
                                return(
                                    <TouchableOpacity 
                                        onPress={() => onPressCategory(item.id)}
                                        style={[styles.item, {backgroundColor: pickedCategory === item.id ? Colors.secondaryLighttest : '#fff'}]}
                                    >
                                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                            <View style={{width: 65, height: 65, borderRadius: 65/2, backgroundColor: Colors.bgColorCircle, justifyContent: 'center', alignItems: 'center'}}>
                                                <Image
                                                    source={{uri: item.icon}}
                                                    style={{width: 40, height: 40}}
                                                />
                                            </View>
                                            <TextRegular
                                                text={item.name}
                                                size={16}
                                                color={Colors.blackBase}
                                                style={{marginLeft: 16}}
                                            />
                                        </View>
                                        {
                                            pickedCategory === item.id ?
                                                <Image
                                                    source={IconAsset.IC_CIRCLE_SELECTED}
                                                    style={{width: 20, height: 20}}
                                                />
                                            :
                                                <View style={{width: 25, height: 25, borderRadius: 25/2, borderColor: Colors.grey_1, borderWidth: 1}} />
                                        }
                                    </TouchableOpacity>
                                )
                            }}
                        />
                    </View> */}
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity
                        style={[styles.btnSubmit, 
                            // validate() && {backgroundColor: Colors.grey_1}
                        ]}
                        // disabled={validate()}
                        onPress={onSubmit}
                    >
                        <TextBold
                            text="Terapkan"
                            size={16}
                            color="#fff"
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    catPicked: {
        // marginLeft: 16,
        marginTop: 10,
        padding: 7,
        borderRadius: 4,
        backgroundColor: Colors.secondary
    },
    btnSubmit: {
        width: '100%',
        borderRadius: 16,
        backgroundColor: Colors.primary,
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        padding: 18,
        backgroundColor: '#fff',                
    },
    item: {
        width: '100%',
        marginTop: 18,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 18
    }
})

export default ModalFilter