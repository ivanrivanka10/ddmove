import React from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  TextInput,
  FlatList,
  StyleSheet,
  ScrollView,
} from 'react-native';
import {TextBold, TextRegular} from '@components/global';
import {Colors, Size} from '@styles/index';
import {IconAsset} from '@utils/index';
import {style} from 'deprecated-react-native-prop-types/DeprecatedImagePropType';

const SearchScreenComponent = ({
  onPressBack,
  clearHistory,
  deleteItem,
  dataHistory,
  dataimage,
  keyword,
  setKeyword,
  navigation,
  onPressSearch,
}) => {
  console.log('ini data image', dataimage);
  return (
    <ScrollView contentContainerStyle={{paddingBottom: 100}}>
      <View
        style={{
          width: '100%',
          paddingTop: 24,
          paddingHorizontal: 18,
        }}>
        {dataHistory.length > 0 ? (
          <View style={styles.titleWording}>
            <TextRegular text="Pencarian terakhir" color={Colors.normal} />
            <TouchableOpacity onPress={clearHistory}>
              <TextRegular text="Hapus riwayat" color={Colors.blackBase} />
            </TouchableOpacity>
          </View>
        ) : null}

        {dataHistory.map((value, index) => (
          <TouchableOpacity
            key={index.toString()}
            onPress={() => {
              onPressSearch(value.suggestion);
            }}
            style={styles.itemSearch}>
            <TextRegular text={value.suggestion} color={Colors.blackBase} />
            <TouchableOpacity
              onPress={() => {
                deleteItem(value.id);
              }}>
              <Image
                source={IconAsset.IcClose}
                style={{width: 20, height: 20}}
              />
            </TouchableOpacity>
          </TouchableOpacity>
        ))}

        {dataimage.length > 0 && (
          <View
            style={[styles.titleWording, {marginVertical: Size.scaleSize(5)}]}>
            <TextRegular text="Produk terakhir dilihat" color={Colors.normal} />
          </View>
        )}

        <View style={styles.itemSearch2}>
          {dataimage.length !== 0 &&
            dataimage.map((item, index) => (
              <TouchableOpacity
                key={index.toString()}
                onPress={() => {
                  onPressSearch(item.id);
                }}
                style={styles.itemSearch3}>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('Product', {product: item})
                  }>
                  <Image
                    source={{uri: item.photo}}
                    style={{
                      width: Size.scaleSize(50),
                      borderRadius: Size.scaleSize(10),
                      height: Size.scaleSize(50),
                    }}
                  />
                </TouchableOpacity>
              </TouchableOpacity>
            ))}
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  itemSearch: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 16,
  },
  itemSearch2: {
    flexDirection: 'row',
    gap: Size.scaleSize(10),
  },
  itemSearch3: {
    marginTop: Size.scaleSize(5),
    marginBottom: Size.scaleSize(5),
    marginHorizontal: Size.scaleSize(1),
  },
  titleWording: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default SearchScreenComponent;
