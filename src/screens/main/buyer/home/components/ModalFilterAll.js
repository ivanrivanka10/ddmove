import React from 'react'
import {
    View,
    Modal,
    StyleSheet,
    TouchableOpacity,
    Image
} from 'react-native'
import {IconAsset} from '@utils/index'
import {TextBold, TextRegular, SelectDropDown} from '@components/global'
import Colors from '@styles/Colors'

const ModalAllFilter = ({
    selectedFilter,
    show,
    onClose,
    pickFilter,
    typeCategory
}) => {
    const [categoryType, setCategoryType] = React.useState(typeCategory)
    const [typeFilter, setTypeFilter] = React.useState(selectedFilter)
    
    const data = [
        {
            id: 1,
            name: 'Utama',
            value: 'main'
        },
        {
            id: 2,
            name: 'Sub Kategori',
            value: 'sub'
        },
        {
            id: 3,
            name: 'Sub Sub Kategori',
            value: 'sub-sub'
        }
    ]

    const typeCat = [
        {
            id: 1,
            name: 'Material',
            value: 'material'
        },
        {
            id: 2,
            name: 'Not Material',
            value: 'non-material'
        }
    ]

    return(
        <Modal
            transparent
            visible={show}
            onRequestClose={onClose}
        >
            <View style={styles.containerModal}>
                <View style={styles.content}>
                    <View style={styles.title}>
                        <TextBold
                            text="Terapkan filter kategori"
                            color={Colors.blackBase}
                        />
                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={onClose}
                        >
                            <Image
                                source={IconAsset.IcClose}
                                style={{width: 20, height: 20}}
                            />
                        </TouchableOpacity>
                    </View>
                    <SelectDropDown
                        onSelectItem={(val) => {
                            setCategoryType(val.value)
                        }}
                        value={categoryType}
                        valueColor="#000"
                        colorArrowIcon="#000"
                        style={{marginTop: 10, width: '100%', alignSelf: 'center', borderRadius: 6}}
                        placeholderText="Pilih Tipe Kategori"
                        items={typeCat}
                        maxHeight={300}
                        typeValueText="Medium"
                    />
                    {/* <SelectDropDown
                        onSelectItem={(val) => {
                            setTypeFilter(val.value)
                        }}
                        value={typeFilter}
                        valueColor="#000"
                        colorArrowIcon="#000"
                        style={{marginTop: 10, width: '100%', alignSelf: 'center', borderRadius: 6}}
                        placeholderText="Pilih Kategori"
                        items={data}
                        maxHeight={300}
                        typeValueText="Medium"
                    /> */}
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.submit}
                        onPress={() => pickFilter("main", categoryType)}
                    >
                        <TextBold
                            text="Simpan"
                            color="#fff"
                            size={16}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    submit: {
        width: '100%',
        marginTop: 20,
        backgroundColor: Colors.primary,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 15
    },
    title: {
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    content: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 18,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },
    containerModal: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    }
})

export default ModalAllFilter