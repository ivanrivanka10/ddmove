import React from 'react'
import {
    View,
    StyleSheet,
    Dimensions,
    Image,
    FlatList,
    TouchableOpacity
} from 'react-native'
import Modal from 'react-native-modal'
import {TextBold, TextSemiBold} from '@components/global'
import Colors from '@styles/Colors'
import {IconAsset} from '@utils/index'

const ModalSortBy = ({
    show,
    onClose,
    data,
    pickedData,
    onPressItem
}) => {
    return (
        <Modal
            style={{margin: 0, justifyContent: 'flex-end', backgroundColor: 'rgba(0,0,0,0.4)'}}
            onBackdropPress={onClose}
            isVisible={show}
        >
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.btnClose}
                    onPress={onClose}
                >
                    <Image
                        source={IconAsset.IC_CLOSE_MODAL}
                        style={{width: 15, height: 15, resizeMode: 'contain'}}
                    />
                </TouchableOpacity>
                <TextBold
                    text="Urut Berdasarkan"
                    size={18}
                    color={Colors.blackBase}
                    style={{marginTop: 25}}
                />
                <FlatList
                    data={data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item, index}) => {
                        return(
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={[styles.item, {backgroundColor: pickedData === item.value ? Colors.secondaryLighttest : '#fff' }]}
                                onPress={() => onPressItem(index)}
                            >
                                <TextSemiBold
                                    text={item.title}
                                    size={16}
                                    color={Colors.blackBase}
                                />
                                {
                                    pickedData !== item.value ?
                                    <View style={{width: 20, height: 20, borderRadius: 10, borderColor: Colors.grey_1, borderWidth: 1}} />
                                    :
                                    <Image
                                        source={IconAsset.IC_CIRCLE_SELECTED}
                                        style={{width: 20, height: 20}}
                                    />
                                }
                            </TouchableOpacity>
                        )
                    }}
                />
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    item: {
        marginTop: 20,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 20,
        paddingHorizontal: 18,
        backgroundColor: '#fff'
    },
    btnClose: {
        width: 135,
        paddingVertical: 8,
        borderRadius: 8,
        backgroundColor: Colors.bgGrey,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        width: '100%',
        height: Dimensions.get('window').height * 0.7,
        backgroundColor: '#fff',
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        padding: 18,
    }
})

export default ModalSortBy;