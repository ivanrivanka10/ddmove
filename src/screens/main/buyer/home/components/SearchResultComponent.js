import React from 'react'
import {
    View,
    Image,
    TouchableOpacity,
    StyleSheet,
    FlatList
} from 'react-native'
import {TextBold, TextRegular} from '@components/global'
import {IconAsset} from '@utils/index'
import {Colors, Icons} from '@styles/index'
import {inputRupiah} from '@helpers/functionFormat'

const SearchResultComponent = ({
    navigation,
    data,
    onPressItem,
    onPressSort,
    onPressFilter,
    sortBy
}) => {
    return(
        <View style={{
            flex: 1,
            paddingVertical: 24,
            paddingHorizontal: 18
        }}>
            <TextBold
                text={sortBy}
                size={16}
                color={Colors.blackBase}
            />
            <TextRegular
                text={`Menampilkan ${data.length} hasil`}
                color={Colors.grey_1}
                style={{marginTop: 8}}
            />
            <FlatList
                data={data}
                keyExtractor={(item, index) => index.toString()}
                numColumns={2}
                columnWrapperStyle={{justifyContent: 'space-between'}}                
                renderItem={({item, index}) => {
                    return (
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={styles.cardItem}
                            onPress={() => navigation.navigate('Product', { product: item })}
                        >
                            <Image
                                source={{uri: item.photo}}
                                style={{width: '100%', height: 132, resizeMode: 'contain'}}
                            />
                            <View style={{paddingHorizontal: 15, height: 158, justifyContent: 'space-between', paddingVertical: 12}}>
                                <View style={{width: '100%', marginTop: 10, flexDirection: 'row', alignItems: 'center'}}>
                                    <Icons.LocationSVG 
                                        size={16}
                                        color={"#B4B6B8"}
                                    />
                                    <TextRegular
                                        text={item.location}
                                        size={12}
                                        style={{marginLeft: 10}}
                                    />
                                </View>
                                <TextBold
                                    text={item.name}
                                    size={16}
                                    
                                    color={Colors.blackBase}
                                    style={{marginTop: 8}}
                                />
                                <TextRegular
                                    text={item.type === 'material' ? 'Material Bangunan' : 'Non Material Bangunan'}
                                    size={12}
                                    color={Colors.grey_1}
                                    style={{marginTop: 8}}
                                />
                                <TextBold
                                    text={inputRupiah(`${item.price}`, 'Rp.')}
                                    size={16}
                                    color={Colors.primary}
                                    style={{marginTop: 8}}
                                />
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />
            <View style={{
                width: '100%',
                position: 'absolute',
                bottom: 10,                
            }}>
                <View style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    padding: 8,
                    borderRadius: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: Colors.border
                }}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={{
                            padding: 18,
                            borderRadius: 16,
                            backgroundColor: '#fff'
                        }}
                        onPress={onPressSort}
                    >
                        <Icons.SortSVG 
                            size={24}
                            color={Colors.primary}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={onPressFilter}
                        style={{
                            marginLeft: 15,
                            padding: 18,
                            borderRadius: 16,
                            backgroundColor: '#fff'
                        }}
                    >
                        <Icons.FilterSVG 
                            size={24}
                            color={Colors.primary}
                        />
                    </TouchableOpacity>
                </View>                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    cardItem: {
        width: '48%',
        height: 290,
        marginTop: 20,
        borderRadius: 8,
        borderColor: '#F2F4F5',
        borderWidth: 1,
        // paddingVertical: 12,
    },
})

export default SearchResultComponent