import React from 'react'
import {
    View,
    Image,
    FlatList,
    TouchableOpacity,
    StyleSheet
} from 'react-native'
import {TextSemiBold, TextBold, HeaderTitle, TextRegular} from '@components/global'
import {IconAsset} from '@utils/index'
import {Colors, Icons, Size} from '@styles/index'

const AllCategoryComponent = ({
    navigation,
    data,
    onPressItem,
    openFilter
}) => {
    return (
        <View>
            <HeaderTitle
                goBack={() => navigation.goBack()}
                title="Semua Kategori"
            />
            <View style={{width: '100%', paddingHorizontal: 18}}>
                <View style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 25
                }}>
                    <TextBold
                        text="Kategori"
                        size={16}
                        color={Colors.blackBase}                        
                    />
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={openFilter}
                    >
                        <Icons.FilterSVG 
                            size={24}
                            color={Colors.primary}
                        />
                    </TouchableOpacity>
                </View>                
                <FlatList
                    data={data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item, index}) => {
                        return (
                            <TouchableOpacity 
                                activeOpacity={0.8}
                                onPress={() => onPressItem(item)}
                                style={styles.item}>
                                <View style={{width: '90%', flexDirection: 'row', alignItems: 'center'}}>
                                    <View style={styles.viewCircle}>                                        
                                        <Image
                                            source={{uri: item.icon}}
                                            style={{width:Size.scaleSize(49), height: Size.scaleSize(49), borderRadius: 15, resizeMode: 'cover',overflow:'hidden',borderRadius:Size.scaleSize(30)}}
                                        />
                                    </View>
                                    <TextRegular
                                        text={item.name}
                                        size={16}
                                        color={Colors.blackBase}
                                        style={{marginLeft: 16}}
                                    />
                                </View>
                                <Image
                                    source={IconAsset.IC_CHEVRON_RIGHT}
                                    style={{width: 25, height: 25}}
                                />
                            </TouchableOpacity>
                        )
                    }}
                />
            </View>            
        </View>
    )
}

const styles = StyleSheet.create({
    viewCircle: {
        width: 50, 
        height: 50, 
        borderRadius: 25, 
        backgroundColor: Colors.bgColorCircle,
        justifyContent: 'center',
        alignItems: 'center'
    },
    item: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 20
    }
})

export default AllCategoryComponent;