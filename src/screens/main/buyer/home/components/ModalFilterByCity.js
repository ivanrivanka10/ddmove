import React, {useState, useEffect} from 'react';
import {
  Modal,
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  FlatList,
} from 'react-native';
import {
  TextSemiBold,
  TextRegular,
  TextBold,
  HeaderTitle,
  SelectDropDown,
} from '@components/global';
import {IconAsset} from '@utils/index';
import {Colors} from '@styles/index';
import {useSelector, useDispatch} from 'react-redux';
import API from '@utils/services/settingProvider';

const ModalFilterCity = ({selectedCityId, onPressSubmit, show, onClose}) => {
  const {userData} = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const [listData, setListData] = useState([]);
  const [activeSection, setActiveSection] = useState(undefined);
  const [cityIdSelected, setCityIdSelected] = useState(undefined);
  const [gratisAntar, setGratisAntar] = useState("");

  const getData = async () => {
    dispatch({type: 'SHOW_LOADING'});
    var dataProv = [];
    const res = await API.GetProvince();
    dataProv = res.data.data;
    for (let i = 0; i < res.data.data.length; i++) {
      const resCity = await API.GetCity(res.data.data[i].province_id);
      dataProv[i].city = resCity.data.data;
    }
    setListData(dataProv);
    dispatch({type: 'HIDE_LOADING'});
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <Modal visible={show} onRequestClose={onClose} transparent>
      <View
        style={{
          flex: 1,
          backgroundColor: '#fff',
        }}>
        <HeaderTitle title="Filter" back={onClose} />
        <View style={{paddingHorizontal: 18}}>
          <TextBold
            text="Gratis Antar"
            size={16}
            color={Colors.blackBase}
            style={{marginTop: 24}}
          />

          <SelectDropDown
            searchable
            value={gratisAntar}
            onSelectItem={val => {
                setGratisAntar(val.name)
            }}
            // disabled={listCity.length < 1}
            valueColor="#000"
            colorArrowIcon="#000"
            style={{marginTop: 14, borderRadius: 6}}
            placeholderText="Pilih Opsi Filter"
            items={[
              {id: 1, name: 'Ya'},
              {id: 2, name: 'Tidak'},
            ]}
            maxHeight={200}
            typeValueText="Medium"
          />

          <TextBold
            text="Pilih Lokasi"
            size={16}
            color={Colors.blackBase}
            style={{marginTop: 24}}
          />
          <FlatList
            data={listData}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{paddingBottom: 100}}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  style={{
                    width: '100%',
                    borderRadius: 8,
                    paddingVertical: 18,
                    borderColor: Colors.border,
                    borderWidth: 1,
                    marginTop: 14,
                  }}
                  onPress={() => {
                    if (activeSection === index) {
                      setActiveSection(undefined);
                    } else {
                      setActiveSection(index);
                    }
                  }}>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingHorizontal: 18,
                      justifyContent: 'space-between',
                    }}>
                    <TextSemiBold
                      text={item.province}
                      color={Colors.blackBase}
                    />
                    <Image
                      source={
                        activeSection === index
                          ? IconAsset.ChevronUp
                          : IconAsset.ChevronDown
                      }
                      style={{width: 20, height: 20, tintColor: Colors.primary}}
                    />
                  </View>
                  {activeSection === index && (
                    <FlatList
                      data={item.city}
                      keyExtractor={(item, index) => index.toString()}
                      renderItem={({item, index}) => {
                        return (
                          <TouchableOpacity
                            style={[
                              styles.item,
                              {
                                backgroundColor:
                                  cityIdSelected === item.city_id
                                    ? '#FFF9F3'
                                    : '#fff',
                              },
                            ]}
                            onPress={() => setCityIdSelected(item.city_id)}>
                            <TextRegular
                              text={item.city_name}
                              color={
                                cityIdSelected === item.city_id
                                  ? Colors.blackBase
                                  : Colors.normal
                              }
                            />
                            <Image
                              source={
                                cityIdSelected === item.city_id
                                  ? IconAsset.IC_CIRCLE_SELECTED
                                  : IconAsset.IcUnselect
                              }
                              style={{width: 25, height: 25}}
                            />
                          </TouchableOpacity>
                        );
                      }}
                    />
                  )}
                </TouchableOpacity>
              );
            }}
          />
        </View>
        <TouchableOpacity
          style={styles.btnFooter}
          onPress={() => onPressSubmit(cityIdSelected === undefined ? 0 : cityIdSelected,gratisAntar)}>
          <TextBold text="Terapkan" color="#fff" size={16} />
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  btnFooter: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 10,
    width: '90%',
    borderRadius: 16,
    paddingVertical: 17,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  item: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 18,
  },
});

export default ModalFilterCity;
