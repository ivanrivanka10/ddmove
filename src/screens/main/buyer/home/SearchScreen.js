import React, {useState, useCallback, useEffect} from 'react';
import {
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  Platform,
  TextInput,
  StyleSheet,
} from 'react-native';
import {Colors} from '@styles/index';
import SearchScreenComponent from './components/SearchScreenComponent';
import SearchResultComponent from './components/SearchResultComponent';
import {useSelector, useDispatch} from 'react-redux';
import API from '@utils/services/buyer/homeProvider';
import {IconAsset} from '@utils/index';
import {TextRegular} from '@components/global';
import ModalSortBy from './components/ModalSortBy';
import ModalFilterCity from './components/ModalFilterByCity';
import {debugConsoleLog} from '@helpers/functionFormat';

const dataSortBy = [
  {
    title: 'Rekomendasi',
    value: 'rekomendasi',
  },
  {
    title: 'Produk Terlaris',
    value: 'terlaris',
  },
  {
    title: 'Produk Terbaru',
    value: 'terbaru',
  },
  {
    title: 'Harga: Rendah ke Tinggi',
    value: 'harga_rendah',
  },
  {
    title: 'Harga: Tinggi ke Rendah',
    value: 'harga_tinggi',
  },
  {
    title: 'Nama: A-Z',
    value: 'a-z',
  },
  {
    title: 'Nama: Z-A',
    value: 'z-a',
  },
];

const SearchScreen = ({navigation, route}) => {
  const {userData} = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const [currentFocus, setCurrentFocus] = useState('history');
  const [dataHistory, setDataHistory] = useState([]);
  const [dataIMagae, setDataImage] = useState([]);
  const [dataSearach, setDataSearch] = useState([]);
  const [keyWord, setKeyword] = useState('');
  const [sortBy, setSortBy] = useState('terlaris');
  const [titleSort, setTitleSort] = useState('Produk Terlaris');
  const [cityId, setCityId] = useState(0);
  const [showModalSortBy, setShowModalSortBy] = useState(false);
  const [showModalFilter, setShowModalFilter] = useState(false);
  const [gratisAntar, setGratisAntar] = useState('');

  const getDataHistory = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const [res, imagres] = await Promise.all([
      API.GetHistorySearch(userData.token),
      API.GetProdukHistory(userData.token),
    ]);

    const historyImage = await API.GetProdukHistory(userData.token);
    console.log('res history', historyImage);
    debugConsoleLog('respoxxxdwi', imagres.data.data);
    dispatch({type: 'HIDE_LOADING'});
    if (imagres.status === 200) {
      setDataImage(imagres.data.data);
    }
    if (res.status === 200) {
      setDataHistory([]);
      if (res.data.data.length >= 5) {
        let arr = [];
        for (let x = 0; x < 5; x++) {
          const element = res.data.data[x];
          arr.push(element);
        }
        setDataHistory(arr);
      } else {
        setDataHistory(res.data.data);
      }
    } else {
      setDataHistory([]);
    }
  };

  const searchProduct = async (key, sortBy = '') => {
    setDataSearch([]);
    dispatch({type: 'SHOW_LOADING'});
    console.log('cek key didalam', key);
    const params = {
      query_: key,
      sort_by: sortBy == 'Ya' || sortBy == 'Tidak' ? 'terlaris' : sortBy,
    };

    if (sortBy === 'Ya') {
      params.is_free_shipping = 'yes';
    }

    // if(sortBy !== '') {
    //     params.sort_by = sortBy
    // }
    if (cityId !== 0) {
      params.city_id = cityId;
    }
    console.log('cek params', params);
    const res = await API.SearchProduct(params, userData.token);
    console.log('res search', res.data.data);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      setDataSearch(res.data.data);
      setCurrentFocus('listItems');
    }
    console.log('key', key);
    console.log('sort', sortBy);
  };

  const onDelete = async id => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.DeleteSearchProduct(id, userData.token);
    dispatch({type: 'HIDE_LOADING'});
    console.log('res delete', res);
    if (res.status === 200) {
      getDataHistory();
    }
  };

  const clearAllSearch = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.ClearAllSearch(userData.token);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      getDataHistory();
    }
  };

  useEffect(() => {
    getDataHistory();
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <StatusBar backgroundColor={Colors.primary} barStyle="light-content" />
      <ModalSortBy
        show={showModalSortBy}
        onClose={() => setShowModalSortBy(false)}
        data={dataSortBy}
        pickedData={sortBy}
        onPressItem={index => {
          console.log('pickeditem', index);
          setTitleSort(dataSortBy[index].title);
          setSortBy(dataSortBy[index].value);
          setShowModalSortBy(false);
          searchProduct(keyWord, dataSortBy[index].value);
        }}
      />
      <ModalFilterCity
        show={showModalFilter}
        onClose={() => setShowModalFilter(false)}
        onPressSubmit={(id, val) => {
          setShowModalFilter(false);
          setCityId(id);
          setGratisAntar(val);
          searchProduct(keyWord, val);
        }}
      />
      <View style={styles.viewTitle}>
        <View style={{width: '80%'}}>
          <View style={styles.txtInput}>
            <TextInput
              placeholder="Cari Produk"
              style={{
                width: '90%',
                color: '#000',
                paddingVertical: Platform.OS === 'ios' ? 15 : 0,
              }}
              value={keyWord}
              onChangeText={text => {
                if (text === '') {
                  setCurrentFocus('history');
                  setKeyword(text);
                } else {
                  setKeyword(text);
                }
              }}
              onSubmitEditing={() => searchProduct(keyWord)}
            />
            <Image
              source={IconAsset.IcSearch}
              style={{width: 20, height: 20}}
            />
          </View>
        </View>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => navigation.goBack()}>
          <TextRegular text="Kembali" color="#fff" />
        </TouchableOpacity>
      </View>
      {currentFocus === 'history' ? (
        <SearchScreenComponent
          onPressBack={() => navigation.goBack()}
          keyword={keyWord}
          setKeyword={text => {
            console.log('cek cek', text);
            setKeyword(text);
          }}
          onPressSearch={key => {
            setKeyword(key);
            searchProduct(key);
          }}
          dataHistory={dataHistory}
          dataimage={dataIMagae}
          navigation={navigation}
          deleteItem={id => onDelete(id)}
          clearHistory={() => clearAllSearch()}
        />
      ) : (
        <SearchResultComponent
          data={dataSearach}
          navigation={navigation}
          sortBy={titleSort}
          onPressSort={() => setShowModalSortBy(true)}
          onPressFilter={() => setShowModalFilter(true)}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  txtInput: {
    width: '90%',
    borderRadius: 8,
    backgroundColor: '#fff',
    paddingHorizontal: 18,
    // paddingVertical: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  viewTitle: {
    paddingHorizontal: 18,
    paddingTop: 25,
    paddingBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.primary,
  },
});

export default SearchScreen;
