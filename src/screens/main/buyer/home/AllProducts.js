import React, {useState, useEffect} from 'react';
import {View, Alert} from 'react-native';
import AllProductsComponent from './components/AllProductsComponent';
import {useSelector} from 'react-redux';
import API from '@utils/services/buyer/homeProvider';
import {Loader} from '@components/global';
import ModalFilter from './components/ModalFilter';
import ModalSortBy from './components/ModalSortBy';

const dataSortBy = [
  {
    title: 'Rekomendasi',
    value: 'rekomendasi',
  },
  {
    title: 'Produk Terlaris',
    value: 'terlaris',
  },
  {
    title: 'Produk Terbaru',
    value: 'terbaru',
  },
  {
    title: 'Harga: Rendah ke Tinggi',
    value: 'harga_tinggi',
  },
  {
    title: 'Harga: Tinggi ke Rendah',
    value: 'harga_redah',
  },
  {
    title: 'Nama: A-Z',
    value: 'a-z',
  },
  {
    title: 'Nama: Z-A',
    value: 'z-a',
  },
];

const dataType = [
  {
    id: 1,
    name: 'Material',
    value: 'material',
  },
  {
    id: 2,
    name: 'Non Material',
    value: 'non-material',
  },
];

const AllProducts = ({navigation, route}) => {
  const {userData} = useSelector(state => state.auth);
  const [dataAllProducts, setDataAllProducts] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [modalFilter, setModalFilter] = React.useState(false);
  const [modalAllFilter, setModalAllFilter] = React.useState(false);
  const [pickedCategory, setPickedCategory] = React.useState('all');
  const [typeCategory, setTypeCategory] = React.useState('rekomendasi');
  const [lower_price, setLowerPrice] = React.useState('');
  const [higher_price, setHigherPrice] = React.useState('');
  const [dataAllCategory, setDataAllCategory] = React.useState([]);
  const [subCategory, setSubCategory] = useState([]);
  const [subsubCategory, setSubSubCategory] = useState([]);
  const [categoryPicked, setCategoryPicked] = useState([]);
  const [pickedSubCategory, setPickedSubCategory] = useState([]);
  const [pickedSubSubCategory, setPickedSubSubCategory] = useState([]);
  const [group, setGroup] = useState(undefined);

  /**
   * get all data products
   */
  const getAllData = async () => {
    setLoading(true);
    const objCategories = {};
    const params = {
      sort_by: typeCategory,
    };
    if (lower_price !== '') {
      params.harga_rendah =
        lower_price.length > 3 ? lower_price.split('.').join('') : lower_price;
    }
    if (higher_price !== '') {
      params.harga_tinggi =
        higher_price.length > 3
          ? higher_price.split('.').join('')
          : higher_price;
    }
    if (group !== undefined) {
      objCategories.main = categoryPicked[0].id;
      if (pickedSubCategory.length > 0 && pickedSubSubCategory.length > 0) {
        objCategories.sub = pickedSubCategory[0].id;
        objCategories.subsub = pickedSubSubCategory[0].id;
      } else {
        return Alert.alert(
          'Peringatan',
          'Harus memilih Sub dan Sub Sub Kategori',
        );
      }
      params.category = JSON.stringify(objCategories);
    }
    const res = await API.GetProduct(userData.token, params);
    console.log('res all products', res);
    if (res.status == 200) {
      setLoading(false);
      setDataAllProducts(res.data.data);
    }
  };

  const getAllCategory = async groupBy => {
    setLoading(true);
    const params = {
      group: groupBy,
      type: 'main',
    };
    const res = await API.GetAllCategory(userData.token, params);
    console.log('res all cat', res);
    if (res.status == 200) {
      setLoading(false);
      setDataAllCategory(res.data.data);
    } else {
      setLoading(false);
    }
  };

  const getSubCategory = async id => {
    const paramsSub = {
      group: group,
      type: 'sub',
      id: id,
    };
    const res = await API.GetAllCategory(userData.token, paramsSub);
    console.log('=== res sub', res);
    setLoading(false);
    var arrCat = [];
    if (res.status === 200) {
      for (let i = 0; i < res.data.data.length; i++) {
        arrCat.push({
          id: res.data.data[i].id,
          name: res.data.data[i].name,
        });
      }
      setSubCategory(arrCat);
    }
  };

  const getSubSubCategory = async id => {
    const paramsSub = {
      group: group,
      type: 'sub-sub',
      id: id,
    };
    const res = await API.GetAllCategory(userData.token, paramsSub);
    console.log('res subsub', res);
    var arrCat = [];
    if (res.status === 200) {
      for (let i = 0; i < res.data.data.length; i++) {
        arrCat.push({
          id: res.data.data[i].id,
          name: res.data.data[i].name,
        });
      }
      setSubSubCategory(arrCat);
    }
  };

  React.useEffect(() => {
    setLoading(false);
    getAllData();
  }, [typeCategory]);

  React.useEffect(() => {
    if (group !== undefined) {
      getAllCategory();
    }
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Loader show={loading} />
      <ModalSortBy
        show={modalFilter}
        onClose={() => setModalFilter(false)}
        data={dataSortBy}
        pickedData={typeCategory}
        onPressItem={index => {
          console.log('pickeditem', index);
          setTypeCategory(dataSortBy[index].value);
          setModalFilter(false);
        }}
      />
      <ModalFilter
        show={modalAllFilter}
        onClose={() => setModalAllFilter(false)}
        dataCategory={dataAllCategory}
        pickedCategory={pickedCategory}
        onPressCategory={value => setPickedCategory(value)}
        lowestPrice={lower_price}
        setLowestPrice={text => {
          setLowerPrice(text);
          // setLowestPrice(inputRupiah(`${text}`, ''))
        }}
        highestPrice={higher_price}
        setHighestPrice={text => {
          setHigherPrice(text);
          // setHighestPrice(inputRupiah(`${text}`, ''))
        }}
        onSubmit={data => {
          setModalAllFilter(false);
          getAllData();
        }}
        type="AllProduct"
        dataMainCategory={dataAllCategory}
        dataSubCategory={subCategory}
        dataSubSubCategory={subsubCategory}
        pickedMainCategory={categoryPicked}
        pickedSubCategory={pickedSubCategory}
        pickedSubSubCategory={pickedSubSubCategory}
        dataGroup={dataType}
        group={group}
        setGroup={value => {
          setCategoryPicked([]);
          setPickedSubCategory([]);
          setGroup(value);
          getAllCategory(value);
        }}
        setPickedMainCategory={item => {
          console.log('picked main', item);
          setCategoryPicked(item);
          setPickedSubCategory([]);
          getSubCategory(item[0].id);
        }}
        setPickedSub={item => {
          setPickedSubCategory(item);
          getSubSubCategory(item[0].id);
        }}
        setPickedSubSub={item => setPickedSubSubCategory(item)}
      />
      <AllProductsComponent
        navigation={navigation}
        data={dataAllProducts}
        onPressItem={value => navigation.navigate('Category', {item: value})}
        openFilter={() => setModalFilter(true)}
        openSortBy={() => setModalAllFilter(true)}
      />
    </View>
  );
};

export default AllProducts;
