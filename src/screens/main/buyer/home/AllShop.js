import React, {useState, useEffect} from 'react'
import {
    View,
    TouchableOpacity,
    ScrollView,
    Image,
    TextInput,
    StyleSheet
} from 'react-native'
import {TextBold, TextRegular, HeaderTitle} from '@components/global'
import {IconAsset, ImageAsset} from '@utils/index'
import {Colors} from '@styles/index'
import {useSelector, useDispatch} from 'react-redux'
import API from '@utils/services/buyer/homeProvider'

const AllShop = ({
    navigation,
    route
}) => {
    const dispatch = useDispatch()
    const { userData, location } = useSelector(state => state.auth);
    const [key, setKey] = useState('')
    const [type, setType] = useState('ASC')
    const [allShop, setAllShop] = useState([])
    
    const getData = async () => {
        const formData = new FormData()
        formData.append('latitude', location ? location.lat : '-7.797068');
        formData.append('longitude', location ? location.lng : '110.370529');
        formData.append('keywords', key);
        formData.append('sort', type);
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetAllShop(formData, userData.token)
        console.log('res all shop', res);
        if(res.status === 200) {
            setAllShop(res.data.data)
        }
        dispatch({type: "HIDE_LOADING"});
    }

    useEffect(() => {
        getData()
    },[type, key])

    return(
        <View style={{flex: 1, backgroundColor: '#fff'}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <HeaderTitle
                    title="Toko Disekitarmu"
                />
                <View style={{width: '100%', padding: 18}}>
                    <View style={styles.viewSearch}>
                        <TextInput
                            placeholder='Cari nama toko'
                            style={{width: '80%'}}
                            value={key}
                            onChangeText={(text) => setKey(text)}
                        />
                        <Image
                            source={IconAsset.IcSearch}
                            style={{width: 20, height: 20, tintColor: '#000'}}
                        />
                    </View>
                    {/* <View style={{
                        marginTop: 24,
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
                        <TouchableOpacity
                            style={[styles.tab, type === 'ASC' && {borderWidth: 1, backgroundColor: '#FFF9F3'}]}
                            onPress={() => setType("ASC")}
                        >
                            <TextRegular
                                text="Terdekat"
                                color={type === 'ASC' ? Colors.secondary : Colors.blackBase}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.tab, {marginLeft: 10}, type === 'DESC' && {borderWidth: 1, backgroundColor: '#FFF9F3'}]}
                            onPress={() => setType("DESC")}
                        >
                            <TextRegular
                                text="Terjauh"
                                color={type === 'DESC' ? Colors.secondary : Colors.blackBase}
                            />
                        </TouchableOpacity>
                    </View> */}
                    {
                        allShop.map((value) => (
                            <TouchableOpacity
                                style={styles.item}
                                onPress={() => navigation.navigate('DetailStore', {value})}
                            >
                                <Image
                                    source={value.photo === '' ? ImageAsset.DummyImg1 : {uri: value.photo}}
                                    style={{width: 45, height: 45, borderRadius: 45/2, resizeMode: 'cover'}}
                                />
                                <View style={{paddingHorizontal: 18}}>
                                    <TextBold
                                        text={value.name}
                                        color={Colors.blackBase}
                                    />
                                    {/* <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Image
                                            source={IconAsset.IcPinMap}
                                            style={{width: 15, height: 15}}
                                        />
                                        <TextRegular
                                            text={`${value.distance} dari Anda`}
                                            style={{marginLeft: 10}}
                                            color="#B4B6B8"
                                        />
                                    </View> */}
                                </View>
                            </TouchableOpacity>
                        ))
                    }
                </View>                
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        width: '100%',
        marginTop: 24,
        flexDirection: 'row',
        paddingBottom: 24,
        borderBottomColor: Colors.border,
        borderBottomWidth: 1,
        alignItems: 'flex-start'
    },
    tab: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 24,
        backgroundColor: '#F7F9F8',
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderColor: Colors.secondary,
        // borderWidth: 1
    },
    viewSearch: {
        borderRadius: 16,
        width: '100%',
        borderColor: Colors.border,
        borderWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20
    }
})

export default AllShop;