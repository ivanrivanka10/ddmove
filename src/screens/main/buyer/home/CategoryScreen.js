import React, { useEffect, useState, useRef } from 'react';
import { View, Text, StatusBar, Image, FlatList, TouchableOpacity, StyleSheet, Animated, Dimensions, TouchableHighlight, TextBase } from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';

import { Size, Icons, Fonts, Images, Colors } from '@styles/index';
import Store from '@components/part/Store';
import Product from '@components/part/Product';
import {Loader, TextBold, TextRegular, HeaderTitle} from '@components/global'
import {useSelector} from 'react-redux'
import API from '@utils/services/buyer/homeProvider'
import {inputRupiah} from '@helpers/functionFormat'

import ModalSortBy from './components/ModalSortBy';
import ModalFilter from './components/ModalFilter';

// const dataSortBy = ["Rekomendasi", "Produk Terlaris", "Produk Terbaru",
//     "Harga: Rendah ke Tinggi", "Harga: Tinggi ke Rendah", "Nama: A-z", "Nama: Z-A"]
const dataSortBy = [
    {
        title: 'Rekomendasi',
        value: 'rekomendasi'
    },
    {
        title: 'Produk Terlaris',
        value: 'terlaris'
    },
    {
        title: 'Produk Terbaru',
        value: 'terbaru'
    },
    {
        title: 'Harga: Rendah ke Tinggi',
        value: 'harga_tinggi'
    },
    {
        title: 'Harga: Tinggi ke Rendah',
        value: 'harga_redah'
    },
    {
        title: 'Nama: A-Z',
        value: 'a-z'
    },
    {
        title: 'Nama: Z-A',
        value: 'z-a'
    }
]

const CategoryScreen = props => {
    const { userData } = useSelector(state => state.auth);
    const [isSwiped, setSwiped] = useState(false);
    const [btnContainerY] = useState(new Animated.Value(Dimensions.get('screen').height * 0.27));
    const [headerHeight] = useState(new Animated.Value(Dimensions.get('screen').height * 0.25));
    const [headerBottom] = useState(new Animated.Value(Size.scaleSize(100)))
    const [headerOpacity] = useState(new Animated.Value(1));
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState([])
    const [pickedSortBy, setPickedSortBy] = useState(undefined)
    const [pickedCategory, setPickedCategory] = useState(undefined)
    const [showModalSortBy, setShowModalSortBy] = useState(false)
    const [showModalFilter, setShowModalFilter] = useState(false)
    const [dataAllCategory, setDataAllCategory] = useState([])
    const [lowestPrice, setLowestPrice] = React.useState('')
    const [highestPrice, setHighestPrice] = React.useState('')

    const flatListRef = useRef();

    const [offsetScroll, setOffsetScroll] = useState(0);
    
    useEffect(() => {
        props.navigation.setParams({isSwiped: isSwiped})
    }, [isSwiped])

    const flatListScroll = (event) => {
        setOffsetScroll(event.nativeEvent.contentOffset.y);
        if(event.nativeEvent.contentOffset.y <= 10){
            downAnimation();
        }
        
    }

    const handleRelease = () => {
        flatlistRef.current.scrollToOffset({ y: -100 });
        setTimeout(() => {
            flatlistRef.current.scrollToOffset({ y: 0 });
        }, 1000)
    }
    

    const upAnimation = () => {
        Animated.timing(headerOpacity, {
            toValue: 0,
            duration: 100,
            useNativeDriver: false,
        }).start(() => {
            setSwiped(true);
            Animated.parallel([
                Animated.sequence([
                    Animated.timing(headerBottom, {
                        toValue: Size.scaleSize(0),
                        duration: 100,
                        useNativeDriver: false
                    }),
                    Animated.timing(headerHeight, {
                        toValue: 0,
                        duration: 100,
                        useNativeDriver: false
                    })
                ]),
                Animated.sequence([
                    Animated.timing(btnContainerY, {
                        toValue: Dimensions.get('screen').height * 0.27 - 20,
                        duration: 80,
                        useNativeDriver: true
                    }),
                    Animated.timing(btnContainerY, {
                        toValue: Dimensions.get('screen').height - 230,
                        duration: 100,
                        useNativeDriver: true
                    })
                ])
            ]).start(() => {
                setSwiped(true);
            })
            
            
        });
        
    }

    const downAnimation = () => {
        Animated.timing(headerBottom, {
            toValue: Size.scaleSize(100),
            duration: 100,
            useNativeDriver: false
        }).start(() => {
            Animated.timing(headerHeight, {
                toValue: Dimensions.get('screen').height * 0.25,
                duration: 100,
                useNativeDriver: false
            }).start(() => {
                setSwiped(false);
                if(offsetScroll > 0){
                    flatListRef.current.scrollToOffset({ animated: true, offset: 0 });
                }
                Animated.timing(headerOpacity, {
                    toValue: 1,
                    duration: 10,
                    useNativeDriver: false,
                }).start(() => {
                    setSwiped(false);
                    Animated.timing(btnContainerY, {
                        toValue: Dimensions.get('screen').height * 0.27,
                        duration: 120,
                        useNativeDriver: true
                    }).start();
                });
            });
            
        })
    }

    const getProduct = async () => {
        const params = {}
        if(pickedSortBy !== undefined) {
            params.sort_by = dataSortBy[pickedSortBy].value
        }
        const res = await API.GetProduct(userData.token, params);
        console.log('res product', res);
        if(res.status === 200) {
            // setLoading(false);
            setData(res.data.data);
            getAllCategory();   
        } else {
            setLoading(false);
        }
    }

    const getAllCategory = async () => {
        setLoading(true)
        const params = {
            group: 'material',
            type: 'main'
        }
        const res = await API.GetAllCategory(userData.token, params);
        console.log('res all cat', res);
        if(res.status == 200) {
            setLoading(false)
            setDataAllCategory(res.data.data)
        } else {
            setLoading(false)
        }
    }

    useEffect(() => {
        console.log('props category', props);
        getProduct();
    },[pickedSortBy])
 
    return (
        <GestureRecognizer
            style={styles.screen}
            config={{
                velocityThreshold: 0.2,
                // directionalOffsetThreshold: 80
            }}
            onSwipeUp={!isSwiped? () => upAnimation() : null}
            // onSwipeDown={() => console.log('test')}
            // onSwipeDown={() => {
            //     if(isSwiped && flatListRef.current && flatListRef.current <= 0){
            //         downAnimation();
            //     }
            // }}
        >
            <StatusBar 
                backgroundColor="#FFF"
                barStyle="dark-content"
            />
            <Loader show={loading} />
            <HeaderTitle
                goBack={() => navigation.goBack()}
                // title="Semua Kategori"
            />
            <Animated.View style={[styles.headerCategory,{ height: headerHeight, marginBottom: headerBottom, opacity: headerOpacity}]}>
                {!isSwiped && <Store 
                    image={props?.route?.params?.item?.icon}
                    label=""
                />}
                <Text 
                    allowFontScaling={false}
                    style={styles.titleCategory}
                >
                    {props?.route?.params?.item?.name}
                </Text>
                {
                    data.length > 0 && (
                        <Text 
                            allowFontScaling={false}
                            style={styles.infoTitleCategory}
                        >
                            {data.length} produk ditemukan
                        </Text>
                    )
                }                
            </Animated.View>
            <Animated.View 
                style={[styles.btnContainer, {
                    transform: [{
                        translateY: btnContainerY
                    }]
                }]}
            >
                <TouchableHighlight 
                    style={{marginHorizontal: Size.scaleSize(5), borderRadius: 10, padding: Size.scaleSize(14), backgroundColor: 'white'}} 
                    onPress={() => setShowModalSortBy(true)}
                    underlayColor={Colors.opacityColor(Colors.primary, 0.2)}
                >
                    <Icons.SortSVG 
                        size={Size.scaleSize(24)}
                        color={Colors.primary}
                    />
                </TouchableHighlight>
                <TouchableHighlight 
                    style={{marginHorizontal: Size.scaleSize(5), borderRadius: 10, padding: Size.scaleSize(14), backgroundColor: 'white'}} 
                    onPress={() => setShowModalFilter(true)}
                    underlayColor={Colors.opacityColor(Colors.primary, 0.2)}
                >
                    <Icons.FilterSVG 
                        size={Size.scaleSize(24)}
                        color={Colors.primary}
                    />
                </TouchableHighlight>
            </Animated.View>
            <View style={{paddingHorizontal: 16}}>
                <FlatList 
                    scrollEventThrottle={16}
                    contentContainerStyle={{paddingBottom: 100}}
                    ref={flatListRef}
                    scrollToOverflowEnabled={true}
                    scrollEnabled={isSwiped}
                    onScroll={flatListScroll}
                    // onScroll={Animated.event(
                    //     [{
                    //         nativeEvent: {
                    //             contentOffset: {
                    //                 y: scrolling
                    //             }
                    //         }
                    //     }],
                    //     { useNativeDriver : true }
                    // )}
                    ListHeaderComponent={<Text allowFontScaling={false} style={styles.title}>Rekomendasi</Text>}
                    // onMoveShouldSetResponder={() => true}
                    data={data}
                    columnWrapperStyle={{marginTop: 10, justifyContent: 'space-between', marginHorizontal: 0}}
                    keyExtractor={item => item.id}
                    numColumns={2}
                    renderItem={({item, index}) => {
                        return (
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={styles.cardItem}
                                onPress={() => props.navigation.navigate('Product', { product: item })}
                            >
                                <Image
                                    source={{uri: item.photo}}
                                    style={{width: '100%', height: 132, resizeMode: 'contain'}}
                                />
                                <View style={{paddingHorizontal: 15, height: 158, justifyContent: 'space-between', paddingVertical: 12}}>
                                    <View style={{width: '100%', marginTop: 10, flexDirection: 'row', alignItems: 'center'}}>
                                        <Icons.LocationSVG 
                                            size={16}
                                            color={"#B4B6B8"}
                                        />
                                        <TextRegular
                                            text={item.location}
                                            size={12}
                                            style={{marginLeft: 10}}
                                        />
                                    </View>
                                    <TextBold
                                        text={item.name}
                                        size={16}
                                        
                                        color={Colors.blackBase}
                                        style={{marginTop: 8}}
                                    />
                                    <TextRegular
                                        text={item.type === 'material' ? 'Material Bangunan' : 'Non Material Bangunan'}
                                        size={12}
                                        color={Colors.grey_1}
                                        style={{marginTop: 8}}
                                    />
                                    <TextBold
                                        // text={inputRupiah(`${item.price}`, 'Rp.')}
                                        text={item.price_text}
                                        size={16}
                                        color={Colors.primary}
                                        style={{marginTop: 8}}
                                    />
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                />
            </View>
            <ModalSortBy
                show={showModalSortBy}
                onClose={() => setShowModalSortBy(false)}
                data={dataSortBy}
                pickedData={pickedSortBy}
                onPressItem={(index) => {
                    console.log('pickeditem', index);
                    setPickedSortBy(index)
                    setShowModalSortBy(false)
                    
                }}
            />
            <ModalFilter
                show={showModalFilter}
                onClose={() => setShowModalFilter(false)}
                dataCategory={dataAllCategory}
                pickedCategory={pickedCategory}
                onPressCategory={(value) => setPickedCategory(value)}
                lowestPrice={lowestPrice}
                setLowestPrice={(text) => {
                    setLowestPrice(Number(text))
                    // setLowestPrice(inputRupiah(`${text}`, ''))
                }}
                highestPrice={highestPrice}
                setHighestPrice={(text) => {
                    setHighestPrice(Number(text))
                    // setHighestPrice(inputRupiah(`${text}`, ''))
                }}
                onSubmit={(data) => {
                    setShowModalFilter(false);
                    getProduct();
                }}
            />
        </GestureRecognizer>
        
    )
}

const styles = StyleSheet.create({
    cardItem: {
        width: '48%',
        height: 290,
        borderRadius: 8,
        borderColor: '#F2F4F5',
        borderWidth: 1,
        // paddingVertical: 12,
    },
    screen: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerCategory: {
        alignItems: 'center', 
        justifyContent: 'center',
        height: Size.scaleSize(Dimensions.get('screen').height * 0.25),
        marginBottom: Size.scaleSize(100),
        opacity: 1
    },
    titleCategory: {
        fontFamily: Fonts.mulishExtraBold, 
        color: '#333', 
        fontSize: Size.scaleFont(24)
    },
    infoTitleCategory: {
        fontFamily: Fonts.mulishRegular, 
        color: '#B4B6B8', 
        fontSize: Size.scaleFont(14),
        marginTop: Size.scaleSize(5),
        
    },
    title: {
        fontFamily: Fonts.mulishExtraBold, 
        color: '#333333', 
        fontSize: Size.scaleFont(16),
        marginHorizontal: Size.scaleSize(15)
    },
    btnContainer: { 
        position: 'absolute', 
        alignSelf: 'center', 
        backgroundColor: '#F7F9F8', 
        // top: Dimensions.get('screen').height * 0.27, 
        zIndex: 2, 
        padding: Size.scaleSize(10), 
        borderRadius: 20, 
        flexDirection: 'row',
        alignItems: 'center'
    }
})

export const screenOptions = navData => {
    const isSwiped = navData.route.params && navData.route.params.isSwiped? navData.route.params.isSwiped : false;
    return {
        headerTitle: "Material Bangunan",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333',
            opacity: isSwiped? 1 : 0
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default CategoryScreen;