import React from 'react'
import {
   View,   
} from 'react-native'
import AllCategoryComponent from './components/AllCategoryComponent'
import {useSelector} from 'react-redux'
import API from '@utils/services/buyer/homeProvider'
import {Loader} from '@components/global'
import ModalAllFilter from './components/ModalFilterAll'

const AllCategory = ({navigation, route}) => {
   const { userData } = useSelector(state => state.auth);
   const [dataAllCategory, setDataAllCategory] = React.useState([]);
   const [loading, setLoading] = React.useState(false)
   const [modalFilter, setModalFilter] = React.useState(false)
   const [pickedCategory, setPickedCategory] = React.useState('main')
   const [typeCategory, setTypeCategory] = React.useState('material')

   /**
    * get all data category
   */
   const getAllData = async () => {
      setLoading(true)
      const params = {
         group: 'material',
         type: 'main'
      }
      const res = await API.GetAllCategory(userData.token, params);
      console.log('res all cat', res);
      if(res.status == 200) {
         setLoading(false)
         setDataAllCategory(res.data.data)
      }
   }

   /**
    * 
   */
   const filterData = async (value, typeCat) => {
      setTypeCategory(typeCat)
      setPickedCategory(value)
      setLoading(true)
      const params = {
         group: typeCat,
         type: pickedCategory
      }
      const res = await API.GetAllCategory(userData.token, params);
      console.log('res all cat', res);
      if(res.status == 200) {
         setLoading(false)
         setDataAllCategory(res.data.data)
      }
   }

   React.useEffect(() => {
      getAllData();
   },[])

   return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
         <Loader show={loading}  />
         <ModalAllFilter
            show={modalFilter}
            onClose={() => setModalFilter(false)}
            selectedFilter={pickedCategory}
            pickFilter={(value, type) => {
               setModalFilter(false)
               filterData(value, type)
            }}
            typeCategory={typeCategory}
         />
         <AllCategoryComponent
            navigation={navigation}
            data={dataAllCategory}
            onPressItem={(value) => navigation.navigate('Category', {item: value})}
            openFilter={() => setModalFilter(true)}
         />
      </View>
   )
}

export default AllCategory;