import React, {useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Modal,
  Text,
} from 'react-native';
import {Size, Fonts, Icons, Colors} from '@styles/index';
import {IconAsset} from '@utils/index';
import {
  TextBold,
  TextSemiBold,
  TextRegular,
  HeaderTitle,
} from '@components/global';
import {rupiahFormat, showToastMessage} from '@helpers/functionFormat';
import Clipboard from '@react-native-community/clipboard';
import moment from 'moment';
import 'moment/locale/id';
import {inputRupiah} from '@helpers/functionFormat';
import ModalSuccessPayment from '../payment/component/ModalSuccessPayment';
import WebView from 'react-native-webview';
import {TextInput} from 'react-native-gesture-handler';

const Paynow = ({navigation, route}) => {
  console.log('route paynow', route.params.item);
  const [successPayment, setSuccessPayment] = useState(false);
  const [isModal, setIsModal] = useState(false);
  const [image, setImage] = useState(null);

  const copyToClipboard = string => {
    Clipboard.setString(string);
    showToastMessage('Nomor Rekening Berhasil Disalin');
  };

  const openPembayaran = () => {
    setIsModal(true);
  };

  const verifikasiPembayaran = () => {
    setIsModal(true);
  };

  const renderModal = () => {
    return (
      <Modal animationType="fade" transparent={true} visible={isModal}>
        <View
          onPress={() => setIsModal(false)}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#00000040',
          }}>
          <View
            style={{
              width: '80%',
              height: '70%',
            }}>
            <WebView
              source={{
                uri:
                  route?.params?.item?.invoice_url ??
                  route?.params?.item?.payment,
              }}
              style={{flex: 1}}
            />
          </View>
          <TouchableOpacity onPress={() => setIsModal(false)}>
            <TextBold
              text={'X'}
              size={24}
              color={'#FFFFFF'}
              style={{marginTop: 8}}
            />
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };

  const chooseImage = () => {
    if (image == null) {
      alert('File tidak boleh kosong!');
    }
  };

  const renderModalForm = () => {
    return (
      <Modal animationType="fade" transparent={true} visible={true}>
        <TouchableOpacity
          onPress={() => setIsModal(false)}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#00000040',
          }}>
          <View
            style={{
              width: '90%',
              backgroundColor: '#FFFFFF',
              borderRadius: 10,
              padding: 16,
            }}>
            <TextBold
              text={'Foto Bukti Pembayaran'}
              size={14}
              color={Colors.blackBase}
              style={{marginTop: 8}}
            />

            <TouchableOpacity
              onPress={chooseImage}
              style={{
                width: '35%',
                height: 40,
                backgroundColor: Colors.primary,
                borderRadius: 8,
                marginTop: 16,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TextBold text={'Pilih Foto'} size={14} color={'#FFF'} />
            </TouchableOpacity>

            <TextBold
              text={'File belum dipilih'}
              size={14}
              color={Colors.blackBase}
              style={{marginTop: 8}}
            />

            <TouchableOpacity
              onPress={chooseImage}
              style={{
                width: '100%',
                height: 40,
                backgroundColor: Colors.secondary,
                borderRadius: 30,
                marginTop: 32,
                paddingStart: 16,
                justifyContent: 'center',
              }}>
              <TextBold
                text={'Kirim Bukti Pembayaran'}
                size={14}
                color={'#FFF'}
              />
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.secondary}}>
      {route.params.showHeader ? <HeaderTitle title="" /> : null}
      {renderModal()}
      {/* {renderModalForm()} */}
      <ModalSuccessPayment
        show={successPayment}
        onClose={() => setSuccessPayment(false)}
        onPress={() => {
          setSuccessPayment(false);
          navigation.replace('MainNavigation');
        }}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingHorizontal: 18,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={IconAsset.IcSuccess}
          style={{width: 80, height: 80, resizeMode: 'contain', marginTop: 20}}
        />
        <TextBold
          text="Pesanan Anda telah berhasil dibuat!"
          size={16}
          color="#fff"
          style={{marginTop: 30}}
        />
        <View style={styles.whiteCard}>
          <TextSemiBold text="ID Pesanan" color={Colors.grey_1} />
          <TextBold
            text={route.params.item.transaction_id}
            size={14}
            color={Colors.blackBase}
            style={{marginTop: 8}}
          />
          <View
            style={{
              marginTop: 18,
              width: '100%',
              height: 2,
              backgroundColor: Colors.border,
            }}
          />
          <TextSemiBold
            text="Bayar Sekarang"
            color={Colors.grey_1}
            style={{marginTop: 18}}
          />
          <TouchableOpacity
            onPress={openPembayaran}
            style={{
              marginTop: 8,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              width: '100%',
            }}>
            <TextBold
              text={'Lanjut ke halaman pembayaran'}
              size={14}
              color="blue"
            />
          </TouchableOpacity>
          <View
            style={{
              marginTop: 18,
              width: '100%',
              height: 2,
              backgroundColor: Colors.border,
            }}
          />
          {/* <TextSemiBold
            text="Status tidak berubah ?"
            color={Colors.grey_1}
            style={{marginTop: 18}}
          />
          <TouchableOpacity
            onPress={verifikasiPembayaran}
            style={{
              marginTop: 8,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              width: '100%',
            }}>
            <TextBold
              text={'Verifikasi Pembayaran Manual'}
              size={14}
              color={Colors.primary}
            />
          </TouchableOpacity> */}
        </View>
        <TouchableOpacity
          style={{
            width: '100%',
            backgroundColor: '#fff',
            borderRadius: 24,
            paddingVertical: 17,
            justifyContent: 'center',
            alignItems: 'center',
            marginVertical: 25,
          }}
          onPress={() => setSuccessPayment(true)}>
          <TextBold
            text="Kembali ke Beranda"
            size={16}
            color={Colors.secondary}
          />
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  whiteCard: {
    marginTop: 32,
    width: '100%',
    borderRadius: 16,
    backgroundColor: '#fff',
    padding: 16,
  },
  textInput: {
    width: '100%',
    borderRadius: 10,
    borderColor: Colors.borderGrey,
    borderWidth: 1,
    padding: 10,
  },
});

export const screenOptions = navData => {
  return {
    headerTitle: '',
    headerTitleAlign: 'left',
    headerTitleStyle: {
      fontFamily: Fonts.mulishExtraBold,
      fontSize: Size.scaleFont(16),
      color: '#333333',
    },
    headerStyle: {
      backgroundColor: Colors.secondary,
    },
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft: Size.scaleSize(5)}}
        onPress={() => navData.navigation.goBack()}>
        <View style={{padding: Size.scaleSize(10)}}>
          <Icons.ChevronLeft color="#fff" size={Size.scaleSize(16)} />
        </View>
      </TouchableOpacity>
    ),
  };
};

export default Paynow;
