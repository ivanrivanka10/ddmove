import React from 'react';
import {View, Image, ScrollView, StyleSheet, FlatList, Pressable} from 'react-native';
import {
  Touchable,
  TextBold,
  TextRegular,
  TextSemiBold,
} from '@components/global';
import {Colors} from '@styles/index';
import {IconAsset, ImageAsset} from '@utils/index';
import moment from 'moment';
import {rupiahFormat} from '@helpers/functionFormat';
import {showToastMessage} from '@helpers/functionFormat';
import Clipboard from '@react-native-community/clipboard';

const TransactionDetailComponent = ({
  dataDetail,
  changeStatus,
  onPressInvoice,
  onPressChatAdmin,
  onPressChatSeller,
  onPressChatEkspedisi,
}) => {
  console.log('=== dataDetail', dataDetail);

  const copyToClipboard = string => {
    Clipboard.setString(string);
    showToastMessage('Nomor Resi Berhasil Disalin');
  };

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{paddingBottom: 100}}>
      {
        //order id
      }
      <View style={styles.cardWhite}>
        <View style={styles.viewStatus}>
          <TextBold
            text={dataDetail ? dataDetail.status : ''}
            color={Colors.yellowBase}
          />
        </View>
        <View style={styles.rowBetween}>
          <TextBold
            text={dataDetail ? dataDetail.transaction_id : ''}
            color={Colors.blackBase}
            style={{maxWidth: '50%'}}
          />
          <Pressable onPress={() => onPressInvoice(dataDetail.transaction_id ?? "")}>
            <TextSemiBold text={'Lihat Invoice'} color={Colors.blackBase} />
          </Pressable>
        </View>
        <View style={styles.rowBetween}>
          <TextBold
            text={'Tanggal Pembelian'}
            color={Colors.blackBase}
            style={{maxWidth: '50%'}}
          />
          <View
            style={{
              maxWidth: '48%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TextRegular
              text={
                dataDetail
                  ? moment(dataDetail.transaction_date).format('DD MMM YYYY')
                  : ''
              }
              color={Colors.normal}
            />
            <View
              style={{
                width: 4,
                height: 4,
                marginHorizontal: 8,
                borderRadius: 2,
                backgroundColor: Colors.normal,
              }}
            />
            <TextRegular
              text={
                dataDetail
                  ? moment(dataDetail.transaction_date).format('HH:mm')
                  : ''
              }
              color={Colors.normal}
            />
          </View>
        </View>
      </View>

      {
        //rincian produk
      }
      <View style={[styles.cardWhite, {marginTop: 18}]}>
        <TextBold text="Rincian Produk" size={16} color={Colors.blackBase} />
        <FlatList
          data={dataDetail ? dataDetail.product : []}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => {
            return (
              <View style={styles.cardProduct}>
                <View
                  style={{
                    width: '30%',
                  }}>
                  <Image
                    // source={ImageAsset.DummyProduct}
                    source={{uri: item.photo}}
                    style={{width: '100%', height: 75, resizeMode: 'contain'}}
                  />
                </View>
                <View style={{width: '70%', paddingLeft: 10}}>
                  <TextSemiBold text={item.name} color={Colors.blackBase} />
                  {/* <TextRegular
                                        text="40 kg"
                                        size={12}
                                        color={Colors.grey_1}
                                        style={{marginVertical: 8}}
                                    /> */}
                  <View style={styles.rowBetween}>
                    <TextRegular
                      text={rupiahFormat(`${item.price}`)}
                      color={Colors.primary}
                    />
                    <TextRegular text={`${item.qty}x`} color={Colors.grey_1} />
                  </View>
                </View>
              </View>
            );
          }}
        />
      </View>

      {
        //detail pengiriman
      }
      <View style={[styles.cardWhite, {marginTop: 18}]}>
        <TextBold text="Detail Pengiriman" size={16} color={Colors.blackBase} />
        <View style={[styles.rowBetween, {marginTop: 18}]}>
          <TextRegular text="Kurir" color={Colors.grey_1} />
          <TextRegular
            text={dataDetail.shipping.shipping_name}
            color={Colors.blackBase}
          />
        </View>
        <View style={[styles.rowBetween, {marginTop: 18}]}>
          <TextRegular text="No. Resi" color={Colors.grey_1} />
          <Touchable
            onPress={() => copyToClipboard(dataDetail.shipping.resi)}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TextRegular
              text={dataDetail ? dataDetail.shipping.resi : ''}
              color={Colors.primary}
            />
            <Image
              source={IconAsset.IcCopy}
              style={{width: 20, height: 20, marginLeft: 10}}
            />
          </Touchable>
        </View>
        <View style={[styles.rowBetween, {marginTop: 18}]}>
          <TextRegular text="Nama Toko" color={Colors.grey_1} />
          <View
            style={{
              width: '60%',
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
            }}>
            <TextRegular
              text={dataDetail ? `${dataDetail.shop_name}` : ''}
              color={Colors.blackBase}
              style={{textAlign: 'right'}}
              numberOfLines={10}
            />
          </View>
        </View>
        <View style={[styles.rowBetween, {marginTop: 18}]}>
          <TextRegular text="Detail Alamat Tujuan" color={Colors.grey_1} />
          <View
            style={{
              width: '60%',
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
            }}>
            <TextRegular
              text={dataDetail ? `${dataDetail.shipping.name} - ${dataDetail.shipping.province},${dataDetail.shipping.city},${dataDetail.shipping.district} ${dataDetail.shipping.address_detail}` : ''}
              color={Colors.blackBase}
              style={{textAlign: 'right'}}
              numberOfLines={10}
            />
          </View>
        </View>
      </View>

      {
        //detail payment
      }
      <View style={[styles.cardWhite, {marginTop: 18}]}>
        <TextBold text="Detail Pembayaran" size={16} color={Colors.blackBase} />
        <View style={[styles.rowBetween, {marginTop: 18}]}>
          <TextRegular text="Metode Pembayaran" color={Colors.grey_1} />
          <TextRegular
            text={
              dataDetail
                ? `${dataDetail.payment.bank} - ${dataDetail.payment.type}`
                : ''
            }
            color={Colors.blackBase}
          />
        </View>
        <View style={[styles.rowBetween, {marginTop: 18}]}>
          <TextRegular text="Item" color={Colors.grey_1} />
          <TextRegular
            text={dataDetail ? dataDetail.product.length : ''}
            color={Colors.blackBase}
          />
        </View>
        <View style={[styles.rowBetween, {marginTop: 18}]}>
          <TextRegular text="Biaya Pengiriman" color={Colors.grey_1} />
          <TextRegular
            text={dataDetail ? rupiahFormat(`${dataDetail.shipping_cost}`) : ''}
            color={Colors.blackBase}
          />
        </View>
        <View style={[styles.rowBetween, {marginTop: 18}]}>
          <TextRegular text="Biaya Penanganan" color={Colors.grey_1} />
          <TextRegular
            text={dataDetail ? rupiahFormat(`${dataDetail.handling_fee}`) : ''}
            color={Colors.blackBase}
          />
        </View>
        <View style={[styles.rowBetween, {marginTop: 18}]}>
          <TextRegular text="Total Pembayaran" color={Colors.grey_1} />
          <TextRegular
            text={dataDetail ? rupiahFormat(`${dataDetail.grand_total}`) : ''}
            color={Colors.blackBase}
          />
        </View>
      </View>
      {dataDetail.status === 'Success' ? null : dataDetail.status ===
        'Being packed' ? null : dataDetail.status ==
        'Waiting to be processed' ? null : (
        <Touchable
          style={styles.button}
          onPress={() => changeStatus('success')}>
          <TextBold text={'Barang Diterima'} size={16} color="#fff" />
        </Touchable>
      )}

      {dataDetail.status === 'Success' ? null : (
        <Touchable
          onPress={onPressChatAdmin}
          style={[
            styles.button,
            {
              backgroundColor: '#fff',
            },
          ]}>
          <TextBold text="Hubungi Admin" size={16} color={Colors.primary} />
        </Touchable>
      )}
      {dataDetail.status === 'Success' ? null : (
        <Touchable
          onPress={onPressChatSeller}
          style={[
            styles.button,
            {
              backgroundColor: Colors.bgGrey,
              borderWidth: 0,
            },
          ]}>
          <TextBold text="Hubungi Penjual" size={16} color={Colors.primary} />
        </Touchable>
      )}
      {dataDetail.type_product === 'material' &&
        dataDetail.status !== 'Success' && (
          <Touchable
            onPress={onPressChatEkspedisi}
            style={[
              styles.button,
              {
                backgroundColor: Colors.bgGrey,
                borderWidth: 0,
                marginBottom: 100,
              },
            ]}>
            <TextBold
              text="Hubungi Ekspedisi"
              size={16}
              color={Colors.primary}
            />
          </Touchable>
        )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  button: {
    marginTop: 18,
    borderColor: Colors.primary,
    borderWidth: 1,
    backgroundColor: Colors.primary,
    borderRadius: 16,
    paddingVertical: 17,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardProduct: {
    marginTop: 18,
    borderRadius: 16,
    borderColor: Colors.border,
    borderWidth: 1,
    padding: 18,
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewStatus: {
    marginBottom: 10,
    width: '100%',
    borderRadius: 8,
    paddingVertical: 10,
    backgroundColor: Colors.semiYellowBg,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowBetween: {
    marginVertical: 5,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  cardWhite: {
    width: '100%',
    borderRadius: 16,
    backgroundColor: '#fff',
    padding: 18,
  },
});

export default TransactionDetailComponent;
