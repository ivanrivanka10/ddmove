import React from 'react'
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    StyleSheet,
    Alert,
    Platform,
    Linking
} from 'react-native'
import { Size, Fonts, Icons, Colors } from '@styles/index';
import TransactionDetailComponent from './component/TransactionDetailComponent';
import { useSelector, useDispatch } from 'react-redux';
import API from '@utils/services/buyer/orderProvider'
import {showToastMessage} from '@helpers/functionFormat'
import {HeaderTitle} from '@components/global'
import APICHAT from '@utils/services/buyer/chatProvider'

import ModalChatAdmin from '@components/modal/ModalChatAdmin';

const TransactionDetail = ({
    navigation,
    route
}) => {
    console.log('route trans detail', route);
    const { userData } = useSelector(state => state.auth);
    const dispatch = useDispatch(); 
    const [dataDetail, setDataDetail] = React.useState({})
    const [sentTo, setSentTo] = React.useState('admin')
    const [showChat, setShowChat] = React.useState(false)
    const [message, setMessage] = React.useState('')

    const getDetail = async () => {
        const params = {
            transaction_id: route.params.item.transaction_id
        }
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetDetailOrder(params, userData.token);
        console.log('=== res detail', res);
        dispatch({type: "HIDE_LOADING"});
        if(res.status === 200) {
            setDataDetail(res.data.data)
        } else {
            if(res.status === 500) {
                Alert.alert('Server error', 'Silahkan coba lagi')
            }
        }
    }

    const changeStatus = async (status) => {
        const data = new FormData();
        data.append('transaction_id', route.params.item.transaction_id)
        data.append('status', status)
        const res = await API.ChangeStatus(data, userData.token)
        console.log('res change status', res);
        if(res.status === 200) {
            showToastMessage('Status Transaksi Berhasil Diubah')
            navigation.goBack()
        }
    }

    const openInvoice = async (id) => {
        const params = {
            transaction_id: id
        }
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetInvoice(params, userData.token);
        dispatch({type: "HIDE_LOADING"});
        if(res.status === 200) {
            console.log(res.data.data);
            await Linking.openURL(res.data.data)
        } else {
            if(res.status === 500) {
                Alert.alert('Server error', 'Silahkan coba lagi')
            }
        }
    }

    const sendChat = async () => {
        setShowChat(false)
        if(sentTo === 'admin') {
            const formData = new FormData();
            formData.append('message', message)
            formData.append('role', userData.is_seller === "no" ? "buyer" : "seller")
            dispatch({type: "SHOW_LOADING"});
            const res = await APICHAT.SendChatToAdmin(formData, userData.token)
            console.log('res chat admin', res);
            dispatch({type: "HIDE_LOADING"});
            if(res.status === 200) {
                setMessage('')
                return showToastMessage('Pesan anda telah dikirim, admin akan memproses pesan anda')
            } else {
                return showToastMessage('Pesan gagal dikirim, silahkan coba lagi')
            }
        }   else if(sentTo === 'ekspedition') {
            const formData = new FormData();
            formData.append('message', message)
            formData.append('expedition_id', dataDetail.shipping.expedition_id)
            dispatch({type: "SHOW_LOADING"});
            const res = await APICHAT.SendChatToEkspedition(formData, userData.token)
            console.log('res chat ekspedisi', res);
            dispatch({type: "HIDE_LOADING"});
            if(res.status === 200) {
                setMessage('')
                return showToastMessage('Pesan anda telah dikirim ke ekspedisi')
            } else {
                return showToastMessage('Pesan gagal dikirim, silahkan coba lagi')
            }
        } else {
            dispatch({type: "SHOW_LOADING"});
            const formData = new FormData();
            formData.append('shop_id', dataDetail.shop_id);
            formData.append('product_id', null);
            formData.append('message', message);
            const res = await APICHAT.CreateChat(formData, userData.token)
            console.log('res create chat', res);
            dispatch({type: "HIDE_LOADING"});
            if(res.status === 200) {
                setMessage('')
                return showToastMessage('Pesan anda telah dikirim')
            } else {
                return showToastMessage('Pesan gagal dikirim, silahkan coba lagi')
            }
        }        
    }

    React.useEffect(() => {
        getDetail();
    },[])

    return(
        <View style={{flex: 1, backgroundColor: Colors.bgGrey}}>
            <ModalChatAdmin
                show={showChat}
                onClose={() => setShowChat(false)}
                sendTo={sentTo === 'admin' ? "Admin" : sentTo === 'ekspedition' ? "Ekspedisi" : dataDetail.shop_name}
                message={message}
                setMessage={(text) => setMessage(text)}
                onSubmit={() => sendChat()}
            />
            <HeaderTitle
                title="Detail"
            />
            <View style={{padding: 18}}>
                {
                    Object.keys(dataDetail).length === 0 ?
                        null
                    :
                        <TransactionDetailComponent
                            dataDetail={dataDetail}
                            changeStatus={(status) => changeStatus(status)}
                            onPressChatAdmin={() => {
                                setSentTo('admin')
                                setTimeout(() => {
                                    setShowChat(true)
                                }, 500);
                            }}
                            onPressInvoice={(id) => openInvoice(id)}
                            onPressChatSeller={() => {
                                setSentTo('seller')
                                setTimeout(() => {
                                    setShowChat(true)
                                }, 500);
                            }}
                            onPressChatEkspedisi={() => {
                                setSentTo('ekspedition')
                                setTimeout(() => {
                                    setShowChat(true)
                                }, 500);
                            }}
                        />
                }
            </View>            
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: "Detail",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default TransactionDetail;