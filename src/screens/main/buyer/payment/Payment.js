import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  FlatList,
  ScrollView,
  StatusBar,
  ActivityIndicator,
  Text,
} from 'react-native';
import {IconAsset, ImageAsset} from '@utils/index';
import {Size, Fonts, Icons, Colors} from '@styles/index';
import {
  TextBold,
  TextSemiBold,
  TextRegular,
  Touchable,
  InputText,
  HeaderTitle,
  SelectDropDown,
  CheckBox,
  SpinnerInputNumber,
} from '@components/global';
import ModalPayment from './component/ModalPayment';
import ModalCourier from './component/ModalCourier';
import ModalSuccessPayment from './component/ModalSuccessPayment';
import {useSelector, useDispatch} from 'react-redux';
import API from '@utils/services/settingProvider';
import APICHECKOUT from '@utils/services/buyer/homeProvider';
import {useIsFocused} from '@react-navigation/native';
import {rupiahFormat, showToastMessage} from '@helpers/functionFormat';

import AddressSection from './component/AdressSection';
import {Pressable} from 'react-native';

const Payment = ({navigation, route}) => {
  const {userData} = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const [showModalPayment, setShowModalPayment] = useState(false);
  const [showModalCourier, setShowModalCourier] = useState(false);
  const [successPayment, setSuccessPayment] = useState(false);
  const [mainAdress, setMainAddress] = useState({});
  const [selectedCourier, setSelectedCourier] = useState({});
  const [selectedPayment, setSelectedPayment] = useState({});
  const [priceTotalAll, setPriceTotalAll] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const [note, setNote] = useState('');
  const [pickedCourier, setPickedCourier] = useState({});
  const [dataCheckout, setDataCheckout] = useState({});
  const [paymentSuccess, setPaymentSuccess] = useState(false);
  const [metodePengiriman, setMetodePengiriman] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [checkOngkir, setCheckOngkir] = useState(null);
  const [checked, setChecked] = useState(null);
  const [checked1, setChecked1] = useState(null);
  const [dataCO, setDataCO] = useState(route.params.product);

  console.log('ini params checkout', route.params);

  const getListAddress = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.GetAddress(userData.token);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status == 200) {
      // setListAddress(res.data.data)
      const data = res.data.data;
      var filter = data.filter(value => {
        return value.is_main === 'yes';
      });
      if (filter.length > 0) {
        setMainAddress(filter[0]);
      }
    }
    if (!paymentSuccess) {
      reCheckout();
    }
  };

  //trigger function to get list courier when user switch main address
  const reCheckout = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await APICHECKOUT.Checkout(
      route.params.dataCheckout,
      userData.token,
    );
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      setDataCheckout(res.data.data);
    } else {
      showToastMessage('Terjadi kesalahan, silahkan ulangi proses checkout');
      return navigation.goBack();
    }
  };

  const countPrice = React.useCallback(() => {
    var total = 0;
    total =
      priceTotalAll +
      parseInt(checked1?.price ?? 0) +
      parseInt(checked?.total_price ?? 0);
    return rupiahFormat(total);
  }, [checked, checked1]);

  const createPayment = async () => {
    var productId = [];
    const params = route.params;
    params.product.map(value => {
      productId.push(value.product_id);
    });

    // if(metodePengiriman?.id == 'full_commercial' && checked == null){
    //   return showToastMessage(
    //     'Silahkan pilih metode pengiriman terlebih dahulu',
    //   );
    // }else if(metodePengiriman?.id == 'full_babamarket' && checked1 == null){
    //   return showToastMessage(
    //     'Silahkan pilih metode pengiriman terlebih dahulu',
    //   );
    // }else if(metodePengiriman?.id == 'babamarket_commercial' && checked == null || checked1 == null){
    //   return showToastMessage(
    //     'Silahkan pilih metode pengiriman terlebih dahulu',
    //   );
    // }

    // if (Object.keys(selectedPayment).length === 0) {
    //   return showToastMessage(
    //     'Silahkan pilih metode pembayaran terlebih dahulu',
    //   );
    // }
    if (Object.keys(mainAdress).length === 0) {
      return showToastMessage(
        'Silahkan pilih alamat pengiriman terlebih dahulu',
      );
    }
    const data = new FormData();
    data.append('shop_id', route.params.data.shop_id);
    data.append('product_id', `${productId.join(',')},`);
    data.append('address_id', mainAdress.id);
    if (metodePengiriman?.id == 'full_commercial') {
      data.append('shipping_id', '');
    }
    if (metodePengiriman?.id == 'babamarket_commercial') {
      data.append('shipping_id', parseInt(checked?.id ?? checked1?.id ?? ''));
    }

    data.append(
      'shipping_cost',
      metodePengiriman.id == 'babamarket_commercial'
        ? parseInt(checked?.total_price ?? 0) +
            parseInt(checked?.price ?? 0) +
            parseInt(checked1?.price ?? 0)
        : checked?.total_price ?? checked?.price ?? 0,
    );
    data.append('subtotal', `${priceTotalAll}`);
    // data.append('payment_method', selectedPayment.id);
    data.append('note', note ?? '-');
    data.append('type_product', params.data.type_product);
    data.append('courier', checked?.logistic?.code ?? checked?.code);
    data.append('courier_service', checked?.rate?.name ?? checked?.name);
    data.append('shipping_type', metodePengiriman.id);
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.CreatePayment(data, userData.token);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      setPaymentSuccess(true);
      navigation.navigate('OrderTab', {
        screen: 'Paynow',
        params: {
          item: res.data.data,
          showHeader: false,
        },
      });
    } else {
      return showToastMessage(res?.data?.info ?? '-');
    }
  };

  const getDetailOngkir = async pick => {
    setIsLoading(true);
    setChecked(null);
    setChecked1(null);
    var productId = [];
    const params = route.params;
    params.product.map(value => {
      productId.push(value.product_id);
    });

    const data = new FormData();
    productId.map((e, i) => {
      data.append('product_id[' + i + ']', e);
    });
    data.append('shipping_type', pick);
    data.append('address_id', mainAdress.id);

    let response = await APICHECKOUT.CheckOngkir(data, userData.token);

    if (response?.data?.data?.info) {
      setCheckOngkir(null);
      alert(response?.data?.data?.info);
    } else {
      setCheckOngkir(response?.data?.data ?? null);
    }

    setIsLoading(false);
  };

  React.useEffect(() => {
    var totalAll = 0;
    for (let i = 0; i < dataCO.length; i++) {
      totalAll += dataCO[i].price * Number(dataCO[i].qty);
    }
    setPriceTotalAll(totalAll);
  }, [dataCO]);

  React.useEffect(() => {
    setDataCheckout(route.params.data);
    getListAddress();
  }, [isFocused]);
  // console.log('=== route payment', route);
  // console.log('userData payment', userData);
  // console.log('selectedkurir payment', selectedCourier);

  const changeQty = index => {
    console.log('ini value dan index', index);
    // dataCO[index].qty = value.toString();
  };
  return (
    <View style={{flex: 1, backgroundColor: Colors.bgGrey}}>
      <StatusBar barStyle="dark-content" backgroundColor={'#FFF'} />
      <HeaderTitle title="Checkout" />
      <ModalPayment
        show={showModalPayment}
        onClose={() => setShowModalPayment(false)}
        data={dataCheckout.payment_method}
        onSubmit={data => {
          setSelectedPayment(data);
          setShowModalPayment(false);
        }}
      />
      <ModalCourier
        show={showModalCourier}
        onClose={() => setShowModalCourier(false)}
        data={dataCheckout.expeditions}
        onSubmit={data => {
          setSelectedCourier(data);
          setShowModalCourier(false);
        }}
        type={dataCheckout.type_product}
      />
      <ModalSuccessPayment
        show={successPayment}
        onClose={() => setSuccessPayment(false)}
        onPress={() => {
          setSuccessPayment(false);
          navigation.replace('MainNavigation');
        }}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.viewWhite}>
          <FlatList
            data={dataCO}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => {
              return (
                console.log('ini itemmmmm', item),
                (
                  <View style={styles.cardProduct}>
                    <View style={{width: '20%'}}>
                      <Image
                        // source={ImageAsset.Kulkas}
                        source={{uri: item.photo}}
                        style={{
                          width: '100%',
                          height: 75,
                          resizeMode: 'contain',
                        }}
                      />
                    </View>
                    <View style={{width: '80%', paddingLeft: 10}}>
                      <TextSemiBold text={item.name} color={Colors.blackBase} />
                      <TextRegular
                        text={
                          item.type === 'material'
                            ? 'Bahan Bangunan'
                            : 'Non Bahan Bangunan'
                        }
                        size={12}
                        color={Colors.grey_1}
                        style={{marginTop: 8}}
                      />
                      <View
                        style={{
                          marginTop: 8,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <TextRegular
                          text={rupiahFormat(item.price)}
                          color={Colors.primary}
                        />

                        <SpinnerInputNumber
                          minCount={item.minimum_purchase}
                          count={`${item.qty}`}
                          maxCount={item.stock}
                          // onChange={val => {
                          //   // handleQuantity(index, i, val)
                          // changeQty(val);
                          // }}
                          // onChange={v => console.log(item.product[index].qty)}

                          onChange={v => {
                            const data = [...dataCO];
                            data[index].qty = v;
                            console.log('ini data setelah', data[index]);
                            setDataCO(data);
                          }}
                        />
                        {/* <TextRegular text={item.product[index].qty} /> */}
                        {/* <TextRegular
                        text={`${item.qty}x`}
                        color={Colors.grey_1}
                      /> */}
                      </View>
                    </View>
                  </View>
                )
              );
            }}
          />
          <TextRegular
            text="Catatan Tambahan (jika ada)"
            color={Colors.normal}
            style={{marginTop: 10}}
          />
          {/* <TextInput
                        style={{
                            width: '100%',
                            marginTop: 5,
                            borderRadius: 10,
                            borderColor: Colors.border,
                            borderWidth: 1,
                            paddingHorizontal: 16
                        }}
                        placeholder="Masukkan catatan"
                        value={note}
                        onChangeText={(text) => setNote(text)}
                    /> */}
          <InputText
            containerStyle={{
              marginHorizontal: Size.scaleSize(1),
              marginTop: 10,
            }}
            placeholder="Masukan catatan"
            value={note}
            onChangeText={text => setNote(text)}
            maxLength={255}
          />
        </View>
        {
          //address section
        }
        {Object.keys(mainAdress).length === 0 ? null : (
          <AddressSection
            data={mainAdress}
            onPressChange={() => {
              navigation.navigate('Address', {
                fromPayment: true,
              });
            }}
          />
        )}
        {
          //courir section
        }
        <View style={[styles.viewWhite, {marginTop: 10}]}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'flex-start',
            }}>
            <Image
              source={IconAsset.CourierBorder}
              style={{width: 40, height: 40}}
            />
            <View style={{marginLeft: 10}}>
              <TextSemiBold text="Metode Pengiriman" color={Colors.blackBase} />
              <TextRegular
                text="Pilih jasa pengiriman"
                size={12}
                color={Colors.grey_3}
              />
            </View>
          </View>

          <View
            style={{
              marginTop: 18,
              flexDirection: 'row',
            }}>
            <TextBold text="Pilih Metode Pengiriman" color={Colors.primary} />
          </View>

          <SelectDropDown
            searchable
            value={metodePengiriman?.name ?? null}
            onSelectItem={val => {
              setCheckOngkir(null);
              setMetodePengiriman(val);
              getDetailOngkir(val.id);
            }}
            valueColor="#000"
            colorArrowIcon="#000"
            style={{marginTop: 8, borderRadius: 6, color: '#000'}}
            placeholderText="Pilih metode pengiriman"
            placeholderTextColor={'#000'}
            items={[
              {id: 'full_commercial', name: 'Full Commercial'},
              {id: 'full_babamarket', name: 'Full Babamarket'},
              {id: 'babamarket_commercial', name: 'Babamarket Commercial'},
            ]}
            maxHeight={300}
            typeValueText="Medium"
          />

          {isLoading && (
            <View style={{marginVertical: 8, flexDirection: 'row'}}>
              <ActivityIndicator color={'blue'} />
            </View>
          )}

          {checkOngkir?.shipping_cost?.babamarket_expedition.length > 0 && (
            <>
              <View style={{height: 15}} />
              <TextBold
                size={11}
                text={'Pilih salah satu'}
                color={Colors.primary}
              />
              <FlatList
                data={checkOngkir?.shipping_cost?.babamarket_expedition}
                renderItem={({item, index}) => {
                  return (
                    <View
                      style={{
                        width: '100%',
                        flexDirection: 'row',
                        marginTop: 18,
                        justifyContent: 'space-between',
                      }}>
                      <View
                        style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Pressable
                          onPress={() => {
                            setChecked1(item);
                            countPrice();
                          }}
                          style={{
                            height: 15,
                            width: 15,
                            backgroundColor:
                              checked1 == item ? Colors.primary : 'white',
                            marginEnd: 10,
                            borderWidth: 1,
                            borderColor: '#000',
                          }}
                        />
                        <TextBold
                          size={11}
                          text={item?.name ?? '-'}
                          color={Colors.blackBase}
                        />
                      </View>
                      <TextBold
                        size={11}
                        text={rupiahFormat(parseInt(item?.price ?? 0))}
                        color={Colors.primary}
                      />
                    </View>
                  );
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </>
          )}

          {checkOngkir?.shipping_cost?.shipper.length > 0 && (
            <>
              <View style={{height: 15}} />
              <TextBold
                size={11}
                text={'Pilih salah satu'}
                color={Colors.primary}
              />
              <FlatList
                data={checkOngkir?.shipping_cost?.shipper}
                renderItem={({item, index}) => {
                  return (
                    <View
                      style={{
                        width: '100%',
                        flexDirection: 'row',
                        marginTop: 18,
                        justifyContent: 'space-between',
                      }}>
                      <View
                        style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Pressable
                          onPress={() => {
                            setChecked(item);
                            countPrice();
                          }}
                          style={{
                            height: 15,
                            width: 15,
                            backgroundColor:
                              checked == item ? Colors.primary : 'white',
                            marginEnd: 10,
                            borderWidth: 1,
                            borderColor: '#000',
                          }}
                        />
                        <TextBold
                          size={11}
                          text={
                            item?.logistic?.name + '(' + item?.rate?.type + ')'
                          }
                          color={Colors.blackBase}
                        />
                      </View>
                      <TextBold
                        size={11}
                        text={rupiahFormat(parseInt(item?.total_price ?? 0))}
                        color={Colors.primary}
                      />
                    </View>
                  );
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </>
          )}
        </View>
        {
          //payment section
        }
        {/* <View style={[styles.viewWhite, {marginTop: 10}]}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'flex-start',
            }}>
            <Image
              source={IconAsset.CourierBorder}
              style={{width: 40, height: 40}}
            />
            <View style={{marginLeft: 10}}>
              <TextSemiBold text="Metode Pembayaran" color={Colors.blackBase} />
              <TextRegular
                text="Pilih metode pembayaran"
                size={12}
                color={Colors.grey_3}
              />
            </View>
          </View>
          {Object.keys(selectedPayment).length !== 0 && (
            <View
              style={{
                marginTop: 18,
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View>
                <TextBold
                  text={selectedPayment.name}
                  color={Colors.blackBase}
                />
                <TextRegular
                  text={selectedPayment.description}
                  size={12}
                  color={Colors.grey_1}
                  style={{marginTop: 2}}
                />
              </View>
              <Image
                source={ImageAsset.LogoBca}
                style={{width: 40, height: 40}}
              />
            </View>
          )}
          <Touchable
            onPress={() => setShowModalPayment(true)}
            style={{
              marginTop: 18,
              flexDirection: 'row',
              alignItems: 'center',
              alignSelf: 'center',
            }}>
            <TextBold text="Pilih Pembayaran" color={Colors.primary} />
            <Image
              source={IconAsset.ArrowRight}
              style={{
                width: 25,
                height: 25,
                marginTop: 2,
                resizeMode: 'contain',
                marginLeft: 10,
              }}
            />
          </Touchable>
        </View> */}

        {
          //count section
        }
        <View style={[styles.viewWhite, {marginTop: 10}]}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View>
              <TextBold text="Sub Total" color={Colors.blackBase} />
              <TextRegular
                text={`${dataCO.length} item`}
                color={Colors.grey_1}
              />
            </View>
            <TextBold
              text={rupiahFormat(priceTotalAll)}
              color={Colors.blackBase}
            />
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              marginTop: 18,
              justifyContent: 'space-between',
            }}>
            <TextBold text="Total Ongkir" color={Colors.blackBase} />
            <TextBold
              text={rupiahFormat(
                parseInt(checked?.total_price ?? 0) +
                  parseInt(checked1?.price ?? 0),
              )}
              color={Colors.blackBase}
            />
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              marginTop: 18,
              justifyContent: 'space-between',
            }}>
            <TextBold text="Total Biaya" color={Colors.blackBase} />
            <TextBold text={countPrice()} color={Colors.primary} />
          </View>
        </View>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.btnPay}
          onPress={createPayment}>
          <TextBold text="Bayar Sekarang" size={16} color="#fff" />
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  cardProduct: {
    width: '100%',
    borderColor: Colors.border,
    borderWidth: 1,
    borderRadius: 16,
    padding: 18,
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnPay: {
    marginVertical: 20,
    alignSelf: 'center',
    width: '90%',
    borderRadius: 16,
    backgroundColor: Colors.primary,
    paddingVertical: 17,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewWhite: {
    width: '100%',
    paddingVertical: 24,
    paddingHorizontal: 18,
    backgroundColor: '#fff',
  },
});

export const screenOptions = navData => {
  return {
    headerTitle: 'Checkout',
    headerTitleAlign: 'left',
    headerTitleStyle: {
      fontFamily: Fonts.mulishExtraBold,
      fontSize: Size.scaleFont(16),
      color: '#333333',
    },
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft: Size.scaleSize(5)}}
        onPress={() => navData.navigation.goBack()}>
        <View style={{padding: Size.scaleSize(10)}}>
          <Icons.ChevronLeft color="#333" size={Size.scaleSize(18)} />
        </View>
      </TouchableOpacity>
    ),
  };
};

export default Payment;
