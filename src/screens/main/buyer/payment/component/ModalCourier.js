import React, {useState} from 'react'
import {
    View,
    Modal,
    StyleSheet,
    TouchableOpacity,
    FlatList,
    Dimensions,
    Image
} from 'react-native'
import {IconAsset, ImageAsset} from '@utils/index'
import {
    TextBold,
    TextSemiBold,
    TextRegular
} from '@components/global'
import {Colors} from '@styles/index'
import {rupiahFormat} from '@helpers/functionFormat'

const ModalCourier = ({
    show,
    data,
    onClose,
    onSubmit,
    type
}) => {

    const [selected, setSelected] = useState({})
    const [selectedService, setSelectedService] = useState({})
    const [indexService, setIndexService] = useState(undefined)
    const [activeSection, setActiveSection] = useState(undefined)

    const selectItem = (index) => {
        if(index === selected) {
            setSelected(undefined)
        } else {
            setSelected(index)
        }
    }

    return(
        <Modal
            transparent
            visible={show}
            onRequestClose={onClose}
        >
            <View style={styles.container}>
                <View style={styles.content}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.btnClose}
                        onPress={onClose}
                    >
                        <Image
                            source={IconAsset.DoubleArrowDown}
                            style={{width: 15, height: 15, resizeMode: 'contain'}}
                        />
                    </TouchableOpacity>
                    <TextBold
                        text="Pilih Jasa Pengiriman"
                        size={16}
                        color={Colors.blackBase}
                        style={{marginTop: 24}}
                    />
                    {
                        type === 'non-material' ?
                            <FlatList                        
                                data={data}
                                keyExtractor={(item, index) => index.toString()}
                                contentContainerStyle={{paddingBottom: 100}}
                                renderItem={({item, index}) => {
                                    return(
                                        <View>
                                            <TouchableOpacity
                                                activeOpacity={0.8}
                                                style={{
                                                    width: '100%',
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',
                                                    alignItems: 'center',
                                                    marginTop: 24
                                                }}
                                                onPress={() => {
                                                    if(index === activeSection) {
                                                        setActiveSection(undefined)
                                                        setSelected({})
                                                    } else {
                                                        setActiveSection(index)
                                                        setSelected(item)
                                                    }
                                                }}
                                            >
                                                <View style={{flexDirection: 'row', width: '100%', alignItems: 'flex-start', justifyContent: 'space-between'}}>
                                                    <TextBold
                                                        text={item.name}
                                                        color={Colors.blackBase}
                                                    />
                                                    <Image
                                                        source={activeSection === index ? IconAsset.ChevronUp : IconAsset.ChevronDown}
                                                        style={{width: 20, height: 20}}
                                                    />
                                                </View>                                        
                                            </TouchableOpacity>
                                            {
                                                activeSection === index && (
                                                    <FlatList
                                                        data={item.cost}
                                                        keyExtractor={(item, index) => index.toString()}
                                                        renderItem={({item, index}) => (
                                                            <TouchableOpacity
                                                                activeOpacity={0.8}
                                                                style={{
                                                                    marginTop: 10,
                                                                    width: '100%',
                                                                    padding: 5,
                                                                    flexDirection: 'row',
                                                                    alignItems: 'center',
                                                                    justifyContent: 'space-between'
                                                                }}
                                                                onPress={() => {                                                            
                                                                    if(index === indexService) {
                                                                        setIndexService(undefined)
                                                                        setSelectedService({})
                                                                    } else {
                                                                        setIndexService(index)
                                                                        setSelectedService(item)
                                                                    }                                                            
                                                                }}
                                                            >
                                                                <View>
                                                                    <TextBold
                                                                        text={item.service}
                                                                    />
                                                                    <View style={{width: '100%', flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'space-between'}}>
                                                                        <View style={{width: '70%'}}>
                                                                            <TextRegular
                                                                                text={`${item.description} : estimasi ${item.cost[0].etd.includes('HARI') ? item.cost[0].etd.replace('HARI', '') : item.cost[0].etd} Hari`}
                                                                            />
                                                                            <TextSemiBold
                                                                                text={`Biaya : ${rupiahFormat(item.cost[0].value)}`}
                                                                            />
                                                                        </View>
                                                                        <View style={{width: '30%', alignItems: 'flex-end'}}>
                                                                            <Image
                                                                                source={ indexService === index ? IconAsset.IcSelected : IconAsset.IcUnselect}
                                                                                style={{width: 20, height: 20}}
                                                                            />
                                                                        </View>
                                                                    </View>                                                    
                                                                </View>
                                                            </TouchableOpacity>
                                                        )}
                                                    />
                                                )
                                            }                                    
                                        </View>                                
                                    )                            
                                }}
                                ItemSeparatorComponent={() => (
                                    <View style={{width: '100%', height: 2, borderColor: Colors.border, borderWidth: 1, marginTop: 10}} />
                                )}
                            />
                        :
                            <FlatList
                                data={data}
                                keyExtractor={(item, index) => index.toString()}
                                contentContainerStyle={{paddingBottom: 100}}
                                renderItem={({item, index}) => (
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        style={{
                                            marginTop: 10,
                                            width: '100%',
                                            padding: 5,
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'space-between'
                                        }}
                                        onPress={() => {                                                            
                                            if(index === indexService) {
                                                setIndexService(undefined)
                                                setSelectedService({})
                                            } else {
                                                setIndexService(index)
                                                setSelectedService(item)
                                            }                                                            
                                        }}
                                    >
                                        <View>
                                            <TextBold
                                                text={item.name}
                                            />
                                            <View style={{width: '100%', flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'space-between'}}>
                                                <View style={{width: '70%'}}>
                                                    <TextRegular
                                                        text={`estimasi ${item.estimation} Hari`}
                                                    />
                                                    <TextSemiBold
                                                        text={`Biaya : ${rupiahFormat(item.shipping_cost)}`}
                                                    />
                                                </View>
                                                <View style={{width: '30%', alignItems: 'flex-end'}}>
                                                    <Image
                                                        source={ indexService === index ? IconAsset.IcSelected : IconAsset.IcUnselect}
                                                        style={{width: 20, height: 20}}
                                                    />
                                                </View>
                                            </View>                                                    
                                        </View>
                                    </TouchableOpacity>
                                )}
                            />
                    }                    
                    <View style={{
                        position: 'absolute',
                        bottom: 10,
                        width: '100%',
                        alignSelf: 'center',
                    }}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={[styles.btnSubmit, Object.keys(selectedService).length === 0 && {backgroundColor: Colors.grey_1}]}
                            disabled={Object.keys(selectedService).length === 0}
                            onPress={() => {
                                var courier;
                                if(type === 'material') {
                                    courier = {
                                        courierCode: '',
                                        service: '',
                                        cost: selectedService.shipping_cost,
                                        name: selectedService.name,
                                        id: selectedService.id,
                                        estimation: `${selectedService.estimation} hari`
                                    }
                                } else {
                                    courier = {
                                        courierCode: selected.code,
                                        service: selectedService.service,
                                        cost: selectedService.cost[0].value,
                                        name: selected.name,
                                        id: 0,
                                        estimation: `${selectedService.cost[0].etd.includes('HARI') ? selectedService.cost[0].etd.replace('HARI', '') : selectedService.cost[0].etd} hari`
                                    }
                                }
                                onSubmit(courier)
                            }}
                        >
                            <TextBold
                                text="Simpan"
                                size={16}
                                color="#fff"
                            />
                        </TouchableOpacity>
                    </View>                    
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    btnSubmit: {
        width: '100%',
        borderRadius: 16,
        paddingVertical: 17,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnClose: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        width: 150,
        paddingVertical: 8,
        backgroundColor: '#F7F9F8'
    },
    content: {
        width: '100%',
        maxHeight: Dimensions.get('window').height * 0.7,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        padding: 18
    },
    container: {
        flex: 1, 
        backgroundColor: 'rgba(0,0,0,0.4)', 
        justifyContent: 'flex-end', 
        alignItems: 'flex-end'
    }
})

export default ModalCourier