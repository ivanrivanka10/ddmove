import React from 'react'
import {
    View,
    StyleSheet,
    Image
} from 'react-native'
import {
    TextBold,
    TextRegular,
    TextSemiBold,
    Touchable
} from '@components/global'
import {IconAsset} from '@utils/index'
import {Colors} from '@styles/index'

const AddressSection = ({
    data,
    onPressChange
}) => {
    return(
        <View style={[styles.viewWhite, {marginTop: 10}]}>
            <View style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'flex-start'
            }}>
                <Image
                    source={IconAsset.LocationBorder}
                    style={{width: 40, height: 40}}
                />
                <View style={{marginLeft: 10}}>
                    <TextSemiBold
                        text="Alamat Pengiriman"
                        color={Colors.blackBase}
                    />
                    <TextRegular
                        text="Pilih alamat pengiriman kamu"
                        size={12}
                        color={Colors.grey_3}
                    />
                </View>
            </View>
            <TextBold
                text={data.label}
                color={Colors.blackBase}
                style={{marginTop: 18}}
            />
            <TextRegular
                text={data.address}
                size={12}
                color={Colors.blackBase}
                style={{marginTop: 4}}
                numberOfLines={2}
            />
            <Touchable 
                onPress={onPressChange}
                style={{
                    marginTop: 18,
                    flexDirection: 'row',
                    alignItems: 'center',
                    alignSelf: 'center'
                }}>
                <TextBold
                    text="Ganti Alamat"
                    color={Colors.primary}
                />
                <Image
                    source={IconAsset.ArrowRight}
                    style={{width: 25, height: 25, marginTop: 2, resizeMode: 'contain', marginLeft: 10}}
                />
            </Touchable>
        </View>
    )
}

const styles = StyleSheet.create({
    viewWhite: {
        width: '100%',
        paddingVertical: 24,
        paddingHorizontal: 18,
        backgroundColor: '#fff'
    }
})

export default AddressSection