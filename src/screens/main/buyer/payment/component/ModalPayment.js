import React, {useState} from 'react'
import {
    View,
    Modal,
    StyleSheet,
    TouchableOpacity,
    FlatList,
    Dimensions,
    Image
} from 'react-native'
import {IconAsset, ImageAsset} from '@utils/index'
import {
    TextBold,
    TextRegular
} from '@components/global'
import {Colors} from '@styles/index'

const ModalPayment = ({
    show,
    onClose,
    onSubmit,
    data
}) => {

    const [selected, setSelected] = useState(undefined)

    const selectItem = (index) => {
        if(index === selected) {
            setSelected(undefined)
        } else {
            setSelected(index)
        }
    }

    return(
        <Modal
            transparent
            visible={show}
            onRequestClose={onClose}
        >
            <View style={styles.container}>
                <View style={styles.content}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.btnClose}
                        onPress={onClose}
                    >
                        <Image
                            source={IconAsset.DoubleArrowDown}
                            style={{width: 15, height: 15, resizeMode: 'contain'}}
                        />
                    </TouchableOpacity>
                    <TextBold
                        text="Pilih Metode Pembayaran"
                        size={16}
                        color={Colors.blackBase}
                        style={{marginTop: 24}}
                    />
                    <TextBold
                        text="Bank"
                        size={16}
                        color={Colors.blackBase}
                        style={{marginTop: 5, alignSelf: 'center'}}
                    />
                    <FlatList                        
                        data={data}
                        keyExtractor={(item, index) => index.toString()}
                        contentContainerStyle={{paddingBottom: 100}}
                        renderItem={({item, index}) => {
                            return(
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={{
                                        width: '100%',
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        marginTop: 24
                                    }}
                                    onPress={() => selectItem(index)}
                                >
                                    <View style={{flexDirection: 'row'}}>
                                        <Image
                                            source={ImageAsset.LogoBca}
                                            style={{width: 25, height: 25}}
                                        />
                                        <View style={{marginLeft: 10}}>
                                            <TextBold
                                                text={item.name}
                                                color={Colors.blackBase}
                                            />
                                            <TextRegular
                                                text={item.description}
                                                size={12}
                                                color={Colors.grey_1}
                                                style={{marginTop: 4}}
                                            />
                                        </View>
                                    </View>
                                    {
                                        selected === index ?
                                            <Image
                                                source={IconAsset.IcSelected}
                                                style={{width: 20, height: 20}}
                                            />
                                        :
                                            <Image
                                                source={IconAsset.IcUnselect}
                                                style={{width: 20, height: 20}}
                                            />
                                    }
                                </TouchableOpacity>
                            )
                        }}
                    />
                    <View style={{
                        position: 'absolute',
                        bottom: 10,
                        width: '100%',
                        alignSelf: 'center',
                    }}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={[styles.btnSubmit, selected === undefined && {backgroundColor: Colors.grey_1}]}
                            disabled={selected === undefined}
                            onPress={() => onSubmit(data[selected])}
                        >
                            <TextBold
                                text="Simpan"
                                size={16}
                                color="#fff"
                            />
                        </TouchableOpacity>
                    </View>                    
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    btnSubmit: {
        width: '100%',
        borderRadius: 16,
        paddingVertical: 17,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnClose: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        width: 150,
        paddingVertical: 8,
        backgroundColor: '#F7F9F8'
    },
    content: {
        width: '100%',
        maxHeight: Dimensions.get('window').height * 0.7,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        padding: 18
    },
    container: {
        flex: 1, 
        backgroundColor: 'rgba(0,0,0,0.4)', 
        justifyContent: 'flex-end', 
        alignItems: 'flex-end'
    }
})

export default ModalPayment