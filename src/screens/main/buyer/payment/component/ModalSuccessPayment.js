import React from 'react'
import {
    Modal,
    View,
    Image,
    TouchableOpacity,
    StyleSheet
} from 'react-native'
import {TextBold, TextRegular} from '@components/global'
import {ImageAsset, IconAsset} from '@utils/index'
import {Colors} from '@styles/index'

const ModalSuccessPayment = ({
    show,
    onClose,
    onPress
}) => {
    return(
        <Modal
            visible={show}
            onRequestClose={onClose}
        >
            <View style={{
                flex: 1,
                backgroundColor: '#fff'
            }}>
                <View style={{
                    width: '100%',
                    padding: 24
                }}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => onClose()}
                    >
                        <Image
                            source={IconAsset.ChevronLeft}
                            style={{width: 25, height: 25, tintColor: Colors.blackBase}}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{flex: 0.7, justifyContent: 'center', alignItems: 'center'}}>
                    <Image
                        source={ImageAsset.SuccessPayment}
                        style={{width: 200, height: 150, resizeMode: 'contain'}}
                    />
                    <TextBold
                        text="Pembayaran Berhasil"
                        size={16}
                        color={Colors.blackBase}
                        style={{marginTop: 30}}
                    />
                    <View style={{width: 200, marginTop: 20}}>
                        <TextRegular
                            text="Silahkan cek pesanan Anda di Menu Pesanan."
                            color={Colors.grey_1}
                            style={{
                                textAlign: 'center'
                            }}
                        />
                    </View>                    
                </View>
                <View style={styles.footer}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => onPress()}
                        style={styles.btnSubmit}
                    >
                        <TextBold
                            text="Saya Mengerti"
                            size={16}
                            color="#fff"
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    footer: {
        position: 'absolute',
        bottom: 10,
        width: '100%',
        backgroundColor: '#fff'
    },
    btnSubmit: {
        width: '90%',
        alignSelf: 'center',
        borderRadius: 16,
        backgroundColor: Colors.primary,
        paddingVertical: 17,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default ModalSuccessPayment