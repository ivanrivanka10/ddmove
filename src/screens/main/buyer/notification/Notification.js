import React, {useState, useEffect} from 'react'
import {
    View,
    StyleSheet,
    Image,
    ScrollView,
    FlatList,
    TouchableOpacity
} from 'react-native'
import {TextBold, TextSemiBold, TextRegular, HeaderTitle} from '@components/global'
import {Colors} from '@styles/index'
import {IconAsset} from '@utils/index'
import { useSelector, useDispatch } from 'react-redux';
import API from '@utils/services/buyer/homeProvider'
import moment from 'moment'
import 'moment/locale/id'

import ModalMessage from '@components/modal/ModalMessage'

const Notification = ({
    navigation,
    route
}) => {
    const { userData, location, userPickedAddress } = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const data = [
        {
            date: '10 September 2021',
            notifData: [
                {
                    title: 'Notification title',
                    desc: 'Notification desc',
                    createdAt: '08:00',
                    isRead: false
                },
                {
                    title: 'Notification title',
                    desc: 'Notification desc',
                    createdAt: '08:00',
                    isRead: false
                },
                {
                    title: 'Notification title',
                    desc: 'Notification desc',
                    createdAt: '08:00',
                    isRead: false
                }
            ]
        },
        {
            date: '9 September 2021',
            notifData: [
                {
                    title: 'Notification title',
                    desc: 'Notification desc',
                    createdAt: '08:00',
                    isRead: true
                },
                {
                    title: 'Notification title',
                    desc: 'Notification desc',
                    createdAt: '08:00',
                    isRead: true
                },
                {
                    title: 'Notification title',
                    desc: 'Notification desc',
                    createdAt: '08:00',
                    isRead: true
                },
                {
                    title: 'Notification title',
                    desc: 'Notification desc',
                    createdAt: '08:00',
                    isRead: true
                },
            ]
        }
    ]
    const [listData, setListData] = useState([])
    const [message, setMessage] = useState('')
    const [idMessage, setIdMessage] = useState(undefined)
    const [showMessage, setShowMessage] = useState(false)


    const getListNotification = async () => {
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetListNotification(userData.token)    
        console.log('=== res notif', res);
        if(res.status === 200) {
            setListData(res.data.data)
        }
        dispatch({type: "HIDE_LOADING"});
    }

    const readMessage = async () => {
        dispatch({type: "SHOW_LOADING"});
        const res = await API.ReadNotification(idMessage, userData.token)
        if(res.status === 200) {
            getListNotification()
        } else {
            dispatch({type: "HIDE_LOADING"});
        }
    }

    useEffect(() => {
        getListNotification()
    },[])

    return(
        <View style={{flex: 1, backgroundColor: Colors.bgGrey}}>
            <ModalMessage
                show={showMessage}
                onClose={() => {
                    setShowMessage(false)
                    readMessage()
                }}
                message={message}
            />
            <HeaderTitle
                title="Notification"
            />
            <ScrollView
                showsVerticalScrollIndicator={false}
            >                
                <View style={{padding: 18}}>
                    <TextBold
                        text="Recent Notification"
                        size={16}
                        color={Colors.blackBase}
                    />
                    <FlatList
                        data={listData}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item, index}) => (
                            <TouchableOpacity
                                style={styles.item}
                                onPress={() => {
                                    setMessage(item.message)
                                    setIdMessage(item.id)
                                    setTimeout(() => {
                                        setShowMessage(true)
                                    }, 500);
                                    // setShowMessage(true)
                                }}
                            >
                                <View style={{
                                    width: '85%',
                                    flexDirection: 'row',
                                    alignItems: 'flex-start'
                                }}>
                                    <Image
                                        source={item.is_read === "no" ? IconAsset.IcNotifUnread : IconAsset.IcNotifRead}
                                        style={{width: 30, height: 30}}
                                    />
                                    <View style={{marginLeft: 10}}>
                                        <TextSemiBold
                                            text={item.title}
                                            color={Colors.blackBase}
                                        />
                                        <TextRegular
                                            text={item.message}
                                            color={Colors.normal}
                                            style={{marginTop: 4}}
                                        />
                                    </View>
                                </View>
                                <TextRegular
                                    text={moment(item.created_at).format('HH:mm')}
                                    color={Colors.blackBase}
                                />
                            </TouchableOpacity>
                        )}
                        ListEmptyComponent={() => (
                            <View style={{padding: 15}}>
                                <TextSemiBold
                                    text="Anda belum menerima notifikasi."
                                    color={Colors.blackBase}
                                />
                            </View>
                        )}
                    />
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        width: '100%',
        borderRadius: 16,
        backgroundColor: '#fff',
        padding: 18,
        marginTop: 14,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    }
})

export default Notification;