import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  Text,
  StatusBar,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Animated,
  Dimensions,
  TouchableHighlight,
  ScrollView,
  ImageBackground,
  Image,
  TextInput,
} from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';
import {HeaderTitle, TextBold, TextRegular} from '@components/global';
import {Size, Icons, Fonts, Images, Colors} from '@styles/index';
import Store from '@components/part/Store';
import Product from '@components/part/Product';
import {SceneMap, TabView, TabBar} from 'react-native-tab-view';
// import { SwipeGesture } from '@components/global';
import {useSelector, useDispatch} from 'react-redux';
import API from '@utils/services/buyer/homeProvider';
import APICHAT from '@utils/services/buyer/chatProvider';
import ModalChatAdmin from '@components/modal/ModalChatAdmin';
import normalize from 'react-native-normalize';
import {BottomSheetModal} from '@gorhom/bottom-sheet';
import ModalSortBy from '../home/components/ModalSortBy';

const filter = [
  {
    label: 'Semua',
    value: 'semua',
  },
  {
    label: 'Material',
    value: 'material',
  },
  {
    label: 'Non Material',
    value: 'non-material',
  },
];

const DetailStore = ({navigation, route}) => {
  const {userData} = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const [showModalChat, setShowModalChata] = useState(false);
  const [message, setMessage] = useState('');
  const [listProduct, setListProduct] = useState([]);
  const [detailShop, setDetailShop] = useState({});
  const [currentFilter, setCurrentFilter] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [showModalSort, setShowModalSort] = useState(false);

  const getProduct = async () => {
    console.log('props toko', route.params.value[0].id);
    console.log('props toko', route.params);
    dispatch({type: 'SHOW_LOADING'});
    var params = {
      page: currentPage,
      filter:
        currentFilter === 0
          ? 'semua'
          : currentFilter === 1
          ? 'material'
          : 'non-material',
    };
    console.log('ini params awal', params);

    const res = await API.GetProductByShop(
      route.params.value[0].id,
      params.filter == 'semua' ? 'semua' : params,
      userData.token,
    );
    console.log('ini params', params);
    console.log('res produk shop', res);
    setListProduct(res.data.data);
    setDetailShop(res.data.shop);
    dispatch({type: 'HIDE_LOADING'});
  };

  const sendChat = async () => {
    const data = route.params.value;
    setShowModalChata(false);
    dispatch({type: 'SHOW_LOADING'});
    const formData = new FormData();
    formData.append('shop_id', data.id);
    formData.append('product_id', null);
    formData.append('message', message);
    const res = await APICHAT.CreateChat(formData, userData.token);
    console.log('res create chat', res);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      navigation.navigate('Chat');
    }
  };

  useEffect(() => {
    getProduct();
  }, [currentFilter]);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
        <HeaderTitle title="Detail Toko" />
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {/* <Store
            image={
              route.params.value.photo
                ? route.params.value.photo
                : detailShop?.photo
            }
            label={route.params.value.name}
          /> */}

          <Image
            source={
              route.params.value.photo
                ? {uri: route.params.value.photo}
                : require('../../../../assets/images/Store.png')
            }
            style={{
              marginTop: 10,
              height: 100,
              width: 100,
              borderRadius: 100,
              borderWidth: 1,
              borderColor: Colors.primary,
            }}
            resizeMode="contain"
          />

          <TextBold
            text={
              route.params?.value[0]?.name
                ? route.params.value[0].name
                : 'Baba Market'
            }
            color={Colors.blackBase}
            style={{marginVertical: 10}}
            size={16}
          />
          {/* <Text allowFontScaling={false} style={styles.titleCategory}>
            {cu}Material Bangunan
          </Text> */}
          {/* chat button */}
          {/* <View style={styles.btnContainer}>
            <TouchableHighlight
              style={{
                marginHorizontal: Size.scaleSize(5),
                borderRadius: 10,
                padding: Size.scaleSize(14),
                backgroundColor: 'white',
              }}
              onPress={() => {}}
              underlayColor={Colors.opacityColor(Colors.primary, 0.2)}>
              <Icons.CommentInfoSVG
                size={Size.scaleSize(24)}
                color={Colors.primary}
              />
            </TouchableHighlight>
            <TouchableHighlight
              style={{
                marginHorizontal: Size.scaleSize(5),
                borderRadius: 10,
                padding: Size.scaleSize(14),
                backgroundColor: 'white',
              }}
              onPress={() => setShowModalChata(true)}
              underlayColor={Colors.opacityColor(Colors.primary, 0.2)}>
              <Icons.ChatSVG size={Size.scaleSize(24)} color={Colors.primary} />
            </TouchableHighlight>
          </View> */}
        </View>

        <View
          style={{
            // backgroundColor: Colors.bgGrey,
            // backgroundColor: 'red',
            flexDirection: 'row',
            alignItems: 'center',
            alignSelf: 'center',
            paddingVertical: 5,
            borderRadius: 10,
            marginBottom: 10,
            width: '100%',
            justifyContent: 'center',
          }}>
          <View
            style={{
              width: '60%',
              // height: normalize(20, 'height'),
              padding: normalize(8, 'width'),
              borderRadius: 10,
              borderWidth: 1,
              borderColor: Colors.primary,
              marginRight: normalize(10, 'width'),
            }}>
            <TextInput
              placeholder="Cari Produk"
              style={{padding: 0, fontSize: 12, margin: 0, minHeight: 0}}
            />
          </View>
          <TouchableOpacity
            style={styles.button}
            // onPress={() => setShowModalSortBy(true)}
            // underlayColor={Colors.opacityColor(Colors.primary, 0.2)}
            onPress={() => setShowModalSort(true)}>
            <Icons.SortSVG
              size={normalize(20, 'height')}
              color={Colors.primary}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.button, {marginLeft: normalize(10, 'width')}]}
            // onPress={() => setShowModalFilter(true)}
            // underlayColor={Colors.opacityColor(Colors.primary, 0.2)}
          >
            <Icons.FilterSVG
              size={normalize(20, 'height')}
              color={Colors.primary}
            />
          </TouchableOpacity>
        </View>
        {/* <FlatList
          contentContainerStyle={{
            paddingHorizontal: 15,
            justifyContent: 'space-between',
            marginTop: 20,
            marginBottom: 10,
          }}
          horizontal={true}
          data={filter}
          keyExtractor={(item, index) => index.toString()}
          showsHorizontalScrollIndicator={false}
          renderItem={({item, index}) => (
            <TouchableOpacity
              style={{
                paddingVertical: 10,
                paddingHorizontal: 20,
                borderRadius: 30,
                borderColor:
                  index === currentFilter ? Colors.primary : '#CDCFD0',
                borderWidth: 1,
                backgroundColor: index === currentFilter ? '#FFF2F3' : '#FFF',
                marginHorizontal: 5,
              }}
              onPress={() => setCurrentFilter(index)}>
              <Text
                allowFontScaling={false}
                style={[
                  {
                    fontFamily: Fonts.mulishBold,
                    color: index === currentFilter ? Colors.primary : '#333',
                    fontSize: Size.scaleFont(14),
                  },
                ]}>
                {item.label}
              </Text>
            </TouchableOpacity>
          )}
        /> */}
        <FlatList
          data={listProduct}
          keyExtractor={(item, index) => index.toString()}
          columnWrapperStyle={{marginTop: 10, marginHorizontal: 0}}
          ListHeaderComponent={
            <View style={{marginTop: 10}}>
              <Text allowFontScaling={false} style={styles.title}>
                {listProduct.length} produk ditemukan
              </Text>
            </View>
          }
          numColumns={2}
          renderItem={({item, index}) => (
            <Product
              onPress={() => navigation.navigate('Product', {product: item})}
              data={item}
            />
          )}
        />
      </ScrollView>

      <ModalSortBy
        show={showModalSort}
        onClose={() => setShowModalSort(false)}
      />
      <ModalChatAdmin
        show={showModalChat}
        onClose={() => setShowModalChata(false)}
        sendTo={route.params.value.name}
        message={message}
        setMessage={text => setMessage(text)}
        onSubmit={() => sendChat()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerCategory: {
    alignItems: 'center',
    justifyContent: 'center',
    height: Size.scaleSize(Dimensions.get('screen').height * 0.25),
    marginBottom: Size.scaleSize(100),
    opacity: 1,
  },
  titleCategory: {
    fontFamily: Fonts.mulishExtraBold,
    color: '#333',
    fontSize: Size.scaleFont(24),
  },
  infoTitleCategory: {
    fontFamily: Fonts.mulishRegular,
    color: '#B4B6B8',
    fontSize: Size.scaleFont(14),
    marginTop: Size.scaleSize(5),
  },
  title: {
    fontFamily: Fonts.mulishExtraBold,
    color: '#333333',
    fontSize: Size.scaleFont(16),
    marginHorizontal: Size.scaleSize(15),
  },
  btnContainer: {
    // position: 'absolute',
    // alignSelf: 'center',
    backgroundColor: '#F7F9F8',
    // top: Dimensions.get('screen').height * 0.27,
    zIndex: 2,
    padding: Size.scaleSize(10),
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  button: {
    borderRadius: 10,
    padding: normalize(10, 'width'),
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: Colors.primary,
  },
});

export default DetailStore;
