import React from 'react'
import {
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Platform,
    StyleSheet
} from 'react-native'
import {HeaderTitle, TextBold, TextRegular} from '@components/global'
import { Size, Fonts, Icons, Colors } from '@styles/index';
import API from '@utils/services/settingProvider'
import {useSelector, useDispatch} from 'react-redux'
import {ImageAsset} from '@utils/index'
import {WebView} from 'react-native-webview'

const AboutUs = ({
    navigation,
    route
}) => {
    const dispatch = useDispatch()
    const { userData } = useSelector(state => state.auth);
    const [dataAboutUs, setDataAboutUs] = React.useState(false)

    const getAboutUs = async () => {
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetAboutUs(userData.token)
        console.log('res about us', res);
        dispatch({type: "HIDE_LOADING"});
        setDataAboutUs(res.data.data)
    }    

    React.useEffect(() => {
        getAboutUs()
    },[])
    const html = `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d44331.09337858865!2d110.38490959502575!3d-7.741550341777735!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a5bd52e7e19b7%3A0xbef22cb0aeba5965!2sJogja%20Bay%20Pirates%20Adventure%20Waterpark!5e0!3m2!1sen!2sid!4v1664286885623!5m2!1sen!2sid" width="850" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>`
    if(typeof(dataAboutUs) === 'boolean') {
        return (
            <View />
        )
    } else {
        return(
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                <HeaderTitle
                    title="Tentang Kami"
                />
                <ScrollView 
                    style={{paddingHorizontal: 18}}
                    contentContainerStyle={{paddingBottom: 100}}
                    showsVerticalScrollIndicator={false}>
                    <Image
                        source={ImageAsset.DummyImg1}
                        style={{
                            width: '100%',
                            height: 160,
                            resizeMode: 'cover',
                            borderRadius: 16,
                            marginTop: 24
                        }}
                    />
                    <TextBold
                        text="Tentang Kami"
                        color={Colors.blackBase}
                        style={{marginTop: 24}}
                    />
                    <TextRegular
                        text={dataAboutUs.description}
                        color={Colors.blackBase}
                        style={{marginTop: 24}}
                    />
                    <TextBold
                        text="Lokasi"
                        color={Colors.blackBase}
                        style={{marginTop: 24}}
                    />
                    <TextRegular
                        text={dataAboutUs.address}
                        color={Colors.blackBase}
                        style={{marginTop: 24}}
                    />
                    <WebView
                        source={{html: html}}
                        // source={{uri: 'https://www.google.com/maps/place/Jogja+Bay+Pirates+Adventure+Waterpark/@-7.744783,110.418508,13z/data=!4m5!3m4!1s0x0:0xbef22cb0aeba5965!8m2!3d-7.7488047!4d110.418878?hl=en'}}
                        style={{
                            width: '100%',
                            height: 320,
                            marginTop: 24,
                            // backgroundColor: 'red'
                        }}
                        javaScriptEnabled
                        scalesPageToFit
                    />
                </ScrollView>                
            </View>
        )
    }    
}

export const screenOptions = navData => {
    return {
        headerTitle: "Tentang Kami",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default AboutUs;