import React from 'react'
import {
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    Platform
} from 'react-native'
import {HeaderTitle, TextBold, TextRegular} from '@components/global'
import { Size, Fonts, Icons, Colors } from '@styles/index';
import API from '@utils/services/settingProvider'
import {useSelector, useDispatch} from 'react-redux'
import {ImageAsset} from '@utils/index'
import {WebView} from 'react-native-webview'

const TermAndCondition = ({
    navigation,
    route
}) => {
    const dispatch = useDispatch()
    const { userData } = useSelector(state => state.auth);
    const [data, setData] = React.useState('')

    const getData = async () => {
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetTermCond(userData.token)
        setData(res.data.data.description)
        dispatch({type: "HIDE_LOADING"});
        console.log('term n cond', res);
    }

    const getTemplate = (content) => `
        <!DOCTYPE html>
        <html>
            <head>
                <meta name="viewport" content="width=device-width" />

                <style>
                    body {
                        margin: 0;
                        color: #373F41;
                        font-size: 12px;
                        line-height: 1.5;
                        font-family: 'Rubik';
                        user-select: none;
                    }
                    p {
                        margin-top: 0px;
                    }
                    ul, ol {
                        padding-left: 16px;
                        margin-top: 0;
                    }
                    body > ul:last-child, body > ol:last-child {
                        margin-bottom: 0;
                    }
                    .marker {
                        background-color: yellow;
                    }
                </style>
            </head>
            <body>
                ${content}
            </body>
        </html>
    `;

    React.useEffect(() => {
        getData()
    },[])

    return(
        <View style={{flex: 1, backgroundColor: '#fff'}}>
            <HeaderTitle
                title="Syarat dan Ketentuan"
            />
            <ScrollView 
                style={{paddingHorizontal: 18}}
                showsVerticalScrollIndicator={false}>
                <Image
                    source={ImageAsset.DummyImg1}
                    style={{
                        width: '100%',
                        height: 160,
                        resizeMode: 'cover',
                        borderRadius: 16,
                        marginTop: 24
                    }}
                />
                <TextBold
                    text="Term & Condition"
                    color={Colors.blackBase}
                    style={{marginTop: 24}}
                />
                <WebView
                    automaticallyAdjustContentInsets
                    showsVerticalScrollIndicator={false}
                    androidHardwareAccelerationDisabled={true}
                    source={{html: getTemplate(data)}}
                    // source={{uri: 'https://www.google.com/maps/place/Jogja+Bay+Pirates+Adventure+Waterpark/@-7.744783,110.418508,13z/data=!4m5!3m4!1s0x0:0xbef22cb0aeba5965!8m2!3d-7.7488047!4d110.418878?hl=en'}}
                    containerStyle={{
                        height: Dimensions.get('window').height,
                        marginTop: 4,
                        // paddingHorizontal: 16,
                        lineHeight: 16,
                        overflow: 'hidden',
                    }}
                    // javaScriptEnabled
                    // scalesPageToFit
                />
            </ScrollView>
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: "Syarat & Ketentuan",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default TermAndCondition;