import React from 'react'
import {
    View,
    TouchableOpacity,
    TextInput,
    ScrollView,
    StyleSheet,
    Switch,
    Keyboard,
    TouchableWithoutFeedback
} from 'react-native'
import {InputText, TextRegular, SelectDropDown, TextBold, Touchable} from '@components/global'
import {Colors, Size} from '@styles/index'

const AddNewAddressComponent = ({
    isEnabled,
    toggleSwitch,
    listProvince,
    listCity,
    listDistrict,
    listSubDistrict,
    onPickProvince,
    onPickCity,
    onPickDistrict,
    onPickSubDistrict,
    pickedProvince,
    pickedCity,
    pickedDistrict,
    pickedSubDistrict,
    state,
    setState,
    isMain,
    onPressSave,
    onPressModalMaps,
    lat,
    long
}) => {
    return(
        <ScrollView 
        keyboardShouldPersistTaps={'always'}
        showsVerticalScrollIndicator={false}>
             <TouchableWithoutFeedback
             onPress={Keyboard.dismiss}>
            <View>
            <InputText 
                containerStyle={styles.textInputContainer}
                containerInputStyle={{width: '100%', paddingHorizontal: 10}}
                placeholder="Masukkan label alamat, ex: Rumah utama"
                label="Label"
                value={state.label}
                onChangeText={(text) => setState(text, 'label')}
            />
            <InputText 
                containerStyle={styles.textInputContainer}
                containerInputStyle={{width: '100%', paddingHorizontal: 10}}
                placeholder="Masukkan nama penerima"
                label="Penerima"
                value={state.name}
                onChangeText={(text) => setState(text, 'name')}
            />
            <InputText 
                containerStyle={styles.textInputContainer}
                containerInputStyle={{width: '100%', paddingHorizontal: 10}}
                placeholder="Masukkan nomor penerima"
                label="Phone"
                keyboardType="number-pad"
                value={state.phone}
                onChangeText={(text) => setState(text, 'phone')}
            />
            <View style={{marginTop: 20, paddingBottom: 10, paddingHorizontal: 16}}>
                <TextRegular
                    text="Provinsi"
                    color="#000"
                />
                <SelectDropDown
                    searchable
                    value={pickedProvince}
                    onSelectItem={(val) => {
                       onPickProvince(val)
                    }}
                    valueColor="#000"
                    colorArrowIcon="#000"
                    style={{marginTop: 4, borderRadius: 6}}
                    placeholderText="Pilih provinsi"
                    items={listProvince}
                    maxHeight={300}
                    typeValueText="Medium"
                />
            </View>
            <View style={{marginTop: 20, paddingBottom: 10, paddingHorizontal: 16}}>
                <TextRegular
                    text="Kota"
                    color="#000"
                />
                <SelectDropDown
                    searchable
                    value={pickedCity}
                    onSelectItem={(val) => {
                       onPickCity(val)
                    }}
                    disabled={listCity.length < 1}
                    valueColor="#000"
                    colorArrowIcon="#000"
                    style={{marginTop: 4, borderRadius: 6}}
                    placeholderText="Pilih kota"
                    items={listCity}
                    maxHeight={200}
                    typeValueText="Medium"
                />
            </View>
            <View style={{marginTop: 20, paddingBottom: 10, paddingHorizontal: 16}}>
                <TextRegular
                    text="Kecamatan"
                    color="#000"
                />
                <SelectDropDown
                    searchable
                    value={pickedDistrict}
                    onSelectItem={(val) => {
                       onPickDistrict(val)
                    }}
                    disabled={listDistrict.length < 1}
                    valueColor="#000"
                    colorArrowIcon="#000"
                    style={{marginTop: 4, borderRadius: 6}}
                    placeholderText="Pilih Kecamatan"
                    items={listDistrict}
                    maxHeight={200}
                    typeValueText="Medium"
                />
            </View>
            <View style={{marginTop: 20, paddingBottom: 10, paddingHorizontal: 16}}>
                <TextRegular
                    text="Kelurahan"
                    color="#000"
                />
                <SelectDropDown
                    searchable
                    value={pickedSubDistrict}
                    onSelectItem={(val) => {
                        onPickSubDistrict(val)
                    }}
                    valueColor="#000"
                    colorArrowIcon="#000"
                    style={{marginTop: 4, borderRadius: 6}}
                    placeholderText="Pilih Kelurahan"
                    items={listSubDistrict}
                    maxHeight={200}
                    typeValueText="Medium"
                />
            </View>
            <View style={{marginTop: 20, paddingHorizontal: 16}}>
                <TextRegular
                    text="Detail Alamat"
                    color="#000"
                />
                <TextInput
                    style={styles.txtArea}
                    placeholder="Masukkan detail alamat, cnth: Gang ketiga dari jalan"
                    placeholderTextColor={Colors.normal}
                    multiline
                    value={state.address_detail}
                    onChangeText={(text) => setState(text, 'address_detail')}
                />
            </View>
            <View style={{ padding : 16 }}>
            <TextBold text="Ambil Titik Lokasi" color={Colors.blackBase} />
            <View
                style={{
                marginTop: 8,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginBottom: 24,
                }}>
                <View style={{
                    width: '70%',
                    borderRadius: 10,
                    borderColor: Colors.primary,
                    borderWidth: 1,
                    paddingVertical: 12,
                    paddingHorizontal: 18,
                    backgroundColor: Colors.secondaryLighttest,
                }}>
                    <TextRegular text={lat + ":" + long} color={Colors.blackBase} />
                </View>
                <TouchableOpacity
                onPress={onPressModalMaps}
                activeOpacity={0.8}
                style={{
                    width: '28%',
                    borderRadius: 10,
                    borderColor: Colors.primary,
                    borderWidth: 1,
                    paddingVertical: 12,
                    paddingHorizontal: 18,
                    backgroundColor: Colors.secondaryLighttest,
                  }}>
                <TextBold text="Pilih" color={Colors.primary} />
                </TouchableOpacity>
            </View>
            </View>
            {/* <View style={{marginTop: 20, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 16}}>
                <Switch
                    trackColor={{ false: "#767577", true: "#81b0ff" }}
                    thumbColor={isMain ? "#f5dd4b" : "#f4f3f4"}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={toggleSwitch}
                    value={isMain}
                />
                <TextRegular
                    text="Jadikan alamat utama ?"
                    style={{marginLeft: 10}}
                />
            </View> */}
            <View style={{marginTop: 20, marginBottom:Size.scaleSize(80), paddingHorizontal: 16}}>
                <Touchable 
                    onPress={onPressSave}
                    style={styles.btnAdd}>
                    <TextBold
                        text="Simpan"
                        color="#fff"
                    />
                </Touchable>
            </View>
            </View>
            </TouchableWithoutFeedback>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    btnAdd: {
        width: '100%',
        borderRadius: 20,
        paddingVertical: 12,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary
    },
    txtArea: {
        marginTop: 4, 
        borderColor: '#E0E0E0', 
        borderWidth: 1, 
        borderRadius: 10, 
        paddingHorizontal: 10,
        textAlignVertical: 'top',
        height: 120,
        color: '#000'
    },
    textInputContainer: {
        marginHorizontal: 16,
        marginTop: 20,
    },
})

export default AddNewAddressComponent;