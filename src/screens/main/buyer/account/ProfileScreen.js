import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useSelector, useDispatch} from 'react-redux';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

import {Images, Size, Icons, Fonts, Colors} from '@styles/index';
import {Button, InputText, HeaderTitle} from '@components/global';
import {IconAsset} from '@utils/index';
import API from '@utils/services/settingProvider';
import API_LOGIN from '@utils/services/apiProvider';
import {showToastMessage} from '@helpers/functionFormat';

import ModalPickImage from '@components/modal/ModalPickImage';

const ProfileScreen = props => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.userData);
  const [stateUser, setStateUser] = useState({});
  const [stateUserr, setStateUserr] = useState({});
  const [newPassword, setNewPassword] = useState('');
  const [localImgPath, setLocalImgPath] = useState('');
  const [showModalOption, setShowModalOption] = useState(false);
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const postUpdateProfile = async () => {
    const foto = {
      type: !localImgPath ? '' : localImgPath.assets[0].type,
      uri: !localImgPath ? '' : localImgPath.assets[0].uri,
      name: !localImgPath ? '' : localImgPath.assets[0].fileName,
    };
    console.log('cek foto', foto);
    const params = new FormData();
    if (localImgPath !== '') {
      params.append('photo', foto);
    }
    params.append('name', stateUser.name);
    params.append('phone', stateUser.phone);
    if (stateUserr.email == stateUser.email) {
    } else {
      params.append('email', stateUser.email);
    }
    console.log('cek params', params);
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.UpdateProfile(params, user.token);
    console.log('res update profile', res.data.data.email);
    if (res.status === 200) {
      showToastMessage('Berhasil memperbarui profile');
      getUserInfo(user.token);
    } else {
      showToastMessage('Email has been taken');
      dispatch({type: 'HIDE_LOADING'});
    }
  };

  const getUserInfo = async userToken => {
    const res = await API_LOGIN.GetUserInfo(userToken);
    console.log('res data user', res);
    if (res.status === 200) {
      const data = {
        token: userToken.includes('Bearer') ? userToken : `${userToken}`,
        email: res.data.data.email,
        is_seller: res.data.data.is_seller,
        name: res.data.data.name,
        phone: res.data.data.phone,
        image: res.data.data.image,
      };
      console.log('data user', data);
      dispatch({type: 'LOGIN_SUCCESS', data});
      dispatch({type: 'HIDE_LOADING'});
      props.navigation.goBack();
    }
  };

  const captureImage = async type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      saveToPhotos: true,
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      setTimeout(() => {
        launchCamera(options, response => {
          if (response.didCancel) {
            // alert('User cancelled camera picker');
            return;
          } else if (response.errorCode == 'camera_unavailable') {
            // alert('Camera not available on device');
            return;
          } else if (response.errorCode == 'permission') {
            // alert('Permission not satisfied');
            return;
          } else if (response.errorCode == 'others') {
            // alert(response.errorMessage);
            return;
          }
          setLocalImgPath(response);
        });
      }, 500);
    }
  };

  const chooseFile = type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    setTimeout(() => {
      launchImageLibrary(options, response => {
        if (response.didCancel) {
          alert('User cancelled camera picker');
          return;
        } else if (response.errorCode == 'camera_unavailable') {
          alert('Camera not available on device');
          return;
        } else if (response.errorCode == 'permission') {
          alert('Permission not satisfied');
          return;
        } else if (response.errorCode == 'others') {
          alert(response.errorMessage);
          return;
        }
        setLocalImgPath(response);
      });
    }, 500);
  };

  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Persetujuan akses kamera',
            message: 'Berikan Baba Market untuk mengakses kamera ?',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Persetujuan akses file',
            message: 'Berikan Baba Market untuk mengakses file ?',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  useEffect(() => {
    // AndroidKeyboardAdjust.setAdjustPan();
    setStateUser(user);
    setStateUserr(user);
    dispatch({type: 'HIDE_LOADING'});
  }, []);
  console.log('stateUser', user);
  return (
    <ScrollView contentContainerStyle={{flex: 1}}>
      <StatusBar barStyle="dark-content" backgroundColor="#FFF" />
      <HeaderTitle title="Profil" />
      <ModalPickImage
        show={showModalOption}
        onClose={() => setShowModalOption(false)}
        onPressCamera={() => {
          setShowModalOption(false);
          captureImage('photo');
        }}
        onPressGalery={() => {
          setShowModalOption(false);
          chooseFile('photo');
        }}
      />
      <KeyboardAwareScrollView
        style={{backgroundColor: '#FFF'}}
        enableOnAndroid={true}>
        <View style={styles.screen}>
          {localImgPath ? (
            <Image
              source={{uri: localImgPath.assets[0].uri}}
              style={styles.image}
            />
          ) : stateUser.image === null ? (
            <Image source={IconAsset.IC_USER} style={styles.image} />
          ) : (
            <Image source={{uri: stateUser.image}} style={styles.image} />
          )}
          <Button
            containerStyle={{borderRadius: 10, marginTop: 10}}
            style={{borderRadius: 10}}
            onPress={() => setShowModalOption(true)}
            textComponent={
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: 10,
                  paddingHorizontal: 24,
                }}>
                <Text
                  style={{
                    fontFamily: Fonts.mulishMedium,
                    color: '#FFF',
                    fontSize: Size.scaleFont(14),
                  }}>
                  Ganti Foto
                </Text>
                <Icons.CameraSVG
                  size={20}
                  color="#FFF"
                  style={{marginLeft: 20}}
                />
              </View>
            }
          />
          <InputText
            containerStyle={{marginHorizontal: 20, marginTop: 30}}
            containerInputStyle={{paddingHorizontal: 10}}
            label="Nama Lengkap"
            placeholder="Masukkan nama lengkap"
            value={stateUser.name}
            onChangeText={text => setStateUser({...stateUser, name: text})}
          />
          <InputText
            containerStyle={{marginHorizontal: 20, marginTop: 30}}
            containerInputStyle={{paddingHorizontal: 10}}
            label="Email"
            placeholder="Masukkan email"
            value={stateUser.email}
            onChangeText={text => setStateUser({...stateUser, email: text})}
          />
          <InputText
            containerStyle={{marginHorizontal: 20, marginTop: 30}}
            containerInputStyle={{paddingHorizontal: 10}}
            keyboardType="phone-pad"
            label="No. Telepon"
            placeholder="Masukkan nomor telepon"
            value={stateUser.phone}
            onChangeText={text => setStateUser({...stateUser, phone: text})}
          />
          <InputText
            containerStyle={{
              marginHorizontal: 20,
              marginTop: 30,
            }}
            containerInputStyle={{paddingHorizontal: 10}}
            secureTextEntry={secureTextEntry}
            label="Kata Sandi"
            placeholder="Masukkan kata sandi baru"
            value={newPassword}
            onChangeText={setNewPassword}
            isPassword={true}
            setSecureTextEntry={v => setSecureTextEntry(v)}
          />
        </View>
      </KeyboardAwareScrollView>
      <Button
        // disabled={localImgPath === ''}
        containerStyle={{
          marginHorizontal: 20,
          marginBottom: 10,
          // backgroundColor: localImgPath === '' ? Colors.grey_1 : Colors.primary
        }}
        title="Simpan Perubahan"
        onPress={postUpdateProfile}
      />
    </ScrollView>
  );
};

export const screenOptions = navData => {
  return {
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft: Size.scaleSize(5)}}
        onPress={() => navData.navigation.goBack()}>
        <View style={{padding: Size.scaleSize(10)}}>
          <Icons.ChevronLeft color="#333" size={Size.scaleSize(18)} />
        </View>
      </TouchableOpacity>
    ),
    headerTitle: 'Profil',
    headerTitleStyle: {
      fontFamily: Fonts.mulishExtraBold,
      fontSize: Size.scaleFont(14),
    },
    headerTitleAlign: 'left',
  };
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'center',
  },
  image: {
    width: Size.scaleSize(60),
    height: Size.scaleSize(60),
    borderRadius: Size.scaleSize(60),
  },
});

export default ProfileScreen;
