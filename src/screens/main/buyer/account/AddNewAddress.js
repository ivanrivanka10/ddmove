import React, {useState, useEffect} from 'react'
import {
    View,
    TouchableOpacity,
    KeyboardAvoidingView,
    Platform,
    Alert,
    Keyboard,
    TouchableWithoutFeedback
} from 'react-native'
import AddNewAddressComponent from './components/AddNewAddressComponent'
import { Size, Fonts, Icons, Colors } from '@styles/index';
import {Loader, HeaderTitle} from '@components/global'
import API from '@utils/services/settingProvider'
import APIREGISTER from '@utils/services/apiProvider'
import {showToastMessage} from '@helpers/functionFormat'
import {useDispatch, useSelector} from 'react-redux'
import ModalMaps from '@components/modal/ModalMaps';
import Geolocation from 'react-native-geolocation-service';
import { PERMISSIONS_TYPE, checkPermissions } from '@helpers/permissions';

const AddNewAddress = ({
    navigation,
    route
}) => {
    const dispatch = useDispatch()
    const { userData } = useSelector(state => state.auth);
    const [isEnabled, setIsEnabled] = useState(false);
    const [loading, setLoading] = useState(false)
    const [listProvince, setListProvince] = useState([])
    const [listCity, setListCity] = useState([])
    const [listDistrict, setListDistrict] = useState([])
    const [listSubDistrict, setListSubDistrict] = useState([])
    const [pickedProvince, setPickedProvince] = useState(null)
    const [pickedCity, setPickedCity] = useState(null)
    const [pickedDistrict, setPickedDistrict] = useState(null)
    const [pickedSubDistrict, setPickedSubDistrict] = useState(null)
    const [state, setState] = useState({
        label: '',
        name: '',
        phone: '',
        address_detail: '',        
    })
    const [isMain, setIsMain] = useState(false)
    const [lat, setLat] = useState(37.78825);
    const [long, setLong] = useState(-122.4324);
    const [modalMaps, setModalMaps] = useState(false);

    const toggleSwitch = () => setIsMain(previousState => !previousState);

    const getProvince = async () => {
        setLoading(true)
        const res = await API.GetProvince();
        console.log('res porvince', res);
        setLoading(false)
        if(res && res.status === 200) {            
            let data = []
            for (let index = 0; index < res.data.data.length; index++) {
                data.push({
                    id: res.data.data[index].province_id,
                    name: res.data.data[index].province
                })
            }
            setListProvince(data)
            console.log('data proccessed');
        }
    }

    const getListCity = async (id) => {
        console.log('get list city', id);
        const res = await API.GetCity(id)
        console.log('res list city', res);
        if(res && res.status == 200) {
            let data = []
            for (let index = 0; index < res.data.data.length; index++) {
                data.push({
                    id: res.data.data[index].city_id,
                    name: res.data.data[index].city_name
                })
            }
            setListCity(data)
        }
    }

    const getListDistrict = async (id) => {
        const res = await API.GetDistrict(id)
        console.log('res distrik', res);
        if(res && res.status == 200) {
            let data = []
            for (let index = 0; index < res.data.data.length; index++) {
                data.push({
                    id: res.data.data[index].district_id,
                    name: res.data.data[index].district_name
                })
            }
            setListDistrict(data)
        }
    }

    const getListSubDistrict = async (id) => {
        const res = await API.GetSubDistrict(id)
        console.log('res sub distrik', res.data.data);
        if(res && res.status == 200) {
            let data = []
            for (let index = 0; index < res.data.data.length; index++) {
                data.push({
                    id: res.data.data[index].subdistrict_id,
                    name: res.data.data[index].subdistrict_name
                })
            }
            setListSubDistrict(data)
        }
    }

    const onPressSave = async () => {
        setLoading(true)
        var register = {}        
        if(route.params) {
            if(route.params.fromRegister) {                
                register = await APIREGISTER.Register(route.params.data)
            }
        }
        if(Object.keys(register).length > 0 && register?.status !== 200) {
            setLoading(false)
            var dataErr = []
            for (const key in register.data.data) {
                dataErr.push(register.data.data[key])
            }
            return Alert.alert('Peringatan', `${dataErr.join(",")}`)
        }
        console.log('=== res regis', register);
        const obj = {
            label: state.label,
            name: state.name,
            phone: state.phone,
            address_detail: state.address_detail,
            province: pickedProvince,
            city: pickedCity,
            district: pickedDistrict
        }
        const dataEmpty = Object.keys(obj).filter(data => obj[data] == '' || obj[data] == null)        
        if(dataEmpty.length > 0) {
            showToastMessage('Pastikan semua data telah diisi.')
        } else {
            const getIdProvince = listProvince.filter((value) => { return value.name === pickedProvince })
            const getIdCity = listCity.filter((value) => { return value.name === pickedCity })
            const getIdDistrict = listDistrict.filter((value) => { return value.name === pickedDistrict })
            const getIdSubDistrict = listSubDistrict.filter((value) => { return value.name === pickedSubDistrict })
            const dataForm = new FormData()
            dataForm.append('label', state.label)
            dataForm.append('name', state.name)
            dataForm.append('phone', state.phone)
            dataForm.append('address_detail', state.address_detail)
            dataForm.append('province_id', getIdProvince[0].id)
            dataForm.append('city_id', getIdCity[0].id)
            dataForm.append('district_id', getIdDistrict[0].id)
            dataForm.append('subdistrict_id', getIdSubDistrict[0].id)
            dataForm.append('is_main', route.params && route.params.fromRegister ? 'yes' : isMain ? 'yes' : 'no')
            setLoading(true)
            var res = {}
            if(route.params && route.params.fromRegister && register.status === 200) {
                const dataRegis = register.data.data;
                res = await API.AddNewAddress(dataForm, `${dataRegis.token}`)
            } else {
                res = await API.AddNewAddress(dataForm, userData.token)
            }
            console.log('=== res add address', res);
            setLoading(false)
            if(res && res.status == 200) {
                if(route.params && route.params.fromRegister) {
                    const dataRegis = register.data.data;
                    dataRegis.token = `${dataRegis.token}`
                    dataRegis.hasAddress = true
                    dataRegis.fromSeller = false
                    console.log('data regis', dataRegis);
                    const data = dataRegis
                    dispatch({type: "LOGIN_SUCCESS", data});
                    navigation.replace('MainNavigation');
                } else {
                    var status = true
                    dispatch({type: "UPDATE_STATUS_ADDRESS", status});
                    showToastMessage('Berhasil menambahkan alamat baru.')
                    navigation.goBack()
                }                
            } else {
                showToastMessage('Server error, silahkan coba lagi.')
            }
        }
        setLoading(false)
    }

    const onPressUpdate = async () => {
        const obj = {
            label: state.label,
            name: state.name,
            phone: state.phone,
            address_detail: state.address_detail,
            province: pickedProvince,
            city: pickedCity,
            district: pickedDistrict
        }
        const dataEmpty = Object.keys(obj).filter(data => obj[data] == '' || obj[data] == null)        
        if(dataEmpty.length > 0) {
            showToastMessage('Pastikan semua data telah diisi.')
        } else {
            const getIdProvince = listProvince.filter((value) => { return value.name === pickedProvince })
            const getIdCity = listCity.filter((value) => { return value.name === pickedCity })
            const getIdDistrict = listDistrict.filter((value) => { return value.name === pickedDistrict })
            const getIdSubDistrict = listSubDistrict.filter((value) => { return value.name === pickedSubDistrict })
            console.log('cek prov', getIdProvince);
            console.log('cek city', getIdCity);
            console.log('cek distrik', getIdDistrict);
            const data = new FormData()
            data.append('label', state.label)
            data.append('name', state.name)
            data.append('phone', state.phone)
            data.append('address_detail', state.address_detail)
            data.append('province_id', getIdProvince[0].id)
            data.append('city_id', getIdCity[0].id)
            data.append('district_id', getIdDistrict[0].id)
            data.append('subdistrict_id', getIdSubDistrict[0].id)
            data.append('is_main', 'yes')            
            setLoading(true)
            const res = await API.UpdateAddress(data, userData.token, route.params.item.id)            
            setLoading(false)
            if(res && res.status == 200) {
                
                showToastMessage('Berhasil memperbaharui alamat.')
                navigation.goBack()
            } else {
                showToastMessage('Server error, silahkan coba lagi.')
            }
        }
    }

    useEffect(() => {
        if(listProvince.length > 0 && route.params && route.params.item) {
            const item = route.params.item
            const getIdProvince = listProvince.filter((value) => { return value.name === item.province })
            getListCity(getIdProvince[0].id)

        }
    },[listProvince])

    useEffect(() => {
        if(listCity.length > 0 && route.params && route.params.item) {
            const item = route.params.item
            console.log('cek item', item);
            const getIdCity = listCity.filter((value) => { return value.name === item.city })
            console.log('cek getIdCity', getIdCity);
            if(getIdCity.length > 0) {
                getListDistrict(getIdCity[0].id)
            }            
        }
    },[listCity])

    useEffect(() => {
        if(listDistrict.length > 0 && route.params && route.params.item) {
            const item = route.params.item
            console.log('cek item', item);
            const getIdDistrict = listDistrict.filter((value) => { return value.name === item.district })
            console.log('cek getIdDistrict', getIdDistrict);
            if(getIdDistrict.length > 0) {
                getListSubDistrict(getIdDistrict[0].id)
            }            
        }
    },[listDistrict])

    useEffect(() => {
        if(route.params) {
            if(route.params.item) {
                const item = route.params.item
                setState({
                    label: item.label,
                    name: item.name,
                    phone: item.phone,
                    address_detail: item.address_detail
                })
                setIsMain(item.is_main === 'yes' ? true : false)
                setPickedCity(item.city)
                setPickedDistrict(item.district)
                setPickedSubDistrict(item.subdistrict)
                setPickedProvince(item.province)
                getListSubDistrict(item?.id_subdistrict ?? 0)
            }
        }
        getProvince()
        loadLocation()
    },[])

    const loadLocation = async res => {
        // props.navigation.replace('MainNavigation');
        try {
          const permissions = await checkPermissions(PERMISSIONS_TYPE.location);
          Geolocation.getCurrentPosition(
            userLocation => {    
            console.log('userLocation', userLocation);
              setLat(userLocation.coords.latitude);
              setLong(userLocation.coords.longitude);
            },
            error => {
                console.log("error");
            },
            {
              enableHighAccuracy: false,
              timeout: 5000,
              maximumAge: 10000,
            },
          );
          if (permissions) {
            console.log('ok');
          } else {
            console.log('error');
          }
        } catch (error) {
          console.log('error');
        }
      };

    return(
        <View style={{flex: 1, backgroundColor: '#fff'}}>
            <Loader show={loading} />
            <ModalMaps
                data={{lat: lat, long: long}}
                setData={e => {
                    console.log(e);
                }}
                show={modalMaps}
                onClose={() => setModalMaps(false)}
                close={() => setModalMaps(false)}
            />
            {
                route.params && route.params.fromRegister ? null
                :
                <HeaderTitle
                    title={route.params === undefined ? 'Tambah Alamat' : 'Edit Alamat'}
                />
            }
            <KeyboardAvoidingView
                enabled
                behavior='padding'
                keyboardVerticalOffset={Platform.OS === 'android' ? -500 : 20}
            >
                <AddNewAddressComponent
                    lat={lat}
                    long={long}
                    onPressModalMaps={() => setModalMaps(true)}
                    isEnabled={isEnabled}
                    toggleSwitch={() => toggleSwitch()}
                    listProvince={listProvince}
                    listCity={listCity}
                    listDistrict={listDistrict}
                    listSubDistrict={listSubDistrict}
                    onPickProvince={(item) => {
                        console.log('picked province', item);
                        setListCity([])
                        setListDistrict([])
                        setPickedCity(null)
                        setPickedDistrict(null)
                        setPickedSubDistrict(null)
                        setPickedProvince(item.name)
                        getListCity(item.id)                    
                    }}
                    onPickCity={(item) => {
                        setPickedCity(item.name)
                        getListDistrict(item.id)
                    }}
                    onPickDistrict={(item) => {
                        setPickedDistrict(item.name)
                        getListSubDistrict(item.id)
                    }}
                    onPickSubDistrict={(item) => {
                        setPickedSubDistrict(item.name)
                    }}
                    pickedProvince={pickedProvince}
                    pickedCity={pickedCity}
                    pickedDistrict={pickedDistrict}
                    pickedSubDistrict={pickedSubDistrict}
                    state={state}
                    setState={(val, key) => {
                        setState({...state, [key]: val})
                    }}
                    isMain={isMain}
                    setIsMain={() => setIsMain(!isMain)}
                    onPressSave={() => {
                        console.log('cek asdasd', route);
                        if(route.params === undefined) {
                            onPressSave()
                        } else {
                            if(route.params.fromRegister) {
                                console.log('a');
                                onPressSave()
                            } else {
                                console.log('b');
                                onPressUpdate()
                            }                        
                        }
                    }}
                />               
            </KeyboardAvoidingView>            
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: "Tambah Alamat",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default AddNewAddress;