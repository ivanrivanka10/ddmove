import React from 'react'
import {
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Platform,
    Linking,
    StyleSheet,
    LogBox
} from 'react-native'
import {HeaderTitle, TextBold, TextRegular} from '@components/global'
import { Size, Fonts, Icons, Colors } from '@styles/index';
import API from '@utils/services/settingProvider'
import {useSelector, useDispatch} from 'react-redux'
import {ImageAsset, IconAsset} from '@utils/index'
import {showToastMessage} from '@helpers/functionFormat'

const ContactUs = ({
    navigation, 
    route
}) => {
    const dispatch = useDispatch()
    const { userData } = useSelector(state => state.auth);
    const [dataContact, setDataContact] = React.useState([])

    const getData = async () => {
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetContactInfo(userData.token)
        console.log('res get contact', res);
        const data = res.data.data
        var arr = []
        for (let index = 0; index < data.length; index++) {
            arr.push({
                label: data[index].label,
                value: data[index].value,
                icon: checkIcon(data[index].type)
            })
        }
        setDataContact(arr)
        dispatch({type: "HIDE_LOADING"});

    }

    const checkIcon = (type) => {
        console.log('types', type);

        switch (type) {
            case "wa":
                return ( <Image source={IconAsset.Whatsapp} style={{width: 20, height: 20}} /> )
            case "email":
                return ( <Image source={IconAsset.Email} style={{width: 20, height: 20}} /> )
            case "ig":
                return ( <Image source={IconAsset.Instagram} style={{width: 20, height: 20}} /> )
            case "website":
                return ( <Image source={IconAsset.Website} style={{width: 20, height: 20}} /> )
            case "fb":
                return ( <Image source={IconAsset.IcFacebook} style={{width: 20, height: 20}} /> )
            default:
                break;
        }
    }

    const onPressHandle = (type, item) => {
        console.log('type', type);
        console.log('item', item);
        switch (type) {
            case "Whatsapp":
                redirectWa(item.value)
                break
            case "Email":
                redirectEmail(item.value)
                break
            case "Instagram":
                redirectIg(item.value)
                break
            case "Website":
                redirectWeb(item.value)
                break
            case "Facebook":
                redirectFb(item.value)
                break
            default:
                redirecttiktok(item.value)
                break;
        }
    }

    const redirectWa = (number) => {
        var numberWa = number.replace(/[^a-zA-Z0-9]/g, '')
        var converted = '62'+numberWa.slice(1) //numberWa.slice(1) === '0' ? '62'+numberWa.slice(1) : numberWa
        console.log('nomor converted', converted);
        console.log('nomor numberWa', numberWa);
        let mobile = Platform.OS == 'ios' ? numberWa : '+' + converted;
        let link = `whatsapp://send?text=Hello Admin%0a&phone=${mobile}`
        
        Linking.openURL(link).then(async (data) => {                  
           
        }).catch(() => {
           alert('Make sure WhatsApp installed on your device');
        });
    }
  
    const redirectEmail = (value) => {
        const email = value
        Linking.openURL('mailto:'+ email +'?subject=Kontak Email')
    }
    const redirecttiktok = (value) => {
        let link = `tiktok://${value}`
        console.log('tiktok',link);
        Linking.openURL(link).then(async (data) => {                  
        }).catch(() => {
            Linking.openURL(`https://www.tiktok.com/${value}`)
 
        });  
    };
    const redirectIg = (value) => {
        // Linking.openURL(`instagram://user?username=aghilarobbani`)
        let link = `instagram://user?username=${value}`
        Linking.openURL(link).then(async (data) => {                  
        }).catch(() => {
            Linking.openURL(`https://www.instagram.com/${value}`)
 
        });  
    }

    const redirectWeb = (value) => {
         Linking.openURL(`https://${value}`)
    }

    const redirectFb = (value) => {
        console.log('webfb',value);
        Linking.openURL(`https://${value}`)
    }
  

    React.useEffect(() => {
        getData()
    },[])


    return(
        <View style={{flex: 1, backgroundColor: '#fff'}}>
            <HeaderTitle
                title="Hubungi Kami"
            />
            <ScrollView 
                style={{paddingHorizontal: 18}}
                contentContainerStyle={{paddingBottom: 50}}
                showsVerticalScrollIndicator={false}>
                <Image
                    source={ImageAsset.DummyImg1}
                    style={{
                        width: '100%',
                        height: 160,
                        resizeMode: 'cover',
                        borderRadius: 16,
                        marginTop: 24
                    }}
                />
                <View style={{marginTop: 6}}>
                    {
                        dataContact.map((value, index) => {
                            return(
                                <TouchableOpacity
                                    key={index.toString()}
                                    activeOpacity={0.8}
                                    style={styles.btnContact}
                                    onPress={() => onPressHandle(value.label, value)}
                                >
                                    {value.icon}
                                    <View style={{width: '70%'}}>
                                        <TextRegular
                                            text={value.label}
                                            color="#fff"
                                        />
                                        <TextBold
                                            text={value.value}
                                            color="#fff"
                                            style={{marginTop: 8}}
                                        />
                                    </View>
                                    <View style={{width: '15%', justifyContent: 'center', alignItems: 'flex-end'}}>
                                        <Image source={IconAsset.ChevronRight} style={{width: 20, height: 20, tintColor: '#fff'}} />
                                    </View>
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>                
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    btnContact: {
        width: '100%',
        marginTop: 18,
        borderRadius: 18,
        paddingVertical: 16,
        paddingHorizontal: 26,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: Colors.primary
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: "Kontak Kami",
        headerTitleAlign: 'left',
        headerTitleStyle: {
            fontFamily: Fonts.mulishExtraBold,
            fontSize: Size.scaleFont(16),
            color: '#333333'
        },
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
    }
}

export default ContactUs