import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity,
  FlatList,
  Pressable,
} from 'react-native';

import {
  CheckBox,
  SpinnerInputNumber,
  Button,
  HeaderTitle,
} from '@components/global';
import {Icons, Fonts, Size, Images, Colors} from '@styles/index';
import API from '@utils/services/buyer/homeProvider';
import {useSelector, useDispatch} from 'react-redux';
import {
  rupiahFormat,
  showToastMessage,
  upperCase,
} from '@helpers/functionFormat';

import ModalValidateCheckout from '@components/modal/ModalValidateCheckout';

const CartScreen = props => {
  const dispatch = useDispatch();
  const {userData} = useSelector(state => state.auth);
  const [dataCart, setDataCart] = useState([]);
  const [selectAll, setSelectAll] = useState(false);
  const [shopSelected, setShopSelected] = useState(undefined);
  const [showModalValidate, setShowModalValidate] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const getListCart = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.GetCart(userData.token);
    console.log('res cart', res.status);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      const cart = res.data.data;
      console.log('isi cart', cart);
      for (let i = 0; i < cart.length; i++) {
        cart[i].checked = false;
        console.log('dapat produrk', cart[i].shop);
        for (let j = 0; j < cart[i].shop.product.length; j++) {
          cart[i].shop.product[j].checked = false;
        }
      }

      console.log('isi cart', cart[0]);
      setDataCart(cart);
    } else {
      setDataCart([]);
    }
  };

  const handleSelectAll = () => {
    const item = [...dataCart];
    if (!selectAll) {
      setSelectAll(true);
      for (let i = 0; i < item.length; i++) {
        item[i].checked = true;
        for (let j = 0; j < item[i].shop.product.length; j++) {
          item[i].shop.product[j].checked = true;
        }
      }
      setDataCart(item);
    } else {
      setSelectAll(false);
      for (let i = 0; i < item.length; i++) {
        item[i].checked = false;
        for (let j = 0; j < item[i].shop.product.length; j++) {
          item[i].shop.product[j].checked = false;
        }
      }
      setDataCart(item);
    }
  };

  const handleSelectShop = shopIndex => {
    const item = [...dataCart];
    if (item[shopIndex].checked === false) {
      item[shopIndex].checked = true;
      setShopSelected(item[shopIndex].shop.shop_id);
      for (let i = 0; i < item[shopIndex].shop.product.length; i++) {
        item[shopIndex].shop.product[i].checked = true;
      }
    } else {
      item[shopIndex].checked = false;
      setShopSelected(undefined);
      for (let i = 0; i < item[shopIndex].shop.product.length; i++) {
        item[shopIndex].shop.product[i].checked = false;
      }
    }
    // const isAllChecked = item.find((value) => { return value.checked === false })
    // if(isAllChecked === undefined) {
    //     setSelectAll(true)
    // } else {
    //     setSelectAll(false)
    // }
    setDataCart(item);
  };

  const handleSelectItem = (shopIndex, itemIndex) => {
    const item = [...dataCart];
    item[shopIndex].shop.product[itemIndex].checked =
      item[shopIndex].shop.product[itemIndex].checked === false ? true : false;
    setShopSelected(item[shopIndex].shop.shop_id);
    const isAllChecked = item[shopIndex].shop.product.find(value => {
      return value.checked === false;
    });
    if (isAllChecked === undefined) {
      item[shopIndex].checked = true;
      // const checkAllShop = item.find((value) => { return value.checked === false })
      // if(checkAllShop !== undefined) {
      //     setSelectAll(false)
      // } else {
      //     setSelectAll(true)
      // }
    } else {
      item[shopIndex].checked = false;
      setSelectAll(false);
    }
    setDataCart(item);
  };

  const countTotalPrice = () => {
    var total = 0;
    const item = [...dataCart];
    for (let i = 0; i < item.length; i++) {
      for (let j = 0; j < item[i].shop.product.length; j++) {
        if (item[i].shop.product[j].checked === true) {
          total += item[i].shop.product[j].price * item[i].shop.product[j].qty;
        }
      }
    }

    return total;
  };

  const countTotalSelected = () => {
    var total = 0;
    const item = [...dataCart];
    for (let i = 0; i < item.length; i++) {
      for (let j = 0; j < item[i].shop.product.length; j++) {
        if (item[i].shop.product[j].checked === true) {
          total += 1;
        }
      }
      console.log('total', total);
    }
    // if(total == item.length){
    //     setSelectAll(!selectAll)
    // }
    return total;
  };

  const handleQuantity = (shopIndex, itemIndex, val) => {
    const newItem = [...dataCart];
    newItem[shopIndex].shop.product[itemIndex].qty = val;
    setDataCart(newItem);
  };

  const doCheckout = async () => {
    if (shopSelected == undefined) {
      showToastMessage('hanya bisa chekout dengan toko yang sama');
    }
    const item = [...dataCart];
    var selectedId = [];
    var indexShopSelected = item.findIndex(value => {
      return value.shop.shop_id === shopSelected;
    });
    var findSelected = item[indexShopSelected].shop.product.filter(value => {
      return value.checked === true;
    });
    findSelected.map(value => selectedId.push(value.product_id));
    console.log('cek findSelected', findSelected);
    if (findSelected && findSelected.length > 0) {
      var disable = false;
      findSelected.filter(value => {
        if (value.qty < value.minimum_purchase) {
          disable = true;
        }
      });
      if (disable) {
        return showToastMessage(
          'Total pembelian kurang dari minimal pembelian',
        );
      }
    }
    dispatch({type: 'SHOW_LOADING'});
    const data = new FormData();
    data.append('shop_id', shopSelected);
    data.append('product_id', `${selectedId.join()},`);
    console.log('data payload', data.shop_id);
    const res = await API.Checkout(data, userData.token);
    console.log('res checkout', res.status);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      props.navigation.navigate('Payment', {
        data: res.data.data,
        product: findSelected,
        dataCheckout: data,
      });
    } else {
      if (res.status == 404) {
        return showToastMessage('hanya bisa chekout dengan toko yang sama');
      } else if (res.status === 500) {
        return showToastMessage(`Server Error code 500`);
      } else if (res.data.data.info) {
        setErrorMessage(res.data.data.info);
        setTimeout(() => {
          setShowModalValidate(true);
        }, 500);
        return;
      } else {
        showToastMessage('hanya bisa chekout dengan toko yang sama');
      }
      // if(res.status === 500) {
      //     return showToastMessage('Silahkan isi alamat pengiriman terlebih dahulu')
      // }
    }
  };

  const updateQty = async (shop_id, product_id, qty) => {
    dispatch({type: 'SHOW_LOADING'});
    const data = new FormData();
    data.append('product_id', product_id);
    data.append('qty', qty);
    const res = await API.UpdateQty(data, shop_id, userData.token);
    console.log('res update', res);
    dispatch({type: 'HIDE_LOADING'});
    if (res.status === 200) {
      getListCart();
    }
  };

  useEffect(() => {
    dispatch({type: 'HIDE_LOADING'});
    getListCart();
  }, []);
  console.log('data cart', dataCart);
  return (
    <View style={{flex: 1}}>
      <StatusBar barStyle="dark-content" backgroundColor={'#FFF'} />
      <HeaderTitle title="Keranjang" />
      <ModalValidateCheckout
        show={showModalValidate}
        message={errorMessage}
        onClose={() => setShowModalValidate(false)}
      />
      <FlatList
        contentContainerStyle={{paddingBottom: 150}}
        ListHeaderComponent={
          <View
            style={{flexDirection: 'row', alignItems: 'center', margin: 20}}>
            <CheckBox
              onClick={() => handleSelectAll()}
              style={{marginRight: 20}}
              isChecked={selectAll}
              uncheckedCheckBoxColor="#C9454A"
              checkBoxColor="#C9454A"
            />
            <Text
              allowFontScaling={false}
              style={{
                fontSize: Size.scaleFont(14),
                fontFamily: Fonts.mulishBlack,
                color: '#333',
              }}>
              Pilih Semua
            </Text>
          </View>
        }
        data={dataCart}
        extraData={dataCart}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <View
            key={index.toString()}
            style={{
              marginHorizontal: 20,
              backgroundColor: '#FFF',
              padding: 20,
              borderRadius: 16,
              marginBottom: 20,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                borderBottomWidth: 1,
                borderColor: '#F2F4F5',
                paddingBottom: 18,
              }}>
              <CheckBox
                onClick={() => handleSelectShop(index)}
                style={{marginRight: 15}}
                isChecked={item.checked}
                uncheckedCheckBoxColor="#C9454A"
                checkBoxColor="#C9454A"
              />
              <TouchableOpacity
                style={{flexDirection: 'row', alignItems: 'center'}}
                onPress={() =>
                  props.navigation.navigate('DetailStore', {
                    value: {
                      name: item.shop.name,
                      photo: item.shop.photo,
                      id: item.shop.shop_id,
                    },
                  })
                }>
                <Image
                  source={{uri: item.shop.photo}}
                  style={{width: 36, height: 36, marginRight: 10}}
                />
                <Text
                  allowFontScaling={false}
                  style={{
                    fontFamily: Fonts.mulishBold,
                    fontSize: Size.scaleFont(14),
                    color: '#333',
                  }}>
                  {item.shop.name}
                </Text>
              </TouchableOpacity>
            </View>
            {item.shop.product.map((product, i) => (
              <View
                key={product.product_id}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginVertical: 10,
                }}>
                <CheckBox
                  onClick={() => handleSelectItem(index, i)}
                  style={{marginRight: 15}}
                  isChecked={product.checked}
                  uncheckedCheckBoxColor="#C9454A"
                  checkBoxColor="#C9454A"
                />
                <Image
                  source={{uri: product.photo}}
                  style={{width: 60, height: 60, marginRight: 15}}
                />
                <View>
                  <Text
                    allowFontScaling={false}
                    style={{
                      fontFamily: Fonts.mulishBold,
                      fontSize: Size.scaleFont(14),
                      color: '#333',
                      marginBottom: 5,
                    }}>
                    {product.name}
                  </Text>
                  <Text
                    allowFontScaling={false}
                    style={{
                      fontFamily: Fonts.mulishRegular,
                      fontSize: Size.scaleFont(14),
                      color: '#B4B6B8',
                      marginBottom: 5,
                    }}>
                    {upperCase(product.type.replace(/-/g, ' '))}
                  </Text>
                  <Text
                    allowFontScaling={false}
                    style={{
                      fontFamily: Fonts.mulishRegular,
                      fontSize: Size.scaleFont(14),
                      color: '#333',
                    }}>
                    {rupiahFormat(product.price * product.qty)}
                  </Text>
                  <Text
                    allowFontScaling={false}
                    style={{
                      fontFamily: Fonts.mulishRegular,
                      fontSize: Size.scaleFont(14),
                      color: '#333',
                    }}>
                    Jumlah stok barang : {product.stock}
                  </Text>
                  <Text
                    allowFontScaling={false}
                    style={{
                      fontFamily: Fonts.mulishRegular,
                      fontSize: Size.scaleFont(14),
                      color: '#333',
                    }}>
                    Minimum pembelian : {product.minimum_purchase} pc(s)
                  </Text>
                  <SpinnerInputNumber
                    containerStyle={{
                      justifyContent: 'flex-start',
                      marginTop: 10,
                    }}
                    count={`${product.qty}`}
                    minCount={product.minimum_purchase}
                    maxCount={product.stock}
                    onChange={val => {
                      // handleQuantity(index, i, val)
                      updateQty(item.id, product.product_id, val);
                    }}
                  />
                  <Pressable
                    onPress={() => updateQty(item.id, product.product_id, 0)}
                    style={{
                      width: 100,
                      height: 30,
                      backgroundColor: '#C9454A',
                      borderRadius: 8,
                      marginTop: 16,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      allowFontScaling={false}
                      style={{
                        fontFamily: Fonts.mulishRegular,
                        fontSize: Size.scaleFont(14),
                        color: '#FFF',
                      }}>
                      Hapus
                    </Text>
                  </Pressable>
                </View>
              </View>
            ))}
          </View>
        )}
      />
      <View
        style={{
          width: '100%',
          backgroundColor: '#FFF',
          height: 150,
          position: 'absolute',
          bottom: 0,
          borderTopRightRadius: 24,
          borderTopLeftRadius: 24,
          padding: 20,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 10,
          }}>
          <Text
            allowFontScaling={false}
            style={{
              fontFamily: Fonts.mulishRegular,
              color: '#333',
              fontSize: Size.scaleFont(14),
            }}>
            Item
          </Text>
          <Text
            allowFontScaling={false}
            style={{
              fontFamily: Fonts.mulishExtraBold,
              color: '#333',
              fontSize: Size.scaleFont(14),
            }}>
            {countTotalSelected()}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 15,
          }}>
          <Text
            allowFontScaling={false}
            style={{
              fontFamily: Fonts.mulishRegular,
              color: '#333',
              fontSize: Size.scaleFont(14),
            }}>
            Total
          </Text>
          <Text
            allowFontScaling={false}
            style={{
              fontFamily: Fonts.mulishExtraBold,
              color: '#333',
              fontSize: Size.scaleFont(14),
            }}>
            {rupiahFormat(countTotalPrice())}
          </Text>
        </View>
        <Button
          title="Pesan Sekarang"
          // disabled={countTotalPrice() === 0}
          style={{
            backgroundColor:
              countTotalPrice() === 0 ? Colors.grey_1 : Colors.primary,
          }}
          onPress={() => {
            doCheckout();
            // props.navigation.navigate('Payment')
          }}
        />
      </View>
    </View>
  );
};

export const screenOptions = navData => {
  return {
    headerTitle: 'Keranjang',
    headerTitleAlign: 'left',
    headerTitleStyle: {
      fontFamily: Fonts.mulishExtraBold,
      fontSize: Size.scaleFont(16),
      color: '#333333',
    },
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft: Size.scaleSize(5)}}
        onPress={() => navData.navigation.goBack()}>
        <View style={{padding: Size.scaleSize(10)}}>
          <Icons.ChevronLeft color="#333" size={Size.scaleSize(18)} />
        </View>
      </TouchableOpacity>
    ),
  };
};

export default CartScreen;
