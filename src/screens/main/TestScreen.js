import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { Header } from '@components/global';
import { Size, Fonts } from '@styles/index';

const OrderScreen = props => {
    return (
        <View style={styles.screen}>
            <Text>OrderScreen</Text>
        </View>
    )
}

export const screenOptions = navData =>{
    return {
        header: () => (
            <Header 
                navData={navData}
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={styles.headerTitle}>Pesanan Saya</Text>
                    </View>
                )}
            />
        )
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'red'
    },
    headerTitle: {
        color: '#FFF', 
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16)
    }
})

export default OrderScreen;