import React, {useEffect} from 'react';
import { View, Text, StyleSheet, useWindowDimensions, FlatList } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import { Header, Button } from '@components/global';
import { Size, Fonts, Colors, Icons } from '@styles/index';
import OrderItem from '@components/part/OrderItem';
import { useNavigation, useIsFocused } from '@react-navigation/native'
import { useSelector, useDispatch } from 'react-redux';
import API from '@utils/services/buyer/orderProvider'

const OrderScreen = props => {   
    const navigation = useNavigation()
    const isFocused = useIsFocused()
    const { userData } = useSelector(state => state.auth);
    const dispatch = useDispatch(); 
    const layout = useWindowDimensions();
    const [index, setIndex] = React.useState(0);
    const [listData, setListData] = React.useState([])
    const [routes] = React.useState([
        { key: 'first', title: 'Pending' },
        { key: 'second', title: 'Diproses' },
        { key: 'third', title: 'Selesai' },
    ]);

    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute,
        third: ThirdRoute
    });

    const getListOrder = async () => {
        const isFromNotif = props.route.params
        const params = { 
            status: isFromNotif !== undefined ? 'process' : index === 0 ? 'pending' : index === 1 ? 'process' : 'success'
        }
        console.log('cek params', params);
        dispatch({type: "SHOW_LOADING"});
        const res = await API.GetListOrder(params, userData.token);
        console.log('res order', res.data.data);
        dispatch({type: "HIDE_LOADING"});
        setListData(res.data.data)
    }

    const payNow = async (item) => {
        dispatch({type: "SHOW_LOADING"});
        const params = {
            transaction_id: item.transaction_id
        }
        const res = await API.Paynow(params, userData.token);
        console.log('res pay', res);
        dispatch({type: "HIDE_LOADING"});
        if(res.status === 200) {
            navigation.navigate('OrderTab', {
                screen: 'Paynow',
                params: {
                    item: res.data.data,
                    showHeader: true
                }
            })
        }
    }

    useEffect(() => {
        console.log('cek props order', props);
        if(props.route.params !== undefined) {
            setIndex(props.route.params.tab)
        }
        getListOrder();
    },[index, isFocused])
    console.log('cek index', index);
    return (
        <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={{ width: layout.width }}
            renderTabBar={props => (
                <TabBar 
                    {...props} 
                    indicatorStyle={styles.indidcatorStyle}
                    style={styles.tabStyle}
                    activeColor={Colors.secondary}
                    inactiveColor={'#CDCFD0'}
                    pressColor={"transparent"}
                    renderLabel={({ route, color }) => (
                        <Text 
                            allowFontScaling={false} 
                            style={[styles.tabLabel, { color: color }]}
                        >
                            {route.title}
                        </Text>
                    )}
                />
            )}
        />
    )

    function FirstRoute() {        
        return (
            <View style={styles.screen}>
                <FlatList 
                    style={{paddingHorizontal: Size.scaleSize(18), paddingVertical: Size.scaleSize(20)}}
                    contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
                    data={listData}
                    keyExtractor={item => item.id}
                    ListHeaderComponent={(
                        <View style={styles.titleContainer}>
                            <Text allowFontScaling={false} style={styles.title}>{listData.length} pesanan pending</Text>
                        </View>
                    )}
                    renderItem={(itemData) => (
                        <OrderItem 
                            {...itemData.item}
                            statusType={1}
                            onPressDetail={() =>                                 
                                payNow(itemData.item)
                            }
                        />
                    )}
                />
            </View>
        )
    };
      
    function SecondRoute(){
        const navigation = useNavigation()
        return (
            <View style={styles.screen}>
                <FlatList 
                    style={{paddingHorizontal: Size.scaleSize(18), paddingVertical: Size.scaleSize(20)}}
                    contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
                    data={listData}
                    ListHeaderComponent={(
                        <View style={styles.titleContainer}>
                            <Text allowFontScaling={false} style={styles.title}>{listData.length} pesanan aktif</Text>
                            {/* <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: Colors.primary}}>Semua</Text>
                                <Icons.FilterSVG 
                                    color={Colors.primary}
                                    size={Size.scaleSize(20)}
                                    style={{marginLeft: Size.scaleSize(5)}}
                                />
                            </View> */}
                        </View>
                    )}
                    renderItem={(itemData) => (
                        <OrderItem 
                            {...itemData.item}
                            statusType={2}
                            onPressDetail={() => navigation.navigate('OrderTab', {
                                screen: 'TransactionDetail',
                                params: {
                                    item: itemData.item
                                }
                            })}
                        />
                    )}
                />
            </View>
        )
    };
    
    function ThirdRoute(){
        return (
            <View style={styles.screen}>
                <FlatList
                    style={{paddingHorizontal: Size.scaleSize(18), paddingVertical: Size.scaleSize(20)}}
                    contentContainerStyle={{paddingBottom: Size.scaleSize(30)}}
                    data={listData}
                    ListHeaderComponent={(
                        <View style={styles.titleContainer}>
                            <Text allowFontScaling={false} style={styles.title}>Semua Transaksi</Text>
                            {/* <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular, fontSize: Size.scaleFont(14), color: Colors.primary}}>Semua</Text>
                                <Icons.FilterSVG 
                                    color={Colors.primary}
                                    size={Size.scaleSize(20)}
                                    style={{marginLeft: Size.scaleSize(5)}}
                                />
                            </View> */}
                        </View>
                    )}
                    renderItem={(itemData) => (
                        <OrderItem 
                            {...itemData.item}
                            statusType={4}
                            onPressDetail={() => navigation.navigate('OrderTab', {
                                screen: 'TransactionDetail',
                                params: {
                                    item: itemData.item
                                }
                            })}
                        />
                    )}
                />
            </View>
        )
    };
}

const styles = StyleSheet.create({
    screen: { 
        flex: 1, 
        backgroundColor: '#FFF'
    },
    headerTitle: {
        color: '#FFF', 
        fontFamily: Fonts.mulishExtraBold, 
        fontSize: Size.scaleFont(16)
    },
    indidcatorStyle: {
        backgroundColor: Colors.secondary, 
        height: Size.scaleSize(3), 
        borderRadius: 3
    },
    tabStyle: {
        elevation: 0.5, 
        backgroundColor: '#FFF'
    },
    tabLabel: {
        fontFamily: Fonts.mulishMedium,
        textTransform: 'capitalize',
        fontSize: Size.scaleFont(14)
    },
    titleContainer: { 
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: Size.scaleSize(8)
    },
    title: {
        fontFamily: Fonts.mulishExtraBold,
        fontSize: Size.scaleFont(16),
        color: '#333'
    }
})

export const screenOptions = navData =>{
    return {
        header: () => (
            <Header 
                navData={navData}
                headerLeft={(
                    <View>
                        <Text allowFontScaling={false} style={styles.headerTitle}>Pesanan Saya</Text>
                    </View>
                )}
            />
        )
    }
}

export default OrderScreen;