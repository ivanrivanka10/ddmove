import React from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableOpacity, 
    Image,
    Alert
} from 'react-native';

import { Icons, Size, Fonts, Images, Colors } from '@styles/index';
import { Button, InputText, Loader } from '@components/global';
import {IconAsset} from '@utils/index'
import API from '@utils/services/apiProvider'

const ForgotPasswordScreen = props => {

    const [email, setEmail] = React.useState('');
    const [loading, setLoading] = React.useState(false)

    const onResetPassword = async () => {
        const data = new FormData();
        data.append('email', email);
        setLoading(true)
        const res = await API.RequestResetPassword(data);
        console.log('res request', res);
        if(res.status === 200) {
            setLoading(false)
            props.navigation.navigate('VerificationCode', {email})
        } else {
            setLoading(false)
            Alert.alert('Pesan Kesalahan', 'User tidak ditemukan')
            return false;
        }        
    }

    return (
        <View style={styles.screen}>
            <Loader show={loading} />
            <View style={styles.content}>
                <Image  
                    style={styles.box}
                    source={IconAsset.IC_APP}
                    tintColor={Colors.primary}
                />
                <Text allowFontScaling={false} style={styles.label}>Masukkan email anda yang terdaftar untuk mereset kata sandi</Text>
                <InputText 
                    label="Email Aktif Anda"
                    placeholder="Masukkan Email Aktif Anda"
                    style={{
                        borderColor: Colors.secondaryBase,
                        borderWidth: email !== '' ? 1 : 0,
                        backgroundColor: email != '' ? Colors.secondaryLighttest : '#fff',
                        borderRadius: 10,
                        paddingHorizontal: 15,
                        color: '#000'
                    }}
                    onChangeText={(text) => setEmail(text)}
                    
                />
            </View>
            <Button 
                containerStyle={styles.buttonSend}
                title="Kirim"
                onPress={() => onResetPassword()}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF',
        
    },
    content: {
        flex: 1,
        alignSelf: 'center',
        alignItems: 'center',
        width: '90%'
    },
    box: {
        width: 100, 
        height: 100,
        // backgroundColor: '#C4C4C4',
        marginTop: 20,
        marginBottom: 50,
        resizeMode: 'contain'
    },
    label: {
        textAlign: 'center',
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        marginTop: Size.scaleSize(20),
        width: '80%',
        marginBottom: Size.scaleSize(30)
    },
    buttonSend: {
        alignSelf: 'center',
        marginBottom: Size.scaleSize(20), 
        width: '90%',
        marginTop: Size.scaleSize(20)
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: 'Lupa Kata Sandi',
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
        
    }
}

export default ForgotPasswordScreen;