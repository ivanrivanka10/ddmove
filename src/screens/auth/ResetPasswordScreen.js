import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Platform, TouchableOpacity, Image, Alert } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {IconAsset} from '@utils/index'
import API from '@utils/services/apiProvider'
import { Icons, Size, Fonts, Images, Colors } from '@styles/index';
import { Button, InputText } from '@components/global';
import {showToastMessage} from '@helpers/functionFormat'
import {useDispatch} from 'react-redux'

const ResetPasswordScreen = props => {
    console.log('props reset password', props);
    const dispatch = useDispatch()
    const [hidePassword, setHidePassword] = useState(true)
    const [newPassword, setNewPassword] = useState('')
    const [showSuccess, setShowSuccess] = useState(false)

    const sendResetPassword = async () => {
        if(newPassword.length < 8) {
            Alert.alert('Peringatan', 'Password minimal 8 karakter')
            return false
        }else{
            dispatch({type: "SHOW_LOADING"});
            const data = new FormData();
            data.append('email', props.route.params.email)
            data.append('otp', props.route.params.otp)
            data.append('new_password', newPassword)
            const res = await API.ResetPassword(data)
            console.log('res reset', res);
            dispatch({type: "HIDE_LOADING"});
            if(res.status === 200) {
                showToastMessage('Password baru berhasil diganti')
                props.navigation.replace('AuthNavigation', {screen: 'Login'})
            }
        }
        
    }

    // useEffect(() => {
    //     if(Platform.OS === 'android') {
    //         AndroidKeyboardAdjust.setAdjustPan();
    //     }        
    // }, [])

    return (
        <View style={styles.screen}>
            <KeyboardAwareScrollView enableOnAndroid>
                <View style={styles.content}>
                    <Image  
                        style={styles.box}
                        source={IconAsset.IC_APP}
                        tintColor={Colors.primary}
                    />
                    <Text allowFontScaling={false} style={styles.label}>Silahkan buat kata sandi baru</Text>
                    <InputText 
                        containerStyle={{marginBottom: Size.scaleSize(20)}}
                        label="Kata Sandi Baru"
                        placeholder="Masukkan kata sandi baru"
                        isPassword={true}
                        secureTextEntry={hidePassword}
                        setSecureTextEntry={(value) => setHidePassword(value)}
                        style={{paddingLeft: 10}}
                        stylePassword={{marginRight: 10, marginLeft: 0}}
                        onChangeText={(text) => setNewPassword(text)}
                    />
                    {/* <InputText 
                        label="Konfirmasi Kata Sandi Baru"
                        placeholder="Ketikkan Konfirmasi kata sandi baru"
                        isPassword={true}
                    /> */}
                </View>
            </KeyboardAwareScrollView>
            <Button 
                containerStyle={[styles.buttonSend, newPassword === '' && {backgroundColor: Colors.grey_1}]}
                title="Kirim"
                disabled={newPassword === ''}
                onPress={() => {
                    sendResetPassword()
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF',
        
    },
    content: {
        flex: 1,
        alignSelf: 'center',
        alignItems: 'center',
        width: '90%'
    },
    box: {
        width: 100, 
        height: 100,
        marginTop: 20,
        marginBottom: 50,
        resizeMode: 'contain'
    },
    label: {
        textAlign: 'center',
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        marginTop: Size.scaleSize(20),
        width: '80%',
        marginBottom: Size.scaleSize(30)
    },
    buttonSend: {
        alignSelf: 'center',
        marginBottom: Size.scaleSize(20), 
        width: '90%',
        marginTop: Size.scaleSize(20)
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: 'Buat Kata Sandi Baru',
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
        
    }
}

export default ResetPasswordScreen;