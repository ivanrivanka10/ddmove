import React, { useState } from 'react';
import { 
    View, 
    Text, 
    StatusBar, 
    StyleSheet, 
    TouchableOpacity, 
    TouchableWithoutFeedback,    
    KeyboardAvoidingView,
    ScrollView,
    Alert
} from 'react-native';
import { useDispatch } from 'react-redux';

import { Icons, Size, Fonts, Colors } from '@styles/index';
import { InputText, Button, Loader } from '@components/global';
import { register, updateModal } from '@store/actions';
import API from '@utils/services/apiProvider'
import {validateEmail} from '@helpers/functionFormat'
import AsyncStorage from '@react-native-async-storage/async-storage'

const RegisterScreen = props => {

    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [securePassword, setSecurePassword] = useState(true);
    const [loading, setLoading] = useState(false)

    const dispatch = useDispatch();

    const onRegister = async () => {
        if(phone.length < 11) {
            Alert.alert('Peringatan', 'Nomor handphone minimal 11 dan maksimal 12 karakter')
            return false
        }
        if(!validateEmail(email)) {
            Alert.alert('Peringatan', 'Format email salah')
            return false
        }
        if(password.length < 8) {
            Alert.alert('Peringatan', 'Kata sandi harus lebih dari 8 karakter')
            return false
        }
        // setLoading(true);
        const fcmtoken = await AsyncStorage.getItem('fcmToken')
        const formData = new FormData();
        formData.append('name', name);
        formData.append('phone', phone);
        formData.append('email', email);
        formData.append('password', password);
        formData.append('fcm_token', fcmtoken)
        // console.log('cek form register', formData);
        props.navigation.navigate('AddNewAddress', {fromRegister: true, data: formData});
        // const res = await API.Register(formData);
        // console.log('res register', res);
        // if(res.status === 200) {
        //     setLoading(false)
        //     const data = res.data.data;
        //     data.token = `Bearer ${data.token}`
        //     dispatch({type: "LOGIN_SUCCESS", data});
        //     props.navigation.replace('MainNavigation');
        // } else {
        //     setLoading(false)
        //     Alert.alert('Server Error', 'Registrasi gagal, silahkan coba kembali')
        //     return false
        // }
    }

    return (
        <View style={styles.screen}>
            <Loader show={loading} />
            <StatusBar 
                backgroundColor="#FFF"
                barStyle="dark-content"
            />
            <KeyboardAvoidingView
                enabled
                behavior='position'
                keyboardVerticalOffset={-130}
                style={styles.screen}
            >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <Text 
                        allowFontScaling={false} 
                        style={styles.titleWelcome}
                    >
                        Daftarkan Akun Anda dan Nikmati Berbelanja Disini
                    </Text>
                    <Text allowFontScaling={false} style={styles.title}>Silahkan lengkapi data Anda</Text>
                    <InputText 
                        containerStyle={styles.textInputContainer}
                        containerInputStyle={{width: '100%', paddingHorizontal: 10}}
                        leftIcon={<Icons.UserSVG 
                            color={Colors.secondary}
                            size={Size.scaleSize(20)}
                            style={{marginRight: Size.scaleSize(10)}}
                        />}
                        placeholder="Siapa nama lengkap Anda?"
                        label="Nama Lengkap"
                        value={name}
                        onChangeText={setName}
                    />
                    <InputText 
                        containerStyle={styles.textInputContainer}
                        containerInputStyle={{width: '100%', paddingHorizontal: 10}}
                        keyboardType="phone-pad"
                        leftIcon={<Icons.PhoneSVG 
                            color={Colors.secondary}
                            size={Size.scaleSize(20)}
                            style={{marginRight: Size.scaleSize(10)}}
                        />}
                        placeholder="Masukkan nomor aktif"
                        label="No. Telepon"
                        value={phone}
                        maxLength={12}
                        onChangeText={setPhone}
                    />
                    <InputText 
                        containerStyle={styles.textInputContainer}
                        containerInputStyle={{width: '100%', paddingHorizontal: 10}}
                        keyboardType="email-address"
                        leftIcon={<Icons.EmailSVG 
                            color={Colors.secondary}
                            size={Size.scaleSize(20)}
                            style={{marginRight: Size.scaleSize(10)}}
                        />}
                        placeholder="Masukkan email"
                        label="Email"
                        value={email}
                        onChangeText={setEmail}
                    />
                    <InputText 
                        containerStyle={styles.textInputContainer}
                        containerInputStyle={{width: '100%', paddingHorizontal: 10}}
                        leftIcon={<Icons.LockedSVG 
                            color={Colors.secondary}
                            size={Size.scaleSize(20)}
                            style={{marginRight: Size.scaleSize(10)}}
                        />}
                        isPassword
                        placeholder="Masukkan kata sandi"
                        label="Kata Sandi"
                        secureTextEntry={securePassword}
                        setSecureTextEntry={setSecurePassword}
                        value={password}
                        onChangeText={setPassword}
                    />
                    <Button 
                        containerStyle={{marginHorizontal: Size.scaleSize(20), marginTop: Size.scaleSize(40)}}
                        title="Daftar"
                        onPress={onRegister}
                    />
                    <View style={{flexDirection: 'row', alignSelf: 'center', marginTop: 50, alignItems: 'center'}}>
                        <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishRegular}}>Saya sudah memiliki akun? </Text>
                        <TouchableWithoutFeedback onPress={() => props.navigation.navigate('Login')}>
                            <Text allowFontScaling={false} style={{fontFamily: Fonts.mulishExtraBold, color: Colors.primary}}>Masuk</Text>
                        </TouchableWithoutFeedback>
                    </View>
                </ScrollView>                
            </KeyboardAvoidingView>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    titleWelcome: {
        fontFamily: Fonts.mulishExtraBold,
        color: '#333',
        fontSize: Size.scaleFont(24),
        marginHorizontal: Size.scaleSize(20),
        marginTop: Size.scaleSize(30),
        marginBottom: Size.scaleSize(16)
    },
    title: {
        fontFamily: Fonts.mulishRegular,
        color: '#B4B6B8',
        fontSize: Size.scaleFont(14),
        marginHorizontal: Size.scaleSize(20),
        
    },
    textInputContainer: {
        marginHorizontal: Size.scaleSize(20),
        marginTop: Size.scaleSize(23)
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: 'Daftarkan Akun',
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
        
    }
}

export default RegisterScreen;