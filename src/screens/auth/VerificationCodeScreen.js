import React, {useState} from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Alert } from 'react-native';

import { Icons, Size, Fonts, Colors, Images } from '@styles/index';
import { Button, Loader } from '@components/global';
import OtpInput from '@components/global/OtpInput'
import moment from 'moment'
import {IconAsset} from '@utils/index'
import API from '@utils/services/apiProvider'

const VerificationCodeScreen = props => {
    const otpRef = React.useRef()
    const [otpCode, setOtpCode] = React.useState('');
    const [showResendOtp, setShowResendOtp] = useState(false);
	const [times, setTimes] = useState(180);
	const [timeleft, setTimeleft] = useState('03:00');	
	const [stopTimer, setStopTimer] = useState(false)
    const [loading, setLoading] = useState(false)
    console.log('props otp verif', props);
    
    function getMsakedEmail(email){
        let skipFirstChars = 3;
        let firstThreeChar = email.slice(0, skipFirstChars);
    
        let domainIndexStart = email.lastIndexOf("@");
        let maskedEmail = email.slice(skipFirstChars, domainIndexStart-1)
        maskedEmail = maskedEmail.replace(/./g, '*')
        let domainPlusPreviousChar = email.slice(domainIndexStart-1, email.length);
    
        return firstThreeChar.concat(maskedEmail).concat(domainPlusPreviousChar);
    }

    const sendOtp = async () => {
        const data = new FormData();
        data.append('email', props.route.params.email)
        data.append('otp', otpCode)
        setLoading(true)
        const res = await API.CheckOtp(data);
        console.log('res cek otp', res);
        if(res.status === 200) {
            setLoading(false)
            props.navigation.navigate('ResetPassword', {
                email: props.route.params.email,
                otp: otpCode
            })
        } else {
            setLoading(false)
            Alert.alert('Pesan Kesalahan', 'Kode OTP tidak valid');
            return false
        }
    }

    React.useEffect(() => {
        if(timeleft === '00:00') {
			setShowResendOtp(true);
        }
        const timer = times
        const alertAt = Date.now() + timer * 60 * 1000;
        const intervalId = setInterval(() => {  
            const remaining = alertAt - Date.now();       
            if(remaining < 0) {
                clearInterval(intervalId);
                return;
            } else {
                setTimes(times - 1)
                setTimeleft(moment.utc((times - 1) *1000).format('mm:ss'));
            }
        }, 1000);
            if(stopTimer) {
                clearInterval(intervalId);
                return;
            }
        return () => clearInterval(intervalId);
    },[timeleft, times, stopTimer])

    return (
        <View style={styles.screen}>
            <Loader show={loading} />
            <Image  
                style={styles.box}
                source={IconAsset.IC_APP}
                tintColor={Colors.primary}
            />
            <Text allowFontScaling={false} style={styles.labelTimer}>Waktu tersisa</Text>
            <Text allowFontScaling={false} style={styles.timer}>{timeleft}</Text>
            <Text allowFontScaling={false} style={styles.label}>Masukkan kode OTP yang dikirim{"\n"}ke email {getMsakedEmail(props.route.params.email)}</Text>
            <View style={styles.pinContainer}>
                <OtpInput 
                    ref={otpRef} 
                    containerStyle={styles.otpContainer} 
                    onSubmit={(value) => setOtpCode(value)} 
                    inputStyle={{height: 48, width: 42}}
                />
                <Button 
                    title="Lanjutkan"
                    onPress={() => sendOtp()}
                />
            </View>
            {
                showResendOtp && (
                    <View style={{width: '100%', alignItems: 'center'}}>
                        <Text allowFontScaling={false} style={styles.labelCode}>Tidak menerima kode?</Text>
                        <Button 
                            containerStyle={styles.buttonResend}
                            style={{borderWidth: 2, borderColor: Colors.primary}}
                            textStyle={{color: Colors.primary}}
                            pressColor="white"
                            title="Kirim Ulang"
                        />
                    </View>
                )
            }            
        </View>
    )
}

const styles = StyleSheet.create({
    otpContainer: {
		marginVertical: 24,
	},
    screen: {
        flex: 1,
        backgroundColor: '#FFF',
        alignItems: 'center'
    },
    box: {
        width: 100, 
        height: 100,
        resizeMode: 'contain',
        marginTop: 20,
        marginBottom: 50
    },
    labelTimer: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        color: '#B4B6B8'
    },
    timer: {
        fontFamily: Fonts.mulishExtraBold,
        color: Colors.secondary,
        fontSize: Size.scaleFont(24),
        marginTop: Size.scaleSize(8)
    },
    label: {
        textAlign: 'center',
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        marginTop: Size.scaleSize(20),
        width: '80%'
    },
    pinContainer: {
        backgroundColor: 'white',
        borderRadius: 14,
        marginTop: Size.scaleSize(20),
        marginHorizontal: Size.scaleSize(20),
        padding: Size.scaleSize(16),
        width: '90%',
        shadowColor: 'rgb(15, 25, 35)',
        shadowOpacity: 0.86,
        shadowOffset: { width: 1, height: 8 },
        shadowRadius: 2,
        elevation: 2
    },
    labelCode: {
        fontFamily: Fonts.mulishRegular,
        fontSize: Size.scaleFont(14),
        marginTop: Size.scaleSize(40),
        color: '#333',
    },
    buttonResend: {
        backgroundColor: '#FFF', 
        
        width: '60%',
        marginTop: Size.scaleSize(20)
    }
})

export const screenOptions = navData => {
    return {
        headerTitle: 'Verifikasi Kode OTP',
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.goBack()}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
        
    }
}

export default VerificationCodeScreen;