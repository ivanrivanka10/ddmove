import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  Alert,
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import {useDispatch} from 'react-redux';

import {Size, Icons, Fonts, Colors} from '@styles/index';
import {InputText, Button, Loader} from '@components/global';
import {
  login,
  updateModal,
  setUserLocation,
  updateLoading,
} from '@store/actions';
import {checkPermissions, PERMISSIONS_TYPE} from '@helpers/permissions';
import API from '@utils/services/apiProvider';
import APISETTING from '@utils/services/settingProvider';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debugConsoleLog} from '@helpers/functionFormat';
// Kristalyoung@gmail.com // 12345678
const LoginScreen = props => {
  const [email, setEmail] = useState(''); //agil.dev21@gmail.com
  const [password, setPassword] = useState(''); //12345678
  const [securePassword, setSecurePassword] = useState(true);
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  const onLogin = async () => {
    if (password.length < 8) {
      Alert.alert('Peringatan', 'Password minimal 8 karakter');
      return false;
    }
    setLoading(true);
    const fcmtoken = await AsyncStorage.getItem('fcmToken');
    const formData = new FormData();
    formData.append('email', email);
    formData.append('password', password);
    formData.append('fcm_token', fcmtoken);
    // const body = {
    //     email: email,
    //     password: password
    // }
    const res = await API.Login(formData);
    console.log('res login', res);
    if (res.status === 200) {
      getUserInfo(res.data.data.token);
      debugConsoleLog('alert', res.data.data.token);
    } else {
      setLoading(false);
      Alert.alert('Login gagal', 'Email atau password salah.');
    }
    // try{
    //     console.log('cek ini');
    //     await dispatch(login(email, password));
    //     await loadLocation();
    // }catch(error){
    //     console.log('cek ini 2');
    //     dispatch(updateModal(true, 'LOGIN ERROR', error.message))
    // }
  };

  const getUserInfo = async token => {
    const res = await API.GetUserInfo(token);
    console.log('res data user', res);
    if (res.status === 200) {
      const data = {
        token: `${token}`,
        email: res.data.data.email,
        is_seller: res.data.data.is_seller,
        name: res.data.data.name,
        phone: res.data.data.phone,
        image: res.data.data.image,
      };
      if (res.data.data.is_seller === 'no') {
        const getUserAddress = await APISETTING.GetAddress(`${token}`);
        console.log('res useraddress', getUserAddress);
        if (getUserAddress.status === 404) {
          data.hasAddress = false;
          data.fromSeller = false;
        } else {
          data.hasAddress = true;
          data.fromSeller = false;
        }
      } else {
        data.fromSeller = true;
      }
      console.log('data user', data);
      dispatch({type: 'LOGIN_SUCCESS', data});
      setLoading(false);
      if (res.data.data.is_seller === 'yes') {
        dispatch({type: 'UPDATE_TYPE'});
        props.navigation.replace('SellerNavigation');
      } else {
        props.navigation.replace('MainNavigation');
      }
      // await loadLocation(res);
    }
  };

  const loadLocation = async res => {
    console.log('cek ini 3', res);
    // props.navigation.replace('MainNavigation');
    try {
      await dispatch(updateLoading(true));
      const permissions = await checkPermissions(PERMISSIONS_TYPE.location);
      Geolocation.getCurrentPosition(
        userLocation => {
          console.log('userLocation', userLocation);
          const dataLocation = {
            lat: userLocation.coords.latitude,
            lng: userLocation.coords.longitude,
          };
          dispatch({
            type: 'SET_LOCATION',
            dataLocation,
          });
          dispatch(
            setUserLocation(
              userLocation.coords.latitude,
              userLocation.coords.longitude,
            ),
          );
        },
        error => {
          dispatch(updateModal(true, 'LOGIN ERROR', error.message));
        },
        {
          enableHighAccuracy: false,
          timeout: 5000,
          maximumAge: 10000,
        },
      );
      await dispatch(updateLoading(false));
      if (permissions) {
        // props.navigation.replace('MainNavigation');
        if (res.data.data.is_seller === 'yes') {
          dispatch({type: 'UPDATE_TYPE'});
          props.navigation.replace('SellerNavigation');
        } else {
          props.navigation.replace('MainNavigation');
        }
      } else {
        throw {
          message:
            'Pastikan anda mengizinkan lokasi untuk dapat melakukan login',
        };
      }
    } catch (error) {
      dispatch(updateModal(true, 'LOGIN ERROR', error.message));
      await dispatch(updateLoading(false));
    }
  };

  return (
    <ScrollView style={{backgroundColor: 'white'}}>
      <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
      <Loader show={loading} />
      <View style={styles.screen}>
        <View style={{flex: 1}}>
          <Text allowFontScaling={false} style={styles.titleWelcome}>
            {'Selamat Datang\nKembali'}
          </Text>
          <Text allowFontScaling={false} style={styles.title}>
            Silahkan masuk ke akun Anda
          </Text>
          <InputText
            containerStyle={styles.textInputContainer}
            containerInputStyle={{width: '100%', paddingHorizontal: 10}}
            leftIcon={
              <Icons.EmailSVG
                color={Colors.secondary}
                size={Size.scaleSize(20)}
                style={{marginRight: Size.scaleSize(10)}}
              />
            }
            placeholder="Masukkan email"
            label="Email"
            value={email}
            onChangeText={setEmail}
          />
          <InputText
            containerStyle={styles.textInputContainer}
            containerInputStyle={{width: '100%', paddingHorizontal: 10}}
            leftIcon={
              <Icons.LockedSVG
                color={Colors.secondary}
                size={Size.scaleSize(20)}
                style={{marginRight: Size.scaleSize(10)}}
              />
            }
            isPassword
            placeholder="Masukkan kata sandi"
            label="Kata Sandi"
            secureTextEntry={securePassword}
            setSecureTextEntry={setSecurePassword}
            value={password}
            onChangeText={setPassword}
          />
          <Button
            containerStyle={styles.btnContainer}
            title="Masuk"
            onPress={onLogin}
          />
          <Button
            containerStyle={[styles.btnContainer, {backgroundColor: '#FFF'}]}
            textStyle={{color: Colors.primary}}
            underlayColor={Colors.opacityColor(Colors.primary, 0.1)}
            onPress={() => props.navigation.navigate('ForgotPassword')}
            title="Lupa Kata Sandi"
          />
        </View>
        <View style={styles.footer}>
          <Text allowFontScaling={false} style={styles.labelFooter}>
            Saya belum memiliki akun?{' '}
          </Text>
          <TouchableWithoutFeedback
            onPress={() => props.navigation.navigate('Register')}>
            <Text allowFontScaling={false} style={styles.labelBtnFooter}>
              Daftar
            </Text>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </ScrollView>
  );
};

export const screenOptions = navData => {
  return {
    headerTitle: 'Masuk ke Akun',
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft: Size.scaleSize(5)}}
        onPress={() => navData.navigation.goBack()}>
        <View style={{padding: Size.scaleSize(10)}}>
          <Icons.ChevronLeft color="#333" size={Size.scaleSize(18)} />
        </View>
      </TouchableOpacity>
    ),
  };
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: 'white',
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  titleWelcome: {
    fontFamily: Fonts.mulishExtraBold,
    color: '#333',
    fontSize: Size.scaleFont(24),
    marginHorizontal: Size.scaleSize(20),
    marginTop: Size.scaleSize(30),
    marginBottom: Size.scaleSize(16),
  },
  title: {
    fontFamily: Fonts.mulishRegular,
    color: '#B4B6B8',
    fontSize: Size.scaleFont(14),
    marginHorizontal: Size.scaleSize(20),
  },
  textInputContainer: {
    marginHorizontal: Size.scaleSize(20),
    marginTop: Size.scaleSize(23),
  },
  btnContainer: {
    marginHorizontal: Size.scaleSize(20),
    marginTop: Size.scaleSize(40),
  },
  footer: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: Size.scaleSize(150),
    alignItems: 'center',
  },
  labelFooter: {
    fontFamily: Fonts.mulishRegular,
    color: '#333',
    fontSize: Size.scaleFont(14),
  },
  labelBtnFooter: {
    fontFamily: Fonts.mulishExtraBold,
    color: Colors.primary,
    fontSize: Size.scaleFont(14),
  },
});

export default LoginScreen;
