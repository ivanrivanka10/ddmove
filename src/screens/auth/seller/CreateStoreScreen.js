import React, {useState} from 'react';
import { View, Text, StyleSheet, TouchableOpacity, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard } from 'react-native';

import { Fonts, Size, Icons, Colors } from '@styles/index';
import { InputText, Button } from '@components/global';

const CreateStoreScreen = props => {

    const [shopName, setShopName] = useState('')
    const [phone, setPhone] = useState('')

    const validate = () => {
        var isDisable = false
        const obj = {
            shopName,
            phone
        }
        const dataEmpty = Object.keys(obj).filter(data => obj[data] == '' || obj[data] == null)
        if(dataEmpty.length > 0) {
            isDisable = true
        } else {
            isDisable = false
        }
        return isDisable;
    }

    return (
        <KeyboardAvoidingView
        enabled
        behavior={Platform.OS == 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'android' ? 0 : 90}
        keyboardShouldPersistTaps="always"
        style={styles.screen}>
             <TouchableWithoutFeedback 
        onPress={Keyboard.dismiss}>
            <View style={{flex: 1}}>
            <View style={{flex: 1}}>
                <Text allowFontScaling={false} style={styles.title}>Buat Nama Toko Anda, No. Telepon</Text>
                <Text allowFontScaling={false} style={styles.subTitle}>Berikan nama toko Anda dan cantumkan kontak dari toko Anda.</Text>
                <InputText 
                    containerStyle={styles.textInputContainer}
                    placeholder="Berikan Nama Toko"
                    value={shopName}
                    onChangeText={(text) => setShopName(text)}
                />
                <InputText 
                    containerStyle={styles.textInputContainer}
                    placeholder="Tambahkan No. Telepon"
                    value={phone}
                    onChangeText={(text) => setPhone(text)}
                    keyboardType="number-pad"
                    maxLength={13}

                />
            </View>
            <Button 
                onPress={() => props.navigation.navigate('CreateStoreAdress', {
                    data: {
                        shopName,
                        phone
                    }
                })}
                disable={validate()}
                containerStyle={[styles.btnContainer, validate() && {backgroundColor: Colors.borderGrey}]}
                title="Lanjutkan"
            />
            </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: '',
        headerLeft: () => (
            <TouchableOpacity
                style={{marginLeft: Size.scaleSize(5)}}
                onPress={() => navData.navigation.replace('MainNavigation')}
            >
                <View style={{padding: Size.scaleSize(10)}}>
                    <Icons.ChevronLeft 
                        color="#333"
                        size={Size.scaleSize(18)}
                    />
                </View>
            </TouchableOpacity>
        )
        
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    title: {
        color: '#333',
        fontFamily: Fonts.mulishExtraBold,
        fontSize: Size.scaleFont(26),
        paddingHorizontal: Size.scaleSize(18)
    },
    subTitle: {
        fontFamily: Fonts.mulishRegular,
        paddingHorizontal: Size.scaleSize(18),
        marginTop: Size.scaleSize(8),
        marginBottom: Size.scaleSize(8),
        fontSize: Size.scaleFont(14),
        color: '#B4B6B8'
    },
    textInputContainer: {
        marginHorizontal: Size.scaleSize(18),
        marginTop: Size.scaleSize(23)
    },
    btnContainer: {
        marginHorizontal: Size.scaleSize(18),
        marginBottom: Size.scaleSize(10)
    }
})

export default CreateStoreScreen;