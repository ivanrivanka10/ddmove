import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  SafeAreaView,
  FlatList,
} from 'react-native';

import {Fonts, Size, Icons, Colors} from '@styles/index';
import {
  InputText,
  Button,
  SelectDropDown,
  TextBold,
  TextRegular,
} from '@components/global';
import API from '@utils/services/settingProvider';
import APIX from '@utils/services/apiProvider';
import APISELLER from '@utils/services/seller/orderProvider';
import {useSelector, useDispatch} from 'react-redux';
import {IconAsset} from '@utils/index';
import ModalPengiriman from '@screens/main/seller/product/Pengiriman';

import ModalSuccessBottom from '@components/modal/ModalSuccessBottom';
import {showToastMessage} from '@helpers/functionFormat';
import {KeyboardAvoidingView} from 'react-native';
export const LOGOUT = 'LOGOUT';

const CreateAddressStoreScreen = ({navigation, route}) => {
  const {userData, location} = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const [listProvince, setListProvince] = useState([]);
  const [listCity, setListCity] = useState([]);
  const [listDistrict, setListDistrict] = useState([]);
  const [pickedProvince, setPickedProvince] = useState(null);
  const [pickedCity, setPickedCity] = useState(null);
  const [pickedDistrict, setPickedDistrict] = useState(null);
  const [pickedType, setPickedType] = useState(null);
  const [pickedOngkir, setPickedOngkir] = useState(null);
  const [isAgree, setIsAgree] = useState(false);
  const [postCode, setPostCode] = useState(false);
  const [modalSuccess, setModalSuccess] = useState(false);
  const [showModalPengiriman, setShowModalPengiriman] = useState(false);
  const [listCourier, setListCourier] = useState([]);
  const [pickedCourier, setPickedCourier] = useState([]);
  console.log('route store address', route);

  const getProvince = async () => {
    console.log('loction', location);
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.GetProvince();
    if (res && res.status === 200) {
      let data = [];
      for (let index = 0; index < res.data.data.length; index++) {
        data.push({
          id: res.data.data[index].province_id,
          name: res.data.data[index].province,
        });
      }
      setListProvince(data);
    }
    const getListCourier = await APISELLER.GetListCourier(userData.token);
    var kurir = [...getListCourier.data.data];
    for (let i = 0; i < kurir.length; i++) {
      kurir[i].checked = false;
    }
    console.log('listCourier', kurir);
    setListCourier(kurir);
    dispatch({type: 'HIDE_LOADING'});
  };

  const getListCity = async id => {
    const res = await API.GetCity(id);
    if (res && res.status == 200) {
      let data = [];
      for (let index = 0; index < res.data.data.length; index++) {
        data.push({
          id: res.data.data[index].city_id,
          name: res.data.data[index].city_name,
        });
      }
      setListCity(data);
    }
  };

  const getListDistrict = async id => {
    const res = await API.GetDistrict(id);
    if (res && res.status == 200) {
      let data = [];
      for (let index = 0; index < res.data.data.length; index++) {
        data.push({
          id: res.data.data[index].district_id,
          name: res.data.data[index].district_name,
        });
      }
      setListDistrict(data);
    }
  };

  const getUserInfo = async token => {
    const res = await APIX.GetUserInfo(token);
    console.log('res data user', res);
    if (res.status === 200) {
      const data = {
        token: `${token}`,
        email: res.data.data.email,
        is_seller: res.data.data.is_seller,
        name: res.data.data.name,
        phone: res.data.data.phone,
        image: res.data.data.image,
      };
      if (res.data.data.is_seller === 'no') {
        const getUserAddress = await APISETTING.GetAddress(`${token}`);
        console.log('res useraddress', getUserAddress);
        if (getUserAddress.status === 404) {
          data.hasAddress = false;
          data.fromSeller = false;
        } else {
          data.hasAddress = true;
          data.fromSeller = false;
        }
      } else {
        data.fromSeller = true;
      }
      dispatch({type: "LOGIN_SUCCESS", data}); 
      if (res.data.data.is_seller === 'yes') {
        dispatch({type: "UPDATE_TYPE"});
        navigation.replace('SellerNavigation');
      } else {
        navigation.replace('MainNavigation');
      }
      // await loadLocation(res);
    }
  };

  const validate = () => {
    var isDisable = false;
    const obj = {
      pickedProvince,
      pickedCity,
      pickedDistrict,
      isAgree,
      postCode,
    };
    const dataEmpty = Object.keys(obj).filter(
      data => obj[data] == '' || obj[data] == null || obj[data] == false,
    );
    if (dataEmpty.length > 0) {
      isDisable = true;
    } else {
      isDisable = false;
    }
    return isDisable;
  };

  const createStore = async () => {
    console.log('props route', route);
    const params = route.params.data;
    const getIdProvince = listProvince.filter(value => {
      return value.name === pickedProvince;
    });
    const getIdCity = listCity.filter(value => {
      return value.name === pickedCity;
    });
    const getIdDistrict = listDistrict.filter(value => {
      return value.name === pickedDistrict;
    });
    const data = new FormData();
    data.append('name', params.shopName);
    data.append('phone', params.phone);
    data.append('province_id', getIdProvince[0].id);
    data.append('city_id', getIdCity[0].id);
    data.append('district_id', getIdDistrict[0].id);
    data.append('postal_code', postCode);
    data.append('longitude', location?.lng ?? '-');
    data.append('latitude', location?.lat ?? '-');
    data.append('agree_policy', isAgree ? 'yes' : 'no');
    data.append('address_details', '-');

    if (pickedType == 'Non Material') {
      pickedCourier.map(e => {
        data.append('courier[]', e);
      });
    }

    data.append(
      'type',
      pickedType == 'Material'
        ? 'material'
        : pickedType == 'Non Material'
        ? 'non-material'
        : 'both',
    );
    data.append(
      'is_free_shipping',
      pickedOngkir == null ? 0 : pickedOngkir == 'Beberapa Produk' ? 1 : 0,
    );

    console.log('body create', data);
    dispatch({type: 'SHOW_LOADING'});
    const res = await API.CreateShop(data, userData.token);
    console.log(res.data);
    console.log('res create shop', res?.status ?? 'error');
    if (res.status === 200) {
      await getUserInfo(userData.token);
      dispatch({type: 'HIDE_LOADING'});
      const timer = setTimeout(() => {
        setModalSuccess(true);
      }, 1000);
    } else {
      dispatch({type: 'HIDE_LOADING'});
      if (res.data.data.info == 'You have created a shop!') {
        showToastMessage('Anda Sudah Memiliki Toko!');
        navigation.replace('SellerNavigation');
      } else {
        showToastMessage(res.data.info);
      }
    }
  };

  useEffect(() => {
    getProvince();
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <FlatList
        style={{flex: 1}}
        ListEmptyComponent={
          <KeyboardAvoidingView
            enabled
            behavior="padding"
            keyboardVerticalOffset={Platform.OS === 'android' ? -200 : 60}
            keyboardShouldPersistTaps="always"
            style={{flex: 1, backgroundColor: '#fff', padding: 18}}>
            <ModalSuccessBottom
              show={modalSuccess}
              onClose={() => {
                setModalSuccess(false);
              }}
              onPressAgree={() => {
                setModalSuccess(false);
              }}
              message="Berhasil Membuat Toko"
              subMessage="Terimakasih telah mendaftar sebagai seller pada platform kami. Informasi selanjutnya akan kami hubungi via e-mail Anda."
            />
            {showModalPengiriman && (
              <ModalPengiriman
                show={showModalPengiriman}
                onClose={() => setShowModalPengiriman(false)}
                listCourier={listCourier}
                showForm={false}
                onSubmit={data => {
                  console.log('data picked kurir', data);
                  var arr = [];
                  for (let i = 0; i < data.selectedCourier.length; i++) {
                    arr.push(data.selectedCourier[i].id);
                  }
                  setPickedCourier(arr);
                  setShowModalPengiriman(false);
                }}
              />
            )}
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <View>
                <TextBold
                  text="Lengkapi Alamat Toko"
                  size={24}
                  color={Colors.blackBase}
                />
                <TextRegular
                  text="Berikan alamat lengkap lokasi dari toko Anda."
                  color={Colors.grey_1}
                  style={{marginTop: 8}}
                />
                <SelectDropDown
                  searchable
                  value={pickedProvince}
                  onSelectItem={val => {
                    setListCity([]);
                    setListDistrict([]);
                    setPickedCity(null);
                    setPickedDistrict(null);
                    setPickedProvince(val.name);
                    getListCity(val.id);
                  }}
                  valueColor="#000"
                  colorArrowIcon="#000"
                  style={{marginTop: 32, borderRadius: 6}}
                  placeholderText="Pilih provinsi"
                  items={listProvince}
                  maxHeight={300}
                  typeValueText="Medium"
                />
                <SelectDropDown
                  searchable
                  value={pickedCity}
                  onSelectItem={val => {
                    setPickedCity(val.name);
                    getListDistrict(val.id);
                  }}
                  disabled={listCity.length < 1}
                  valueColor="#000"
                  colorArrowIcon="#000"
                  style={{marginTop: 24, borderRadius: 6}}
                  placeholderText="Pilih kota"
                  items={listCity}
                  maxHeight={200}
                  typeValueText="Medium"
                />
                <SelectDropDown
                  searchable
                  value={pickedDistrict}
                  onSelectItem={val => {
                    setPickedDistrict(val.name);
                  }}
                  disabled={listDistrict.length < 1}
                  valueColor="#000"
                  colorArrowIcon="#000"
                  style={{marginTop: 24, borderRadius: 6}}
                  placeholderText="Pilih kecamatan"
                  items={listDistrict}
                  maxHeight={200}
                  typeValueText="Medium"
                />
                <TextRegular
                  text="Tipe Barang Yang Di Jual"
                  color={Colors.blackBase}
                  style={{marginTop: 24}}
                />
                <SelectDropDown
                  value={pickedType}
                  onSelectItem={val => {
                    setPickedType(val.name);
                    if(val.name === 'Material dan Non Material'){
                      setPickedOngkir('Semua Produk / Beberapa Produk')
                    }
                  }}
                  valueColor="#000"
                  colorArrowIcon="#000"
                  style={{marginTop: 10, borderRadius: 6}}
                  placeholderText="Pilih tipe"
                  items={[
                    // {id: 2, name: 'Material'},
                    {id: 2, name: 'Non Material'},
                    {id: 2, name: 'Material dan Non Material'},
                  ]}
                  maxHeight={200}
                  typeValueText="Medium"
                />

                <TouchableOpacity
                  style={styles.btnPengiriman}
                  onPress={() => setShowModalPengiriman(true)}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={IconAsset.IcTruck}
                      style={{width: 25, height: 25, resizeMode: 'contain'}}
                    />
                    <TextBold
                      text="Pengiriman"
                      color={Colors.blackBase}
                      style={{marginLeft: 10}}
                    />
                  </View>
                  <Image
                    source={IconAsset.ChevronRight}
                    style={{
                      width: 20,
                      height: 20,
                      tintColor: Colors.blackBase,
                    }}
                  />
                </TouchableOpacity>

                {/* {pickedType == 'Non Material' && (
                  <> */}
                    <TextRegular
                      text="Gratis Antar"
                      color={Colors.blackBase}
                      style={{marginTop: 24}}
                    />
                    <SelectDropDown
                      value={pickedOngkir}
                      containerStyle={{
                        backgroundColor:
                        pickedType === 'Material dan Non Material' ? 'lightgrey' : 'white',
                      }}
                      disabled={pickedType === 'Material dan Non Material' ? true :false}
                      onSelectItem={val => {
                        setPickedOngkir(val.name);
                      }}
                      valueColor="#000"
                      colorArrowIcon="#000"
                      style={{marginTop: 10, borderRadius: 6}}
                      placeholderText="Pilih Gratis Antar"
                      items={[
                        {id: 1, name: 'Beberapa Produk'},
                        {id: 2, name: 'Tidak Ada'},
                      ]}
                      maxHeight={200}
                      typeValueText="Medium"
                    />

                    {pickedOngkir !== 'Tidak Ada' && (
                      <Text
                        style={{
                          fontFamily: Fonts.mulishExtraBold,
                          color: Colors.primary,
                          fontSize: 10,
                          marginTop: 8,
                        }}>
                        * wajib mengantarkan barang ke konsumen. Baik antar
                        langsung maupun melalui ekspedisi selama jangkauan masih
                        tercover
                      </Text>
                    )}
                  {/* </>
                )} */}

                <TextRegular
                  text="Kode Pos"
                  color={Colors.blackBase}
                  style={{marginTop: 24}}
                />
                <InputText
                  containerStyle={styles.textInputContainer}
                  style={{paddingHorizontal: 10}}
                  placeholder="Kode Pos"
                  value={postCode}
                  onChangeText={text => setPostCode(text)}
                  keyboardType="number-pad"
                  maxLength={8}
                />
                <View
                  style={{
                    marginTop: 24,
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={[styles.squere, {borderWidth: isAgree ? 0 : 1}]}
                    onPress={() => {
                      setIsAgree(prevState => !prevState);
                    }}>
                    {isAgree && (
                      <Image
                        source={IconAsset.IcCheckSquere}
                        style={{
                          width: 20,
                          height: 20,
                          borderColor: Colors.borderGrey,
                          borderWidth: 1,
                        }}
                      />
                    )}
                  </TouchableOpacity>
                  <View
                    style={{
                      paddingHorizontal: 18,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'Mulish-Regular',
                        fontSize: 14,
                        color: Colors.blackBase,
                      }}>
                      Saya setuju dengan{' '}
                      <Text
                        onPress={() => alert('S&K')}
                        style={{
                          fontFamily: 'Mulish-Bold',
                          fontSize: 14,
                          color: Colors.primary,
                        }}>
                        Syarat & Ketentuan
                      </Text>
                      ,
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'Mulish-Regular',
                        fontSize: 14,
                        color: Colors.blackBase,
                      }}>
                      Serta{' '}
                      <Text
                        onPress={() => alert('privasi')}
                        style={{
                          fontFamily: 'Mulish-Bold',
                          fontSize: 14,
                          color: Colors.primary,
                        }}>
                        Kebijakan Privasi
                      </Text>{' '}
                      dari Baba Market.
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    height: 100,
                    width: '85%',
                    marginStart: '8.5%',
                  }}>
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={[
                      styles.btnSubmit,
                      validate() && {backgroundColor: Colors.borderGrey},
                    ]}
                    disabled={validate()}
                    onPress={() => {
                      createStore();
                    }}>
                    <TextBold text="Kirim" size={16} color="#fff" />
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        }
        ListFooterComponent={<View style={{height: 50}} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  btnPengiriman: {
    width: '100%',
    // paddingHorizontal: 15,
    marginTop: 25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnSubmit: {
    bottom: 0,
    width: '100%',
    position: 'absolute',
    borderRadius: 16,
    alignSelf: 'center',
    paddingVertical: 17,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
  },
  squere: {
    width: 18,
    height: 18,
    borderRadius: 4,
    borderColor: Colors.borderGrey,
  },
  textInputContainer: {
    marginTop: Size.scaleSize(8),
  },
});

export default CreateAddressStoreScreen;

export const screenOptions = navData => {
  return {
    headerTitle: '',
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft: Size.scaleSize(5)}}
        onPress={() => navData.navigation.replace('MainNavigation')}>
        <View style={{padding: Size.scaleSize(10)}}>
          <Icons.ChevronLeft color="#333" size={Size.scaleSize(18)} />
        </View>
      </TouchableOpacity>
    ),
  };
};

export const logout = () => {
  return {
    type: LOGOUT,
  };
};
