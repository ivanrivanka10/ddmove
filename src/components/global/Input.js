import React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Platform,
  TouchableOpacity,
} from 'react-native';
import Fonts from '@styles/Fonts';
import {Size, Icons} from '@styles/index';

export const InputText = props => {
  const {
    label,
    leftIcon,
    rightIcon,
    isPassword,
    containerStyle,
    value,
    keyboardType,
    containerInputStyle,
    editable,
    multiline,
    textAlignVertical,
  } = props;
  return (
    <View style={[styles.container, containerStyle]}>
      {label && (
        <Text style={styles.labelInputText} allowFontScaling={false}>
          {label}
        </Text>
      )}
      <View style={[styles.containerTextInput, containerInputStyle]}>
        {leftIcon ? leftIcon : null}
        <TextInput
          textAlignVertical={textAlignVertical ? 'top' : 'center'}
          multiline={multiline}
          value={value}
          ref={props.innerRef}
          allowFontScaling={false}
          placeholderTextColor="#BDBDBD"
          autoCapitalize="none"
          keyboardType={keyboardType ? keyboardType : 'default'}
          {...props}
          secureTextEntry={props.secureTextEntry}
          editable={editable}
          style={[
            styles.inputText,
            props.style,
            Platform.OS === 'ios' && {height: 45},
          ]}
        />
        {isPassword ? (
          <TouchableOpacity
            onPress={() => props.setSecureTextEntry(!props.secureTextEntry)}>
            <Icons.EyeLeashedSVG
              size={Size.scaleSize(20)}
              color="#B4B6B8"
              style={[props.stylePassword, {marginLeft: Size.scaleSize(10)}]}
            />
          </TouchableOpacity>
        ) : rightIcon ? (
          rightIcon
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  labelInputText: {
    fontFamily: Fonts.mulishRegular,
    fontSize: Size.scaleFont(14),
    color: '#000', //'#828282',
    marginBottom: Size.scaleSize(4),
  },
  inputText: {
    fontFamily: Fonts.mulishRegular,
    fontSize: Size.scaleFont(14),
    color: '#000',
    marginHorizontal: 5,
    // width: '90%'
    flex: 1,
  },
  containerTextInput: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    width: '100%',
    borderColor: '#E0E0E0',
    borderRadius: 10,
    // paddingHorizontal: 15
  },
});
