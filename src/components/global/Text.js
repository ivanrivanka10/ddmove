import React from 'react';
import {Text, StyleSheet} from 'react-native';
import { Colors, Fonts, Size, Icons, Images } from '@styles/index';

export const TextRegular = ({text, numberOfLines = 2, size = 14, color = Colors.blackBase, style}) => {
  return (
    <Text
      numberOfLines={numberOfLines}
      ellipsizeMode="tail"
      style={[
        styles.regularText,
        {fontSize: size, color: color},
        style,
      ]}>
      {text}
    </Text>
  );
};

export const TextSemiBold = ({text, numberOfLines = 2, size = 14, color = Colors.blackBase, style}) => {
  return (
    <Text
      numberOfLines={numberOfLines}
      ellipsizeMode="tail"
      style={[
        styles.semiBoldText,
        {fontSize: size, color: color},
        style,
      ]}>
      {text}
    </Text>
  );
};

export const TextBold = ({text, numberOfLines = 2, size = 14, color = Colors.blackBase, style}) => {
  return (
    <Text
      numberOfLines={numberOfLines}
      ellipsizeMode="tail"
      style={[
        styles.boldText,
        {fontSize: size, color: color},
        style,
      ]}>
      {text}
    </Text>
  );
};

const styles = StyleSheet.create({
  regularText: {
    fontFamily: Fonts.mulishRegular
  },
  semiBoldText: {
    fontFamily: Fonts.mulishMedium
  },
  boldText: {
    fontFamily: Fonts.mulishBold
  },
});
