import React, {useState} from 'react';
import {View, StyleSheet, ScrollView, TextInput, TouchableOpacity, Image} from 'react-native';
import {TextRegular, TextSemiBold, Icon} from '@components/global';
import {Colors} from '@styles/index';
import {IconAsset} from '@utils/index';

export const SelectDropDown = ({
    //Action
    items = [],
    onSelectItem,
    disabled,
    onPressDropDown = () => null,
    //Style
    style,
    containerStyle,
    labelStyle,
    maxHeight = 100,
    dropUp = false,
    //Value Text
    label,
    valueColor,
    value,
    customLabel,

    //Placeholder
    placeholderTextColor,
    placeholderText = 'Select Drop Down',

    //Icon From Entypo Vector Icon
    iconSize = 20,

    //Left Icon
    leftIcon,
    iconFillColor,
    iconStyle,

    //Searchable
    searchable,
    autoFocus,
    editable,
    secureTextEntry,
    blurOnSubmit,
    keyboardType = 'default',
    autoCapitalize = 'none',
    multiline = false,
    numberOfLines,
    testID,
    listTestID,
    colorArrowIcon = '#000000',
    typeValueText = 'Regular',
    isOpen = () => null,
}) => {
    const [showDropdown, setShowDropdown] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [focus, setFocus] = useState('');
    let data = items;
    if (keyword) {
        data = data.filter((x) => {
            return x.name.toLowerCase().search(keyword.toLowerCase()) !== -1;
        });
    }

    // React.useEffect(() => {
    //     isOpen(showDropdown)
    // },[showDropdown])

    return (
        <View style={style}>
            <View>{customLabel}</View>
            {label ? <TextRegular text={label} style={labelStyle} /> : null}
            <TouchableOpacity
                disabled={disabled}
                onPress={() => {
                    onPressDropDown()
                    setShowDropdown(!showDropdown);
                    isOpen(!showDropdown, undefined)
                }}
                style={[
                    styles.container,
                    containerStyle,
                    showDropdown && {borderColor: '#029FFD'},
                ]}
                testID={testID}>
                <View style={{flexDirection: 'row'}}>
                    {leftIcon ? (
                        <Icon
                            icon={leftIcon}
                            size={iconSize}
                            style={iconStyle}
                            color={iconFillColor}
                        />
                    ) : null}
                    {
                        typeValueText === 'Regular' ? 
                            <TextRegular
                                text={value ?? placeholderText}
                                color={
                                    !showDropdown
                                        ? value
                                            ? valueColor ?? '#758592'
                                            : placeholderTextColor ?? '#758592'
                                        : '#384753'
                                }
                                style={{width:'80%'}}
                                numberOfLines={1}
                            />
                        :
                            <TextSemiBold
                                text={value ?? placeholderText}
                                color={
                                    !showDropdown
                                        ? value
                                            ? valueColor ?? '#758592'
                                            : placeholderTextColor ?? '#758592'
                                        : '#384753'
                                }
                                style={{width:'80%'}}
                                numberOfLines={1}
                            />
                    }                    
                </View>

                {dropUp ? (
                    <Icon
                        icon={
                            showDropdown
                                ? IconAsset.ChevronDown
                                : IconAsset.ChevronUp
                        }
                        size={iconSize}
                        color={showDropdown ? '#029FFD' : '#758592'}
                    />
                ) : (
                    <Icon
                        icon={
                            showDropdown
                                ? IconAsset.ChevronUp
                                : IconAsset.ChevronDown
                        }
                        size={iconSize}
                        color={showDropdown ?'#029FFD' : colorArrowIcon}
                    />
                )}
            </TouchableOpacity>
            {searchable && showDropdown ? (
                <View
                    style={[
                        styles.dropDownContainer,
                        {marginTop: -2},
                        {maxHeight},
                    ]}>
                    <TextInput
                        editable={editable}
                        autoFocus={autoFocus}
                        secureTextEntry={secureTextEntry}
                        onFocus={() => {
                            setFocus(true);
                        }}
                        onBlur={() => {
                            setShowDropdown(false);
                        }}
                        placeholder={placeholderText}
                        placeholderTextColor={placeholderTextColor}
                        keyboardType={keyboardType}
                        autoCapitalize={autoCapitalize}
                        value={keyword}
                        onSubmitEditing={() => {
                            onSelectItem(data[0]);
                            setKeyword(data[0].name);
                            setShowDropdown(false);
                            setFocus(false);
                        }}
                        multiline={multiline}
                        numberOfLines={numberOfLines}
                        blurOnSubmit={blurOnSubmit}
                        onChangeText={setKeyword}
                        style={[
                            styles.contentSearch,
                            {
                                // fontFamily,
                                // fontSize,
                                // height: !height && Platform.OS === 'ios' ? 40:height,
                                color: valueColor ? valueColor : '#000000',
                            },
                            // focus && {borderColor: Colors.B400}
                        ]}
                    />
                    <ScrollView nestedScrollEnabled={true}>
                        {data.map((item, index) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => {
                                        onSelectItem(item, index);
                                        setShowDropdown(false);
                                        setKeyword('');
                                        isOpen(false, item)
                                    }}
                                    style={[
                                        index === 0 && {marginTop: 10},
                                        value === item.name
                                            ? {
                                                  backgroundColor: 'rgba(2, 159, 253, 0.1)',
                                              }
                                            : {},
                                        styles.dropDownItem,
                                    ]}
                                    key={index}
                                    testID={listTestID}>
                                    <TextRegular size={16} text={`${item.name}`} color="#000" />
                                </TouchableOpacity>
                            );
                        })}
                    </ScrollView>
                </View>
            ) : showDropdown ? (
                <View
                    style={[
                        styles.dropDownContainer,
                        {maxHeight},
                        dropUp && styles.dropUpAbsolute,
                    ]}>
                    <ScrollView contentContainerStyle={{zIndex: 3}} nestedScrollEnabled={true}>
                        {data.map((item, index) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => {
                                        onSelectItem(item, index);
                                        setShowDropdown(false);
                                        setKeyword('');
                                        isOpen(false, item)
                                    }}
                                    style={[
                                        value === item.name
                                            ? {
                                                  backgroundColor: 'rgba(2, 159, 253, 0.1)',
                                              }
                                            : {},
                                        styles.dropDownItem,                                        
                                    ]}
                                    key={index}>
                                    <TextRegular text={`${item.name}`} />
                                </TouchableOpacity>
                            );
                        })}
                    </ScrollView>
                </View>
            ) : null}
        </View>
    );
};

const styles = StyleSheet.create({
    dropDownContainer: {
        backgroundColor: '#FFFFFF', //'#FFFFFF',
        width: '100%',
        borderRadius: 7,
        borderColor: '#dedede',
        borderWidth: 1,
        elevation: 5,
        zIndex: 999
        // ...Mixins.boxShadow('#eee'),
    },
    dropDownItem: {
        paddingLeft: 15,
        paddingVertical: 8,
    },
    dropUpAbsolute: {
        position: 'absolute',
        bottom: 50,
        elevation: 10,
        zIndex: 2
    },
    contentSearch: {
        backgroundColor: '#FFF',
        margin: 16,
        marginBottom: 2,
        padding: 12,
        // paddingHorizontal: 15,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#EEEE',
    },
    container: {
        // marginTop: 8,
        borderWidth: 1,
        borderColor: '#D9D9D9',
        borderRadius: 8,
        padding: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
})