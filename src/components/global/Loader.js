import React, { PureComponent } from 'react';
import { View, StyleSheet, Animated, Easing } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

import { Colors } from '@styles/index';

export const Loader = props => {

    if(!props.show){
        return (
            <>
                
            </>
        )
    }

    return (
        <View>
            <Spinner
                visible={true}
                customIndicator = {<DotIndicator color={Colors.primary} count={3} />}
                overlayColor={'rgba(200,200,200,0.6)'}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    background: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0
    },
    container: {
        backgroundColor: 'transparent',
        bottom: 0,
        flex: 1,
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0
    },
    containerIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
})

class DotIndicator extends PureComponent {
    static defaultProps = {
        animationEasing: Easing.inOut(Easing.ease),
    
        color: 'rgb(0, 0, 0)',
        count: 4,
        size: 16,
    };

    constructor(props) {
        super(props);
        this.renderComponent = this.renderComponent.bind(this);
    }

    renderComponent({ index, count, progress }) {
        let { size, color: backgroundColor } = this.props;
    
        let style = {
            width: size,
            height: size,
            margin: size / 2,
            borderRadius: size / 2,
            backgroundColor,
            transform: [{
                scale: progress.interpolate({
                    inputRange: [
                        0.0,
                        (index + 0.5) / (count + 1),
                        (index + 1.0) / (count + 1),
                        (index + 1.5) / (count + 1),
                        1.0,
                    ],
                    outputRange: [
                        1.0,
                        1.36,
                        1.56,
                        1.06,
                        1.0,
                    ],
                }),
            }],
        };
    
        return (
            <Animated.View style={style} {...{ key: index }} />
        );
    }

    render() {
        let { style, ...props } = this.props;
    
        return (
            <Indicator
                style={[styles.containerIndicator, style]}
                renderComponent={this.renderComponent}
                {...props}
            />
        );
    }
}

class Indicator extends PureComponent {
    static defaultProps = {
        animationEasing: Easing.linear,
        animationDuration: 1200,
        hideAnimationDuration: 200,

        animating: true,
        interaction: true,
        hidesWhenStopped: true,

        count: 1,
    };

    constructor(props) {
        super(props);
        this.animationState = 0;
        this.savedValue = 0;
        let { animating } = this.props;
        this.state = {
            progress: new Animated.Value(0),
            hideAnimation: new Animated.Value(animating? 1 : 0),
        };
    }

    componentDidMount() {
        let { animating } = this.props;

        if (animating) {
            this.startAnimation();
        }
    }

    componentDidUpdate(prevProps) {
        let { animating } = this.props;

        if (animating && !prevProps.animating) {
            this.resumeAnimation();
        }

        if (!animating && prevProps.animating) {
            this.stopAnimation();
        }

        if (animating ^ prevProps.animating) {
            let { hideAnimation } = this.state;
            let { hideAnimationDuration: duration } = this.props;

            Animated
                .timing(hideAnimation, { toValue: animating? 1 : 0, duration })
                .start();
        }
    }

    startAnimation() {
        let { progress } = this.state;
        let { interaction, animationEasing, animationDuration } = this.props;

        if (0 !== this.animationState) {
            return;
        }

        let animation = Animated
            .timing(progress, {
                duration: animationDuration,
                easing: animationEasing,
                useNativeDriver: true,
                isInteraction: interaction,
                toValue: 1,
            });

        Animated
            .loop(animation)
            .start();

        this.animationState = 1;
    }

    stopAnimation() {
        let { progress } = this.state;

        if (1 !== this.animationState) {
            return;
        }

        let listener = progress
            .addListener(({ value }) => {
                progress.removeListener(listener);
                progress.stopAnimation(() => this.saveAnimation(value));
            });

        this.animationState = -1;
    }

    saveAnimation(value) {
        let { animating } = this.props;

        this.savedValue = value;
        this.animationState = 0;

        if (animating) {
            this.resumeAnimation();
        }
    }

    resumeAnimation() {
        let { progress } = this.state;
        let { interaction, animationDuration } = this.props;

        if (0 !== this.animationState) {
            return;
        }

        Animated
            .timing(progress, {
                useNativeDriver: true,
                isInteraction: interaction,
                duration: (1 - this.savedValue) * animationDuration,
                toValue: 1,
            })
            .start(({ finished }) => {
                if (finished) {
                    progress.setValue(0);

                    this.animationState = 0;
                    this.startAnimation();
                }
            });

        this.savedValue = 0;
        this.animationState = 1;
    }

    renderComponent(item, index) {
        let { progress } = this.state;
        let { renderComponent, count } = this.props;

        if ('function' === typeof renderComponent) {
            return renderComponent({ index, count, progress });
        }

        return null;
    }

    render() {
        let { hideAnimation } = this.state;
        let { count, hidesWhenStopped, ...props } = this.props;

        if (hidesWhenStopped) {
            props.style = []
                .concat(props.style || [], { opacity: hideAnimation });
        }

        return (
            <Animated.View {...props}>
                {Array.from(new Array(count), this.renderComponent, this)}
            </Animated.View>
        );
    }
}

