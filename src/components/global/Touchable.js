import React from 'react';
import {TouchableOpacity} from 'react-native';

export const Touchable = ({
    children,
    onPress,
    disabled,
    style,
    hitSlop,
    testID,
}) => {
    return (
        <TouchableOpacity
            accessible={true}
            accessibilityLabel={testID}
            testID={testID}
            style={style}
            activeOpacity={0.8}
            opacity={0.5}
            onPress={disabled ? null : onPress}
            hitSlop={hitSlop ? hitSlop : {}}
            disabled={disabled}>
            {children}
        </TouchableOpacity>
    );
};