import React, {useRef} from 'react'
import {StyleSheet, Dimensions, TouchableOpacity, Platform, ImageBackground, ActivityIndicator, Image, View} from 'react-native'
import Carousel, { Pagination, ParallaxImage  } from 'react-native-snap-carousel';
import {Colors} from '@styles/index'
import { useNavigation } from '@react-navigation/native';

const { width: screenWidth } = Dimensions.get('window')
export const SLIDER_WIDTH = Dimensions.get('window').width + 80
export const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7)
const Slider = ({data, activeIndex, setActiveIndex, onPress, width, height}) => {
   const navigation = useNavigation()
   var SwapSlide = useRef()
   const _renderItem = ( {item,index}, parallaxProps) => {      
      return (
         <TouchableOpacity 
            activeOpacity={0.7}
            onPress={() => {
               navigation.navigate("ImageView",{
                  data : data
               })
            }
            }
            style={styles.item}>
            {/* <Image
               style={{ resizeMode: 'stretch', borderRadius: 10, width: width ? width : 318, height: height ? height : 160 }}
               source={{
                  uri: item.images ? item.images : item
                  // uri: 'https://asset.indosport.com/article/image/q/80/282577/dota_22-169.jpg?w=750&h=423'
               }}
            /> */}
            <Image 
               style={{
                  width: Dimensions.get('screen').width * 0.9, 
                  height: Dimensions.get('screen').width * 0.6,
                  borderRadius: 20,
                  resizeMode: 'stretch'
               }}
               source={{uri: item.image ? item.image : item.url}}
            />
         </TouchableOpacity>
      );
   }

   return(
      <View style={{backgroundColor: '#fff', borderTopLeftRadius: 40, borderTopRightRadius: 40, paddingBottom: 20}}>                     
         <Carousel
            layout="default"
            // layoutCardOffset={9}
            ref={c => SwapSlide = c}
            data={data}
            enableMomentum={false}
            lockScrollWhileSnapping={true}
            renderItem={_renderItem}
            sliderWidth={screenWidth}
            itemWidth={Dimensions.get('screen').width * 0.9}
            loop
            // autoplayInterval={3000}
            // autoplay
            onSnapToItem={(index) => setActiveIndex(index)}            
            removeClippedSubviews={false}
            loopClonesPerSide={data.length}
         />
         <Pagination
            dotsLength={data.length}
            activeDotIndex={activeIndex}
            containerStyle={{paddingVertical: 8, marginTop: 10, alignSelf: 'center'}}
            dotColor="#C9454A"
            dotStyle={{
               width: 20,               
            }}            
            inactiveDotStyle={styles.paginationDot}                       
            inactiveDotColor={'#E3E5E5'}
            inactiveDotOpacity={1}
            inactiveDotScale={1}
         />     
      </View>
   )
}

const styles = StyleSheet.create({
   paginationDot: {
      width: 5,
      height: 5,
      borderRadius: 2.5,
      // marginTop: 15,
      // marginHorizontal: -5,
   },
   item: {
      width: Dimensions.get('screen').width * 0.9, //Dimensions.get('window').width - 60,
      marginTop: 20,      
      // zIndex: 10,
      borderRadius: 20,
      alignSelf: 'center'
    },
    imageContainer: {
      width: 300,
      height: 230,
      marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
      backgroundColor: 'white',
      borderRadius: 8,
    },
    image: {
      ...StyleSheet.absoluteFillObject,
      resizeMode: 'cover',
    },
})

export default Slider;