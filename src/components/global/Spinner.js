import React, {useState} from 'react';
import {
  View,
  TextInput,
  Text,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

import {Colors, Size, Fonts, Icons} from '@styles/index';

export const SpinnerInputNumber = props => {
  const {count, onChange, maxCount, minCount, disabled} = props;

  const minCountInt = Number(minCount);
  const maxCountInt = Number(maxCount);
  const [inputValue, setInputValue] = useState('');
  //   const onChangeInput = value => {
  //     if (isNaN(value)) {
  //       return;
  //     }
  //     // if(value >= minCount && value <= maxCount){
  //     //     onChange(value.toString());
  //     // }
  //     onChange(value.toString());
  //   };

  const onChangeInput = value => {
    const intValue = parseInt(value);

    if (isNaN(intValue)) {
      // Handle case where input is not a valid number
      return;
    }

    // setInputValue(value);
    if (intValue >= minCountInt && intValue <= maxCountInt) {
      onChange(intValue.toString());
      setInputValue(intValue);
    } else if (intValue < minCountInt) {
      // Handle case where input is less than minCountInt
      onChange(minCountInt.toString());
      setInputValue(intValue);
    } else {
      // Handle case where input is greater than maxCountInt
      onChange(maxCountInt.toString());
      setInputValue(intValue);
    }
  };

  const handleEndEditing = () => {
    const intValue = parseInt(inputValue);
    if (inputValue == '') {
      // Handle case where input is not a valid number
      onChange(minCountInt.toString());
      setInputValue(minCountInt.toString());
    }
    // Pastikan nilai input tidak kurang dari 10
    if (parseInt(inputValue) < minCountInt) {
      // Jika kurang dari 10, set nilai input menjadi 10
      onChange(minCountInt.toString());
    } else if (parseInt(inputValue) > maxCountInt) {
      onChange(maxCountInt.toString());
    }
    // Tambahkan logika lain jika diperlukan
  };

  if (disabled) {
    return (
      <View style={[styles.container, props.containerStyle]}>
        <View style={styles.btnContainerDisabled}>
          <View
            style={{
              width: Size.scaleSize(23),
              hight: Size.scaleSize(23),
              backgroundColor: Colors.primary,
            }}
          />
        </View>
        <View style={styles.textInputContainerDisabled}>
          <Text allowFontScaling={false} style={styles.input}>
            {count}
          </Text>
        </View>
        <View style={styles.btnContainerDisabled}>
          <View
            style={{
              width: Size.scaleSize(23),
              hight: Size.scaleSize(23),
              backgroundColor: Colors.primary,
            }}
          />
        </View>
      </View>
    );
  }

  return (
    <View style={[styles.container, props.containerStyle]}>
      <TouchableHighlight
        style={[
          styles.btnContainerLeft,
          {opacity: minCountInt == parseInt(count) ? 0.5 : 1.0},
        ]}
        // onPress={() => {}}
        disabled={minCountInt == parseInt(count) ? true : false}
        onPress={() => onChangeInput(parseInt(count) - 1)}>
        <View style={[styles.btnContainer, styles.btnContainerLeft]}>
          <Icons.MinusSVG size={Size.scaleSize(20)} color={Colors.primary} />
        </View>
      </TouchableHighlight>
      <View style={styles.textInputContainer}>
        <TextInput
          allowFontScaling={false}
          keyboardType="number-pad"
          style={styles.input}
          value={count}
          onChangeText={v => {
            onChange(v.toString()), setInputValue(v.toString());
          }}
          onEndEditing={() => handleEndEditing()}
        />
      </View>
      <TouchableHighlight
        style={[
          styles.btnContainerRight,
          {opacity: maxCountInt == parseInt(count) ? 0.5 : 1.0},
        ]}
        // onPress={() => {}}
        disabled={maxCountInt == parseInt(count) ? true : false}
        onPress={() => onChangeInput(parseInt(count) + 1)}>
        <View style={[styles.btnContainer, styles.btnContainerRight]}>
          <Icons.PlusSVG size={Size.scaleSize(20)} color={Colors.primary} />
        </View>
      </TouchableHighlight>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height: 33,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  btnContainer: {
    backgroundColor: '#FFF',
    width: 30,
    height: 33,
    borderWidth: 1,
    borderColor: '#F2F2F2',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnContainerLeft: {
    borderBottomLeftRadius: 8,
    borderTopLeftRadius: 8,
  },
  btnContainerRight: {
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
  },
  btnContainerDisabled: {
    backgroundColor: '#888',
  },
  textInputContainer: {
    height: '100%',
    width: '35%',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#F2F2F2',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    backgroundColor: '#F2F2F2',
  },
  textInputContainerDisabled: {
    height: '100%',
    width: '35%',
    alignItems: 'center',
    borderColor: '#888',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    backgroundColor: '#F2F2F2',
  },
  input: {
    textAlign: 'center',
    color: '#000',
    fontFamily: Fonts.mulishRegular,
    fontSize: Size.scaleFont(14),
    paddingHorizontal: Size.scaleSize(4),
    paddingVertical: 0,
  },
});
