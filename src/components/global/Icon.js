import React from 'react';
import {Image} from 'react-native';
import {Colors} from '@styles';

export const Icon = ({icon, size = 25, color = '#384753', style, testID}) => {
    return (
        <Image
            source={icon}
            resizeMode="contain"
            style={[
                style,
                {
                    width: size,
                    height: size,
                },
                color && {tintColor: color},
            ]}
            accessible={true}
            accessibilityLabel={testID}
            testID={testID}
        />
    );
};

export default Icon;
