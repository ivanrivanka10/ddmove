import React from 'react';
import {
  View,
  ImageBackground,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {IconAsset} from '@utils/index';
import {TextBold} from '@components/global';

import {Images, Colors, Size, Icons} from '@styles/index';
import {useNavigation} from '@react-navigation/native';
import API from '@utils/services/buyer/homeProvider';
import {useSelector, useDispatch} from 'react-redux';

export const Header = props => {
  const {navData, headerLeft, isSeller} = props;
  const navigation = useNavigation();
  const {userData} = useSelector(state => state.auth);
  const [cart, setCart] = React.useState(0);

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getTotalCart();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  const getTotalCart = async () => {
    const res = await API.GetCart(userData.token);
    setCart(res?.data?.data?.length ?? 0);
  };

  return (
    <ImageBackground source={Images.StatusBarHome} style={styles.container}>
      <View style={styles.headerLeft}>{headerLeft}</View>
      <View style={styles.headerRight}>
        <TouchableOpacity
          style={{marginRight: Size.scaleSize(5), padding: Size.scaleSize(5)}}
          onPress={() => navData.navigation.navigate('Notification')}>
          <Icons.BellSVG size={isSeller ? 28 : 24} color={'#FFF'} />
        </TouchableOpacity>
        {isSeller === true ? (
          <TouchableOpacity
            onPress={() => navData.navigation.navigate('ChatSeller')}
            style={{padding: Size.scaleSize(5), marginLeft: 5}}>
            <Icons.ChatSVG size={22} color={'#FFF'} />
          </TouchableOpacity>
        ) : (
          <>
            <TouchableOpacity
              onPress={() => navData.navigation.navigate('Cart')}
              style={{padding: Size.scaleSize(5)}}>
              <Icons.CartSVG size={24} color={'#FFF'} />
              {cart !== 0 && (
                <View
                  style={{
                    width: 10,
                    backgroundColor: 'orange',
                    height: 10,
                    position: 'absolute',
                    right: 2,
                    top: 2,
                    borderRadius: 10 / 2,
                  }}
                />
              )}
            </TouchableOpacity>
          </>
        )}
      </View>
    </ImageBackground>
  );
};

export const HeaderTitle = props => {
  const navigation = useNavigation();
  return (
    <View
      style={{
        width: '100%',
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 15,
      }}>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => (props.back ? props.back() : navigation.goBack())}
        style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
        <Image
          source={IconAsset.IC_CHEVRON_LEFT}
          style={{width: 25, height: 25}}
        />
      </TouchableOpacity>
      <View style={{width: '80%', paddingLeft: 15}}>
        <TextBold text={props.title} size={14} color={Colors.blackBase} />
      </View>
      <View style={{width: '10%'}} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.primary,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 80,
  },
  headerLeft: {
    marginLeft: Size.scaleSize(15),
    marginVertical: Size.scaleSize(20),
  },
  headerRight: {
    marginRight: Size.scaleSize(15),
    flexDirection: 'row',
    alignItems: 'center',
  },
});
