import React, {
  useEffect,
  useRef,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from 'react';
import {View, StyleSheet, TextInput} from 'react-native';
import {Colors, Mixins, Typography} from '@styles/index';

const OtpInput = ({column = 5, onSubmit, containerStyle, inputStyle}, ref) => {
  const [inputs, setInputs] = useState({});

  const refs = useRef();
  refs.current = [];

  const addToRefs = el => {
    if (el && !refs.current.includes(el)) {
      refs.current.push(el);
    }
  };

  useEffect(() => {
    let arr = {};
    for (var i = 0; i < column; i++) {
      arr[i] = '';
    }

    setInputs(arr);
  }, []);

  const submitValue = lastValue => {
    let output = '';
    var keys = Object.keys(inputs);
    for (var i = 0; i < keys.length; i++) {
      output += inputs[keys[i]];
    }
    if (lastValue) output += lastValue;

    onSubmit(output);
  };

  function clearInput() {
    for (var i = 0; i < column; i++) {
      refs.current[i].clear();
    }
  }

  useImperativeHandle(ref, () => ({
    // methods connected to `ref`
    clearInput: () => {
      clearInput();
    },
  }));

  return (
    <View style={[styles.container, containerStyle]}>
      {Object.keys(inputs).map((item, index) => {
        let previous = index - 1;
        let next = index + 1;

        return (
          <TextInput
            key={index}
            ref={addToRefs}
            maxLength={1}
            style={[styles.inputText, inputStyle]}
            keyboardType="number-pad"
            selectTextOnFocus
            returnKeyType="next"
            onChangeText={text => {
              setInputs({...inputs, [index]: text});
              if (text || text !== '') {
                if (index === column - 1) {
                  // submitValue(text)
                  refs.current[index].blur();
                  submitValue(text);
                } else if (next === column) {
                } else {
                  refs.current[next].focus();
                }
              } else {
                if (previous >= 0) {
                  refs.current[previous].focus();
                }
              }
            }}
            onSubmitEditing={() => {
              if (next <= column - 2) {
                refs.current[next].focus();
              } else {
                submitValue();
              }
            }}
          />
        );
      })}
    </View>
  );
};

export default forwardRef(OtpInput);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 8,
  },
  inputText: {
    borderBottomColor: Colors.secondaryBase,
    borderBottomWidth: 1,
    width: 50,
    height: 56,
    borderRadius: 4,
    marginRight: 8,
    // fontFamily: 'Inter-Medium',
    fontSize: 25,
    textAlign: 'center',
    color: '#000',
  },
});
