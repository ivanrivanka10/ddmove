import React, { useState, useEffect, useCallback, useRef } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';
import Slideshow from 'react-native-image-slider-show';
import { Colors } from '@styles/index';

export const ImageSlider = props => {
    const { images, type, containerStyle, mainImageStyle, onPress } = props;
    
    const newImages = images.map(image => {
        return {
            url: image.filename
        }
    });

    const [index, setIndex] = useState(0);
    const [loadingImage, setLoadingImage] = useState(false);
    const imageListRef = useRef();

    const updateIndex = useCallback((idx) => {
        setIndex(idx);
        // setMainImage({uri: images[idx].filename});
    }, [index, setIndex])

    const onSwipeLeft = () => {
        let idx = index - 1;
        if(idx === -1){
            idx = images.length - 1;
        }
        if(type === 'indicatorImage'){
            if(imageListRef.current){
                if((idx + 1)/2 === 2 || idx === 0){
                    imageListRef.current.scrollToIndex({ index: idx })
                }
            }
        }
        updateIndex(idx);
    }

    const onSwipeRight = () => {
        let idx = index + 1;
        if(idx === images.length){
            idx = 0;
        }
        if(type === 'indicatorImage'){
            if(imageListRef.current){
                if((idx + 1)/2 === 2 || idx === 0){
                    imageListRef.current.scrollToIndex({ index: idx })
                }
            }
        }
        updateIndex(idx);
    }

    useEffect(() => {
        // if(type !== 'indicatorCircle'){
            if(!loadingImage){
                // setMainImage({uri: images[index].filename});
            }
            const interval = setInterval(() => {
                let idx = index + 1;
                if(idx === images.length){
                    idx = 0;
                }
                if(type === 'indicatorImage'){
                    if(imageListRef.current){
                        if((idx + 1)/2 === 2 || idx === 0){
                            imageListRef.current.scrollToIndex({ index: idx })
                        }
                    }
                }
                if(!loadingImage){
                    updateIndex(idx);
                }
            }, 3000);

            return () => clearInterval(interval);
        // }
    }, [index, loadingImage])

    return (
        <View style={[styles.container, containerStyle]}>
            <View style={[{width: '100%'}, mainImageStyle]}>
                <GestureRecognizer
                    style={[{width: '100%', height: 250}, mainImageStyle]}
                    onSwipeLeft={onSwipeRight}
                    onSwipeRight={onSwipeLeft}
                >
                    <Slideshow 
                        dataSource={newImages}
                        height={250}
                        position={index}
                        arrowSize={0}
                        indicatorSize={0}
                        scrollEnabled={false}
                        resizeMode='contain'
                    />
                    {/* <View style={[{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', position: 'absolute', top: (250 * 0.4)}, mainImageStyle && mainImageStyle.height && { top: mainImageStyle.height * 0.4 }]}>
                        <TouchableOpacity onPress={onSwipeLeft}>
                            <Entypo 
                                color="#FFF"
                                name="chevron-left"
                                size={50}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={onSwipeRight}>
                            <Entypo 
                                color="#FFF"
                                name="chevron-right"
                                size={50}
                            />
                        </TouchableOpacity>
                    </View> */}
                </GestureRecognizer>
                <View style={{width: '100%', backgroundColor: 'rgba(0, 0, 0, 0.01)', justifyContent: 'center', marginVertical: 10}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', alignSelf: 'center', justifyContent: 'space-around', width: images.length * 16 }}>
                        {images.map((image, idx) => (
                            <TouchableOpacity
                                key={idx.toString()}
                                onPress={() => {
                                    // if(imageListRef.current){
                                    //     imageListRef.current.snapToItem(idx);
                                    // }
                                    updateIndex(idx);
                                }}
                            >
                                <View style={{backgroundColor: 'white', height: 8, width: 8, borderRadius: 20, alignItems: 'center', justifyContent: 'center'}}>
                                    <View style={{backgroundColor: index === idx? Colors.primary : '#E3E5E5', height: 8, width: index === idx? 8 : 8, borderRadius: 16}}/>
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({

})