import React from 'react'
import {
    Modal,
    View,
    StyleSheet,
    TextInput,
    Image,
    TouchableOpacity
} from 'react-native'
import {Colors, Size} from '@styles/index'
import {InputText, TextBold, TextRegular} from '@components/global'
import {IconAsset} from '@utils/index'

const ModalInputResi = ({
    show,
    onClose,
    resi,
    setResi,
    onConfirm
}) => {
    return(
        <Modal
            transparent
            visible={show}
            onRequestClose={onClose}
        >
            <View style={styles.container}>
                <View style={styles.content}>
                    <View style={styles.header}>
                        <TextBold
                            text="Masukkan Nomor Resi"
                            size={16}
                            color={Colors.blackBase}
                        />
                        <TouchableOpacity
                            style={{padding: 10}}
                            onPress={onClose}
                        >
                            <Image
                                source={IconAsset.IcClose}
                                style={{width: 20, height: 20}}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        width: '100%',
                        paddingHorizontal: 15
                    }}>
                        <TextRegular
                            text="Masukkan Nomor Resi pengiriman kepada pembeli"
                            color={Colors.blackBase}
                        />
                        <InputText
                        containerStyle={{ marginHorizontal: Size.scaleSize(1) ,marginTop:10}}
                        placeholder="892038xxx"
                        value={resi}
                        onChangeText={(text) => setResi(text)}
                        maxLength={30} />

                        <TouchableOpacity
                            style={[styles.btnSubmit, resi === '' && {backgroundColor: Colors.greyDummy}]}
                            onPress={onConfirm}
                            disabled={resi === ''}
                        >
                            <TextBold
                                text="Submit"
                                size={16}
                                color="#fff"
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    btnSubmit: {
        width: '100%',
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 15,
        borderRadius: 10,
        backgroundColor: Colors.primary
    },
    txtInput: {
        marginTop: 15,
        width: '100%',
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 6
    },
    header: {
        width: '100%',
        flexDirection: 'row',
        alignItems : 'center',
        justifyContent: 'space-between',
        padding: 15
    },
    content: {
        width: '100%',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        paddingBottom: 10,
        backgroundColor: '#fff'
    },
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        backgroundColor: 'rgba(0,0,0,0.4)'
    }
})

export default ModalInputResi;