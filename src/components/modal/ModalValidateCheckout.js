import React from 'react'
import {
    Modal,
    View,
    Image,
    StyleSheet
} from 'react-native'
import {TextRegular} from '@components/global'
import {IconAsset} from '@utils/index'
import {Colors} from '@styles/index'

const ModalValidateCheckout = ({
    show,
    onClose,
    message
}) => {

    React.useEffect(() => {
        setTimeout(() => {
            onClose()
        }, 2000);
    },[])

    return(
        <Modal
            transparent
            visible={show}
            onRequestClose={onClose}
        >
            <View style={styles.container}>
                <View style={styles.conten}>
                    <Image
                        source={IconAsset.InfoCircle}
                        style={{width: 80, height: 80, resizeMode: 'contain'}}
                    />
                    <TextRegular
                        // text="Anda hanya dapat checkout satu jenis produk yang sama, Bahan Bangunan atau Non Bahan Bangunan Saja."
                        text={`${message}`}
                        size={16}
                        color="#fff"
                        style={{
                            marginTop: 24,
                            textAlign: 'center'
                        }}
                        numberOfLines={10}
                    />
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    conten: {
        width: '90%',
        borderRadius: 16,
        paddingHorizontal: 18,
        paddingVertical: 40,
        backgroundColor: Colors.secondaryBase,
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)'
    }
})

export default ModalValidateCheckout