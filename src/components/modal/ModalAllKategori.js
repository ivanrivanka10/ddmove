import React from 'react';
import {
  Modal,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Text,
  Platform,
  KeyboardAvoidingView,
  StyleSheet,
} from 'react-native';
import {
  HeaderTitle,
  SelectDropDown,
  TextBold,
  Button,
  TextRegular,
  TextSemiBold,
} from '@components/global';
import {Colors, Fonts, Size} from '@styles/index';
import {IconAsset} from '@utils/index';
import {FlatList} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {debugConsoleLog, showToastMessage} from '@helpers/functionFormat';

const MoalAllKategori = ({
  show,
  onClose,
  setPickedCategory,
  setPickedSubCategory,
  setPickedSubSubCategory,
  getSubCategory,
  category,
  pickedCategory,
  setselected,
  setRekomendasi,
  pickedSubSubCategory,
  getSubSubCategory,
  tipematerial,
  listSubCategory,
  listSubSubCategory,
  pickedSubCategory,
}) => {
  debugConsoleLog('daataki', pickedSubCategory);
  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <KeyboardAvoidingView
        enabled
        behavior={Platform.OS == 'ios' ? 'height' : null}
        keyboardVerticalOffset={Platform.OS === 'android' ? 0 : 0}
        style={styles.container}>
        <HeaderTitle title="Kategori lainnya" back={() =>{
           setPickedCategory([]);
           setPickedSubCategory([]);
           setPickedSubSubCategory([]);
           onClose()
        }} />
        <View style={styles.maincontainer}>
          <View
            style={{
              marginTop: 20,
              flexDirection: 'row',
              alignItems: 'center',
              marginHorizontal: 15,
            }}>
            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
              Kategori Utama <Text style={{color: Colors.primary}}>*</Text>
            </Text>
          </View>
          <SelectDropDown
            searchable
            onSelectItem={val => {
              var arr = [];
              arr.push(val);
              debugConsoleLog('xxaadta', arr);
              setPickedCategory(arr);
              setPickedSubCategory([]);
              setPickedSubSubCategory([]);
              getSubCategory(val.id);
            }}
            valueColor="#000"
            colorArrowIcon="#000"
            style={{
              marginTop: 10,
              width: '93%',
              alignSelf: 'center',
              borderRadius: 6,
            }}
            placeholderText="Pilih Kategori Produk"
            items={category}
            maxHeight={300}
            typeValueText="Medium"
          />
          <FlatList
            data={pickedCategory}
            contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap'}}
            keyExtractor={(_item, index) => index.toString()}
            renderItem={({item, index}) => {
              return (
                <TextSemiBold
                  text={item.name}
                  style={styles.catPicked}
                  color="#fff"
                />
              );
            }}
          />
         
            <>
              <View
                style={{
                  marginTop: 20,
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginHorizontal: 15,
                }}>
                <Text
                  style={{
                    fontFamily: Fonts.mulishExtraBold,
                    color: '#333',
                  }}>
                  Sub Kategori <Text style={{color: Colors.primary}}>*</Text>
                </Text>
              </View>
              <SelectDropDown
                searchable
                onSelectItem={val => {
                  var arr = [];
                  arr.push(val);
                  setPickedSubCategory(arr);
                  setPickedSubSubCategory([]);
                  getSubSubCategory(val.id);
                }}
                valueColor="#000"
                colorArrowIcon="#000"
                style={{
                  marginTop: 10,
                  width: '93%',
                  alignSelf: 'center',
                  borderRadius: 6,
                }}
                placeholderText="Pilih Sub Kategori Produk"
                items={listSubCategory}
                maxHeight={300}
                typeValueText="Medium"
              />
              <FlatList
                data={pickedSubCategory}
                contentContainerStyle={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                }}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, index}) => {
                  debugConsoleLog('ajaib', item.name);
                  return (
                    <TextSemiBold
                      text={item.name}
                      style={styles.catPicked}
                      color="#fff"
                    />
                  );
                }}
              />
              <View
                style={{
                  marginTop: 20,
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginHorizontal: 15,
                }}>
                <Text
                  style={{
                    fontFamily: Fonts.mulishExtraBold,
                    color: '#333',
                  }}>
                  Sub-Sub Kategori{' '}
                  <Text style={{color: Colors.primary}}>*</Text>
                </Text>
              </View>
              <SelectDropDown
                searchable
                onSelectItem={val => {
                  var arr = [];
                  arr.push(val);
                  setPickedSubSubCategory(arr);
                }}
                valueColor="#000"
                colorArrowIcon="#000"
                style={{
                  marginTop: 10,
                  width: '93%',
                  alignSelf: 'center',
                  borderRadius: 6,
                }}
                placeholderText="Pilih Sub Kategori Produk"
                items={listSubSubCategory}
                maxHeight={300}
                typeValueText="Medium"
              />
              <FlatList
                data={pickedSubSubCategory}
                contentContainerStyle={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                }}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, index}) => {
                  return (
                    <TextSemiBold
                      text={item.name}
                      style={styles.catPicked}
                      color="#fff"
                    />
                  );
                }}
              />
            </>
         
          <View style={{marginVertical:Size.scaleSize(10)}}/>
          <Button
            containerStyle={{width: '100%'}}
            style={{
              borderColor:Colors.borderGrey ,
              borderWidth: 2,
              marginHorizontal:Size.scaleSize(10),
              backgroundColor: Colors.primary,
              alignItems:'center',
              alignSelf:'center',
              justifyContent:'center',
            }}
            title={'Simpan'}
            onPress={() => {
              if(pickedCategory.length === 0){
                showToastMessage('Harap Pilih Kategori Anda')
              }else if(pickedSubCategory.length === 0){
                showToastMessage('Harap Pilih Sub Kategori Anda')
              }else if (pickedSubSubCategory.length === 0){
                showToastMessage('Harap Pilih Sub-Sub Kategori Anda')
              }else{
                const dataa = `${pickedCategory[0].id}, ${pickedSubCategory[0].id}, ${pickedSubSubCategory[0].id}`
                const dataadi = `${pickedCategory[0].name}/${pickedSubCategory[0].name}/${pickedSubSubCategory[0].name}`
                const paarams ={
                  id:dataa,
                  group:tipematerial,
                  name:dataadi,
                }
                setRekomendasi([paarams])
                setselected(dataa)
                onClose();
              }
            }}
          />
         <View style={{marginVertical:Size.scaleSize(5)}}/>
        </View>
      </KeyboardAvoidingView>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  maincontainer: {
    marginHorizontal: Size.scaleSize(4),
    marginTop: Size.scaleSize(4),
  },
  txtInput: {
    width: '100%',
    height: 120,
    marginTop: 20,
    borderRadius: 6,
    borderColor: Colors.border,
    borderWidth: 1,
    textAlignVertical: 'top',
    padding: 10,
  },
  btnSend: {
    marginTop: 20,
    width: '100%',
    borderRadius: 24,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
  },
  content: {
    width: '100%',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: '#fff',
    padding: 15,
  },
  catPicked: {
    marginLeft: 16,
    marginTop: 10,
    padding: 7,
    borderRadius: 4,
    backgroundColor: Colors.secondary,
  },
});

export default MoalAllKategori;
