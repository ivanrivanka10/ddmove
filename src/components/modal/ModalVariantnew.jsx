import React from 'react';
import {
  Modal,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Text,
  Platform,
  KeyboardAvoidingView,
  StyleSheet,
  Switch,
  ScrollView,
  PermissionsAndroid,
  BackHandler,
} from 'react-native';
import {
  HeaderTitle,
  SelectDropDown,
  TextBold,
  Button,
  TextRegular,
  TextSemiBold,
  InputText,
  Icon,
} from '@components/global';
import {Colors, Fonts, Icons, Size} from '@styles/index';
import {IconAsset} from '@utils/index';
import {FlatList} from 'react-native-gesture-handler';
import {
  debugConsoleLog,
  inputRupiah,
  showToastMessage,
} from '@helpers/functionFormat';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import APISELLER from '@utils/services/seller/orderProvider';

import {useState} from 'react';
import {set} from 'react-native-reanimated';
import {useCallback} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import {useFocusEffect} from '@react-navigation/native';
import ModalPickImage from './ModalPickImage';

const Modalvariaantnew = ({
  show,
  onClose,
  datasubsubs,
  datasubs,
  idvariant,
  isvarian,
}) => {
  const [data, setData] = useState([]);
  const {userData} = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const [datasub, setDatasub] = useState([]);
  const [target, settarger] = useState();
  const [startPrice, setStartPrice] = useState('');
  const [showModalOption, setShowModalOption] = useState(false);
  const [datasubadd, setdatasubadd] = useState({
    target: '',
    name: '',
    photo: '',
    stok: '',
    price: '',
    multiprice: false,
    isedit: false,
  });
  const [datammultiprice, setDatamultiprice] = useState([]);
  const [titleWholePrice, setTitleWholePrice] = useState('');
  const [showmodallvarian, setmodal] = useState(false);
  const [showmmodaaldetai, setModaldetail] = useState(false);
  const [showmo, setmo] = useState(false);
  const isSubvarianEmpty = datasub.every(item => item.subvarian?.length !== 0);

  const dataisi = useCallback(() => {
    if (datasubs.length !== 0) {
      debugConsoleLog('dtxxxx', datasubs);
      debugConsoleLog('1update',idvariant)
      setData(datasubs);
      setDatasub(datasubs);
    }
  }, [datasubs]);
  useFocusEffect(dataisi);
  const updateproduk = async (dattaa, stat)=> {
    debugConsoleLog('1update',dattaa)
    debugConsoleLog('1update',stat)
    debugConsoleLog('1update',idvariant)
    if (stat === 'no') {
      dispatch({type: 'SHOW_LOADING'});
      const dat = dattaa;
      const tdas = new FormData();
      const photo = {
        type: 'image/jpg',
        uri: dat.photo,
        name: `Variant ${dat.namavarian}`,
      };
      debugConsoleLog('dassss', photo.uri);
      tdas.append('v_name', dat.namavarian);
      tdas.append('sv_name', dat.namasubvarian);
      if (photo.uri !== '') {
        tdas.append('sv_photo', photo);
      }
      tdas.append('sv_price', dat.price);
      tdas.append('sv_multi_price', 'no');
      tdas.append('sv_stock', dat.stok);
      const res = await APISELLER.AddVariantsUpdate(tdas, idvariant, userData.token);
      dispatch({type: 'HIDE_LOADING'});
      if (res.data.message === 'Sukses') {
        const updatedData = data.map(item =>
          item.name === dat.namavarian
            ? {...item, subvarian: [...item.subvarian, dat]}
            : item,
        );

        // Log sebelum dan setelah pembaruan
        console.log('Data sebelum pembaruan:', data);
        console.log('Data sebelum pembaruan:', dat);

        setData(updatedData);

        // Update array kedua
        const updatedDatasub = datasub.map(item =>
          item.name === dat.namavarian
            ? {...item, subvarian: [...item.subvarian, dat]}
            : item,
        );
        // Log sebelum dan setelah pembaruan
        console.log('Datasub sebelum pembaruan:', datasub[0].subvarian);
        console.log('Datasub setelah pembaruan:', updatedDatasub[0].subvarian);
        console.log('Inputaan', dat);

        setDatasub(updatedDatasub);

        setdatasubadd(prevState => ({...prevState, name: ''}));
        setdatasubadd(prevState => ({...prevState, stok: ''}));
        setdatasubadd(prevState => ({...prevState, photo: ''}));
        setdatasubadd(prevState => ({...prevState, target: ''}));
        setdatasubadd(prevState => ({...prevState, price: ''}));
        setdatasubadd(prevState => ({
          ...prevState,
          multiprice: false,
        }));
        setDatamultiprice([]);
        setmo(false);
      }
    } else {
      dispatch({type: 'SHOW_LOADING'});
      const tdas = new FormData();
      const dat = dattaa;
      const photo = {
        type: 'image/jpg',
        uri: dat.photo,
        name: `Variant ${dat.name}`,
      };
      tdas.append('v_name', dat.namavarian);
      tdas.append('sv_name', dat.namasubvarian);
      tdas.append('sv_photo', photo);
      tdas.append('sv_price', dat.price);
      tdas.append('sv_multi_price', 'yes');
      tdas.append('sv_range_prices', JSON.stringify(datammultiprice));
      tdas.append('sv_stock', dat.stok);
      const res = await APISELLER.AddVariantsUpdate(tdas, idvariant, userData.token);
      dispatch({type: 'HIDE_LOADING'});
      if (res.data.message === 'Sukses') {
        const updatedData = data.map(item =>
          item.name === dat.namavarian
            ? {...item, subvarian: [...item.subvarian, dat]}
            : item,
        );

        // Log sebelum dan setelah pembaruan
        console.log('Data sebelum pembaruan:', data);
        console.log('Data sebelum pembaruan:', dat);

        setData(updatedData);

        // Update array kedua
        const updatedDatasub = datasub.map(item =>
          item.name === dat.namavarian
            ? {...item, subvarian: [...item.subvarian, dat]}
            : item,
        );
        // Log sebelum dan setelah pembaruan
        console.log('Datasub sebelum pembaruan:', datasub[0].subvarian);
        console.log('Datasub setelah pembaruan:', updatedDatasub[0].subvarian);
        console.log('Inputaan', dat);

        setDatasub(updatedDatasub);

        setdatasubadd(prevState => ({...prevState, name: ''}));
        setdatasubadd(prevState => ({...prevState, stok: ''}));
        setdatasubadd(prevState => ({...prevState, photo: ''}));
        setdatasubadd(prevState => ({...prevState, target: ''}));
        setdatasubadd(prevState => ({...prevState, price: ''}));
        setdatasubadd(prevState => ({
          ...prevState,
          multiprice: false,
        }));
        setDatamultiprice([]);
        setmo(false);
      }
    }
  };
  const addvariant = async (dattaa, stat) => {
    if (stat === 'no') {
      dispatch({type: 'SHOW_LOADING'});
      const dat = dattaa;
      const tdas = new FormData();
      const photo = {
        type: 'image/jpg',
        uri: dat.photo,
        name: `Variant ${dat.namavarian}`,
      };
      debugConsoleLog('dassss', photo.uri);
      tdas.append('v_name', dat.namavarian);
      tdas.append('sv_name', dat.namasubvarian);
      if (photo.uri !== '') {
        tdas.append('sv_photo', photo);
      }
      tdas.append('sv_price', dat.price);
      tdas.append('sv_multi_price', 'no');
      tdas.append('sv_stock', dat.stok);
      const res = await APISELLER.AddVariants(tdas, idvariant, userData.token);
      dispatch({type: 'HIDE_LOADING'});
      if (res.data.message === 'Sukses') {
        const updatedData = data.map(item =>
          item.name === dat.namavarian
            ? {...item, subvarian: [...item.subvarian, dat]}
            : item,
        );

        // Log sebelum dan setelah pembaruan
        console.log('Data sebelum pembaruan:', data);
        console.log('Data sebelum pembaruan:', dat);

        setData(updatedData);

        // Update array kedua
        const updatedDatasub = datasub.map(item =>
          item.name === dat.namavarian
            ? {...item, subvarian: [...item.subvarian, dat]}
            : item,
        );
        // Log sebelum dan setelah pembaruan
        console.log('Datasub sebelum pembaruan:', datasub[0].subvarian);
        console.log('Datasub setelah pembaruan:', updatedDatasub[0].subvarian);
        console.log('Inputaan', dat);

        setDatasub(updatedDatasub);

        setdatasubadd(prevState => ({...prevState, name: ''}));
        setdatasubadd(prevState => ({...prevState, stok: ''}));
        setdatasubadd(prevState => ({...prevState, photo: ''}));
        setdatasubadd(prevState => ({...prevState, target: ''}));
        setdatasubadd(prevState => ({...prevState, price: ''}));
        setdatasubadd(prevState => ({
          ...prevState,
          multiprice: false,
        }));
        setDatamultiprice([]);
        setmo(false);
      }
    } else {
      dispatch({type: 'SHOW_LOADING'});
      const tdas = new FormData();
      const dat = dattaa;
      const photo = {
        type: 'image/jpg',
        uri: dat.photo,
        name: `Variant ${dat.name}`,
      };
      tdas.append('v_name', dat.namavarian);
      tdas.append('sv_name', dat.namasubvarian);
      tdas.append('sv_photo', photo);
      tdas.append('sv_price', dat.price);
      tdas.append('sv_multi_price', 'yes');
      tdas.append('sv_range_prices', JSON.stringify(datammultiprice));
      tdas.append('sv_stock', dat.stok);
      const res = await APISELLER.AddVariants(tdas, idvariant, userData.token);
      dispatch({type: 'HIDE_LOADING'});
      if (res.data.message === 'Sukses') {
        const updatedData = data.map(item =>
          item.name === dat.namavarian
            ? {...item, subvarian: [...item.subvarian, dat]}
            : item,
        );

        // Log sebelum dan setelah pembaruan
        console.log('Data sebelum pembaruan:', data);
        console.log('Data sebelum pembaruan:', dat);

        setData(updatedData);

        // Update array kedua
        const updatedDatasub = datasub.map(item =>
          item.name === dat.namavarian
            ? {...item, subvarian: [...item.subvarian, dat]}
            : item,
        );
        // Log sebelum dan setelah pembaruan
        console.log('Datasub sebelum pembaruan:', datasub[0].subvarian);
        console.log('Datasub setelah pembaruan:', updatedDatasub[0].subvarian);
        console.log('Inputaan', dat);

        setDatasub(updatedDatasub);

        setdatasubadd(prevState => ({...prevState, name: ''}));
        setdatasubadd(prevState => ({...prevState, stok: ''}));
        setdatasubadd(prevState => ({...prevState, photo: ''}));
        setdatasubadd(prevState => ({...prevState, target: ''}));
        setdatasubadd(prevState => ({...prevState, price: ''}));
        setdatasubadd(prevState => ({
          ...prevState,
          multiprice: false,
        }));
        setDatamultiprice([]);
        setmo(false);
      }
    }
  };
  const handleBackButton = async () => {
    dispatch({type: 'SHOW_LOADING'});
    // Tambahkan logika atau perpindahan sesuai kebutuhan
    const res = await APISELLER.GetVarints(idvariant, userData.token);
    dispatch({type: 'HIDE_LOADING'});
    if (res.data.message === 'Gagal') {
      isvarian(false);
      onClose();
    } else {
      onClose();
    }
    // Jika Anda ingin mencegah modal ditutup, kembalikan true
    return true;
  };
  const back = useCallback(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
    };
  }, []);
  useFocusEffect(back);
  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Persetujuan akses kamera',
            message: 'Berikan Baba Market untuk mengakses kamera ?',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else {
      return true;
    }
  };

  const response = async () => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await APISELLER.GetVarints(idvariant, userData.token);
    debugConsoleLog('dat', res.data);
    dispatch({type: 'HIDE_LOADING'});
    if (res.data.message === 'Gagal' || res.data.data.have_variant === 'no') {
      isvarian(false);
      onClose();
    } else {
      onClose();
    }
  };
  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Persetujuan akses file',
            message: 'Berikan Baba Market untuk mengakses file ?',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else {
      return true;
    }
  };
  const captureImage = async type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      saveToPhotos: true,
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      setTimeout(() => {
        launchCamera(options, response => {
          if (response.didCancel) {
            // alert('User cancelled camera picker');
            return;
          } else if (response.errorCode == 'camera_unavailable') {
            // alert('Camera not available on device');
            return;
          } else if (response.errorCode == 'permission') {
            // alert('Permission not satisfied');
            return;
          } else if (response.errorCode == 'others') {
            // alert(response.errorMessage);
            return;
          }
          setdatasubadd(prevState => ({
            ...prevState,
            photo: response.assets[0].uri,
          }));
        });
      }, 500);
    }
  };

  const chooseFile = type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    setTimeout(() => {
      launchImageLibrary(options, response => {
        if (response.didCancel) {
          alert('User cancelled camera picker');
          return;
        } else if (response.errorCode == 'camera_unavailable') {
          alert('Camera not available on device');
          return;
        } else if (response.errorCode == 'permission') {
          alert('Permission not satisfied');
          return;
        } else if (response.errorCode == 'others') {
          alert(response.errorMessage);
          return;
        }
        setdatasubadd(prevState => ({
          ...prevState,
          photo: response.assets[0].uri,
        }));
      });
    }, 500);
  };
  const handleItemPress = (index, name) => {
    const clickedItem = data[index];
    const trueStatusObjects = data.filter(item => item.status === true);
    if (
      clickedItem &&
      clickedItem.name === name &&
      trueStatusObjects.length < 2
    ) {
      // Buat salinan objek dan ubah status menjadi true
      const updatedItem = {
        ...clickedItem,
        status: !clickedItem.status,
      };

      const updatedData = [...data];
      updatedData[index] = updatedItem;
      const trueStatusObjects = updatedData.filter(
        item => item.status === true,
      );

      setDatasub(trueStatusObjects);
      // Perbarui state dengan data yang sudah diperbarui
      setData(updatedData);
    } else {
      if (clickedItem.status === true) {
        const updatedItem = {
          ...clickedItem,
          status: !clickedItem.status,
        };

        // Salin array data, ganti objek "helmi" yang diubah dengan yang baru
        const updatedData = [...data];
        updatedData[index] = updatedItem;
        const trueStatusObjects = updatedData.filter(
          item => item.status === true,
        );

        setDatasub(trueStatusObjects);
        // Perbarui state dengan data yang sudah diperbarui
        setData(updatedData);
        setDatasub(trueStatusObjects);
      } else {
        showToastMessage('Hanya bisa memilih 2 tipe varian');
      }
    }
  };

  return (
    <Modal
      transparent
      visible={show}
      onRequestClose={() => {
        response();
      }}>
      <KeyboardAvoidingView
        enabled
        behavior={Platform.OS == 'ios' ? 'height' : null}
        keyboardVerticalOffset={Platform.OS === 'android' ? 0 : 0}
        style={styles.container}>
        <HeaderTitle
          title="Varian"
          back={() => {
            response();
          }}
        />
        <View style={styles.maincontainer}>
          <View
            style={{
              marginTop: 0,
              marginHorizontal: 15,
            }}>
            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
              Pilih Tipe Varian<Text style={{color: Colors.primary}}>*</Text>
            </Text>
            <Text
              style={{
                fontFamily: Fonts.mulishRegular,
                fontSize: Size.scaleFont(14),
                color: Colors.greyDummy,
              }}>
              Pilih Maksimum 2 Tipe Varian
            </Text>
          </View>
          <FlatList
            data={data}
            numColumns={4}
            renderItem={({item, index}) => {
              debugConsoleLog('dataaitem', item);
              return (
                <TouchableOpacity
                  onPress={() => {
                    handleItemPress(index, item.name);
                  }}
                  style={
                    item.status === true ? styles.catPicked : styles.catPicked2
                  }>
                  <Text
                    style={{
                      fontFamily: Fonts.mulishBold,
                      color: '#FFF',
                      fontSize: Size.scaleFont(12),
                    }}>
                    {item.name}
                  </Text>
                </TouchableOpacity>
              );
            }}
          />
          <View>
            <Button
              onPress={() => {
                setmodal(true);
              }}
              containerStyle={{
                marginTop: Size.scaleSize(10),
                marginHorizontal: Size.scaleSize(8),
                width: '35%',
              }}
              textComponent={
                <View
                  style={{
                    flexDirection: 'row',
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                  }}>
                  <Icons.PlusSVG size={15} color="#FFF" />
                  <Text
                    style={{
                      fontFamily: Fonts.mulishBold,
                      color: '#FFF',
                      fontSize: Size.scaleFont(10),
                      marginLeft: 10,
                      marginTop: 0,
                    }}>
                    Tambah Varian
                  </Text>
                </View>
              }
            />
            <View
              style={{
                width: '100%',
                height: Size.scaleSize(5),
                backgroundColor: Colors.borderGrey,
                marginVertical: Size.scaleSize(5),
              }}
            />
            <FlatList
              data={datasub}
              nestedScrollEnabled
              ItemSeparatorComponent={
                <View style={{margin: Size.scaleSize(5)}} />
              }
              style={{marginHorizontal: Size.scaleSize(10)}}
              renderItem={item => {
                debugConsoleLog('datups',item)
                return (
                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontFamily: Fonts.mulishBlack,
                          fontSize: Size.scaleFont(14),
                        }}>
                        {item.item.name}
                      </Text>
                      <TouchableOpacity
                        onPress={() => {
                          settarger(item.item.name);
                          setdatasubadd(prevState => ({...prevState, name: ''}));
                          setdatasubadd(prevState => ({...prevState, stok: ''}));
                          setdatasubadd(prevState => ({...prevState, photo: ''}));
                          setdatasubadd(prevState => ({...prevState, target: ''}));
                          setdatasubadd(prevState => ({...prevState, price: ''}));
                          setdatasubadd(prevState => ({...prevState, isedit: false}));
                          setdatasubadd(prevState => ({
                            ...prevState,
                            multiprice: false,
                          }));
                          setDatamultiprice([]);
                          setmo(true);
                        }}
                        style={{
                          backgroundColor: Colors.primary,
                          borderRadius: Size.scaleSize(10),
                          padding: Size.scaleSize(5),
                        }}>
                        <Icons.PlusSVG size={Size.scaleSize(14)} color="#FFF" />
                      </TouchableOpacity>
                    </View>
                    <FlatList
                     nestedScrollEnabled
                      data={item.item.subvarian}
                      horizontal
                      ItemSeparatorComponent={
                        <View style={{margin: Size.scaleSize(5)}} />
                      }
                      renderItem={datasubitme => {
                        debugConsoleLog('datasubss', datasubitme);
                        return (
                          <>
                            <TouchableOpacity
                              onPress={() => {
                                debugConsoleLog('asss', datasubitme.item);
                                settarger(datasubitme.item.namavarian);
                                setdatasubadd(prevState => ({
                                  ...prevState,
                                  name: datasubitme.item.namasubvarian,
                                  stok: '' + datasubitme.item.stok,
                                  target: '' + datasubitme.item.namavarian,
                                  photo: '' + datasubitme.item.photo,
                                  price: '' + datasubitme.item.price,
                                  multiprice:
                                    datasubitme.item.multiprice === 'no'
                                      ? false
                                      : true,
                                  isedit: true,
                                }));

                                if (datasubitme.item.multiprice === 'yes') {
                                  setDatamultiprice(datasubitme.item.range);
                                }
                                setmo(true);
                              }}
                              style={[
                                styles.catPicked,
                                {
                                  flexDirection: 'column',
                                  marginLeft: 0,
                                  padding: 0,
                                  flexGrow: 1,
                                  paddingTop: 0,
                                },
                              ]}>
                              {datasubitme.item.photo !== '' && (
                                <Image
                                  source={{uri: datasubitme.item.photo}}
                                  resizeMethod={'scale'}
                                  style={{
                                    marginBottom: Size.scaleSize(5),
                                    width: '100%',
                                    borderTopLeftRadius: Size.scaleSize(5),
                                    borderTopRightRadius: Size.scaleSize(5),
                                    overflow: 'hidden',
                                    height: Size.scaleSize(78),
                                    alignSelf: 'center',
                                  }}
                                />
                              )}
                              <Text
                                numberOfLines={3}
                                style={{
                                  fontFamily: Fonts.mulishBold,
                                  color: '#FFF',
                                  width: Size.scaleSize(78),
                                  marginTop:
                                    datasubitme.item.photo !== ''
                                      ? 0
                                      : Size.scaleSize(10),
                                  marginBottom: Size.scaleSize(5),
                                  textAlign: 'center',
                                  fontSize: Size.scaleFont(12),
                                }}>
                                {datasubitme.item.namasubvarian}
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              activeOpacity={0.8}
                              style={{
                                position: 'absolute',
                                right: 0,
                                borderRadius: 5,
                                top: 8,
                                alignContent: 'center',
                                padding: Size.scaleSize(3),
                                backgroundColor: Colors.primary,
                                alignSelf: 'center',
                              }}
                              onPress={() => {
                                const varianName = item.item.name;
                                const subvarianName =
                                  datasubitme.item.namasubvarian;
                                // Update datasub
                                const updatedDatasub = datasub.map(item =>
                                  item.name === varianName
                                    ? {
                                        ...item,
                                        subvarian: item.subvarian.filter(
                                          subItem =>
                                            subItem.namasubvarian !==
                                            subvarianName,
                                        ),
                                      }
                                    : item,
                                );
                                setDatasub(updatedDatasub);

                                // Update data
                                const updatedData = data.map(item =>
                                  item.name === varianName
                                    ? {
                                        ...item,
                                        subvarian: item.subvarian.filter(
                                          subItem =>
                                            subItem.namasubvarian !==
                                            subvarianName,
                                        ),
                                      }
                                    : item,
                                );
                                setData(updatedData);
                              }}>
                              <Image
                                source={IconAsset.IcClose}
                                style={{
                                  width: 15,
                                  height: 15,
                                  tintColor: '#FFf',
                                }}
                              />
                            </TouchableOpacity>
                          </>
                        );
                      }}
                    />
                  </View>
                );
              }}
            />
          </View>
          <Modal
            transparent
            visible={showmodallvarian}
            onRequestClose={() => {
              setmodal(false);
            }}>
            <View style={styles.containerModal}>
              <View
                style={{
                  backgroundColor: 'white',
                  borderTopStartRadius: 12,
                  borderTopEndRadius: 12,
                }}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    paddingTop: Size.scaleSize(10),
                    paddingHorizontal: Size.scaleSize(10),
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextBold
                    text="Masukkan Nama Varian"
                    color={Colors.blackBase}
                    size={16}
                  />
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => {
                      setmodal(false);
                    }}>
                    <Image
                      source={IconAsset.IcClose}
                      style={{width: 25, height: 25}}
                    />
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    marginTop: 15,
                    marginBottom: Size.scaleSize(5),
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginHorizontal: 15,
                  }}>
                  <Text
                    style={{
                      fontFamily: Fonts.mulishExtraBold,
                      color: '#333',
                    }}>
                    Nama Varian{' '}
                  </Text>
                </View>
                <InputText
                  containerStyle={{marginHorizontal: 15}}
                  placeholder="Masukan Nama varian"
                  value={datasubadd.name}
                  onChangeText={text =>
                    setdatasubadd(prevState => ({...prevState, name: text}))
                  }
                />
                <Button
                  onPress={() => {
                    if (datasubadd.name === '') {
                      showToastMessage('Nama varian tidak boleh kosong');
                    } else {
                      const newVariant = {
                        status: false,
                        name: datasubadd.name,
                        subvarian: [],
                      };

                      // Check if the name already exists in the data array
                      const nameExists = data.some(
                        item => item.name === datasubadd.name,
                      );

                      if (!nameExists) {
                        // If the name does not exist, add the new variant to the data array
                        setData(prevData => [...prevData, newVariant]);
                        setdatasubadd(prevState => ({...prevState, name: ''}));
                        setmodal(false);
                      } else {
                        // If the name already exists, show an error message or handle it accordingly
                        showToastMessage('Nama varian sudah ada');
                      }
                    }
                  }}
                  containerStyle={{
                    marginHorizontal: Size.scaleSize(10),
                    marginVertical: Size.scaleSize(10),
                  }}
                  title={'Tambah Varian'}
                />
              </View>
            </View>
          </Modal>
          <Modal
            transparent
            visible={showmo}
            onRequestClose={() => {
              setmo(false);
            }}>
            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{justifyContent: 'flex-end', flexGrow: 1}}>
              <View style={styles.containerModal}>
                <View
                  style={{
                    backgroundColor: 'white',
                    borderTopStartRadius: 12,
                    borderTopEndRadius: 12,
                  }}>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      paddingTop: Size.scaleSize(10),
                      paddingHorizontal: Size.scaleSize(10),
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <TextBold
                      text={`${target}\nTambah atau edit sub varian`}
                      color={Colors.blackBase}
                      size={16}
                    />
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => {
                        setmo(false);
                      }}>
                      <Image
                        source={IconAsset.IcClose}
                        style={{width: 25, height: 25}}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    {datasubadd.photo === '' ? (
                      <TouchableOpacity
                        onPress={() => setShowModalOption(true)}
                        style={{marginLeft: 15, marginTop: 15}}>
                        <View
                          style={{
                            borderColor: Colors.primary,
                            width: 78,
                            height: 78,
                            borderStyle: 'dashed',
                            backgroundColor: '#FFF2F3',
                            borderWidth: 2,
                            borderRadius: 10,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Icons.PlusSVG size={28} color={Colors.primary} />
                          <Text
                            style={{
                              fontFamily: Fonts.mulishRegular,
                              color: Colors.primary,
                            }}>
                            Foto
                          </Text>
                        </View>
                      </TouchableOpacity>
                    ) : (
                      <View style={{marginLeft: 15, marginTop: 15}}>
                        <View
                          style={{
                            borderColor: '#F2F4F5',
                            width: 78,
                            height: 78,
                            borderWidth: 2,
                            borderRadius: 10,
                            alignItems: 'center',
                            justifyContent: 'center',
                            overflow: 'hidden',
                          }}>
                          <Image
                            source={{uri: datasubadd.photo}}
                            style={{width: 78, height: 78}}
                          />
                          <View
                            style={{
                              backgroundColor: Colors.primaryDark,
                              width: 15,
                              height: 15,
                              position: 'absolute',
                              top: 2,
                              right: 2,
                              borderRadius: 15,
                            }}>
                            <TouchableOpacity
                              onPress={() =>
                                setdatasubadd(prevState => ({
                                  ...prevState,
                                  photo: '',
                                }))
                              }>
                              <Icons.MinusSVG color={'#FFF'} size={15} />
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    )}

                    <View style={{flex: 1}}>
                      <View
                        style={{
                          marginTop: 15,
                          marginBottom: Size.scaleSize(5),
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginHorizontal: 15,
                        }}>
                        <Text
                          style={{
                            fontFamily: Fonts.mulishExtraBold,
                            color: '#333',
                          }}>
                          Nama sub varian{' '}
                        </Text>
                      </View>
                      <InputText
                        containerStyle={{marginHorizontal: 15}}
                        placeholder="Masukan Nama Sub Variant"
                        value={datasubadd.name}
                        onChangeText={text => {
                          setdatasubadd(prevState => ({
                            ...prevState,
                            name: text,
                          }));
                        }}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      marginTop: 15,
                      marginBottom: Size.scaleSize(5),
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: '#333',
                      }}>
                      Masukan stok sub varian
                    </Text>
                  </View>
                  <InputText
                    containerStyle={{marginHorizontal: 15}}
                    placeholder="Masukan Nama Sub Variant"
                    value={datasubadd.stok}
                    keyboardType="number-pad"
                    onChangeText={text => {
                      setdatasubadd(prevState => ({...prevState, stok: text}));
                    }}
                  />
                  <View
                    style={{
                      marginTop: 15,
                      marginBottom: Size.scaleSize(5),
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: '#333',
                      }}>
                      Masukan harga sub varian
                    </Text>
                  </View>
                  <InputText
                    containerStyle={{marginHorizontal: 15}}
                    keyboardType="number-pad"
                    placeholder="Masukan Nama Sub Variant"
                    value={inputRupiah(datasubadd.price)}
                    onChangeText={text => {
                      setdatasubadd(prevState => ({...prevState, price: text}));
                    }}
                  />
                  <View
                    style={{
                      marginTop: 15,
                      marginBottom: Size.scaleSize(5),
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: '#333',
                      }}>
                      Harga Borongan
                    </Text>
                    <Switch
                      value={datasubadd.multiprice}
                      trackColor={{
                        false: Colors.borderGrey,
                        true: Colors.primaryDark,
                      }}
                      thumbColor={
                        datasubadd.multiprice === true
                          ? Colors.primary
                          : '#f4f3f4'
                      }
                      onValueChange={() => {
                        if (datasubadd.multiprice === true) {
                          setDatamultiprice([]);
                        }
                        setdatasubadd(prevState => ({
                          ...prevState,
                          multiprice: !datasubadd.multiprice,
                        }));
                      }}
                    />
                  </View>
                  {datasubadd.multiprice === true && (
                    <>
                      <View
                        style={{
                          marginTop: 20,
                          marginBottom: 10,
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginHorizontal: 15,
                        }}>
                        <Image
                          source={IconAsset.IcPriceTag}
                          style={{
                            width: 24,
                            height: 24,
                            resizeMode: 'contain',
                            marginRight: 10,
                          }}
                        />
                        <Text
                          style={{
                            fontFamily: Fonts.mulishExtraBold,
                            color: '#333',
                            fontSize: 12,
                          }}>
                          Harga Borongan (Hanya Jumlah Pembelian)
                        </Text>
                      </View>
                      <InputText
                        containerStyle={{marginHorizontal: 15}}
                        placeholder="cnth: 5"
                        value={titleWholePrice}
                        onChangeText={text => setTitleWholePrice(text)}
                      />
                      <View
                        style={{
                          width: '100%',
                          paddingHorizontal: 15,
                          justifyContent: 'space-between',
                          flexDirection: 'row',
                          marginTop: 15,
                        }}>
                        <InputText
                          containerStyle={{width: '70%'}}
                          placeholder="Masukkan Harga Borongan"
                          value={startPrice}
                          keyboardType="number-pad"
                          onChangeText={text =>
                            setStartPrice(inputRupiah(text))
                          }
                        />
                        <TouchableOpacity
                          style={styles.btnAddPrice}
                          onPress={() => {
                            if (titleWholePrice !== '' && startPrice !== '') {
                              var arr = [...datammultiprice];
                              arr.push({
                                qty: titleWholePrice,
                                price: startPrice.split('.').join(''),
                              });
                              setDatamultiprice(arr);
                              setTitleWholePrice('');
                              setStartPrice('');
                            } else {
                              showToastMessage(
                                'Harga dan jumlah borongan tidak boleh kosong',
                              );
                            }
                          }}>
                          <Image
                            source={IconAsset.IcPlus}
                            style={{width: 20, height: 20, tintColor: '#fff'}}
                          />
                        </TouchableOpacity>
                      </View>
                      <FlatList
                        data={datammultiprice}
                        contentContainerStyle={{
                          flexDirection: 'row',
                          flexWrap: 'wrap',
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item, index}) => {
                          return (
                            <View
                              style={[
                                styles.catPicked,
                                {
                                  flexDirection: 'row',
                                  alignItems: 'flex-start',
                                  justifyContent: 'space-between',
                                },
                              ]}>
                              <TextSemiBold
                                text={`${item.qty} - ${inputRupiah(
                                  item.price,
                                  'Rp',
                                )}`}
                                color="#fff"
                                style={{marginRight: 15}}
                              />
                              <TouchableOpacity
                                style={styles.btnDeleteItem}
                                onPress={() => {
                                  var arr = [...datammultiprice];
                                  arr.splice(index, 1);
                                  setDatamultiprice(arr);
                                }}>
                                <Image
                                  source={IconAsset.IcClose}
                                  style={{
                                    width: 10,
                                    height: 10,
                                    tintColor: '#fff',
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                          );
                        }}
                      />
                    </>
                  )}
                  <Button
                    onPress={() => {
                      if (datasubadd.name === '') {
                        showToastMessage('Nama sub varian tidak boleh kosong');
                      } else if (datasubadd.stok === '') {
                        showToastMessage('Stok sub varian tidak boleh kosong');
                      } else if (datasubadd.price === '') {
                        showToastMessage('Harga sub varian tidak boleh kosong');
                      } else if (
                        datasubadd.multiprice === true &&
                        datammultiprice.length === 0
                      ) {
                        showToastMessage('Harga borongan tidak boleh kosong');
                      } else if (datasubadd.isedit === true) {
                        let newData;
                        if (
                          datasubadd.multiprice === true &&
                          datammultiprice.length !== 0
                        ) {
                          newData = {
                            namavarian: target,
                            namasubvarian: datasubadd.name, // Ganti dengan nilai yang diinginkan
                            photo: datasubadd.photo, // Ganti dengan nilai yang diinginkan
                            price: datasubadd.price, // Ganti dengan nilai yang diinginkan
                            stok: datasubadd.stok, // Ganti dengan nilai yang diinginkan
                            multiprice: 'yes',
                            range: datammultiprice,
                          };
                          updateproduk(newData,'yes')
                        } else {
                          newData = {
                            namavarian: target,
                            namasubvarian: datasubadd.name, // Ganti dengan nilai yang diinginkan
                            photo: datasubadd.photo, // Ganti dengan nilai yang diinginkan
                            price: datasubadd.price, // Ganti dengan nilai yang diinginkan
                            stok: datasubadd.stok, // Ganti dengan nilai yang diinginkan
                            multiprice: 'no',
                          };
                          updateproduk(newData,'no')

                        }
                      } else {
                        let newData;
                        if (
                          datasubadd.multiprice === true &&
                          datammultiprice.length !== 0
                        ) {
                          newData = {
                            namavarian: target,
                            namasubvarian: datasubadd.name, // Ganti dengan nilai yang diinginkan
                            photo: datasubadd.photo, // Ganti dengan nilai yang diinginkan
                            price: datasubadd.price, // Ganti dengan nilai yang diinginkan
                            stok: datasubadd.stok, // Ganti dengan nilai yang diinginkan
                            multiprice: 'yes',
                            range: datammultiprice,
                          };
                          addvariant(newData, 'yes');
                          settarger('');
                        } else {
                          newData = {
                            namavarian: target,
                            namasubvarian: datasubadd.name, // Ganti dengan nilai yang diinginkan
                            photo: datasubadd.photo, // Ganti dengan nilai yang diinginkan
                            price: datasubadd.price, // Ganti dengan nilai yang diinginkan
                            stok: datasubadd.stok, // Ganti dengan nilai yang diinginkan
                            multiprice: 'no',
                          };
                          addvariant(newData, 'no');
                          settarger('');
                        }
                      }
                    }}
                    containerStyle={{
                      marginHorizontal: Size.scaleSize(10),
                      marginVertical: Size.scaleSize(10),
                    }}
                    title={'Simpan Varian'}
                  />
                </View>
              </View>
            </ScrollView>
          </Modal>
        </View>
      </KeyboardAvoidingView>
      <ModalPickImage
        show={showModalOption}
        onClose={() => setShowModalOption(false)}
        onPressCamera={() => {
          setShowModalOption(false);
          captureImage('photo');
        }}
        onPressGalery={() => {
          setShowModalOption(false);
          chooseFile('photo');
        }}
      />
      <View
        style={{
          position: 'absolute',
          width: '90%',
          bottom: 0,
          justifyContent: 'center',
          alignContent: 'center',
          alignSelf: 'center',
        }}>
        <Button
          disable={isSubvarianEmpty}
          onPress={() => {
            if (isSubvarianEmpty === false) {
            } else {
              datasubsubs(datasub);
              setDatasub([]);
              setData([]);
              onClose();
            }
          }}
          style={{
            backgroundColor:
              isSubvarianEmpty === false ? Colors.grey_3 : Colors.primary,
          }}
          title={'Simpan'}
        />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  maincontainer: {
    marginHorizontal: Size.scaleSize(8),
    marginTop: Size.scaleSize(4),
  },
  txtInput: {
    width: '100%',
    height: 120,
    marginTop: 20,
    borderRadius: 6,
    borderColor: Colors.border,
    borderWidth: 1,
    textAlignVertical: 'top',
    padding: 10,
  },
  btnSend: {
    marginTop: 20,
    width: '100%',
    borderRadius: 24,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
  },
  content: {
    width: '100%',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: '#fff',
    padding: 15,
  },
  catPicked: {
    marginLeft: 16,
    marginTop: 10,
    padding: 7,
    borderRadius: 4,
    backgroundColor: Colors.secondary,
  },
  catPicked2: {
    marginLeft: 16,
    marginTop: 10,
    padding: 7,
    borderRadius: 4,
    backgroundColor: Colors.grey_3,
  },
  btnAddPrice: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 15,
    backgroundColor: Colors.primary,
    borderRadius: 6,
  },
  containerModal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
  },
});

export default Modalvariaantnew;
