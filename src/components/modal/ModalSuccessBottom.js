import React from 'react'
import {
    Modal,
    View,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native'
import {
    TextSemiBold,
    TextRegular,
    TextBold
} from '@components/global'
import {IconAsset} from '@utils/index'
import Colors from '@styles/Colors'

const ModalSuccessBottom = ({
    show,
    message,
    subMessage,
    onClose,
    onPressAgree
}) => {
    return(
        <Modal
            transparent
            visible={show}
            onRequestClose={onClose}
        >
            <View style={styles.container}>
                <View style={{
                    width: '100%',
                    paddingVertical: 40,
                    backgroundColor: '#fff',
                    borderTopLeftRadius: 30,
                    borderTopRightRadius: 30
                }}>
                    <View style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',                    
                    }}>
                        <Image
                            source={IconAsset.IconDone}
                            style={{
                                width: 100,
                                height: 100,
                                resizeMode: 'contain'
                            }}
                        />
                        <TextSemiBold
                            text={message}
                            color={Colors.blackBase}
                            style={{
                                maxWidth: '60%',
                                textAlign: 'center',
                                marginTop: 25
                            }}
                        />
                        <TextRegular
                            text={subMessage}
                            color="#4F4F4F"
                            style={{
                                maxWidth: '70%',
                                textAlign: 'center',
                                marginTop: 23
                            }}
                            numberOfLines={10}
                        />                        
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => onPressAgree()}
                        style={{
                            marginTop: 35,
                            width: '90%',
                            alignSelf: 'center',
                            paddingVertical: 17,
                            borderRadius: 16,
                            backgroundColor: Colors.primary,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <TextBold
                            text="Saya Mengerti"
                            color="#fff"
                            size={16}
                        />
                    </TouchableOpacity>
                </View>                
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        backgroundColor: 'rgba(0,0,0,0.4)'
    }
})

export default ModalSuccessBottom