import React from 'react';
import {
  Modal,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  StyleSheet,
} from 'react-native';
import {TextSemiBold, TextRegular, TextBold} from '@components/global';
import {Colors} from '@styles/index';
import {IconAsset} from '@utils/index';

const ModalChooseLocation = ({show, onClose, data, onPick}) => {
  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.header}>
            <TextBold
              text="Pilih Lokasi Anda"
              size={16}
              color={Colors.blackBase}
            />
            <TouchableOpacity onPress={onClose}>
              <Image
                source={IconAsset.IcClose}
                style={{width: 20, height: 20, tintColor: Colors.blackBase}}
              />
            </TouchableOpacity>
          </View>
          <FlatList
            data={data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <TouchableOpacity
                key={index.toString()}
                style={{
                  padding: 15,
                  marginTop: 15,
                  backgroundColor:
                    item.is_main === 'yes' ? Colors.secondaryLighttest : '#fff',
                }}
                onPress={() => onPick(item)}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignContent: 'center',
                    alignItems: 'center',
                    marginBottom: 5,
                  }}>
                  <TextBold text={item.name} />
                  {item.is_main == 'yes' ? (
                    <View
                      style={{
                        marginLeft: 5,
                        paddingVertical: 2,
                        paddingHorizontal: 5,
                        backgroundColor: Colors.greyDummy,
                        borderRadius: 5,
                      }}>
                      <TextBold
                        text={'Utama'}
                        size={10}
                        color={Colors.blackBase}
                      />
                    </View>
                  ) : null}
                </View>
                <TextSemiBold
                  text={`${item.label} (${item.phone})`}
                  color={Colors.blackBase}
                />
                <TextRegular
                  text={item.address}
                  color={Colors.grey_1}
                  style={{marginTop: 4}}
                  size={12}
                />
              </TouchableOpacity>
            )}
            ItemSeparatorComponent={() => (
              <View
                style={{
                  width: '100%',
                  height: 0,
                  backgroundColor: Colors.grey_3,
                }}
              />
            )}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  content: {
    width: '100%',
    maxHeight: '50%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    paddingVertical: 15,
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
});

export default ModalChooseLocation;
