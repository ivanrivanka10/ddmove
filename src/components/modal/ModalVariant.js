import React from 'react';
import {
  Modal,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Text,
  Platform,
  KeyboardAvoidingView,
  StyleSheet,
  PermissionsAndroid,
} from 'react-native';
import {
  HeaderTitle,
  SelectDropDown,
  TextBold,
  Button,
  TextRegular,
  TextSemiBold,
  InputText,
  Icon,
} from '@components/global';
import {Colors, Fonts, Icons, Size} from '@styles/index';
import {IconAsset} from '@utils/index';
import {FlatList} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {debugConsoleLog, showToastMessage} from '@helpers/functionFormat';
import {useState} from 'react';
import {debug, set} from 'react-native-reanimated';
import {useCallback} from 'react';
import APISELLER from '@utils/services/seller/orderProvider';
import {useFocusEffect} from '@react-navigation/native';
import normalize from 'react-native-normalize';
import {useReducer} from 'react';
import {Nameuser} from '@utils/reducers/InputPorduct/Nameuserreducer';
import {
  inputVarianState,
  VariantReducer,
} from '@utils/reducers/InputPorduct/variantReducer';
import {assets} from 'react-native.config';
import ModalPickImage from './ModalPickImage';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ModalCombination from './ModalCombination';

const ModalVariants = ({
  show,
  onClose,
  datasubsubs,
  datasubs,
  isvarian,
  idvarint,
  onupdate,
  isupdate,
}) => {
  const dispatch = useDispatch();
  const {userData} = useSelector(state => state.auth);
  const [state, reducer] = useReducer(VariantReducer, inputVarianState);
  const [filteredData, setFilteredData] = useState([]);
  const [old, setold] = useState([]);
  const [showModalOption, setShowModalOption] = useState(false);
  const [showModalacc, setshowmodalacc] = useState(false);
  const [indeximage, setindeximage] = useState();
  const [idvaarint, setidvariant] = useState();
  const [datasubadd, setdatasubadd] = useState({
    target: '',
    name: '',
    photo: '',
    stok: '',
    price: '',
    multiprice: false,
    isedit: false,
  });
  const focus = useCallback(() => {
    if (datasubs.length !== 0) {
      const transformedArray = datasubs.map(item => {
        return {
          id: item.id,
          isclick: item.is_selected === 1 ? true : false,
          name: item.name,
          subvariatn: item.detail.map(detailItem => {
            return {
              id: detailItem.id,
              sv_nme: detailItem.name,
              photo: detailItem.photo,
            };
          }),
        };
      });

      const transformedArray2 = datasubs.map(item => {
        return {
          id: item.id,
          isclick: item.is_selected === 1 ? true : false,
          name: item.name,
          subvariatn: item.detail.map(detailItem => {
            return {
              id: detailItem.id,
              sv_nme: detailItem.name,
              photo: detailItem.photo,
              price: detailItem.price,
              range: detailItem.range_price === undefined ? '' : detailItem.range_price,
              stok: detailItem.stock,
              multiprice: detailItem.range_price !== undefined ? 'yes' : 'no',
            };
          }),
        };
      });
      setold(prevOld => [...prevOld, ...transformedArray2]);
      const filteredArray = transformedArray.filter(item => item.isclick);
      if (filteredArray[0].subvariatn.length === 0) {
        reducer({type: Nameuser.deletefoto});
      } else {
        let ph = [];
        const subvariatnAtIndex0 = filteredArray[0].subvariatn;
        for (let i = 0; i < subvariatnAtIndex0.length; i++) {
          const paraams = {
            name_sv: subvariatnAtIndex0[i].sv_nme,
            img: subvariatnAtIndex0[i].photo,
            id: subvariatnAtIndex0[i].id,
          };
          ph.push(paraams);
        }
        reducer({type: Nameuser.addfoto, payload: ph});
      }
      setFilteredData(filteredArray);
      reducer({type: Nameuser.addvarianmain, payload: transformedArray});
    }
  }, [datasubs]);
  useFocusEffect(focus);
  const Postvarian = async dat => {
    dispatch({type: 'SHOW_LOADING'});
    const params = {
      name: dat,
      is_selected: 0,
    };
    const res = await APISELLER.AddVariants(params, idvarint, userData.token);
    dispatch({type: 'HIDE_LOADING'});
    return res;
  };
  const Postsubvarian = async dat => {
    dispatch({type: 'SHOW_LOADING'});
    const params = {
      name: dat,
    };
    debugConsoleLog('daaatsen', dat);
    debugConsoleLog('daaatsen', idvaarint);
    const res = await APISELLER.AddsubVariant(
      params,
      idvaarint,
      userData.token,
    );
    dispatch({type: 'HIDE_LOADING'});
    return res;
  };
  const compareArrays = (arr1, arr2) => {
    const isIdentical =
      JSON.stringify(arr1[0].subvariatn) === JSON.stringify(arr2[0].subvariatn);

    debugConsoleLog('arr1', arr1);
    debugConsoleLog('arr1', arr2);
    return isIdentical;
  };
  const deletevariant = async id => {
    dispatch({type: 'SHOW_LOADING'});
    const res = await APISELLER.DeleteVaariaan(id, userData.token);
    dispatch({type: 'HIDE_LOADING'});
    return res;
  };
  const deletesubvariant = async id => {
    dispatch({type: 'SHOW_LOADING'});
    debugConsoleLog('delete', id);
    const res = await APISELLER.DeletesubVaariaan(id, userData.token);
    dispatch({type: 'HIDE_LOADING'});
    return res;
  };
  const handleItemPress = (index, name) => {
    const data = [...state.mainvarian];
    const clickedItem = data[index];
    const trueStatusObjects = data.filter(item => item.isclick === true);
    if (
      clickedItem &&
      clickedItem.name === name &&
      trueStatusObjects.length < 2
    ) {
      // Buat salinan objek dan ubah status menjadi true
      const updatedItem = {
        ...clickedItem,
        isclick: !clickedItem.isclick,
      };
      let phoot = [...state.photo];
      const updatedData = [...data];
      updatedData[index] = updatedItem;
      const filteredArray = updatedData.filter(item => item.isclick);
      debugConsoleLog('length', filteredArray.length);
      if (filteredArray.length !== 0) {
        const subvariatnNames = filteredArray[0].subvariatn.map(
          item => item.sv_nme,
        );

        // Assuming phoot is an array of objects with the property "name_sv"
        const phootNames = phoot.map(item => item.name_sv);

        // Check if the arrays have the same content
        const arraysAreEqual =
          JSON.stringify(subvariatnNames) === JSON.stringify(phootNames);
        debugConsoleLog('arr', arraysAreEqual);
        if (!arraysAreEqual) {
          if (filteredArray[0].subvariatn.length === 0) {
            reducer({type: Nameuser.deletefoto});
          } else {
            let ph = [];
            const subvariatnAtIndex0 = filteredArray[0].subvariatn;
            for (let i = 0; i < subvariatnAtIndex0.length; i++) {
              const paraams = {
                name_sv: subvariatnAtIndex0[i].sv_nme,
                img: IconAsset.IcPlus,
                id: subvariatnAtIndex0[i].id,
              };
              ph.push(paraams);
            }
            reducer({type: Nameuser.addfoto, payload: ph});
          }
        }
      }
      setFilteredData(filteredArray);
      reducer({type: Nameuser.addsubvarinmain, payload: updatedData});
    } else {
      let phoot = [...state.photo];
      if (clickedItem.isclick === true) {
        const updatedItem = {
          ...clickedItem,
          isclick: !clickedItem.isclick,
        };

        // Salin array data, ganti objek "helmi" yang diubah dengan yang baru
        const updatedData = [...data];
        updatedData[index] = updatedItem;
        const filteredArray = updatedData.filter(item => item.isclick);
        if (filteredArray.length !== 0) {
          const subvariatnNames = filteredArray[0].subvariatn.map(
            item => item.sv_nme,
          );

          // Assuming phoot is an array of objects with the property "name_sv"
          const phootNames = phoot.map(item => item.name_sv);

          // Check if the arrays have the same content
          const arraysAreEqual =
            JSON.stringify(subvariatnNames) === JSON.stringify(phootNames);
          debugConsoleLog('arr', arraysAreEqual);

          if (!arraysAreEqual) {
            if (filteredArray[0].subvariatn.length === 0) {
              reducer({type: Nameuser.deletefoto});
            } else {
              let ph = [];
              const subvariatnAtIndex0 = filteredArray[0].subvariatn;
              for (let i = 0; i < subvariatnAtIndex0.length; i++) {
                const paraams = {
                  name_sv: subvariatnAtIndex0[i].sv_nme,
                  id: subvariatnAtIndex0[i].id,
                  img: IconAsset.IcPlus,
                };
                ph.push(paraams);
              }
              reducer({type: Nameuser.addfoto, payload: ph});
            }
          }
        }
        setFilteredData(filteredArray);
        reducer({type: Nameuser.addsubvarinmain, payload: updatedData});
      } else {
        showToastMessage('Hanya bisa memilih 2 tipe varian');
      }
    }
  };
  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Persetujuan akses file',
            message: 'Berikan Baba Market untuk mengakses file ?',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else {
      return true;
    }
  };
  const Postimaage = async (name, id, uri, image) => {
    dispatch({type: 'SHOW_LOADING'});
    let dat = new FormData();
    const foto = {
      type: image.type,
      uri: image.uri,
      name: image.fileName,
    };
    dat.append('name', name);
    dat.append('photo', foto);
    debugConsoleLog('daataavaar', idvarint);
    debugConsoleLog('footoda', image.fileName);
    debugConsoleLog('footoda', id);
    const res = APISELLER.UpdateProductSub(dat, id, userData.token);
    dispatch({type: 'HIDE_LOADING'});
    return res;
  };
  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Persetujuan akses kamera',
            message: 'Berikan Baba Market untuk mengakses kamera ?',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else {
      return true;
    }
  };
  const captureImage = async type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      saveToPhotos: true,
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      setTimeout(() => {
        launchCamera(options, async response => {
          if (response.didCancel) {
            // alert('User cancelled camera picker');
            return;
          } else if (response.errorCode == 'camera_unavailable') {
            // alert('Camera not available on device');
            return;
          } else if (response.errorCode == 'permission') {
            // alert('Permission not satisfied');
            return;
          } else if (response.errorCode == 'others') {
            // alert(response.errorMessage);
            return;
          }
          let phot = [...state.photo];
          if (indeximage >= 0 && indeximage < phot.length) {
            debugConsoleLog('daataxx', phot);
            const datimge = response.assets[0].uri;
            const imgae = response.assets[0];
            const res = await Postimaage(
              phot[indeximage].name_sv,
              phot[indeximage].id,
              datimge,
              imgae,
            );
            if (res.data.message === 'Sukses') {
              phot[indeximage].img = response.assets[0].uri;
              reducer({type: Nameuser.addfoto, payload: phot});
            }
          }
        });
      }, 500);
    }
  };

  const chooseFile = type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    setTimeout(() => {
      launchImageLibrary(options, async response => {
        if (response.didCancel) {
          alert('User cancelled camera picker');
          return;
        } else if (response.errorCode == 'camera_unavailable') {
          alert('Camera not available on device');
          return;
        } else if (response.errorCode == 'permission') {
          alert('Permission not satisfied');
          return;
        } else if (response.errorCode == 'others') {
          alert(response.errorMessage);
          return;
        }
        let phot = [...state.photo];
        if (indeximage >= 0 && indeximage < phot.length) {
          const datimge = response.assets[0].uri;
          const imgae = response.assets[0];
          const res = await Postimaage(
            phot[indeximage].name_sv,
            phot[indeximage].id,
            datimge,
            imgae,
          );
          if (res.data.message === 'Sukses') {
            phot[indeximage].img = response.assets[0].uri;
            reducer({type: Nameuser.addfoto, payload: phot});
          }
        }
      });
    }, 500);
  };
  const Footercomponent = () => {
    return (
      <FlatList
        data={state.photo}
        numColumns={4}
        style={{
          marginHorizontal: normalize(5, 'height'),
        }}
        ListHeaderComponentStyle={{marginVertical: normalize(5, 'height')}}
        ListHeaderComponent={
          <View style={{width: '100%'}}>
            <Text
              style={{
                fontFamily: Fonts.mulishExtraBold,
                color: '#333',
                marginHorizontal: 0,
              }}>
              Foto Varian ( Opsional )
            </Text>
          </View>
        }
        renderItem={({item, index}) => {
          const isiurl = isImageUrl(item.img);
          debugConsoleLog('dataa', isiurl);
          debugConsoleLog('dataa', item.img);
          return (
            <TouchableOpacity
              onPress={() => {
                setShowModalOption(true);
                debugConsoleLog('dat', index);
                setindeximage(index);
              }}
              style={styles.footerstylecontainer}>
              {isiurl === true ? (
                <>
                  <Image source={{uri: item.img}} style={styles.imgfooter} />
                  <Text style={[styles.txtfooter]}>{item.name_sv}</Text>
                </>
              ) : (
                <View
                  style={{
                    borderColor: Colors.primary,
                    width: 78,
                    height: 78,
                    borderStyle: 'dashed',
                    backgroundColor: '#FFF2F3',
                    borderWidth: 2,
                    borderRadius: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Icons.PlusSVG size={28} color={Colors.primary} />
                  <Text
                    style={{
                      fontFamily: Fonts.mulishRegular,
                      color: Colors.primary,
                    }}>
                    {item.name_sv}
                  </Text>
                </View>
              )}
            </TouchableOpacity>
          );
        }}
      />
    );
  };
  function isImageUrl(url) {
    // Regex untuk memeriksa apakah string adalah URL gambar
    const imageUrlRegex = /\.(jpeg|jpg|gif|png)$/;

    // Menggunakan metode test untuk memeriksa apakah string sesuai dengan regex
    return imageUrlRegex.test(url);
  }
  const renderItem = ({item}) => {
    debugConsoleLog('console', item);
    return (
      <View style={styles.rendercontainer}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.text}>
            Varian{' '}
            <TextBold
              color="white"
              size={normalize(14, 'height')}
              text={item.name}
            />
          </Text>
          {item.subvariatn.length < 10 && (
            <TouchableOpacity
              onPress={() => {
                setdatasubadd(prevState => ({...prevState, target: item.name}));
                setidvariant(item.id);
                reducer({type: Nameuser.showsubModal});
              }}
              style={{
                backgroundColor: Colors.primary,
                borderRadius: normalize(10, 'height'),
                padding: normalize(5, 'height'),
              }}>
              <Image
                source={IconAsset.IcPlus}
                style={{
                  width: normalize(20, 'width'),
                  height: normalize(15, 'height'),
                  tintColor: '#FFf',
                }}
              />
            </TouchableOpacity>
          )}
        </View>
        <View></View>
        <FlatList
          numColumns={3}
          ItemSeparatorComponent={<View style={{margin: Size.scaleSize(5)}} />}
          data={item.subvariatn}
          style={{
            padding: normalize(0, 'width'),
            margin: 0,
            height: '100%',
            marginHorizontal: normalize(5, 'width'),
          }}
          renderItem={({item: subitem, index}) => {
            debugConsoleLog('dataietm', subitem);
            return (
              <View>
                <View
                  style={[
                    styles.catPicked,
                    {
                      flexDirection: 'column',
                      marginHorizontal: 0,
                      borderRadius: normalize(6, 'height'),
                      padding: normalize(10, 'height'),
                      flexGrow: 1,
                      paddingTop: 0,
                    },
                  ]}>
                  <Text
                    numberOfLines={3}
                    style={{
                      fontFamily: Fonts.mulishBold,
                      color: '#FFF',
                      marginTop: normalize(15, 'height'),
                      alignSelf: 'center',
                      width: Size.scaleSize(78),
                      alignContent: 'center',
                      marginBottom: Size.scaleSize(5),
                      textAlign: 'center',
                      fontSize: Size.scaleFont(12),
                    }}>
                    {subitem.sv_nme}
                  </Text>
                </View>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={{
                    position: 'absolute',
                    right: 0,
                    borderRadius: 5,
                    top: 8,
                    alignContent: 'center',
                    padding: Size.scaleSize(3),
                    backgroundColor: Colors.primary,
                    alignSelf: 'center',
                  }}
                  onPress={async () => {
                    const res = await deletesubvariant(subitem.id);
                    if (res.data.message === 'Sukses') {
                      let dat = [...state.mainvarian];
                      let phot = [...state.photo];
                      dat.forEach(mainVarian => {
                        // Check if the current main variant has the name we want to delete
                        if (mainVarian.name === item.name) {
                          // Assuming you have an index variable or the desired condition to find the subvariant to delete
                          const indexToDelete = index; // Replace with the desired index or condition

                          // Check if the index is valid
                          if (
                            indexToDelete >= 0 &&
                            indexToDelete < mainVarian.subvariatn.length
                          ) {
                            // Remove the subvariant at the specified index
                            mainVarian.subvariatn.splice(indexToDelete, 1);
                          }
                        }
                      });
                      const filteredArray = dat.filter(item => item.isclick);
                      if (filteredArray.length !== 0) {
                        const subvariatnNames = filteredArray[0].subvariatn.map(
                          item => item.sv_nme,
                        );

                        // Assuming phoot is an array of objects with the property "name_sv"
                        const phootNames = phot.map(item => item.name_sv);

                        // Check if the arrays have the same content
                        const arraysAreEqual =
                          JSON.stringify(subvariatnNames) ===
                          JSON.stringify(phootNames);

                        if (!arraysAreEqual) {
                          if (filteredArray[0].subvariatn.length === 0) {
                            reducer({type: Nameuser.deletefoto});
                          } else {
                            let ph = [];
                            const subvariatnAtIndex0 =
                              filteredArray[0].subvariatn;
                            for (
                              let i = 0;
                              i < subvariatnAtIndex0.length;
                              i++
                            ) {
                              const paraams = {
                                name_sv: subvariatnAtIndex0[i].sv_nme,
                                id: subvariatnAtIndex0[i].id,
                                img:
                                  subvariatnAtIndex0[i].sv_nme !==
                                  phot[i]?.name_sv
                                    ? IconAsset.IcPlus
                                    : phot[i]?.img || IconAsset.IcPlus,
                              };
                              ph.push(paraams);
                            }
                            reducer({type: Nameuser.addfoto, payload: ph});
                          }
                        }
                      }
                      reducer({type: Nameuser.addvarianmain, payload: dat});
                      debugConsoleLog('pemalangan', dat);
                      setFilteredData(filteredArray);
                    }
                  }}>
                  <Image
                    source={IconAsset.IcClose}
                    style={{
                      width: 15,
                      height: 15,
                      tintColor: '#FFf',
                    }}
                  />
                </TouchableOpacity>
              </View>
            );
          }}
        />
      </View>
    );
  };
  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <KeyboardAvoidingView
        enabled
        behavior={Platform.OS == 'ios' ? 'height' : null}
        keyboardVerticalOffset={Platform.OS === 'android' ? 0 : 0}
        style={styles.container}>
        <HeaderTitle
          title="Varian"
          back={() => {
            onClose();
          }}
        />
        <View style={styles.maincontainer}>
          <View
            style={{
              marginTop: 0,
              marginHorizontal: 15,
            }}>
            <Text style={{fontFamily: Fonts.mulishExtraBold, color: '#333'}}>
              Pilih Tipe Varian<Text style={{color: Colors.primary}}>*</Text>
            </Text>
            <Text
              style={{
                fontFamily: Fonts.mulishRegular,
                fontSize: Size.scaleFont(14),
                color: Colors.greyDummy,
              }}>
              Pilih Maksimum 2 Tipe Varian
            </Text>
          </View>
          <FlatList
            data={state.mainvarian}
            numColumns={4}
            style={{maxHeight: normalize(150, 'height')}}
            renderItem={({item, index}) => {
              debugConsoleLog('mainvarian', item);
              return (
                <>
                  <TouchableOpacity
                    onPress={async () => {
                      const data = [...state.mainvarian];
                      const trueStatusObjects = data.filter(
                        item => item.isclick === true,
                      );

                      const prams = {
                        name: item.name,
                        is_selected: data[index].isclick === true ? 0 : 1,
                      };
                      const res = await APISELLER.AddVariantsUpdate(
                        prams,
                        item.id,
                        userData.token,
                      );
                      debugConsoleLog('data', res.data);
                      debugConsoleLog('data', !data[index].isclick);
                      debugConsoleLog('data', item.id);
                      if (res.data.message === 'Sukses') {
                        handleItemPress(index, item.name);
                      }
                    }}
                    style={[
                      item.isclick === true
                        ? styles.catPicked
                        : styles.catPicked2,
                      {
                        maxWidth: normalize(100, 'width'),
                        minWidth: normalize(50, 'width'),
                        minHeight: normalize(30, 'height'),
                        maxHeight: normalize(50, 'height'),
                        justifyContent: 'center',
                        alignItems: 'center',
                      },
                    ]}>
                    <Text
                      style={{
                        fontFamily: Fonts.mulishBold,
                        color: '#FFF',
                        fontSize: Size.scaleFont(12),
                      }}>
                      {item.name}
                    </Text>
                    <TouchableOpacity
                      activeOpacity={0.8}
                      style={{
                        position: 'absolute',
                        right: 0,
                        marginRight: -5,
                        borderRadius: 5,
                        top: -10,
                        alignContent: 'center',
                        padding: Size.scaleSize(3),
                        backgroundColor: Colors.primary,
                        alignSelf: 'center',
                      }}
                      onPress={async () => {
                        const res = await deletevariant(item.id);
                        if (res.data.message === 'Sukses') {
                          let dat = [...state.mainvarian];
                          let phot = [...state.photo];
                          dat.splice(index, 1);
                          const filteredArray = dat.filter(
                            item => item.isclick,
                          );
                          if (filteredArray.length !== 0) {
                            const subvariatnNames =
                              filteredArray[0].subvariatn.map(
                                item => item.sv_nme,
                              );

                            // Assuming phoot is an array of objects with the property "name_sv"
                            const phootNames = phot.map(item => item.name_sv);

                            // Check if the arrays have the same content
                            const arraysAreEqual =
                              JSON.stringify(subvariatnNames) ===
                              JSON.stringify(phootNames);

                            if (!arraysAreEqual) {
                              if (filteredArray[0].subvariatn.length === 0) {
                                reducer({type: Nameuser.deletefoto});
                              } else {
                                let ph = [];
                                const subvariatnAtIndex0 =
                                  filteredArray[0].subvariatn;
                                for (
                                  let i = 0;
                                  i < subvariatnAtIndex0.length;
                                  i++
                                ) {
                                  const paraams = {
                                    name_sv: subvariatnAtIndex0[i].sv_nme,
                                    id: subvariatnAtIndex0[i].id,
                                    img: IconAsset.IcPlus,
                                  };
                                  ph.push(paraams);
                                }
                                reducer({type: Nameuser.addfoto, payload: ph});
                              }
                            }
                          }
                          reducer({type: Nameuser.addvarianmain, payload: dat});
                          setFilteredData(filteredArray);
                        }
                      }}>
                      <Image
                        source={IconAsset.IcClose}
                        style={{
                          width: 15,
                          height: 15,
                          tintColor: '#FFf',
                        }}
                      />
                    </TouchableOpacity>
                  </TouchableOpacity>
                </>
              );
            }}
          />
          <View style={{flexGrow: 1, maxHeight: '99%'}}>
            <Button
              onPress={() => {
                reducer({type: Nameuser.showmodalvarian});
              }}
              containerStyle={{
                marginTop: Size.scaleSize(10),
                marginHorizontal: Size.scaleSize(8),
                marginBottom: normalize(10, 'height'),
                width: '35%',
              }}
              textComponent={
                <View
                  style={{
                    flexDirection: 'row',
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                  }}>
                  <Icons.PlusSVG size={15} color="#FFF" />
                  <Text
                    style={{
                      fontFamily: Fonts.mulishBold,
                      color: '#FFF',
                      fontSize: Size.scaleFont(10),
                      marginLeft: 10,
                      marginTop: 0,
                    }}>
                    Tambah Varian
                  </Text>
                </View>
              }
            />
            <View
              style={{
                width: '100%',
                height: Size.scaleSize(10),
                marginVertical: normalize(5, 'height'),
                backgroundColor: Colors.borderGrey,
              }}
            />
            {filteredData.length !== 0 && (
              <FlatList
                data={filteredData}
                ListHeaderComponentStyle={{
                  marginBottom: normalize(5, 'height'),
                }}
                ListHeaderComponent={
                  <View style={{width: '100%'}}>
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: '#333',
                        marginHorizontal: 0,
                      }}>
                      Sub Varian<Text style={{color: Colors.primary}}>*</Text>
                    </Text>
                  </View>
                }
                showsVerticalScrollIndicator={false}
                renderItem={renderItem}
                style={styles.containerSub}
                ItemSeparatorComponent={
                  <View style={{margin: normalize(5, 'height')}} />
                }
                ListFooterComponentStyle={{
                  marginVertical: normalize(10, 'height'),
                }}
                ListFooterComponent={
                  state.photo.length !== 0 && Footercomponent
                }
              />
            )}
          </View>
          <Modal
            transparent
            visible={state.modalvariant}
            onRequestClose={() => {
              setdatasubadd(prevState => ({...prevState, name: ''}));
              reducer({type: Nameuser.hidemodalvarian});
            }}>
            <View style={styles.containerModal}>
              <View
                style={{
                  backgroundColor: 'white',
                  borderTopStartRadius: 12,
                  borderTopEndRadius: 12,
                }}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    paddingTop: Size.scaleSize(10),
                    paddingHorizontal: Size.scaleSize(10),
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <TextBold
                    text="Masukkan Nama Varian"
                    color={Colors.blackBase}
                    size={16}
                  />
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => {
                      reducer({type: Nameuser.hidemodalvarian});
                      setdatasubadd(prevState => ({...prevState, name: ''}));
                    }}>
                    <Image
                      source={IconAsset.IcClose}
                      style={{width: 25, height: 25}}
                    />
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    marginTop: 15,
                    marginBottom: Size.scaleSize(5),
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginHorizontal: 15,
                  }}>
                  <Text
                    style={{
                      fontFamily: Fonts.mulishExtraBold,
                      color: '#333',
                    }}>
                    Nama Varian{' '}
                  </Text>
                </View>
                <InputText
                  containerStyle={{marginHorizontal: 15}}
                  placeholder="Masukan Nama varian"
                  value={datasubadd.name}
                  onChangeText={text =>
                    setdatasubadd(prevState => ({...prevState, name: text}))
                  }
                />
                <Button
                  onPress={async () => {
                    if (datasubadd.name === '') {
                      showToastMessage('Nama Varian Tidak Boleh Kosong');
                    } else {
                      const data = [...state.mainvarian];
                      const res = await Postvarian(datasubadd.name);
                      debugConsoleLog('datares', res.data);
                      if (res.data.message === 'Sukses') {
                        const params = {
                          id: res.data.data.id,
                          name: datasubadd.name,
                          isclick: false,
                          subvariatn: [],
                        };
                        data.push(params);
                        reducer({
                          type: Nameuser.addsubvarinmain,
                          payload: data,
                        });
                        setdatasubadd(prevState => ({...prevState, name: ''}));
                        reducer({type: Nameuser.hidemodalvarian});
                        setdatasubadd(prevState => ({...prevState, name: ''}));
                      }
                    }
                  }}
                  containerStyle={{
                    marginHorizontal: Size.scaleSize(10),
                    marginVertical: Size.scaleSize(10),
                  }}
                  title={'Tambah Varian'}
                />
              </View>
            </View>
          </Modal>
          <ModalPickImage
            show={showModalOption}
            onClose={() => setShowModalOption(false)}
            onPressCamera={() => {
              setShowModalOption(false);
              captureImage('photo');
            }}
            onPressGalery={() => {
              setShowModalOption(false);
              chooseFile('photo');
            }}
          />
          <MOdalsub
            visible={state.modalsubVariant}
            onClose={async () => {
              setdatasubadd(prevState => ({...prevState, name: ''}));
              reducer({type: Nameuser.hidesubModl});
            }}
            setname={setdatasubadd}
            simpan={async () => {
              const res = await Postsubvarian(datasubadd.name);
              if (res.data.message === 'Sukses') {
                debugConsoleLog('liveku', res.data.data);
                const data = [...state.mainvarian];
                const phoot = [...state.photo];
                const newdata = data.map(item => {
                  if (item.name === datasubadd.target) {
                    // Tambahkan data ke subvarian
                    const params = {
                      id: res.data.data.id,
                      sv_nme: datasubadd.name,
                    };
                    item.subvariatn.push(params);
                  }
                  return item;
                });
                let ph = [];
                const subvariatnAtIndex0 = data[0].subvariatn;
                const subvariatnNames = data[0].subvariatn.map(
                  item => item.sv_nme,
                );
                const phootNames = phoot.map(item => item.name_sv);

                // Check if the arrays have the same content
                const arraysAreEqual =
                  JSON.stringify(subvariatnNames) ===
                  JSON.stringify(phootNames);
                debugConsoleLog('arr', arraysAreEqual);
                debugConsoleLog('arr', phoot);
                debugConsoleLog('arr', subvariatnNames);
                if (!arraysAreEqual) {
                  for (let i = 0; i < subvariatnAtIndex0.length; i++) {
                    const paraams = {
                      name_sv: subvariatnAtIndex0[i].sv_nme,
                      id: subvariatnAtIndex0[i].id,
                      img: phoot[i]?.img || IconAsset.IcPlus,
                    };
                    ph.push(paraams);
                  }
                  reducer({type: Nameuser.addfoto, payload: ph});
                }
                debugConsoleLog('xxtada', newdata);
                debugConsoleLog('xxtada', newdata.length);
                for (let i = 0; i < newdata.length; i++) {
                  debugConsoleLog('raki', newdata[i].subvariatn);
                }
                reducer({type: Nameuser.addvarianmain, payload: newdata});
                reducer({type: Nameuser.hidesubModl});
                setdatasubadd(prevState => ({...prevState, target: ''}));
                setdatasubadd(prevState => ({...prevState, name: ''}));
              }
            }}
          />
        </View>
      </KeyboardAvoidingView>
      {state.photo && state.photo.length !== 0 && (
        <View
          style={{
            position: 'absolute',
            width: '90%',
            bottom: 0,
            justifyContent: 'center',
            alignContent: 'center',
            alignSelf: 'center',
          }}>
          <Button
            disable={state.photo.length === 0 ? true : false}
            onPress={() => {
              let combine = [];
              const data = [...state.mainvarian];
              const phoot = [...state.photo];

              debugConsoleLog('pakta', data.length);
              debugConsoleLog('pakta', filteredData.length);
              if (data.length === old.length) {
                const filteredArray = filteredData.filter(item => item.isclick);
                const newArray = filteredArray.map(item => {
                  const {isclick, ...newItem} = item;
                  return newItem;
                });
                const combine = [];
                const isClickTrueCount = data.filter(
                  item => item.isclick === true,
                ).length;
                if (isClickTrueCount >= 2) {
                  const array = filteredArray[0];
                  const second = filteredArray[1];
                  const newData = {...array};
                  delete newData.isclick;
                  const newData2 = {...second};
                  delete newData2.isclick;
                  const length1 = newData.subvariatn.length || 0;
                  const length2 = newData2.subvariatn.length || 0;
                  const finalFilteredOld = old.filter(
                    item =>
                      item.subvariatn &&
                      item.subvariatn.length > 0 &&
                      item.subvariatn.some(subItem => subItem.price !== 0),
                  );

                  for (let i = 0; i < newData.subvariatn.length; i++) {
                    for (let num = 0; num < newData2.subvariatn.length; num++) {
                      const prams = {
                        namaminsub: newData.name,
                        namasecond: newData2.name,
                        svmain: newData.subvariatn[i]?.sv_nme,
                        svsecond: newData2.subvariatn[num].sv_nme,
                        sv_id: newData.subvariatn[i].id,
                        id: newData2.subvariatn[num].id,
                        price: finalFilteredOld[i]?.subvariatn[i]?.price,
                        multiprice:
                          finalFilteredOld[i]?.subvariatn[i]?.range.length === 0
                            ? 'no'
                            : 'yes',
                        stok: finalFilteredOld[i]?.subvariatn[i]?.stok || '',
                        range: finalFilteredOld[i]?.subvariatn[i]?.range || '',
                        show: false,
                      };

                      debugConsoleLog('ooolld', old[1].subvariatn);
                      debugConsoleLog(
                        'ooolld',
                        finalFilteredOld[0].subvariatn[0].price,
                      );
                      debugConsoleLog('xxoold', prams);

                      combine.push(prams);
                    }

                    reducer({type: Nameuser.addCombinasi, payload: combine});
                    setshowmodalacc(true);
                  }
                } else {
                  const filteredArray = data.filter(item => item.isclick);
                  const filteredArray2 = old.filter(item => item.isclick);
                  const finalFilteredOld = old.filter(
                    item =>
                      item.subvariatn &&
                      item.subvariatn.length > 0 &&
                      item.subvariatn.some(subItem => subItem.price !== 0),
                  );
                  const newArray = filteredArray.map(item => {
                    const {isclick, ...newItem} = item;
                    return newItem;
                  });
                  const newArray2 = filteredArray2.map(item => {
                    const {isclick, ...newItem} = item;
                    return newItem;
                  });
                  const combine = [];

                  for (const newItem of newArray) {
                    const {name, subvariatn, id} = newItem;
                    debugConsoleLog('datpekok', newItem);
                    for (const subvarian of subvariatn) {
                      const params = {
                        id: subvarian.id,
                        namaminsub: name,
                        namasecond: '',
                        svmain: subvarian.sv_nme,
                        svsecond: '',
                        price: finalFilteredOld[0].subvariatn[0].price,
                        multiprice:
                          finalFilteredOld[0].subvariatn[0].range?.length === 0
                            ? 'no'
                            : 'yes',
                        stok: finalFilteredOld[0].subvariatn[0].stok || '',
                        range: finalFilteredOld[0].subvariatn[0].range || '',
                        show: false,
                      };
                      combine.push(params);
                      debugConsoleLog('data', params);
                      debugConsoleLog('old', finalFilteredOld[0].subvariatn[0]);
                    }
                  }
                  reducer({type: Nameuser.addCombinasi, payload: combine});
                  setshowmodalacc(true);
                }
              } else {
                const isClickTrueCount = data.filter(
                  item => item.isclick === true,
                ).length;
                if (isClickTrueCount >= 2) {
                  const subvariantsNotEmpty = data.every(
                    item => item.subvariatn.length > 0,
                  );

                  if (subvariantsNotEmpty) {
                    const filteredArray = data.filter(item => item.isclick);
                    const array = filteredArray[0];
                    const second = filteredArray[1];
                    const newData = {...array};
                    delete newData.isclick;
                    const newData2 = {...second};
                    delete newData2.isclick;
                    const length1 = newData.subvariatn.length || 0;
                    const length2 = newData2.subvariatn.length || 0;
                    if (length1 > length2) {
                      for (let i = 0; i < newData.subvariatn.length; i++) {
                        for (
                          let num = 0;
                          num < newData2.subvariatn.length;
                          num++
                        ) {
                          const prams = {
                            namaminsub: newData.name,
                            namasecond: newData2.name,
                            svmain: newData.subvariatn[i]?.sv_nme,
                            svsecond: newData2.subvariatn[num].sv_nme,
                            id: newData.subvariatn[i].id,
                            sv_id : newData2.subvariatn[num].id,
                          };
                          combine.push(prams);
                        }
                      }
                      debugConsoleLog('dataacombine', combine);
                    } else if (length1 < length2) {
                      for (let i = 0; i < newData.subvariatn.length; i++) {
                        for (
                          let num = 0;
                          num < newData2.subvariatn.length;
                          num++
                        ) {
                          const prams = {
                            namaminsub: newData.name,
                            namasecond: newData2.name,
                            svmain: newData.subvariatn[i]?.sv_nme,
                            svsecond: newData2.subvariatn[num].sv_nme,
                            id: newData.subvariatn[i].id,
                            sv_id : newData2.subvariatn[num].id,
                          };
                          combine.push(prams);
                        }
                      }
                      debugConsoleLog('datacombine', combine);
                    } else {
                      for (let i = 0; i < newData.subvariatn.length; i++) {
                        for (
                          let num = 0;
                          num < newData2.subvariatn.length;
                          num++
                        ) {
                          const prams = {
                            namaminsub: newData.name,
                            namasecond: newData2.name,
                            svmain: newData.subvariatn[i]?.sv_nme,
                            svsecond: newData2.subvariatn[num].sv_nme,
                            id: newData.subvariatn[i].id,
                            sv_id : newData2.subvariatn[num].id,
                          };
                          combine.push(prams);
                        }
                      }
                    }
                    const resultArray = combine.map(item => {
                      const {svmain} = item;

                      // Mencari foto yang sesuai dengan svmain

                      const matchingFoto = phoot.find(
                        fotoItem => fotoItem.name_sv === svmain,
                      );

                      // Jika foto ditemukan, tambahkan ke dalam item
                      if (matchingFoto) {
                        item.price = '';
                        item.multiprice = 'no';
                        item.stok = '';
                        item.range = [];
                        item.show = false;
                      } else {
                        item.price = '';
                        item.multiprice = 'no';
                        item.stok = '';
                        item.range = [];
                        item.show = false;
                      }
                      return item;
                    });
                    resultArray[0].show = true;
                    reducer({
                      type: Nameuser.addCombinasi,
                      payload: resultArray,
                    });
                    setshowmodalacc(true);
                  } else {
                    showToastMessage('Harap Cek sub varian anda');
                  }
                } else {
                  const filteredArray = data.filter(item => item.isclick);
                  const newArray = filteredArray.map(item => {
                    const {isclick, ...newItem} = item;
                    return newItem;
                  });
                  const combine = [];

                  for (const newItem of newArray) {
                    const {name, subvariatn, id} = newItem;
                    debugConsoleLog('datpekok', newItem);
                    for (const subvarian of subvariatn) {
                      const params = {
                        id: subvarian.id,
                        namaminsub: name,
                        namasecond: '',
                        svmain: subvarian.sv_nme,
                        svsecond: '',
                      };

                      combine.push(params);
                    }
                    debugConsoleLog('arr', combine);
                  }
                  const resultArray = combine.map(item => {
                    const {svmain} = item;

                    // Mencari foto yang sesuai dengan svmain

                    const matchingFoto = phoot.find(
                      fotoItem => fotoItem.name_sv === svmain,
                    );

                    // Jika foto ditemukan, tambahkan ke dalam item
                    if (matchingFoto) {
                      item.price = '';
                      item.multiprice = 'no';
                      item.stok = '';
                      item.range = [];
                      item.show = false;
                    } else {
                      item.price = '';
                      item.multiprice = 'no';
                      item.stok = '';
                      item.range = [];
                      item.show = false;
                    }

                    return item;
                  });
                  resultArray[0].show = true;
                  reducer({type: Nameuser.addCombinasi, payload: resultArray});
                  setshowmodalacc(true);
                }
              }
            }}
            style={{
              backgroundColor:
                state.photo.length === 0 ? Colors.grey_3 : Colors.primary,
            }}
            title={'Lanjut'}
          />
        </View>
      )}
      <ModalCombination
        show={show}
        onClose={onClose}
        onupdate={onupdate}
        idvaarint={idvarint}
        state={state}
        isvarian={isvarian}
        reducer={reducer}
        datasubsubs={datasubsubs}
        showmodal={showModalacc}
        onclosemodal={() => {
          setshowmodalacc(false);
        }}
      />
    </Modal>
  );
};

const MOdalsub = ({visible, onClose, setname, name, simpan}) => {
  //
  return (
    <Modal transparent visible={visible} onRequestClose={onClose}>
      <View style={styles.containerModal}>
        <View
          style={{
            backgroundColor: 'white',
            borderTopStartRadius: 12,
            borderTopEndRadius: 12,
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              paddingTop: Size.scaleSize(10),
              paddingHorizontal: Size.scaleSize(10),
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <TextBold
              text="Masukkan Nama Sub Varian"
              color={Colors.blackBase}
              size={16}
            />
            <TouchableOpacity activeOpacity={0.8} onPress={onClose}>
              <Image
                source={IconAsset.IcClose}
                style={{width: 25, height: 25}}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: 15,
              marginBottom: Size.scaleSize(5),
              flexDirection: 'row',
              alignItems: 'center',
              marginHorizontal: 15,
            }}>
            <Text
              style={{
                fontFamily: Fonts.mulishExtraBold,
                color: '#333',
              }}>
              Nama Sub Varian{' '}
            </Text>
          </View>
          <InputText
            containerStyle={{marginHorizontal: 15}}
            placeholder="Masukan Nama Sub Varian"
            value={name}
            onChangeText={text =>
              setname(prevState => ({...prevState, name: text}))
            }
          />
          <Button
            onPress={simpan}
            containerStyle={{
              marginHorizontal: Size.scaleSize(10),
              marginVertical: Size.scaleSize(10),
            }}
            title={'Tambah Sub Varian'}
          />
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  imgfooter: {
    width: '100%',
    flex: 0.8,
    backgroundColor: 'white',
    borderBottomColor: Colors.grey_3,
    borderBottomWidth: normalize(5, 'height'),
    borderWidth: normalize(2, 'width'),
    borderColor: '#F2F4F5',
    height: undefined,
    resizeMode: 'cover',
    overflow: 'hidden',
    borderTopRightRadius: normalize(10, 'height'),
    borderTopLeftRadius: normalize(10, 'height'),
  },
  imgfooter2: {
    width: '100%',
    flex: 0.8,
    tintColor: Colors.primary,
    backgroundColor: 'white',
    borderBottomColor: Colors.grey_3,
    borderBottomWidth: normalize(5, 'height'),
    borderWidth: normalize(2, 'width'),
    borderColor: '#F2F4F5',
    resizeMode: 'contain',
    overflow: 'hidden',
    borderTopRightRadius: normalize(10, 'height'),
    borderTopLeftRadius: normalize(10, 'height'),
  },
  footerstylecontainer: {
    width: normalize(80, 'width'),
    margin: normalize(3, 'height'),
    height: normalize(80, 'height'),
    borderRadius: normalize(10, 'height'),
    borderWidth: normalize(4, 'width'),
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#F2F4F5',
  },
  txtfooter: {
    flex: 0.5,
    justifyContent: 'center',
    paddingTop: normalize(10, 'height'),
    textAlign: 'center',
    fontFamily: Fonts.mulishRegular,
    fontSize: normalize(10, 'height'),
    alignItems: 'flex-end',
  },
  rendercontainer: {
    flex: 1,
  },
  text: {
    fontFamily: Fonts.mulishRegular,
    color: 'white',
    backgroundColor: Colors.primaryDark,
    marginStart: normalize(3, 'width'),
    borderRadius: normalize(10, 'height'),
    maxWidth: normalize(150, 'width'),
    textAlign: 'center',
    padding: normalize(5, 'height'),
    paddingTop: normalize(5, 'height'),
    paddingHorizontal: normalize(10, 'width'),
    fontSize: normalize(12, 'height'),
  },
  maincontainer: {
    marginHorizontal: Size.scaleSize(8),
    marginTop: Size.scaleSize(4),
  },
  txtInput: {
    width: '100%',
    height: 120,
    marginTop: 20,
    borderRadius: 6,
    borderColor: Colors.border,
    borderWidth: 1,
    textAlignVertical: 'top',
    padding: 10,
  },
  containerSub: {
    margin: 0,
    width: '100%',
    maxHeight: '80%',
  },
  btnSend: {
    marginTop: 20,
    width: '100%',
    borderRadius: 24,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
  },
  content: {
    width: '100%',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: '#fff',
    padding: 15,
  },
  catPicked: {
    marginLeft: normalize(10, 'width'),
    marginTop: normalize(10, 'height'),
    padding: 7,
    borderRadius: 4,
    backgroundColor: Colors.secondary,
  },
  catPicked2: {
    marginLeft: normalize(10, 'width'),
    marginTop: normalize(10, 'height'),
    padding: 7,
    borderRadius: 4,
    backgroundColor: Colors.grey_3,
  },
  containerModal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
  },
});

export default ModalVariants;
