import React from 'react';
import {
  View,
  Modal,
  FlatList,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {Colors} from '@styles/index';
import {TextBold, TextRegular} from '@components/global';
import {IconAsset} from '@utils/index';
import {data} from './month.json';

const ModalPickMonth = ({show, onClose, onPick}) => {
  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.header}>
            <TextBold text="Pilih Bulan" size={16} color={Colors.blackBase} />
            <TouchableOpacity style={{padding: 10}} onPress={onClose}>
              <Image
                source={IconAsset.IcClose}
                style={{width: 20, height: 20}}
              />
            </TouchableOpacity>
          </View>
          <FlatList
            data={data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={{padding: 15}}
                onPress={() => onPick(item)}>
                <TextRegular text={item.label} color={Colors.blackBase} />
              </TouchableOpacity>
            )}
            ItemSeparatorComponent={() => (
              <View
                style={{
                  width: '100%',
                  height: 2,
                  backgroundColor: Colors.borderGrey,
                }}
              />
            )}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
  },
  content: {
    width: '100%',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: '#fff',
    paddingBottom: 10,
    maxHeight: '50%',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
});

export default ModalPickMonth;
