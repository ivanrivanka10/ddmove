import React from 'react';
import {
  Modal,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Text,
  Platform,
  KeyboardAvoidingView,
  StyleSheet,
  ScrollView,
  PermissionsAndroid,
  Switch,
} from 'react-native';
import {
  HeaderTitle,
  SelectDropDown,
  TextBold,
  Button,
  TextRegular,
  TextSemiBold,
  InputText,
  Icon,
} from '@components/global';
import {Colors, Fonts, Icons, Size} from '@styles/index';
import {IconAsset} from '@utils/index';
import {FlatList} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {
  debugConsoleLog,
  inputRupiah,
  rupiahFormat,
  showToastMessage,
} from '@helpers/functionFormat';
import {useState} from 'react';
import {debug, set} from 'react-native-reanimated';
import {useCallback} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import normalize from 'react-native-normalize';
import APISELLER from '@utils/services/seller/orderProvider';
import {useReducer} from 'react';
import {Nameuser} from '@utils/reducers/InputPorduct/Nameuserreducer';
import {
  inputVarianState,
  VariantReducer,
} from '@utils/reducers/InputPorduct/variantReducer';
import {assets} from 'react-native.config';
import ModalPickImage from './ModalPickImage';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

const ModalCombination = ({
  show,
  onClose,
  datasubsubs,
  datasubs,
  showmodal,
  onupdate,
  isvarian,
  onclosemodal,
  state,
  idvaarint,
  reducer,
}) => {
  const dispatch = useDispatch();
  const {userData} = useSelector(state => state.auth);
  const [titleWholePrice, setTitleWholePrice] = useState('');
  const [startPrice, setStartPrice] = useState('');
  const [showmodalfull,setmodalfull] = useState(false);
  const [datammultiprice, setDatamultiprice] = useState([]);
  const [datasubadd, setdatasubadd] = useState({
    stok: '',
    price: '',
    multiprice: false,
  });
 const postData = async (data) =>{
  dispatch({type: 'SHOW_LOADING'});
  let dat = new FormData();
   let das = []
   debugConsoleLog('datid',data)
  const resultArray = data.map(item => {
    return {
      "id": item.id,
      "price": item.price,
      "multi_price": item.multiprice,
      "stock": item.stok,
      "range_prices":item.range
    };

  })
  debugConsoleLog('dataaku',JSON.stringify(resultArray))
  dat.append('details[]', JSON.stringify(resultArray))
  debugConsoleLog('aarray',idvaarint)
  const res = await APISELLER.AddProductnext(dat,userData.token)
  debugConsoleLog('data',res.data)
  if(res.data.message === 'Sukses'){
      const dat = await APISELLER.GetVarints(idvaarint,userData.token);
      isvarian(true)
      if(dat.data.message === 'Sukses'){
        debugConsoleLog('daata',dat.data.data.variant)
        datasubsubs(dat.data.data.variant)
         onclosemodal();
         onClose();
         showToastMessage('Berhasil Menambahkan Varian')
      }else{
        showToastMessage(dat.data.data.info);
      }
    // datasubsubs(resultArray);
    dispatch({type: 'HIDE_LOADING'});
  }
 }
 const postData2 = async (data) =>{
  dispatch({type: 'SHOW_LOADING'});
  let dat = new FormData();
   let das = []
   debugConsoleLog('datid',data)
  const resultArray = data.map(item => {
    return {
      "id": item.id,
      "price": item.price,
      "multi_price": item.multiprice,
      "stock": item.stok,
      "range_prices":item.range
    };

  })
  console.log('dataku',resultArray);
  debugConsoleLog('dataaku',resultArray)
  dat.append('details[]', JSON.stringify(resultArray))
  debugConsoleLog('aarray',idvaarint)
  const res = await APISELLER.UpdaateProductnext(dat,userData.token)
  debugConsoleLog('data',res.data)
  if(res.data.message === 'Sukses'){
      const dat = await APISELLER.GetVarints(idvaarint,userData.token);
      isvarian(true)
      if(dat.data.message === 'Sukses'){
        debugConsoleLog('daata',dat.data.data.variant)
        datasubsubs(dat.data.data.variant)
         onclosemodal();
         onClose();
         showToastMessage('Varian berhasil berubah')
      }else{
        showToastMessage(dat.data.data.info);
      }
    // datasubsubs(resultArray);
    dispatch({type: 'HIDE_LOADING'});
  }
 }
  const renderItem = ({item, index}) => {
    debugConsoleLog('dataddd',item)
    return (
      <View style={styles.contenrender}>
        <TouchableOpacity
         onPress={() =>{
          const data =[...state.combinedvarian]
          const newDataArray = data.map(item => ({ ...item, show: false }));
          newDataArray[index].show = !newDataArray[index].show;

          debugConsoleLog('dapat',newDataArray[index].show)
          reducer({type:Nameuser.addCombinasi,payload:newDataArray})
         }}
        style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View
            style={{
              marginBottom: Size.scaleSize(5),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: Fonts.mulishExtraBold,
                color: '#333',
              }}>
              {item.svmain}
            </Text>
            {item.svsecond !== '' && (
              <>
                <Text
                  style={{
                    fontFamily: Fonts.mulishBlack,
                    color: '#333',
                    marginHorizontal: normalize(10, 'width'),
                  }}>
                  -
                </Text>
                <Text
                  style={{
                    fontFamily: Fonts.mulishBlack,
                    color: '#333',
                  }}>
                  {item.svsecond}
                </Text>
              </>
            )}
          </View>
          <Icon
            icon={item.show ? IconAsset.ChevronDown : IconAsset.ChevronUp}
            size={normalize(15,'height')}
            color={item.show ? '#029FFD' : '#758592'}
          />
        </TouchableOpacity>
        {item.show === true &&(<View>
        <View
          style={{
            marginTop: normalize(5, 'height'),
            marginBottom: normalize(5, 'height'),
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: Fonts.mulishExtraBold,
              color: '#333',
            }}>
            Masukan stok varian <Text style={{color: Colors.primary}}>*</Text>
          </Text>
        </View>
        <InputText
          containerStyle={{marginTop: normalize(5, 'height')}}
          placeholder="Masukan jumlah stok varian"
          value={""+item.stok}
          onChangeText={text => {
            const updatedState = [...state.combinedvarian];
            updatedState[index].stok = text;
            reducer({type: Nameuser.addCombinasi, payload: updatedState});
          }}
          keyboardType="number-pad"
        />
        <View
          style={{
            marginTop: normalize(10, 'height'),
            marginBottom: normalize(5, 'height'),
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: Fonts.mulishExtraBold,
              color: '#333',
            }}>
            Masukan harga <Text style={{color: Colors.primary}}>*</Text>
          </Text>
        </View>
        <InputText
          containerStyle={{marginTop: normalize(5, 'height')}}
          placeholder="Masukan harga varian"
          value={""+item.price}
          keyboardType="number-pad"
          onChangeText={text => {
            const updatedState = [...state.combinedvarian];
            updatedState[index].price =inputRupiah (text);
            reducer({type: Nameuser.addCombinasi, payload: updatedState});
          }}
        />
        <View
          style={{
            marginTop: normalize(10, 'height'),
            marginBottom: normalize(10, 'height'),
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: Fonts.mulishExtraBold,
              color: '#333',
            }}>
            Harga borongan
          </Text>
          <Switch
            value={item.multiprice === 'yes' ? true : false}
            trackColor={{
              false: Colors.borderGrey,
              true: Colors.primaryDark,
            }}
            onValueChange={() => {
              const updatedState = [...state.combinedvarian];
              if (updatedState[index].multiprice === 'yes') {
                updatedState[index].multiprice = 'no';
              } else {
                updatedState[index].multiprice = 'yes';
              }
              reducer({type: Nameuser.addCombinasi, payload: updatedState});
            }}
            thumbColor={
              item.multiprice === 'yes' ? Colors.bgColorCircle : '#f4f3f4'
            }
          />
        </View>

        {item.multiprice === 'yes' && (
          <>
            <View
              style={{
                marginTop: normalize(10, 'height'),
                marginBottom: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={IconAsset.IcPriceTag}
                style={{
                  width: 24,
                  height: 24,
                  resizeMode: 'contain',
                  marginRight: 10,
                }}
              />
              <Text
                style={{
                  fontFamily: Fonts.mulishExtraBold,
                  color: '#333',
                  fontSize: 12,
                }}>
                Harga Borongan (Hanya Jumlah Pembelian)
              </Text>
            </View>
            <InputText
              containerStyle={{marginHorizontal: 15}}
              placeholder="cnth: 5"
              keyboardType="number-pad"
              value={titleWholePrice}
              onChangeText={text => setTitleWholePrice(text)}
            />
            <View
              style={{
                width: '100%',
                paddingHorizontal: 15,
                justifyContent: 'space-between',
                flexDirection: 'row',
                marginTop: 15,
              }}>
              <InputText
                containerStyle={{width: '70%'}}
                placeholder="Masukkan Harga Borongan"
                value={startPrice}
                keyboardType="number-pad"
                onChangeText={text => setStartPrice(inputRupiah(text))}
              />
              <TouchableOpacity
                style={styles.btnAddPrice}
                onPress={() => {
                  if (titleWholePrice !== '' && startPrice !== '') {
                    const data = [...state.combinedvarian];
                    const datammultiprice = data[index].range;
                    var arr = [...datammultiprice];
                    arr.push({
                      qty: titleWholePrice,
                      price: startPrice.split('.').join(''),
                    });
                    setTitleWholePrice('');
                    setStartPrice('');
                    data[index].range = arr;
                    reducer({type: Nameuser.addCombinasi, payload: data});
                  } else {
                    showToastMessage(
                      'Harga dan jumlah borongan tidak boleh kosong',
                    );
                  }
                }}>
                <Image
                  source={IconAsset.IcPlus}
                  style={{width: 20, height: 20, tintColor: '#fff'}}
                />
              </TouchableOpacity>
            </View>
            <FlatList
              data={item.range}
              contentContainerStyle={{
                flexDirection: 'row',
                flexWrap: 'wrap',
              }}
              keyExtractor={(item, iindex) => iindex.toString()}
              renderItem={({item, iindex}) => {
                return (
                  <View
                    style={[
                      styles.catPicked,
                      {
                        flexDirection: 'row',
                        alignItems: 'flex-start',
                        justifyContent: 'space-between',
                      },
                    ]}>
                    <TextSemiBold
                      text={`${item.qty} - ${inputRupiah(item.price, 'Rp')}`}
                      color="#fff"
                      style={{marginRight: 15}}
                    />
                    <TouchableOpacity
                      style={styles.btnDeleteItem}
                      onPress={() => {
                        const data = [...state.combinedvarian];
                        const datammultiprice = data[index].range;
                        var arr = [...datammultiprice];
                        arr.splice(iindex, 1);
                        data[index].range = arr;
                        reducer({type: Nameuser.addCombinasi, payload: data});
                      }}>
                      <Image
                        source={IconAsset.IcClose}
                        style={{
                          width: 10,
                          height: 10,
                          tintColor: '#fff',
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                );
              }}
            />
          </>
        )}
        </View>)}
      </View>
    );
  };
  return (
    <Modal visible={showmodal} onRequestClose={onclosemodal}>
      <KeyboardAvoidingView
        enabled
        behavior={Platform.OS == 'ios' ? 'height' : null}
        keyboardVerticalOffset={Platform.OS === 'android' ? 0 : 0}
        style={styles.container}>
        <HeaderTitle
          title="Varian"
          back={() => {
            onclosemodal();
          }}
        />
        <View style={styles.maincontainer}>
          <FlatList
             ListHeaderComponentStyle={{marginBottom:normalize(10,'height')}}
             ListHeaderComponent={
             <View 
              style={{
                maxWidth:'40%',
                justifyContent:'flex-end',
                alignContent:'flex-end',
                alignSelf:'flex-end'
              }}
             >
                <Button
                onPress={() =>{
                  setmodalfull(true)
                }}
                containerStyle={{maxHeight:normalize(90,'height')}}
                textComponent={
                  <View
                    style={{
                      flexDirection: 'row',
                      paddingHorizontal: 10,
                      paddingVertical: 10,
                    }}>
                    <Icons.ClipboardNotesSVG size={15} color="#FFF" />
                    <Text
                      style={{
                        fontFamily: Fonts.mulishBold,
                        color: '#FFF',
                        fontSize: Size.scaleFont(12),
                        marginLeft: 10,
                        marginTop: 0,
                      }}>
                      Atur Sekaligus
                    </Text>
                  </View>
                }
                />

             </View>}
            showsVerticalScrollIndicator={false}
            data={state.combinedvarian}
            style={{maxHeight: '99%'}}
            ItemSeparatorComponent={
              <View style={{margin: normalize(5, 'height')}} />
            }
            renderItem={renderItem}
            ListFooterComponentStyle={{
              marginVertical: normalize(15, 'height'),
            }}
            ListFooterComponent={
              <View
                style={{
                  width: '90%',
                  justifyContent: 'center',
                  alignContent: 'center',
                  alignSelf: 'center',
                }}>
                <Button
                  onPress={async () => {
                    const data = [...state.combinedvarian];
                    const multipriceYesData = data.filter(
                      item => item.multiprice === 'yes',
                    );
                    debugConsoleLog('spike',multipriceYesData)
                    if (multipriceYesData.length === 0) {
                      const isStokEmpty = data.some(item => item.stok === '');
                      const isPriceEmpty = data.some(item => item.price === '');
                     
                      if (isStokEmpty === true) {
                        showToastMessage('Stok varian tidak boleh kosong')
                      }else if(isPriceEmpty === true){
                        showToastMessage('Stok varian tidak boleh kosong')
                      }else{
                        if(onupdate === true){
                          const res = await postData2(data);

                        }else{
                          const res = await postData(data);

                        }                      }
                    } else {
                      const isRangeEmpty = multipriceYesData.some(
                        item => item.range.length === 0,
                      );
                      debugConsoleLog('daataku',isRangeEmpty)
                      if (isRangeEmpty === true) {
                        showToastMessage('Harga Borongan Tidak Boleh Kosong');
                      } else {
                        const isStokEmpty = data.every(
                          item => item.stok === '',
                        );
                        const isPriceEmpty = data.every(
                          item => item.price === '',
                        );
                        if (isStokEmpty === true) {
                          showToastMessage('Stok varian tidak boleh kosong')
                        }else if(isPriceEmpty === true){
                          showToastMessage('Stok varian tidak boleh kosong')
                        }else{
                          if(onupdate === true){
                            const res = await postData2(data);

                          }else{
                            const res = await postData(data);

                          }
                        }
                        ///
                      }
                    }
                  }}
                  title={'Simpan'}
                />
              </View>
            }
          />
        </View>
        <Modal
            transparent
            visible={showmodalfull}
            onRequestClose={() => {
              setmodalfull(false);
            }}>
            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{justifyContent: 'flex-end', flexGrow: 1}}>
              <View style={styles.containerModal}>
                <View
                  style={{
                    backgroundColor: 'white',
                    borderTopStartRadius: 12,
                    borderTopEndRadius: 12,
                  }}>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      paddingTop: Size.scaleSize(10),
                      paddingHorizontal: Size.scaleSize(10),
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <TextBold
                      text={`Atur Sekaligus`}
                      color={Colors.blackBase}
                      size={16}
                    />
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => {
                        setmodalfull(false);
                      }}>
                      <Image
                        source={IconAsset.IcClose}
                        style={{width: 25, height: 25}}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                  </View>
                  <View
                    style={{
                      marginTop: 15,
                      marginBottom: Size.scaleSize(5),
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: '#333',
                      }}>
                      Masukan stok varian
                    </Text>
                  </View>
                  <InputText
                    containerStyle={{marginHorizontal: 15}}
                    placeholder="Masukan Nama Sub Variant"
                    value={datasubadd.stok}
                    keyboardType="number-pad"
                    onChangeText={text => {
                      setdatasubadd(prevState => ({...prevState, stok: text}));
                    }}
                  />
                  <View
                    style={{
                      marginTop: 15,
                      marginBottom: Size.scaleSize(5),
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: '#333',
                      }}>
                      Masukan harga varian
                    </Text>
                  </View>
                  <InputText
                    containerStyle={{marginHorizontal: 15}}
                    keyboardType="number-pad"
                    placeholder="Masukan harga varian"
                    value={inputRupiah(datasubadd.price)}
                    onChangeText={text => {
                      setdatasubadd(prevState => ({...prevState, price: text}));
                    }}
                  />
                  <View
                    style={{
                      marginTop: 15,
                      marginBottom: Size.scaleSize(5),
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginHorizontal: 15,
                    }}>
                    <Text
                      style={{
                        fontFamily: Fonts.mulishExtraBold,
                        color: '#333',
                      }}>
                      Harga Borongan
                    </Text>
                    <Switch
                      value={datasubadd.multiprice}
                      trackColor={{
                        false: Colors.borderGrey,
                        true: Colors.primaryDark,
                      }}
                      thumbColor={
                        datasubadd.multiprice === true
                          ? Colors.primary
                          : '#f4f3f4'
                      }
                      onValueChange={() => {
                        if (datasubadd.multiprice === true) {
                          setDatamultiprice([]);
                        }
                        setdatasubadd(prevState => ({
                          ...prevState,
                          multiprice: !datasubadd.multiprice,
                        }));
                      }}
                    />
                  </View>
                  {datasubadd.multiprice === true && (
                    <>
                      <View
                        style={{
                          marginTop: 20,
                          marginBottom: 10,
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginHorizontal: 15,
                        }}>
                        <Image
                          source={IconAsset.IcPriceTag}
                          style={{
                            width: 24,
                            height: 24,
                            resizeMode: 'contain',
                            marginRight: 10,
                          }}
                        />
                        <Text
                          style={{
                            fontFamily: Fonts.mulishExtraBold,
                            color: '#333',
                            fontSize: 12,
                          }}>
                          Harga Borongan (Hanya Jumlah Pembelian)
                        </Text>
                      </View>
                      <InputText
                        containerStyle={{marginHorizontal: 15}}
                        placeholder="cnth: 5"
                        value={titleWholePrice}
                        onChangeText={text => setTitleWholePrice(text)}
                      />
                      <View
                        style={{
                          width: '100%',
                          paddingHorizontal: 15,
                          justifyContent: 'space-between',
                          flexDirection: 'row',
                          marginTop: 15,
                        }}>
                        <InputText
                          containerStyle={{width: '70%'}}
                          placeholder="Masukkan Harga Borongan"
                          value={startPrice}
                          keyboardType="number-pad"
                          onChangeText={text =>
                            setStartPrice(inputRupiah(text))
                          }
                        />
                        <TouchableOpacity
                          style={styles.btnAddPrice}
                          onPress={() => {
                            if (titleWholePrice !== '' && startPrice !== '') {
                              var arr = [...datammultiprice];
                              arr.push({
                                qty: titleWholePrice,
                                price: startPrice.split('.').join(''),
                              });
                              setDatamultiprice(arr);
                              setTitleWholePrice('');
                              setStartPrice('');
                            } else {
                              showToastMessage(
                                'Harga dan jumlah borongan tidak boleh kosong',
                              );
                            }
                          }}>
                          <Image
                            source={IconAsset.IcPlus}
                            style={{width: 20, height: 20, tintColor: '#fff'}}
                          />
                        </TouchableOpacity>
                      </View>
                      <FlatList
                        data={datammultiprice}
                        contentContainerStyle={{
                          flexDirection: 'row',
                          flexWrap: 'wrap',
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item, index}) => {
                          return (
                            <View
                              style={[
                                styles.catPicked,
                                {
                                  flexDirection: 'row',
                                  alignItems: 'flex-start',
                                  justifyContent: 'space-between',
                                },
                              ]}>
                              <TextSemiBold
                                text={`${item.qty} - ${inputRupiah(
                                  item.price,
                                  'Rp',
                                )}`}
                                color="#fff"
                                style={{marginRight: 15}}
                              />
                              <TouchableOpacity
                                style={styles.btnDeleteItem}
                                onPress={() => {
                                  var arr = [...datammultiprice];
                                  arr.splice(index, 1);
                                  setDatamultiprice(arr);
                                }}>
                                <Image
                                  source={IconAsset.IcClose}
                                  style={{
                                    width: 10,
                                    height: 10,
                                    tintColor: '#fff',
                                  }}
                                />
                              </TouchableOpacity>
                            </View>
                          );
                        }}
                      />
                    </>
                  )}
                  <Button
                    onPress={() => {
                      if (datasubadd.stok === '') {
                        showToastMessage('Stok varian tidak boleh kosong');
                      } else if (datasubadd.price === '') {
                        showToastMessage('Harga  varian tidak boleh kosong');
                      } else if (
                        datasubadd.multiprice === true &&
                        datammultiprice.length === 0
                      ) {
                        showToastMessage('Harga borongan tidak boleh kosong');
                      } else{
                        const data = [...state.combinedvarian];
                         const newDataArray = data.map(item => ({ ...item, 
                          show: false,
                          price :datasubadd.price,
                          multiprice :datasubadd.multiprice === true ?'yes':'no',
                          stok :datasubadd.stok,
                          range:datammultiprice
                        }));
                        setdatasubadd(prevState => ({...prevState, name: ''}));
                        setdatasubadd(prevState => ({...prevState, stok: ''}));
                        setdatasubadd(prevState => ({...prevState, price: ''}));
                        setdatasubadd(prevState => ({
                          ...prevState,
                          multiprice: false,
                        }));
                        setDatamultiprice([]);
                        reducer({type:Nameuser.addCombinasi,payload:newDataArray})
                        setmodalfull(false)

                      }
                    }}
                    containerStyle={{
                      marginHorizontal: Size.scaleSize(10),
                      marginVertical: Size.scaleSize(10),
                    }}
                    title={'Atur Sekaligus'}
                  />
                </View>
              </View>
            </ScrollView>
          </Modal>
      </KeyboardAvoidingView>
    </Modal>
  );
};

export default ModalCombination;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  contenrender: {
    borderRadius: normalize(5, 'height'),
    borderWidth: normalize(2, 'height'),
    padding: normalize(10, 'height'),
    borderColor: Colors.borderGrey,
  },
  imgfooter: {
    width: '100%',
    flex: 0.8,
    backgroundColor: 'white',
    borderBottomColor: Colors.grey_3,
    borderBottomWidth: normalize(5, 'height'),
    borderWidth: normalize(2, 'width'),
    borderColor: '#F2F4F5',
    height: undefined,
    resizeMode: 'cover',
    overflow: 'hidden',
    borderTopRightRadius: normalize(10, 'height'),
    borderTopLeftRadius: normalize(10, 'height'),
  },
  imgfooter2: {
    width: '100%',
    flex: 0.8,
    tintColor: Colors.primary,
    backgroundColor: 'white',
    borderBottomColor: Colors.grey_3,
    borderBottomWidth: normalize(5, 'height'),
    borderWidth: normalize(2, 'width'),
    borderColor: '#F2F4F5',
    resizeMode: 'contain',
    overflow: 'hidden',
    borderTopRightRadius: normalize(10, 'height'),
    borderTopLeftRadius: normalize(10, 'height'),
  },
  footerstylecontainer: {
    width: normalize(80, 'width'),
    margin: normalize(3, 'height'),
    height: normalize(80, 'height'),
    borderRadius: normalize(10, 'height'),
    borderWidth: normalize(4, 'width'),
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#F2F4F5',
  },
  txtfooter: {
    flex: 0.5,
    justifyContent: 'center',
    paddingTop: normalize(10, 'height'),
    textAlign: 'center',
    fontFamily: Fonts.mulishRegular,
    fontSize: normalize(10, 'height'),
    alignItems: 'flex-end',
  },
  rendercontainer: {
    flex: 1,
  },
  text: {
    fontFamily: Fonts.mulishRegular,
    color: 'white',
    backgroundColor: Colors.primaryDark,
    marginStart: normalize(3, 'width'),
    borderRadius: normalize(10, 'height'),
    maxWidth: normalize(150, 'width'),
    textAlign: 'center',
    padding: normalize(5, 'height'),
    paddingTop: normalize(5, 'height'),
    paddingHorizontal: normalize(10, 'width'),
    fontSize: normalize(12, 'height'),
  },
  maincontainer: {
    marginHorizontal: Size.scaleSize(8),
    marginTop: Size.scaleSize(4),
    marginBottom: normalize(30, 'height'),
  },
  txtInput: {
    width: '100%',
    height: 120,
    marginTop: 20,
    borderRadius: 6,
    borderColor: Colors.border,
    borderWidth: 1,
    textAlignVertical: 'top',
    padding: 10,
  },
  containerSub: {
    margin: 0,
    width: '100%',
    maxHeight: '80%',
  },
  btnSend: {
    marginTop: 20,
    width: '100%',
    borderRadius: 24,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
  },
  content: {
    width: '100%',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: '#fff',
    padding: 15,
  },
  catPicked: {
    marginLeft: normalize(10, 'width'),
    marginTop: normalize(10, 'height'),
    padding: 7,
    borderRadius: 4,
    backgroundColor: Colors.secondary,
  },
  catPicked2: {
    marginLeft: normalize(10, 'width'),
    marginTop: normalize(10, 'height'),
    padding: 7,
    borderRadius: 4,
    backgroundColor: Colors.grey_3,
  },
  btnAddPrice: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 15,
    backgroundColor: Colors.primary,
    borderRadius: 6,
  },
  containerModal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
  },
});
