import React from 'react';
import {
  Modal,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Text,
  Platform,
  KeyboardAvoidingView,
  StyleSheet,
} from 'react-native';
import {TextBold, TextRegular} from '@components/global';
import {Colors} from '@styles/index';
import {IconAsset} from '@utils/index';
import {FlatList} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import { useCallback } from 'react';
import { debugConsoleLog } from '@helpers/functionFormat';
import { useFocusEffect } from '@react-navigation/native';

const ModalChatAdmin = ({
  show,
  onClose,
  message,
  sendTo = 'admin',
  daata ,
  setMessage,
  onSubmit,
}) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.userData);
  const chatUser = ['Apakah barang ini ada ?', 'Apakah bisa kirim hari ini ?']
  const chatSeller = ['Barang ini ready stok, silakan order', 'Mohon maaf barang ini sementara kosong','Kami cek ketersediaan stok terlebih dahulu, segera kami infokan kembali']

  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <KeyboardAvoidingView
        enabled
        behavior="padding"
        keyboardVerticalOffset={Platform.OS === 'android' ? -500 : 100}
        style={styles.container}>
        <View style={styles.content}>
          <View
            style={{
              width: '100%',
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <TextBold
              text={`Kirim pesan ke ${sendTo}`}
              size={16}
              color={Colors.blackBase}
            />
            <TouchableOpacity onPress={onClose}>
              <Image
                source={IconAsset.IcClose}
                style={{width: 20, height: 20}}
              />
            </TouchableOpacity>
          </View>
          {/* <TextRegular
                        text={`Tulis pesan anda untuk ${sendTo}`}
                        style={{marginTop: 24}}
                    /> */}
          <Text style={{color: Colors.blackBase, marginTop: 24}}>
            Tulis pesan anda untuk{' '}
            <Text style={{fontWeight: 'bold', color: Colors.blackBase}}>
              {sendTo}
            </Text>
          </Text>
          <TextInput
            multiline
            placeholder="Tulis pesan..."
            value={message}
            onChangeText={text => setMessage(text)}
            style={styles.txtInput}
          />
          {sendTo !== 'admin' && (
            <FlatList
              showsHorizontalScrollIndicator={false}
              style={{height: 60, marginTop: 16}}
              horizontal={true}
              data={user?.is_seller == "no" || user?.is_seller == undefined ? chatUser : chatSeller}
              renderItem={({item}) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      setMessage(item);
                    }}>
                    <View
                      style={{
                        height: 45,
                        borderRadius: 30,
                        marginEnd: 8,
                        backgroundColor: Colors.primary,
                        paddingHorizontal: 12,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <TextBold text={item} size={12} color="#fff" />
                    </View>
                  </TouchableOpacity>
                );
              }}
              keyExtractor={item => item.toString()}
            />
          )}
          <TouchableOpacity
            style={[
              styles.btnSend,
              message === '' && {backgroundColor: Colors.grey_1},
            ]}
            onPress={onSubmit}
            disabled={message === ''}>
            <TextBold text="Kirim Pesan" size={16} color="#fff" />
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  txtInput: {
    width: '100%',
    height: 120,
    marginTop: 20,
    borderRadius: 6,
    borderColor: Colors.border,
    borderWidth: 1,
    textAlignVertical: 'top',
    padding: 10,
  },
  btnSend: {
    marginTop: 20,
    width: '100%',
    borderRadius: 24,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
  },
  content: {
    width: '100%',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: '#fff',
    padding: 15,
  },
});

export default ModalChatAdmin;
