import React from 'react'
import {
    View,
    Modal,
    StyleSheet,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView,
    Keyboard,
} from 'react-native'
import {TextBold, InputText} from '@components/global'
import {Colors} from '@styles/index'
import {IconAsset} from '@utils/index'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'

const ModalPassword = ({
    show,
    onClose,
    password,
    setPassword,
    onSubmit
}) => {
    const [hidePassword, setHidePassword] = React.useState(true)
    return(
        <Modal
            transparent
            visible={show}
            onRequestClose={onClose}
        >
            <KeyboardAvoidingView 
             behavior='padding'
             keyboardVerticalOffset={Platform.OS === 'android' ? 0 : 0}
            style={styles.container}>
            <View style ={styles.content}>
            <TouchableOpacity
                        style={styles.btnClose}
                        onPress={onClose}
                    >
                        <Image
                            source={IconAsset.DoubleArrowDown}
                            style={{width: 18, height: 18, resizeMode: 'contain'}}
                        />
                    </TouchableOpacity>
                <TouchableWithoutFeedback 
                onPress={() =>{Keyboard.dismiss()}}>
                    <TextBold
                        text="Masukkan Password Anda"
                        size={16}
                        color={Colors.blackBase}
                        style={{marginTop: 24}}
                    />
                    <InputText
                        isPassword={true}
                        secureTextEntry={hidePassword}
                        setSecureTextEntry={(val) => setHidePassword(val)}
                        placeholder="Masukkan password"
                        style={{paddingHorizontal: 10}}
                        containerInputStyle={{paddingRight: 10, marginTop: 24}}
                        value={password}
                        onChangeText={(text) => setPassword(text) }
                    />
                  
                </TouchableWithoutFeedback>
                <TouchableOpacity
                        style={[styles.btnSubmit, password === '' && {backgroundColor: Colors.grey_1}]}
                        disabled={password === ''}
                        onPress={onSubmit}
                    >
                        <TextBold
                            text="Konfirmasi"
                            size={16}
                            color="#fff"
                        />
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        </Modal>
    )
}

const styles = StyleSheet.create({
    btnSubmit: {
        width: '100%',
        marginTop: 32,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 17,
        backgroundColor: Colors.primary
    },
    container: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    content: {
        width: '100%',
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        backgroundColor: '#fff',
        padding: 18
    },
    btnClose: {
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F7F9F8',
        borderRadius: 8,
        paddingVertical: 8,
        paddingHorizontal: 64
    }
})

export default ModalPassword