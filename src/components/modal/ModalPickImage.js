import React from 'react'
import {
    View,
    TouchableOpacity,
    Image,
    StyleSheet
} from 'react-native'
import Modal from 'react-native-modal'
import {TextBold, TextRegular} from '@components/global'
import { Icons, Colors } from '@styles/index';
import {IconAsset} from '@utils/index'

const ModalPickImage = ({
    show,
    onClose,
    onPressCamera,
    onPressGalery
}) => {
    return(
        <Modal
            style={{margin: 0, justifyContent: 'flex-end',}}
            isVisible={show}
            onBackButtonPress={onClose}
            onBackdropPress={onClose}
            animationIn="slideInUp"
        >
            <View style={styles.container}>
                <TextBold
                    text="Pilih gambar dari ?"
                    size={16}
                    color={Colors.blackBase}
                    style={{marginLeft: 16}}
                />
                <TouchableOpacity 
                    activeOpacity={0.8}
                    onPress={onPressCamera}
                    style={styles.cardItem}>
                    <Image
                        source={IconAsset.IC_CAMERA}
                        style={{width: 25, height: 25, resizeMode: 'contain'}}
                    />
                    <TextRegular
                        text="Pilih dari kamera"
                        style={{marginLeft: 10}}
                    />
                </TouchableOpacity>
                <TouchableOpacity 
                    activeOpacity={0.8}
                    onPress={onPressGalery}
                    style={styles.cardItem}>
                    <Image
                        source={IconAsset.IC_FOLDER}
                        style={{width: 25, height: 25, resizeMode: 'contain'}}
                    />
                    <TextRegular
                        text="Pilih dari galery"
                        style={{marginLeft: 10}}
                    />
                </TouchableOpacity>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    cardItem: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 15,
        paddingHorizontal: 15,
        borderBottomColor: Colors.grey_1,
        borderBottomWidth: 1,
        paddingVertical: 15
    },
    container: {
        width: '100%',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: '#fff',
        paddingTop: 10,
        alignSelf: 'flex-end'
    },
})

export default ModalPickImage