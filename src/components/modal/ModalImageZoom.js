import React, {useState} from 'react';
import {
  Modal,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Text,
  Platform,
  KeyboardAvoidingView,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {Colors} from '@styles/index';
import ImageViewer from 'react-native-image-zoom-viewer';
import {IconAsset} from '@utils/index';

const ModalImageZoom = ({show, onClose, imageUrl}) => {
  const [screenHeight, setScreenHeight] = useState(
    Dimensions.get('window').height,
  );
  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={{flex: 1, backgroundColor: 'black'}}>
        <ImageViewer imageUrls={imageUrl} />
        <TouchableOpacity
          style={{
            width: 20,
            height: 20,
            marginTop: 20,
          }}
          onPress={onClose}>
          <Image
            source={IconAsset.IC_CHEVRON_LEFT}
            style={{
              width: 25,
              height: 25,
              tintColor: Colors.primary,
              marginTop: screenHeight * -1 + 50,
            }}
          />
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  txtInput: {
    width: '100%',
    height: 120,
    marginTop: 20,
    borderRadius: 6,
    borderColor: Colors.border,
    borderWidth: 1,
    textAlignVertical: 'top',
    padding: 10,
  },
  btnSend: {
    marginTop: 20,
    width: '100%',
    borderRadius: 24,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
  },
  content: {
    width: '100%',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: '#fff',
    padding: 15,
  },
});

export default ModalImageZoom;
