import React from 'react'
import {
    View,
    Modal,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native'
import {TextBold, TextRegular} from '@components/global'
import {Colors} from '@styles/index'
import {IconAsset} from '@utils/index'

const ModalMessage = ({
    show,
    onClose,
    message
}) => {
    return(
        <Modal
            transparent
            visible={show}
            onRequestClose={onClose}
        >
            <View style={styles.container}>
                <View style={styles.content}>                    
                    <View style={styles.title}>
                        <TextBold
                            text="Detail Pesan"
                            color={Colors.blackBase}
                        />
                        <TouchableOpacity
                            style={{padding: 10}}
                            onPress={onClose}
                        >
                            <Image
                                source={IconAsset.IcClose}
                                style={{width: 18, height: 18, tintColor: Colors.blackBase}}
                            />
                        </TouchableOpacity>
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false}
                        contentContainerStyle={{paddingHorizontal: 15}}
                    >
                        <TextRegular
                            // text="Rebum vero no dolore at lorem sed ipsum. Dolore diam rebum at at sed est kasd. Et invidunt aliquyam nonumy sanctus et voluptua, et stet erat dolor est amet diam dolore rebum, aliquyam amet takimata dolore et est et duo. Sit nonumy invidunt amet gubergren lorem kasd dolore dolore diam. Invidunt clita sea dolor ipsum. Voluptua est et elitr sadipscing kasd stet, justo gubergren aliquyam labore ut sit tempor nonumy duo, amet sed dolores et stet sea, eirmod justo sed sit takimata erat lorem amet nonumy dolor, takimata labore ut dolore ipsum magna sit. Et elitr diam ut et no."
                            text={message}
                            numberOfLines={1000}
                        />
                    </ScrollView>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    title: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 15
    },
    content: {
        width: '100%',
        maxHeight: Dimensions.get('window').height*0.5,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: '#fff',
        paddingVertical: 10
    },
    container: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    }
})

export default ModalMessage;