import React from 'react';
import {Image, StyleSheet, Text} from 'react-native';
import {View} from 'react-native';
import {Button} from '@components/global';
import {IconAsset, ImageAsset} from '@utils/index';
import {Colors, Size} from '@styles/index';
import Modal from 'react-native-modal';

const ModalHapusPesanan = ({show, img, Onpressyes, onClose, Onpressno,onSwipeComplete}) => {
  return (
    <Modal
      transparent
      visible={show}
      onRequestClose={onClose}
      onSwipeComplete={onSwipeComplete}
      swipeDirection="down"
      style={{margin:0}}
      >
      <View style={styles.content}>
      <View style={styles.midcontain}>
      <Image
        source={IconAsset.DoubleArrowDown}
          style={{width: 8, height: 10, resizeMode: 'cover'}}
        />
      </View>
      <Image
        source={img}
          style={styles.imageup}
        />
        <Text style={styles.texttitle}>
          Yakin anda menghapus{'\n'}produk ini dari daftar
        </Text>
        <View style={{width:'100%',justifyContent:'center',flexDirection:'column',marginTop:10,}}>
        <Button
            containerStyle={{
              width:"100%",
              height: Size.scaleSize(45),
              alignItems: 'center',
              backgroundColor: '#FFF',
              borderRadius: 15,
              borderColor: Colors.primary,
              borderWidth: 2,
            }}
          style={{paddingHorizontal: Size.scaleSize(5)}}
          textStyle={{
            paddingVertical: Size.scaleSize(8),
            fontSize: Size.scaleFont(12),
            color: Colors.primary
          }}
          onPress={Onpressyes}
            title="Ya"
        />
        <View style={{margin:3}}></View>
        <Button
            containerStyle={{
              width: "100%",
              height: Size.scaleSize(45),
              alignSelf: 'center',
            }}
          style={{paddingHorizontal: Size.scaleSize(25)}}
          textStyle={{
            paddingVertical: Size.scaleSize(8),
            fontSize: Size.scaleFont(12),
          }}
          onPress={Onpressno}
          title="Tidak"
        />
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  midcontain: {
    width: 136,
    height: 26,
    position:'relative',
    marginBottom:10,
    backgroundColor:'#F7F9F8',
    alignItems:'center',
    justifyContent:'center',
    borderRadius: 5,
  },
  imageup: {width: 150, height: 150, resizeMode: 'cover', marginBottom: 5},
  texttitle: {
    marginBottom: 5,
    textAlign:'center',
  },
  content: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    borderColor:'#C9454A',
    alignContent:'center',
    alignItems:'center',
    borderWidth:2,
    backgroundColor: '#fff',
    padding: 18,
  },
});
export default ModalHapusPesanan;

