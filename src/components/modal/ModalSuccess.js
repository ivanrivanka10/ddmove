import React from 'react'
import {
    Modal,
    View,
    Image,
    StyleSheet
} from 'react-native'
import {
    TextSemiBold
} from '@components/global'
import {IconAsset} from '@utils/index'

const ModalSuccess = ({
    show,
    message,
    onClose
}) => {
    return(
        <Modal
            transparent
            visible={show}
            onRequestClose={onClose}
        >
            <View style={styles.container}>
                <View style={{
                    width: '80%',
                    paddingVertical: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#5F2527',
                    borderRadius: 30
                }}>
                    <Image
                        source={IconAsset.IcDone}
                        style={{
                            width: 100,
                            height: 100,
                            resizeMode: 'contain'
                        }}
                    />
                    <TextSemiBold
                        text={message}
                        color="#fff"
                        style={{
                            maxWidth: '60%',
                            textAlign: 'center',
                            marginTop: 25
                        }}
                    />
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)'
    }
})

export default ModalSuccess