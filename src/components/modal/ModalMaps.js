import React from 'react';
import {Modal, View, Image, StyleSheet} from 'react-native';
import {TextSemiBold} from '@components/global';
import {IconAsset} from '@utils/index';
import MapView, {Marker} from 'react-native-maps';
import {Button, InputText, HeaderTitle} from '@components/global';

const ModalMaps = ({show, data, close,setData, message, onClose}) => {
  const [lat, setLat] = React.useState(-6.9632193);
  const [long, setLong] = React.useState(107.6848131);

  React.useEffect(() => {
    setLat(data.lat);
    setLong(data.long);
  }, []);

  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={styles.container}>
        <View
          style={{
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'white',
          }}>
          <MapView
            style={{width: '100%', height: '100%'}}
            initialRegion={{
              latitude: data.lat,
              longitude: data.long,
              latitudeDelta: 0.005,
              longitudeDelta: 0.005,
            }}
            onPress={e => {
              setLat(e.nativeEvent.coordinate.latitude);
              setLong(e.nativeEvent.coordinate.longitude);
            }}
            onRegionChange={e => {
              setLat(e.latitude);
              setLong(e.longitude);
            }}>
            <Marker coordinate={{latitude: lat, longitude: long}} />
          </MapView>

          <Button
            // disabled={localImgPath === ''}
            containerStyle={{
              position: 'absolute',
              bottom: 10,
              width: '90%',
              left: '5%',
              // backgroundColor: localImgPath === '' ? Colors.grey_1 : Colors.primary
            }}
            title="Tandai Lokasi"
            onPress={() => {
              setData({lat : lat,long : long})
              close();
            }}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
});

export default ModalMaps;
