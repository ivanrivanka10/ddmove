import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/core';

import { animationHorizontal } from './animation';
import { Colors, Fonts, Size, Icons } from '@styles/index';
import DashboardScreen, { screenOptions as dashboardOptions } from '@screens/main/seller/DashboardScreen';
import TarikDanaScreen, { screenOptions as tarikDanaOptions } from '@screens/main/seller/TarikDanaScreen';
import DetailTarikDanaScreen, { screenOptions as detailTarikDanaOptions } from '@screens/main/seller/DetailTarikDanaScreen';
import SellingScreen, { screenOptions as sellingOptions } from '@screens/main/seller/SellingScreen';
import MyProductListScreen, { screenOptions as myProductListOptions } from '@screens/main/seller/MyProductListScreen';
import ChatScreen, { screenOptionstwo as chatOptions } from '@screens/main/ChatScreen';
import ChatDetailScreen, { screenOptions as chatDetailOptions } from '@screens/main/ChatDetailScreen';
import SellerScreen, { screenOptions as sellerOptions } from '@screens/main/seller/SellerScreen';
import ProfileScreen, { screenOptions as profileOptions } from '@screens/main/buyer/account/ProfileScreen';
import InputProductScreen, { screenOptions as inputProductOptions } from '@screens/main/seller/InputProductScreen';

import SellerTransactionDetail, { screenOptions as sellerTransactionDetailOptions } from '@screens/main/seller/order/SellerTransactionDetail'

//setting seller
import ProfileShop, {screenOptions as profileShopOptions} from '@screens/main/seller/setting/profileShop/ProfileShop'
import PaymentAccount, {screenOptions as paymentAccountOptions} from '@screens/main/seller/setting/paymentAccount/PaymentAccount'
import AddNewPaymentAccount, {screenOptions as addPaymentAccountOptions} from '@screens/main/seller/setting/paymentAccount/AddNewPaymentAccount';
import AboutUs, {screenOptions as aboutUsOption} from '@screens/main/buyer/account/AboutUs'
import ContactUs, {screenOptions as contactUsOption} from '@screens/main/buyer/account/ContactUs'
import TermAndCondition, {screenOptions as snkUsOption} from '@screens/main/buyer/account/TermAndCondition'
import Coverage from '@screens/main/seller/setting/coverage/Coverage';
import AddCoverageArea from '@screens/main/seller/setting/coverage/AddCoverageArea';

import Notification from '@screens/main/buyer/notification/Notification';
import InputProductsScreendua from '@screens/main/seller/InputProductsScreendua';

const Stack = createStackNavigator();
const BottomTab = createBottomTabNavigator();

const TabNavigation = props => {
    return (
        <BottomTab.Navigator
            tabBarOptions={{
                activeTintColor: Colors.primary,
                inactiveTintColor: '#CDCFD0',
                labelStyle: {
                    fontFamily: Fonts.mulishRegular,
                    fontSize: Size.scaleFont(12)
                },
                allowFontScaling: false,
                style: {
                    height: Size.scaleSize(50),
                    paddingTop: Size.scaleSize(5)
                },
                keyboardHidesTabBar: true,
                tabBarHideOnKeyboard: true
            }}
            
        >
            <BottomTab.Screen 
                name="DashboardSellerTab"
                component={HomeStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "DashboardSeller";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Dashboard",
                        tabBarIcon: props => (
                            <Icons.HomeSVG
                                size={21}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="SellingTab"
                component={SellingStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "Selling";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Penjualan",
                        tabBarIcon: props => (
                            <Icons.ClipboardNotesSVG
                                size={28}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="MyProductTab"
                component={ProductStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "MyProductList";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Produk Saya",
                        tabBarIcon: props => (
                            <Icons.BoxSVG
                                size={30}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="SellerTab"
                component={SellerStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "Seller";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Pengaturan",
                        tabBarIcon: props => (
                            <Icons.StoreSVG
                                size={24}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
        </BottomTab.Navigator>
    )
}

const HomeStack = ({navigation, route}) => {
    return (
        <Stack.Navigator screenOptions={{
            // headerShown: route?.state ? route?.state?.index === 0 ? true : false : true,
            ...animationHorizontal
        }}>
            
            <Stack.Screen 
                name="DashboardSeller"
                component={DashboardScreen}
                options={{ headerShown: false }}
                // options={dashboardOptions}
            />
            <Stack.Screen 
                name="TarikDana"
                component={TarikDanaScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen 
                name="DetailTarikDana"
                component={DetailTarikDanaScreen}
                options={detailTarikDanaOptions}
            />
            <Stack.Screen 
                name="ChatSeller"
                component={ChatScreen}
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatDetail"
                component={ChatDetailScreen}
                options={{ headerShown: false }}

            />
            <Stack.Screen 
                name="Notification"
                options={{ headerShown: false }}
                component={Notification}
            />
        </Stack.Navigator>
    )
}

const SellingStack = ({navigation, route}) => {
    return (
        <Stack.Navigator screenOptions={{
            ...animationHorizontal
        }}>
            
            <Stack.Screen 
                name="Selling"
                component={SellingScreen}
                options={{headerShown:false}}
            />
            <Stack.Screen 
                name="SellerTransactionDetail"
                component={SellerTransactionDetail}
                options={{headerShown:false}}
            />
            <Stack.Screen 
                name="ChatSeller"
                component={ChatScreen}
                
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatDetail"
                component={ChatDetailScreen}
                options={{ headerShown: false }}

            />
        </Stack.Navigator>
    )
}

const ProductStack = () => {
    return (
        <Stack.Navigator screenOptions={{
            ...animationHorizontal
        }}>
            <Stack.Screen 
                name="MyProductList"
                component={MyProductListScreen}
                options={{headerShown:false}}
            />
            <Stack.Screen 
                name="ChatSeller"
                component={ChatScreen}
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatDetail"
                component={ChatDetailScreen}
                options={{ headerShown: false }}

            />
            <Stack.Screen 
                name="InputProduct"
                component={InputProductScreen}
                options={inputProductOptions}
            />
        </Stack.Navigator>
    )
}

const SellerStack = ({navigation, route}) => {
    return (
        <Stack.Navigator screenOptions={{
            ...animationHorizontal
        }}>
            <Stack.Screen 
                name="Seller"
                component={SellerScreen}
                options={{headerShown:false}}
            />
            <Stack.Screen 
                name="ChatSeller"
                component={ChatScreen}
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatDetail"
                component={ChatDetailScreen}
                options={{ headerShown: false }}

            />
            <Stack.Screen 
                name="Profile"
                component={ProfileScreen}
                options={{ headerShown: false }}

            />
            <Stack.Screen 
                name="ProfileShop"
                component={ProfileShop}
                options={{ headerShown: false }}
            />
            <Stack.Screen 
                name="PaymentAccount"
                component={PaymentAccount}
                options={{ headerShown: false }}
            />
            <Stack.Screen 
                name="AddPaymentAccount"
                component={AddNewPaymentAccount}
                options={addPaymentAccountOptions}
            />
            <Stack.Screen 
                name="AboutUs"
                component={AboutUs}
                options={aboutUsOption}
            />
            <Stack.Screen 
                name="ContactUs"
                component={ContactUs}
                options={{ headerShown: false }}
            />
            <Stack.Screen 
                name="TermCond"
                component={TermAndCondition}
                options={{ headerShown: false }}
            />
            <Stack.Screen 
                name="Coverage"
                component={Coverage}
                options={{ headerShown: false }}

            />
            <Stack.Screen 
                name="AddCoverageArea"
                component={AddCoverageArea}
                options={{ headerShown: false }}

            />
        </Stack.Navigator>
    )
}


export default TabNavigation;