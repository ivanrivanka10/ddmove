import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/core';

import { Icons, Fonts, Colors, Size } from '@styles/index';
import { animationHorizontal } from './animation';
import HomeScreen, { screenOptions as homeOptions } from '@screens/main/HomeScreen';
import OrderScreen, { screenOptions as orderOptions } from '@screens/main/OrderScreen';
import ChatScreen, { screenOptions as chatOptions } from '@screens/main/ChatScreen';
import UserScreen, { screenOptions as userOptions } from '@screens/main/UserScreen';
import CategoryScreen, { screenOptions as categoryOptions } from '@screens/main/buyer/home/CategoryScreen';
import ChatDetailScreen, { screenOptions as chatDetailOptions } from '@screens/main/ChatDetailScreen';
import ProductScreen, { screenOptions as productOptions } from '@screens/main/ProductScreen';
// import TestScreen from '@screens/main/TestScreen';
import ProfileScreen, { screenOptions as profileOptions } from '@screens/main/buyer/account/ProfileScreen';
import AddressScreen, { screenOptions as addressOptions } from '@screens/main/AddressScreen';
import StoreScreen, { screenOptions as storeOptions } from '@screens/main/StoreScreen';
import CartScreen, { screenOptions as cartOptions } from '@screens/main/CartScreen';
import AllProducts from '@screens/main/buyer/home/AllProducts';
import AllShop from '@screens/main/buyer/home/AllShop';
import DetailStore from '@screens/main/buyer/store/DetailStore';
import DetailCategory from '@screens/main/buyer/home/DetailCategory';

import AllCategory from '@screens/main/buyer/home/AllCategory';
import AddNewAddress, { screenOptions as addNewAddressOptions } from '@screens/main/buyer/account/AddNewAddress';

//akun buyer
import AboutUs, {screenOptions as aboutUsOption} from '@screens/main/buyer/account/AboutUs'
import ContactUs, {screenOptions as contactUsOption} from '@screens/main/buyer/account/ContactUs'
import TermAndCondition, {screenOptions as snkUsOption} from '@screens/main/buyer/account/TermAndCondition'

//payment buyer
import Payment, {screenOptions as paymentOptions} from '@screens/main/buyer/payment/Payment'

//order buyer
import TransactionDetail, {screenOptions as transactionDetailOptions} from '@screens/main/buyer/order/TransactionDetail'
import Paynow, {screenOptions as paynowOptions} from '@screens/main/buyer/order/Paynow';

import SearchScreen from '@screens/main/buyer/home/SearchScreen';

import Notification from '@screens/main/buyer/notification/Notification';

const BottomTab = createBottomTabNavigator();
const Stack = createStackNavigator();

const TabNavigation = props => {
    return (
        <BottomTab.Navigator
            tabBarOptions={{
                activeTintColor: Colors.primary,
                inactiveTintColor: '#CDCFD0',
                labelStyle: {
                    fontFamily: Fonts.mulishRegular,
                    fontSize: Size.scaleFont(12)
                },
                allowFontScaling: false,
                style: {
                    height: Size.scaleSize(50),
                    paddingTop: Size.scaleSize(5)
                },
                keyboardHidesTabBar: true,
                tabBarHideOnKeyboard: true
            }}
            
        >
            <BottomTab.Screen 
                name="HomeTab"
                component={HomeStack}                
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "Home";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Beranda",
                        tabBarIcon: props => (
                            <Icons.HomeSVG
                                size={21}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="OrderTab"
                component={OrderStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "Order";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Pesanan",
                        tabBarIcon: props => (
                            <Icons.OrderSVG
                                size={21}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="ChatTab"
                component={ChatStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "Chat";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Obrolan",
                        tabBarIcon: props => (
                            <Icons.ChatSVG
                                size={21}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
            <BottomTab.Screen 
                name="UserTab"
                component={UserStack}
                options={({route}) => {
                    let tabBarVisible = true;
                    if(getFocusedRouteNameFromRoute(route)){
                        tabBarVisible = getFocusedRouteNameFromRoute(route) === "User";
                    }
                    return {
                        tabBarVisible: tabBarVisible,
                        title: "Akun",
                        tabBarIcon: props => (
                            <Icons.UserCircleSVG
                                size={24}
                                color={props.color}
                            />
                        )
                    }
                }}
            />
        </BottomTab.Navigator>
    )
}

const HomeStack = ({navigation, route}) => {
    console.log('route state home', getFocusedRouteNameFromRoute(route));
    return (
        <Stack.Navigator screenOptions={{
            headerShown: route?.state ? route?.state?.index === 0 ? true : getFocusedRouteNameFromRoute(route) === "Payment" ? false : false : true,
            ...animationHorizontal
        }}>
            <Stack.Screen 
                name="Home"
                component={HomeScreen}
                options={homeOptions}
            />
            <Stack.Screen 
                name="Category"
                // component={CategoryScreen}
                component={DetailCategory}
                options={categoryOptions}
            />
            <Stack.Screen 
                name="Product"
                component={ProductScreen}
                options={productOptions}
            />
            <Stack.Screen 
                name="Store"
                component={StoreScreen}
                options={storeOptions}
            />
            <Stack.Screen 
                name="Cart"
                component={CartScreen}
                options={cartOptions}
            />
            <Stack.Screen 
                name="Chat"
                component={ChatScreen}
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatDetail"
                component={ChatDetailScreen}
            />
            <Stack.Screen 
                name="AllCategory"
                component={AllCategory}
            />
            <Stack.Screen 
                name="Payment"
                component={Payment}
                options={paymentOptions}
            />
            <Stack.Screen 
                name="Address"
                component={AddressScreen}
                options={addressOptions}
            />
            <Stack.Screen 
                name="AddNewAddress"
                component={AddNewAddress}
                options={addNewAddressOptions}
            />
            <Stack.Screen 
                name="AllProducts"
                component={AllProducts}
            />
            <Stack.Screen 
                name="AllShop"
                component={AllShop}
            />
            <Stack.Screen 
                name="SearchScreen"
                component={SearchScreen}
            />
            <Stack.Screen 
                name="Notification"
                component={Notification}
            />
            <Stack.Screen 
                name="DetailStore"
                component={DetailStore}
            />
        </Stack.Navigator>
    )
}

const OrderStack = ({navigation, route}) => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: route?.state ? route?.state?.index === 0 ? true : false : true,
        }}>
            <Stack.Screen 
                name="Order"
                component={OrderScreen}
                options={orderOptions}
            />
            <Stack.Screen 
                name="TransactionDetail"
                component={TransactionDetail}
                options={transactionDetailOptions}
            />
            <Stack.Screen 
                name="Paynow"
                component={Paynow}
                options={paynowOptions}
            />
        </Stack.Navigator>
    )
}

const ChatStack = ({navigation, route}) => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: route?.state ? route?.state?.index === 0 ? true : false : true,
            ...animationHorizontal
        }}>
            <Stack.Screen 
                name="Chat"
                component={ChatScreen}
                options={chatOptions}
            />
            <Stack.Screen 
                name="ChatDetail"
                component={ChatDetailScreen}
            />
        </Stack.Navigator>
    )
}

const UserStack = ({navigation, route}) => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: route?.state ? route?.state?.index === 0 ? true : false : true,
            ...animationHorizontal
        }}>
            <Stack.Screen 
                name="User"
                component={UserScreen}
                options={userOptions}
            />
            <Stack.Screen 
                name="Profile"
                component={ProfileScreen}
                options={profileOptions}
            />
            <Stack.Screen 
                name="Address"
                component={AddressScreen}
                options={addressOptions}
            />
            <Stack.Screen 
                name="AddNewAddress"
                component={AddNewAddress}
                options={addNewAddressOptions}
            />
            <Stack.Screen 
                name="AboutUs"
                component={AboutUs}
                options={aboutUsOption}
            />
            <Stack.Screen 
                name="ContactUs"
                component={ContactUs}
                options={contactUsOption}
            />
            <Stack.Screen 
                name="TermCond"
                component={TermAndCondition}
                options={snkUsOption}
            />
        </Stack.Navigator>
    )
}

export default TabNavigation;