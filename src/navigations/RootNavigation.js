import React, {useEffect, useRef, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {useSelector, useDispatch} from 'react-redux';
import useFirebaseNotification from '@utils/hook/useNotification';
import Linking from '@utils/hook/deeplink';

import SplashScreen from '@screens/startup/SplashScreen';
import AuthNavigation from './AuthNavigation';
import MainNavigation from './MainNavigation';
import SellerNavigation from './SellerNavigation';
import AuthSellerNavigation from './AuthSellerNavigation';
import {Alert, Loader} from '@components/global';
import {showModal} from '@store/actions';

const Stack = createStackNavigator();
export const navigationRef = React.createRef();

export default function RootNavigation() {
  const routeNameRef = useRef();
  const {data} = useFirebaseNotification();
  const [navigationReady, setNavigationReady] = useState(false);
  const [proccess, setProccess] = useState(false);
  const [dataNotif, setDataNotif] = useState({});
  // const isAuth = useSelector(state => !!state.auth.token);
  // const didTryAutoLogin = useSelector(state => state.ui.didTryAutoLogin);

  // const modal = useSelector(state => state.ui.modal);
  const {userData} = useSelector(state => state.auth);
  const {loading} = useSelector(state => state.general);
  // const dispatch = useDispatch();

  const onNavigatorContainerStateChange = () => {
    try {
      const currentRouteName =
        navigationRef?.current.getCurrentRoute() === undefined
          ? ''
          : navigationRef.current.getCurrentRoute().name;

      routeNameRef.current = currentRouteName;
      // routingInstrumentation.onRouteWillChange({
      // name: currentRouteName,
      // op: 'navigation'
      // });
    } catch (e) {
      console.log('[err] navigator container state change', e);
    }
  };

  const onNavigatorReady = () => {
    try {
      routeNameRef.current = navigationRef.current.getCurrentRoute().name;
      // routingInstrumentation.registerNavigationContainer(navigationRef);
    } catch (err) {
      console.log('[err] navigator on ready', err);
    }
  };

  useEffect(() => {
    const setNotification = () => {
      const type = data?.type ?? data?.clickAction ?? '';
      if (!data) {
        return true;
      } else {
        switch (type) {
          case 'detail_chat':
            if (userData.is_seller === 'yes') {
              navigationRef.current.navigate('SellerNavigation', {
                screen: 'DashboardSellerTab',
                params: {
                  screen: 'ChatDetail',
                  params: {
                    chat: {
                      id: data?.value,
                    },
                  },
                },
              });
            } else {
              navigationRef.current.navigate('MainNavigation', {
                screen: 'HomeTab',
                params: {
                  screen: 'ChatDetail',
                  params: {
                    chat: {
                      id: data?.value,
                    },
                  },
                },
              });
            }
            break;
          case 'order_send':
            navigationRef.current.navigate('MainNavigation', {
              screen: 'OrderTab',
              params: {
                screen: 'Order',
                params: {
                  fromNotif: true,
                  tab: 1,
                },
              },
            });
            break;
          case 'payment_receive':
            navigationRef.current.navigate('MainNavigation', {
              screen: 'OrderTab',
              params: {
                screen: 'Order',
                params: {
                  fromNotif: true,
                  tab: 0,
                },
              },
            });
            break;
          case 'order_receive':
            if (userData.is_seller === 'yes') {
              navigationRef.current.navigate('SellerNavigation', {
                screen: 'SellingTab',
                params: {
                  screen: 'Selling',
                  params: {
                    fromNotif: true,
                    tab: 1,
                  },
                },
              });
            } else {
              navigationRef.current.navigate('MainNavigation', {
                screen: 'Home',
              });
            }
            break;
          case 'order_completed':
            if (userData.is_seller === 'yes') {
              navigationRef.current.navigate('SellerNavigation', {
                screen: 'SellingTab',
                params: {
                  screen: 'Selling',
                  params: {
                    fromNotif: true,
                    tab: 4,
                  },
                },
              });
            } else {
              navigationRef.current.navigate('MainNavigation', {
                screen: 'Home',
              });
            }
            break;
        }
      }
    };

    if (navigationReady) {
      setTimeout(() => setNotification(), 2100);
    }
  }, [data, navigationReady]);

  return (
    <>
      <Loader show={loading} />
      <NavigationContainer
        ref={navigationRef}
        linking={Linking}
        onStateChange={onNavigatorContainerStateChange}
        onReady={() => {
          onNavigatorReady();
          setNavigationReady(true);
        }}>
        <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen name="Splash" component={SplashScreen} />
          <Stack.Screen name="AuthNavigation" component={AuthNavigation} />
          <Stack.Screen name="MainNavigation" component={MainNavigation} />
          <Stack.Screen
            name="AuthSellerNavigation"
            component={AuthSellerNavigation}
          />
          <Stack.Screen name="SellerNavigation" component={SellerNavigation} />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

// export default RootNavigation;
