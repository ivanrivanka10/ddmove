export const DummyImg1 = require('@assets/images/dummy_img1.png');
export const Kulkas = require('@assets/images/dummy_kulkas.png');
export const LogoBca = require('@assets/images/logo-BCA.png');
export const SuccessPayment = require('@assets/images/shopping-bag.png');
export const DummyProduct = require('@assets/images/Product.png');
export const Placeholder = require('@assets/images/placeholder.png');