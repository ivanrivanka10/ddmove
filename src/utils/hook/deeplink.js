import { DEEP_LINK } from '../env';
const config = {
	screens: {
		Splash: {
			path: "/:screen/:data",
			parse: {
				screen: (screen) => `${screen}`,
				data: (data) => `${data}`,
			},
		},
		// Register: {
		// 	path: "register/",
		// },
	},
};

const Linking = {
	// prefixes: [DEEP_LINK],
	prefixes: ['babamarket://', DEEP_LINK],
	config,
};

export default Linking;