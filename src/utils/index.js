import * as IconAsset from './icon_assets';
import * as ImageAsset from './image_assets';

export {IconAsset, ImageAsset};