import {debugConsoleLog} from '@helpers/functionFormat';
import axios from 'axios';
import {BASE_URL} from '../env';

const API = async (
  url,
  options = {
    method: 'GET' || 'DELETE',
    body: {},
    head: {},
  },
) => {
  const request = {
    baseURL: BASE_URL,
    method: options.method,
    timeout: 10000,
    url,
    headers: options.head,
    responseType: 'json',
  };
  if (request.method === 'POST' || request.method === 'PUT')
    request.data = options.body;

  const res = await axios(request);
  if (res.status === 200) {
    // return res.data;
    return res;
  } else {
    alert('dataku');
    return res;
  }
};

export default API;
