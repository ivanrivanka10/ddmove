import API from '../axiosConfig';
import {debugConsoleLog, queryParams} from '@helpers/functionFormat';
import {showToastMessage} from '@helpers/functionFormat';

function handleResponse(response) {
  debugConsoleLog('responsenilai',response.status)
  // if(response.status === 401 || response.status === 403){
  //     ToastAndroid.showWithGravityAndOffset(
  //       "Session anda telah habis, silahkan login kembali",
  //       ToastAndroid.LONG,
  //       ToastAndroid.BOTTOM,
  //       25,
  //       50
  //     );
  //   return RootNavigation.forceLogout();
  // }else{
  //   return response;
  // }
  if (response.status === 500) {
    showToastMessage('Server Error');
    return response;
  } else {
    return response;
  }
}

export default {
  GetDataHome: async (value, token) => {
    console.log('=== cek value', typeof value === 'string');
    console.log('=== isi value', value);
    var url =
      typeof value === 'string'
        ? `home-seller?filter=${value}`
        : `home-seller?filter=${value} day`;
    return API(url, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  RequestWd: async (params, token) => {
    return API(`withdraw/request`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response));
  },
  CancelWd: async (id, params, token) => {
    return API(`withdraw/cancel/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => {
        console.log('err cancel wd', err);
        return handleResponse(err.response);
      });
  },
  GetHistoryWD: async token => {
    return API(`withdraw`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => {
        console.log('cek wd', err);
        return handleResponse(err.response);
      });
  },
  GetDetailHistoryWD: async (id, token) => {
    return API(`withdraw/${id}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
};
