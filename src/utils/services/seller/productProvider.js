import API from '../axiosConfig';
import {debugConsoleLog, queryParams} from '@helpers/functionFormat';

export default {
  GetListProduct: async (params, token) => {
    return API(`product-seller?${queryParams(params)}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      debugConsoleLog('datatime', `product-seller?${queryParams(params)}`);
      debugConsoleLog('datatime', `${token}`);
      debugConsoleLog('datatime', `${err}`);
      return err.response;
    });
  },
  GetDetailProduct: async (id, token) => {
    return API(`product-seller/${id}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  DeleteProduct: async (id, token) => {
    return API(`product-seller/delete/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  ArchiveProduct: async (id, token) => {
    return API(`product-seller/archive/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
};
