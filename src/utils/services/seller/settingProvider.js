import API from '../axiosConfig';
import {debugConsoleLog, queryParams} from '@helpers/functionFormat';

export default {
  GetListPaymentAccount: async token => {
    return API(`account-bank`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetListBank: async token => {
    return API(`list-bank`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  AddBankAccount: async (params, token) => {
    return API(`account-bank/add`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      return err.response;
    });
  },
  AddUpdatebank: async (params, id, token) => {
    return API(`account_bank/update/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      return err.response;
    });
  },
  DeleteBank: async (params, token) => {
    return API(`account_bank/delete/${params}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  AddCoverageArea: async (params, token) => {
    try {
      const response = await API(`area-coverage/in-city/add`, {
        method: 'POST',
        head: {
          'Content-Type': 'multipart/form-data',
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: params,
      });

      return response;
    } catch (err) {
      // Tangani kesalahan
      if (err.response) {
        // Jika respons server ada, tampilkan pesan kesalahan
        debugConsoleLog('niali', params);
        debugConsoleLog('Server error response:', err.response.data);
        return err.response;
      } else {
        // Jika tidak ada respons server, tampilkan pesan kesalahan lainnya
        debugConsoleLog('Error:', err.message);
        return err;
      }
    }
  },

  AddCoverageAreaSekitar: async (params, token) => {
    return API(`area-coverage/around-city/add`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      return err.response;
    });
  },
  GetDataShop: async token => {
    return API(`shop`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetRekomendasi: async (token, params, tp) => {
    return API(`search-categories?keyword=${params}&group=${tp}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetDataCoverage: async token => {
    return API(`area-coverage/in-city`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      debugConsoleLog('xxdar', token);
      return err.response;
    });
  },
  GetSubDistrict: async (params, token) => {
    return API(`/subdistrict/` + params, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetDataCoverageSekitar: async token => {
    return API(`area-coverage/around-city`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetDataExpedition: async token => {
    return API(`expedition`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  updateStatusExpedition: async (params, token) => {
    return API(`shop-expedition`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      return err.response;
    });
  },
  deleteRoute: async (id, token) => {
    return API(`area-coverage/in-city/del/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  deleteRouteSekitar: async (id, token) => {
    return API(`area-coverage/around-city/del/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
};
