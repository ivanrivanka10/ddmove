import API from '../axiosConfig';
import {debugConsoleLog, queryParams} from '@helpers/functionFormat';

export default {
  GetListOrder: async (params, token) => {
    return API(`seller-transactions?${queryParams(params)}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      debugConsoleLog('debugerro', err);
      return err.response;
    });
  },
  GetDetailOrder: async (params, token) => {
    return API(`seller-transactions-detail?${queryParams(params)}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetListCourier: async token => {
    return API(`courier-list`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  ChangeStatus: async (params, token) => {
    return API('update-order-status', {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      return err.response;
    });
  },
  
  AddProduct: async (params, token) => {
    return API('product-seller/add', {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('error add produk', err);
      console.log('error add produk', err.response.data);
      console.log('error add produk', err.response.data);
      console.log('error add produk', err.response.data);
      return err.response;
    });
  },
  AddProductoke: async (params, id,token) => {
    return API(`product-seller/add-ok/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('error add produk', err);
      console.log('error add produk', err.response.data);
      return err.response;
    });
  },
  AddProductnext: async (params,token) => {
    return API(`product-seller/add-variant-next`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('error add produk', err);
      console.log('error add produk', err.response.data);
      return err.response;
    });
  },
  UpdaateProductnext: async (params,token) => {
    return API(`product-seller/update-variant-next`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('error add produk', err);
      console.log('error add produk', err.response.data);
      return err.response;
    });
  },

  AddVariantsUpdate: async (params,id, token) => {
    return API(`product-seller/update-variant/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('error add produksss', err);
      console.log('error add produk', id);
      console.log('error add produk', params);
      console.log('error add produk', id);
      console.log('error add produk', token);
      return err.response;
    });
  },
  DeleteVariantsUpdate: async (params,id, token) => {
    return API(`product-seller/delete-variant/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      console.log('error add produk', err.response);
      console.log('error add produk', id);
      console.log('error add produk', params);
      return err.response;
    });
  },
  AddVariants: async (params,id, token) => {
    return API(`product-seller/add-variant/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('error add produk', err.response.data);
      return err.response;
    });
  },
  AddsubVariant: async (params,id, token) => {
    return API(`product-seller/add-variant-detail/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('error add produk', err.response.data);
      return err.response;
    });
  },
  GetVarints: async (id, token) => {
    return API(`product-seller/variant/${id}`, {
      method: 'GET',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      console.log('error add produk', err);
      console.log('error add produk', err.message);
      return err.response;
    });
  },
  UpdateProduct: async (params, id, token) => {
    return API(`product-seller/update/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('error add produk', err.response.data);
      return err.response;
    });
  },
  AddProductPre: async (params, token) => {
    return API(`product-seller/add-pre`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('error add produk', err.response.data);
      return err;
    });
  },
  UpdateProductSub: async (params,id, token) => {
    return API(`product-seller/update-variant-detail/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('error add produk', err.response.data);
      return err;
    });
  },
  DeleteFile: async (itemId, id, token) => {
    return API(`product-seller/delete-photo/${itemId}?id=${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      // body: params,
    }).catch(err => {
      console.log('error delete file', err);
      return err.response;
    });
  },
  DeleteVaariaan: async ( id, token) => {
    return API(`product-seller/delete-variant/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      console.log('error delete file', err);
      return err.response;
    });
  },
  DeletesubVaariaan: async ( id, token) => {
    return API(`product-seller/delete-variant-detail/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      console.log('error delete file', err);
      return err.response;
    });
  },
};
