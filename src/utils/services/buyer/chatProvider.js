import API from '../axiosConfig';
import {debugConsoleLog, queryParams} from '@helpers/functionFormat';
import {showToastMessage} from '@helpers/functionFormat';

function handleResponse(response) {
  debugConsoleLog('responsenilai',response.status)
  // if(response.status === 401 || response.status === 403){
  //     ToastAndroid.showWithGravityAndOffset(
  //       "Session anda telah habis, silahkan login kembali",
  //       ToastAndroid.LONG,
  //       ToastAndroid.BOTTOM,
  //       25,
  //       50
  //     );
  //   return RootNavigation.forceLogout();
  // }else{
  //   return response;
  // }
  if (response.status === 200) {
    return response;
  } else {
    if (response.status === 500) {
      showToastMessage('Server Error');
      return response;
    }
  }
}

export default {
  GetDataChat: async token => {
    return API(`conversation-buyer`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  GetDetailChat: async (id, token) => {
    return API(`message-buyer/${id}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  SendChat: async (params, id, token) => {
    return API(`message-buyer/send/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  CreateChat: async (params, token) => {
    return API(`conversation-buyer/create`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  SendChatToAdmin: async (params, token) => {
    return API(`conversation-admin`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  SendChatToEkspedition: async (params, token) => {
    return API(`conversation-buyer-expedition/create`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => {
        // handleResponse(err.response ?? null)
        return err.response;
      });
  },
};
