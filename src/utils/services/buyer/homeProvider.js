import API from '../axiosConfig';
import {debugConsoleLog, queryParams} from '@helpers/functionFormat';
import {showToastMessage} from '@helpers/functionFormat';

function handleResponse(response) {
  debugConsoleLog('responsenilai', response.status);
  // if(response.status === 401 || response.status === 403){
  //     ToastAndroid.showWithGravityAndOffset(
  //       "Session anda telah habis, silahkan login kembali",
  //       ToastAndroid.LONG,
  //       ToastAndroid.BOTTOM,
  //       25,
  //       50
  //     );
  //   return RootNavigation.forceLogout();
  // }else{
  //   return response;
  // }
  if (response.status === 404) {
    return response;
  }
  if (response.status === 200) {
    return response;
  } else {
    if (response.status === 500) {
      showToastMessage('Server Error');
      return response;
    } else {
      // showToastMessage('Server Error')
      return response;
    }
  }
}

export default {
  GetHomeBuyer: async token => {
    return API(`home-buyer`, {
      method: 'GET',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      // body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  GetAllCategory: async (token, params) => {
    return API(`categories?${queryParams(params)}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => {
        // console.log('error subsub', err);
        // handleResponse(err.response)
        return err.response;
      });
  },
  GetProduct: async (token, params) => {
    return API(`product-buyer?${queryParams(params)}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  GetDetailProduct: async (token, params) => {
    return API(`product-buyer/${params}}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  GetProductDetail: async (token, id) => {
    return API(`product-buyer/${id}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  AddToCart: async (params, token) => {
    return API('cart/add', {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  GetCart: async token => {
    return API(`cart`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  UpdateQty: async (params, id, token) => {
    return API(`cart/update/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  CheckOngkir: async (params, token) => {
    return API(`check-ongkir`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  Checkout: async (params, token) => {
    return API('checkout', {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => {
        console.log('err checkout', err);
        // handleResponse(err.response ?? null)
        return err.response;
      });
  },
  GetProductByShop: async (id, filter, token) => {
    debugConsoleLog('datahen', `product-by-shop/${id}?${queryParams(filter)}`);
    return API(`product-by-shop/${id}?${queryParams(filter)}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  GetHistorySearch: async token => {
    return API(`search-history`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  GetProdukHistory: async token => {
    return API(`product-history`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  SearchProduct: async (params, token) => {
    return API(`search-product?${queryParams(params)}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  DeleteSearchProduct: async (id, token) => {
    return API(`search-delete/${id}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  GetAllShop: async (params, token) => {
    return API('shop-around-buyer', {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  GetListNotification: async token => {
    return API(`get-notification/`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  ReadNotification: async (id, token) => {
    return API(`read-notification/${id}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  ClearAllSearch: async token => {
    return API(`search-clear`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
};
