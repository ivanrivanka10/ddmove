import API from '../axiosConfig';
import {debugConsoleLog, queryParams} from '@helpers/functionFormat';
import {showToastMessage} from '@helpers/functionFormat';

function handleResponse(response) {
	debugConsoleLog('responsenilai',response.status)
  // if(response.status === 401 || response.status === 403){
  //     ToastAndroid.showWithGravityAndOffset(
  //       "Session anda telah habis, silahkan login kembali",
  //       ToastAndroid.LONG,
  //       ToastAndroid.BOTTOM,
  //       25,
  //       50
  //     );
  //   return RootNavigation.forceLogout();
  // }else{
  //   return response;
  // }
  if (response.status === 200) {
    return response;
  } else {
    if (response.status === 500) {
      showToastMessage('Server Error');
      return response;
    }
  }
}

export default {
  GetListOrder: async (params, token) => {
    return API(`buyer-transactions?${queryParams(params)}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  GetDetailOrder: async (params, token) => {
    return API(`buyer-transactions-detail?${queryParams(params)}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  GetInvoice: async (params, token) => {
    return API(`buyer-transactions-invoice?${queryParams(params)}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  Paynow: async (params, token) => {
    return API(`pay-now?${queryParams(params)}`, {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
  ChangeStatus: async (params, token) => {
    return API('update-order-status', {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    })
      .then(response => handleResponse(response))
      .catch(err => handleResponse(err.response ?? null));
  },
};
