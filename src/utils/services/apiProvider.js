import API from './axiosConfig';

export default {
   Login: async (params) => {
      return API('login', {
        method: 'POST',
        head: {
            'Content-Type': 'multipart/form-data',
        },
        body: params,
      }).catch((err) => {
        return err.response;
      });
   },
	Register: async (params) => {
    return API('register', {
      method: 'POST',
      head: {
          'Content-Type': 'multipart/form-data',
      },
      body: params,
    }).catch((err) => {
      return err.response;
    });
  },
  GetUserInfo: async (token) => {    
    return API(`profile`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,

    },
    }).catch((err) => {
      return err.response;
    });
  },
  RequestResetPassword: async (params) => {
    return API('request-reset-password', {
      method: 'POST',
      head: {
          'Content-Type': 'multipart/form-data',
      },
      body: params,
    }).catch((err) => {
      return err.response;
    });
  },
  ResetPassword: async (params) => {
    return API('reset-password', {
      method: 'POST',
      head: {
          'Content-Type': 'multipart/form-data',
      },
      body: params,
    }).catch((err) => {
      return err.response;
    });
  },
  CheckOtp: async (params) => {
    return API('check-otp', {
      method: 'POST',
      head: {
          'Content-Type': 'multipart/form-data',
      },
      body: params,
    }).catch((err) => {
      return err.response;
    });
  },
}