import {debugConsoleLog} from '@helpers/functionFormat';
import API from './axiosConfig';

export default {
  UpdateProfile: async (params, token) => {
    return API('profile/update', {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('err update', err);
      return err.response;
    });
  },
  AddNewAddress: async (params, token) => {
    return API('address/add', {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('err update', err);
      return err.response;
    });
  },
  UpdateAddress: async (params, token, id) => {
    return API(`address/update/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('err update', err.response.data);
      return err.response;
    });
  },
  GetProvince: async token => {
    return API(`province`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        //         Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetCity: async id => {
    return API(`city/${id}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        //         Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetDistrict: async id => {
    return API(`district/${id}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        //         Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetSubDistrict: async (params, token) => {
    return API(`subdistrict/` + params, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetAddress: async token => {
    return API(`address`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetAddressDetail: async (token, id) => {
    return API(`address/${id}`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  DeleteAddress: async (id, token) => {
    return API(`address/delete/${id}`, {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      console.log('err update', err);
      return err.response;
    });
  },
  GetAboutUs: async token => {
    return API(`about`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetContactInfo: async token => {
    return API(`contact-info`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  GetTermCond: async token => {
    return API(`term-and-condition`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).catch(err => {
      return err.response;
    });
  },
  CreatePayment: async (body, token) => {
    return API(`create-order`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: token,
      },
      body: body,
    }).catch(err => {
      console.log('err update', err);
      return err.response;
    });
  },
  CreateShop: async (params, token) => {
    return API('shop/create', {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: params,
    }).catch(err => {
      console.log('err update', err.response);
      return err.response;
    });
  },
  UpdateShop: async (body, token) => {
    return API(`shop/update`, {
      method: 'POST',
      head: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
      body: body,
    }).catch(err => {
      debugConsoleLog('databody', body);
      console.log('err update', err.response);
      return err.response;
    });
  },
};
