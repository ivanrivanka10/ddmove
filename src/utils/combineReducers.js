import {combineReducers} from 'redux';

import authReducer from './reducers/authReducer';
import generalReducer from './reducers/generalReducer'

const rootReducer = combineReducers({
  auth: authReducer,
  general: generalReducer
});

export default rootReducer;
