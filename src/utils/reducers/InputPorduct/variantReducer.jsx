import {Nameuser} from './Nameuserreducer';

export const inputVarianState = {
  mainvarian: [],
  modalvariant: false,
  modalsubVariant:false,
  photo: [],
  combinedvarian: [],
};

export const VariantReducer = (state, action) => {
  switch (action.type) {
    case Nameuser.addvarianmain:
      return {
        ...state,
        mainvarian: action.payload,
      };
    case Nameuser.addsubvarinmain:
      return {
        ...state,
        mainvarian: action.payload,
      };
    case Nameuser.showsubModal:
      return{
        ...state,
        modalsubVariant:true
        };
      case Nameuser.hidesubModl:
        return{
          ...state,
          modalsubVariant:false
        }
    case Nameuser.showmodalvarian:
      return {
        ...state,
        modalvariant: true,
      };
    case Nameuser.Delete:
      return {
        ...state,
        mainvarian: [],
        photo: [],
        combinedvarian: [],
      };
    case Nameuser.addCombinasi:
      return {
        ...state,
        combinedvarian:action.payload,
      };
    case Nameuser.addfoto:
      return {
        ...state,
        photo: action.payload,
      };
    case Nameuser.deletefoto:
      return {
        ...state,
        photo: [],
      };
    case Nameuser.hidemodalvarian:
      return {
        ...state,
        modalvariant: false,
      };
    default:
      break;
  }
};
