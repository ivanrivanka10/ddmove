const initialState = {
  isLoggedIn: false,
  userData: {
    nama:'Deddy Cobuzier',
    email:'a@mail.com',
    no_telp:'0852088213',
    username:'dedy',
    password:'123456',
  },
  tokenLogin: '',
  userProfile: false,
  loginError: false,
  password : '',
  adminNumber: 0,
  isFromBuyer: false,
  location: {},
  userPickedAddress: {},
  lastCancel: undefined
}
 
const authReducer = (state = initialState, action) => {   
  // console.log('reducer auth', action);
  switch (action.type) {
    case "LOGIN_SUCCESS":
      return {
        ...state,
        userData: action.data,
        isLoggedIn: true
      };
    case "CHANGE_TYPE":
      return {
				...state,
				userData: {
					...state.userData,
					is_seller: action.data
				}
			};
    case "UPDATE_TYPE":
      return {
				...state,
				isFromBuyer: true
			};
    case "SET_LOCATION":
      return {
				...state,
				location: action.dataLocation
			};
    case "UPDATE_STATUS_ADDRESS":
      return {
				...state,
				userData: {
					...state.userData,
					hasAddress: action.status
				}
			};
    case "SET_USER_PICKED_ADDRESS":
      return {
				...state,
				userPickedAddress: action.addressPicked
			};
    case "SET_DELAY":
      return {
				...state,
				lastCancel: action.data
			};
    case "SET_USER_LOCATION_ADDRESS_NEW":
      return {
				...state,
				location: {
          ...state.location,
          city: action.city
      }
    };
    case "LOGOUT":
      return {
        ...state,
        isLoggedIn: false,
        userData: {
          nama:'',
          email:'',
          no_telp:'',
          username:'',
          password:'',
          device_id: ''
        },
        isFromBuyer: false,
        userPickedAddress: {}
      };
    default:
      return state;
  } 
}

export default authReducer;
