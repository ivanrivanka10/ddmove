export default {
    primary: '#C9454A',
    secondary: '#FF9234',
    primaryDark: '#5F2527',
    secondaryBase: '#FF9234',
    secondaryLighttest: '#FFF9F3',
    blackBase: '#333333',
    bgColorCircle: '#FFF2F3',
    grey_1: '#B4B6B8',
    grey_3: '#9E9E9E',
    bgGrey: '#F7F9F8',
    normal: '#828282',
    border: '#F2F4F5',
    borderGrey: '#E3E5E6',
    yellowBase: '#FFB323',
    semiYellowBg: '#FFFBF2',
    greyDummy: '#C4C4C4',
    opacityColor: (hex, opacity) => {
        var c;
        if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
            c= hex.substring(1).split('');
            if(c.length== 3){
                c= [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c= '0x'+c.join('');
            
            return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+','+opacity+')';
        }
        throw new Error('Bad Hex');
    }
}