module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    'react-native-reanimated/plugin',
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: [
          '.ios.ts',
          '.android.ts',
          '.ts',
          '.ios.tsx',
          '.android.tsx',
          '.tsx',
          '.jsx',
          '.js',
          '.json',
        ],
        alias: {
          '@assets': './src/assets',
          '@components': './src/components',
          '@navigations': './src/navigations',
          '@screens': './src/screens',
          '@constants': './src/constants',
          '@styles': './src/styles',
          '@store': './src/store',
          '@helpers': './src/helpers',
          '@languages': './src/languages',
          '@modules': './src/modules',
          '@utils': './src/utils',
        },
      },
    ],
  ],
};
